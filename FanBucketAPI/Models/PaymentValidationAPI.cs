﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Models
{
    public class PaymentValidationAPI
    {
        public string PostId { get; set; }
        public string PIN { get; set; }
        public string CurrId { get; set; }
    }
}
