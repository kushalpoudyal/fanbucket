﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Models
{
    public class PagedResponse<T>

    {
        public PagedResponse()
        {

        }

        public PagedResponse(IList<T> data)
        {

        }

        public IList<T> Data { get; set; }

        public int? PageNumber { get; set; }

        public int? PageSize { get; set; }

        public string NextPage { get; set; }

        public string PreviousPage { get; set; }
    }
}
