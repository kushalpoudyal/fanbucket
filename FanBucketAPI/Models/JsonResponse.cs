﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Models
{
    public class JsonResponse
    {
        public bool success { get; set; }
        public string ReturnMsg { get; set; }
        public TransactionResponseCode ResponseCode { get; set; }

        public string returnPath { get; set; }
        public string returnId { get; set; }

        public object JsonObj { get; set; }
    }

}
