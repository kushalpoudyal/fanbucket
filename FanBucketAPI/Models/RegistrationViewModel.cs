﻿
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Models
{

    public class RegistrationViewModel
    {

        //public string Id { get; set; }
        [Required]
        public string Fullname { get; set; }
        //public string Contact { get; set; }
        //public string Country { get; set; }


        //public string Organization { get; set; }


        //public string WorkTitle { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Password do not match.")]
        public string ConfirmPassword { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }

        //public List<string> Role { get; set; } 
        //public string BuyId { get; set; }
        //public IFormFile Picture { get; set; }


    }
}
