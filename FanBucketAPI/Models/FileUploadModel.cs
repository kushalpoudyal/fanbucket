﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Models
{
    public class FileUploadModel
    {
        public string FilePath { get; set; }
        public string bucketName { get; set; }
        public string filename { get; set; }
        public IFormFile file { get; set; }
        public string KeyName { get; set; }
        public long fileSize { get; set; }
        public string fileExt { get; set; }

        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

    }
}
