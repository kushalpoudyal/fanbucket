﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace FanBucketAPI.Models
{
    public class UserCred
    {
        public int CountryCode { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
