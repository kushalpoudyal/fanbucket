﻿using FanBucketEntity.Wrappers;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Helper
{
    public static class PaginationHelper
    {
        public static PagedResponse<List<T>> CreatePagedReponse<T>(List<T> pagedData, PaginationFilter validFilter, int totalRecords, IUriService uriService, string route)
        {
            var respose = new PagedResponse<List<T>>(pagedData, validFilter.PageNumber, validFilter.PageSize);
            var totalPages = ((double)totalRecords / (double)validFilter.PageSize);
            int roundedTotalPages = Convert.ToInt32(Math.Ceiling(totalPages));
            respose.NextPageNumber = validFilter.PageNumber >= 1 && validFilter.PageNumber < roundedTotalPages ? validFilter.PageNumber + 1 : 0;
            respose.NextPage =
               respose.NextPageNumber != 0
                ? uriService.GetPageUri(new PaginationFilter(respose.NextPageNumber, validFilter.PageSize), route)
                : null;
            respose.PreviousPageNumber =
                validFilter.PageNumber - 1 >= 1 && validFilter.PageNumber <= roundedTotalPages ? validFilter.PageNumber - 1 : 0;
            respose.PreviousPage =
               respose.PreviousPageNumber != 0
                ? uriService.GetPageUri(new PaginationFilter(respose.PreviousPageNumber, validFilter.PageSize), route)
                : null;
            respose.FirstPage = uriService.GetPageUri(new PaginationFilter(1, validFilter.PageSize), route);
            respose.LastPage = uriService.GetPageUri(new PaginationFilter(roundedTotalPages, validFilter.PageSize), route);
            respose.TotalPages = roundedTotalPages;
            respose.TotalRecords = totalRecords;
            return respose;
        }
    }
}
