﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Helper
{
    public static class Helper
    {
        public static string GetIpAddress(this HttpContext httpContext)
        {
            string ipAddressString = string.Empty;
            if (httpContext == null)
            {
                return ipAddressString;
            }

            if (httpContext.Request.Headers != null && httpContext.Request.Headers.Count > 0)
            {
                if (httpContext.Request.Headers.ContainsKey("X-Forwarded-For") == true)
                {
                    string headerXForwardedFor = httpContext.Request.Headers["X-Forwarded-For"];
                    if (string.IsNullOrEmpty(headerXForwardedFor) == false)
                    {
                        string xForwardedForIpAddress = headerXForwardedFor.Split(':')[0];
                        if (string.IsNullOrEmpty(xForwardedForIpAddress) == false)
                        {
                            ipAddressString = xForwardedForIpAddress;
                        }
                    }
                }
            }
            else if (
                httpContext.Connection == null ||
                httpContext.Connection.RemoteIpAddress == null)
            {
                ipAddressString = httpContext.Connection.RemoteIpAddress.ToString();
            }
            return ipAddressString;

        }

    }
}
