﻿namespace FanBucketAPI.Options
{
    public class EmailConfiguration
    {
        public string MailServer { get; set; }
        public int MailPort { get; set; }
        public string SenderName { get; set; }
        public string Sender { get; set; }
        public string Password { get; set; }
        public string NetworkCredentialUsername { get; set; }
        public string NetworkCredentialPassword { get; set; }
        public int TimeOut { get; set; }
        public bool EnableSSL { get; set; }
    }
}
