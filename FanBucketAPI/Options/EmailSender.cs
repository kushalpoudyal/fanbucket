﻿using FanBucketAPI.Models;
using FanBucketAPI.ViewModel;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Templates;
using Templates.ViewModels;

namespace FanBucketAPI.Options
{

    public interface IEmailSender
    {
        Task SendEmailAsync(EmailUserModel userModel);
    }

    public class EmailSender : IEmailSender
    {
        private readonly EmailConfiguration _emailSettings;
        private readonly IRazorViewToStringRenderer _renderer;

        public EmailSender(IOptions<EmailConfiguration> emailSettings, IRazorViewToStringRenderer renderer)
        {
            _emailSettings = emailSettings.Value;
            _renderer = renderer;
        }

        /// <summary>
        /// Gets Email Configuration parameters from appsettings file
        /// </summary>
        /// <returns>Email Configuration parameters</returns>
        public EmailConfiguration GetEmailConfigurations()
        {
            return new EmailConfiguration
            {
                MailServer = _emailSettings.MailServer,
                Sender = _emailSettings.Sender,
                SenderName = _emailSettings.SenderName,
                Password = _emailSettings.Password,
                EnableSSL = Convert.ToBoolean(_emailSettings.EnableSSL),
                MailPort = _emailSettings.MailPort,
                NetworkCredentialUsername = _emailSettings.NetworkCredentialUsername,
                NetworkCredentialPassword = _emailSettings.NetworkCredentialPassword,
                TimeOut = Convert.ToInt32(_emailSettings.TimeOut)
            };
        }

        /// <summary>
        /// Configures SMTP parameters for sending email
        /// </summary>
        /// <param name="message"></param>
        /// <param name="userModel"></param>
        /// <returns></returns>
        private async Task ConfigureSMTPEmailAsync(IdentityMessage message, EmailUserModel userModel = null)
        {
            //Get Email Configurations from config
            var mailConfig = GetEmailConfigurations();

            // Configure the client:
            var client = new SmtpClient(mailConfig.MailServer);
            client.EnableSsl = mailConfig.EnableSSL;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Port = mailConfig.MailPort;
            client.Host = mailConfig.MailServer;
           

            // Create the credentials:
            var credentials = new NetworkCredential(mailConfig.NetworkCredentialUsername, mailConfig.NetworkCredentialPassword);
            client.Credentials = credentials;

            //create email template
            var model = new CreateNewUserViewModel()
            {
                FullName = userModel.FullName,
                ToEmail = userModel.Email,
                Username = userModel.UserName,
                CallbackUrl=userModel.CallbackUrl,
                Message=userModel.Message,
            };
            //var a= Server.MapPath("~/Views/Mail/HelloFriend.cshtml");

             string view = $"/Views/Emails/{userModel.Type.ToString()}/{userModel.Type.ToString()}";
            var htmlBody = await _renderer.RenderViewToStringAsync($"{view}Html.cshtml", model);
            var textBody = await _renderer.RenderViewToStringAsync($"{view}Text.cshtml", model);

            // Create the message
            using (var mail = new MailMessage(
                new MailAddress(mailConfig.Sender, mailConfig.SenderName).ToString(),
                message.Destination)
                )
            {
                mail.To.Add("sujandhungel2015@gmail.com");
                mail.Subject = message.Subject;
                mail.Body = textBody; // message.Body;
                mail.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(htmlBody, Encoding.UTF8, MediaTypeNames.Text.Html));
                mail.IsBodyHtml = true;
                //mail.Attachments.Add(new Attachment("C:\\file.zip"));

                await client.SendMailAsync(mail);
            }
        }

        public async Task SendAsync(IdentityMessage message)
        {
            await ConfigureSMTPEmailAsync(message);
        }

        public async Task SendEmailAsync(EmailUserModel userModel)
        {
            var iMessage = new IdentityMessage
            {
                Destination = userModel.Email,
                Subject = userModel.Subject,
                Body = userModel.Message
            };
            await ConfigureSMTPEmailAsync(iMessage, userModel);
        }
    }
}
