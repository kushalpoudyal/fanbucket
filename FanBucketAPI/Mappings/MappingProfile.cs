﻿using AutoMapper;
using FanBucketEntity.Model;
using FanBucketEntity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<BadgeInfo, BadgeViewModel>().ReverseMap();
            CreateMap<Interest, InterestViewModel>().ReverseMap();
            //.ForMember(dest => dest.Compensation, source => source.MapFrom(source => source.Salary)); //For Specific Mapping
            //Map from BadgeInfo Object to BadgeViewModel Object
        }
    }
}
