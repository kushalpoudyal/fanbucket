﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.Payments;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.Procedure;
using FanBucketEntity.Utility;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Stripe;
using Stripe.BillingPortal;
using Stripe.Checkout;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly FbDbContext _context;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IPaymentRepository payment;
        private readonly StripeSettings _stripe;
        public PaymentController(UserManager<ApplicationUser> userManager,
            FbDbContext context,
            SignInManager<ApplicationUser> signInManager,
            IPaymentRepository payment,
            IDataProtectionProvider protector,
            IOptions<StripeSettings> options)
        {
            this.userManager = userManager;
            this._context = context;
            this.signInManager = signInManager;
            this.payment = payment;
            this._stripe = options.Value;
        }
        // GET: api/Payment
        [Route("Secret")]
        [HttpGet]
        public async Task<JsonResponse> Secret()
        {
            var response = new JsonResponse();

            response.JsonObj = _stripe.PublishableKey;
            response.success = true;


            return response;

        }
        [Route("IsPaymentEligible")]
        [HttpGet]
        public async Task<JsonResponse> IsPaymentEligible()
        {
            var response = new JsonResponse();
            var UserId = Guid.Parse(this.userManager.GetUserId(User));
            var KYCDATA = await _context.ApplicationUserKYC.Where(x => x.UserId == UserId).FirstOrDefaultAsync();
            if (KYCDATA == null)
            {
                response.success = false;
                response.ReturnMsg = "Fill In KYC Details";
            }
            else
            {
                response.success = true;

            }

            return response;

        }
        /// <summary>
        ///  New Wallet load Payment Intent
        /// </summary>
        /// <param name="modal"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create(LoadInitViewModel modal)
        {
            var paymentIntents = new PaymentIntentService();
            var paymentIntent = paymentIntents.Create(new PaymentIntentCreateOptions
            {
                Metadata = new Dictionary<string, string>
                    {
                        {"Name","Wallet Load" },
                        {"Type","WalletLoad" },
                        {"Description","Payment for Wallet Load" },
                        {"Amount",modal.Amount.ToString() },
                        {"UserId",userManager.GetUserId(User) },

                    },
                Description = "Wallet Load",
                Currency = "usd",
                Amount = Decimal.ToInt64(modal.Amount * 100)

            });
            return Ok(new { clientSecret = paymentIntent.ClientSecret });
        }


        [Route("PaymentInit")]
        [Authorize]
        [HttpPost]
        public async Task<JsonResponse> PaymentInit([FromBody] PaymentInitViewModel modal)
        {
            var response = new JsonResponse();
            try
            {
                var options = new Stripe.Checkout.SessionCreateOptions
                {
                    PaymentMethodTypes = new List<string>
                        {
                     "card",
                        },
                    LineItems = new List<SessionLineItemOptions>
                   {
                     new SessionLineItemOptions
                     {
                         Name= modal.PostTitle,
                         Description="Payment",
                        Amount = Decimal.ToInt64(modal.PostAmount * 100),
                         Currency="usd",
                         Quantity=1

                    },
                   },
                    Metadata = new Dictionary<string, string>
                    {
                        {"Name",modal.PostTitle },
                        {"Type","PostUnlock" },
                        {"Description","New Payment" },
                        {"Amount",(modal.PostAmount*100).ToString() },
                        {"UserId",userManager.GetUserId(User) },
                        {"PostId",modal.PostId },
                    },
                    Mode = "payment",
                    SuccessUrl = "https://fanbucket.com/Post/SinglePost?PostId=" + modal.PostId,
                    CancelUrl = "https://fanbucket.com/Post/SinglePost?PostId=" + modal.PostId,
                    PaymentIntentData = new SessionPaymentIntentDataOptions
                    {
                        Metadata = new Dictionary<string, string>
                    {
                        {"Order_Id",Guid.NewGuid().ToString() },
                        {"Description","New Payment" }
                    }
                    }
                };

                CreateStripeSession(response, options);

            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Invalid Request: " + ex.Message;
            }
            return response;
        }

        private void CreateStripeSession(JsonResponse response, Stripe.Checkout.SessionCreateOptions options)
        {
            var service = new Stripe.Checkout.SessionService();
            var session = service.Create(options);
            var model = new SubscriptionViewModel()
            {

                Session = session

            };
            var StripeDetails = new StripeSettings()
            {

                PublishableKey = _stripe.PublishableKey,
                SecretKey = _stripe.SecretKey
            };
            response.success = true;
            response.ReturnMsg = model.Session.Id;
            response.JsonObj = JsonConvert.SerializeObject(StripeDetails);
        }

        [Route("LoadInit")]
        [Authorize]
        [HttpPost]
        public async Task<JsonResponse> LoadInit([FromBody] LoadInitViewModel modal)
        {
            var response = new JsonResponse();
            try
            {
                var options = new Stripe.Checkout.SessionCreateOptions
                {
                    PaymentMethodTypes = new List<string>
                        {
                     "card",
                        },
                    LineItems = new List<SessionLineItemOptions>
                   {
                     new SessionLineItemOptions
                     {
                         Name="Wallet Load",
                         Description="Wallet load",
                         Amount = Decimal.ToInt64(modal.Amount * 100),
                         Currency="usd",
                         Quantity=1

                    },
                   },
                    Metadata = new Dictionary<string, string>
                    {
                        {"Name","Wallet Load" },
                        {"Type","WalletLoad" },
                        {"Description","Payment for Wallet Load" },
                        {"Amount",modal.Amount.ToString() },
                        {"UserId",userManager.GetUserId(User) },

                    },
                    Mode = "payment",
                    SuccessUrl = "https://www.fanbucket.com/" + User.Identity.Name,
                    CancelUrl = "https://www.fanbucket.com/" + User.Identity.Name,
                    PaymentIntentData = new SessionPaymentIntentDataOptions
                    {
                        Metadata = new Dictionary<string, string>
                    {
                        {"Order_Id",Guid.NewGuid().ToString() },
                        {"Description","New Payment" }
                    }
                    }
                };

                CreateStripeSession(response, options);

            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Invalid Request: " + ex.Message;
            }
            return response;
        }
        [Route("WeebHook")]
        [HttpPost]
        public async Task<IActionResult> WeebHook()
        {
            var json = await new StreamReader(HttpContext.Request.Body).ReadToEndAsync();

            try
            {
                PaymentIntent paymentIntent = null;

                var stripeEvent = EventUtility.ParseEvent(json);
                switch (stripeEvent.Type)
                {
                    // Handle the event
                    case Events.PaymentIntentSucceeded:
                        paymentIntent = stripeEvent.Data.Object as PaymentIntent;
                        await UpdateStripePayment(paymentIntent);
                        break;

                }

                return Ok();
            }
            catch (StripeException e)
            {
                return BadRequest();
            }
        }

        private async Task UpdateStripePayment(PaymentIntent session)
        {
            var apiModel = new PaymentValidationModel();
            var MetaData = session.Metadata;
            var payType = MetaData["Type"];
            if (payType == "WalletLoad")
            {
                apiModel.CurrId = Guid.Parse(MetaData["UserId"]);
                apiModel.PayingAmount = Decimal.Parse(MetaData["Amount"]);
                await payment.LoadFund(apiModel, false);
            }
            else if (payType == "PostUnlock")
            {
                var tranId = Guid.Parse(MetaData["Order_Id"]);

                apiModel.PostId = Guid.Parse(MetaData["PostId"]);
                apiModel.CurrId = Guid.Parse(this.userManager.GetUserId(User));

                await payment.PaymentForViaStripe(apiModel, tranId);
            }
            else if (payType == "Subscription")
            {

            }

        }
        [Route("SetPinCode")]
        [HttpPost]
        [Authorize]
        public async Task<JsonResponse> SetPinCode(int PIN, string Password)
        {
            var response = new JsonResponse();
            var user = await this.userManager.GetUserAsync(User);
            var IsCorrect = await this.userManager.CheckPasswordAsync(user, Password);
            if (!IsCorrect)
            {
                response.success = false;
                response.ReturnMsg = "Incorrect Password.";
                return response;
            }
            return await SetNewPin(PIN, response, user);
        }

        private async Task<JsonResponse> SetNewPin(int PIN, JsonResponse response, ApplicationUser user)
        {
            if (PIN.ToString().Length == 6)
            {
                var passHash = PasswordHasher.HashPassword(PIN.ToString());
                if (PinUserExist(user.Id))
                {
                    var pin = await _context.PaymentUPIN.Where(x => x.UserId == user.Id).FirstOrDefaultAsync();
                    pin.PINHash = passHash;

                }
                else
                {
                    var Model = new PaymentUPIN();
                    Model.PINHash = passHash;
                    Model.UserId = user.Id;
                    await _context.AddAsync(Model);
                }


                await _context.SaveChangesAsync();
                response.success = true;
                response.ReturnMsg = "Your Pincode has been Successfully Set!";
                return response;
            }

            else
            {
                response.success = false;
                response.ReturnMsg = "Pincode Has to be 6 digits";
                return response;
            }
        }

        [Route("ChangeWalletPin")]
        [HttpPost]
        [Authorize]
        public async Task<JsonResponse> ChangeWalletPin(PinChangeViewModel PinChangeViewModel)
        {
            var response = new JsonResponse();
            var user = await this.userManager.GetUserAsync(User);
            var IsCorrect = await this.userManager.CheckPasswordAsync(user, PinChangeViewModel.CurrentPassword);
            if (!IsCorrect)
            {
                response.success = false;
                response.ReturnMsg = "Incorrect Password.";
                return response;
            }
            var jsonResponse = await VerifyPinCode(PinChangeViewModel.CurrentPin);
            if (jsonResponse.success)
            {
                return await SetNewPin(PinChangeViewModel.NewPin, response, user);

            }
            else
            {
                return jsonResponse;
            }
        }

        private bool PinUserExist(Guid UserId)
        {
            return _context.PaymentUPIN.Any(x => x.UserId == UserId);
        }
        [Route("GetBalance")]
        [HttpGet]
        [Authorize]
        public async Task<JsonResponse> GetBalance()
        {
            var response = new JsonResponse();
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var Balance = await payment.GetBalance(CurrId);

            response.success = true;
            response.ReturnMsg = Balance.ToString();

            return response;

        }
        [Route("History")]
        [HttpGet]
        [Authorize]
        public async Task<List<PaymentHistory>> GetPaymentHistory(Guid NotifyId = default)
        {

            try
            {

                var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                var param = new[] { new SqlParameter("@USERID", CurrId) };
                var Balance = await payment.ExecuteProcedure<PaymentHistory>("PAYMENT_HISTORY @USERID", param);
                if (NotifyId != default)
                {
                    var activenotification = await _context.AppNotifications.FirstOrDefaultAsync(x => x.PostCreatorId == CurrId && !x.Viewed && x.Id == NotifyId);
                    if (activenotification != null)
                    {
                        activenotification.Viewed = true;
                        await _context.SaveChangesAsync();
                    }
                }
                return Balance;

            }
            catch (Exception ex)
            {
                return null;// $"Could not receive transaction ledger, {ex.Message}";
            }



        }
        [Route("TestSplitPayment")]
        [HttpGet]
        public JsonResponse TestSplitPayment(decimal Percentage, decimal Amount)
        {
            var response = new JsonResponse();


            response.success = true;
            response.ReturnMsg = payment.TestPaymentSplit(Percentage, Amount);

            return response;

        }
        [Route("PaymentValidation")]
        [HttpPost]
        [Authorize]
        public async Task<JsonResponse> PaymentValidation()
        {
            var response = new JsonResponse();
            var model = new PaymentValidationModel();
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            model.CurrId = CurrId;

            var Balance = await payment.ValidateBeforePayment(model);

            if (Balance == FanBucketEntity.Model.ENUM.TransactionResponseCode.OK)
            {
                response.success = true;
                response.ResponseCode = Balance;
            }
            else
            {
                response.success = false;
                response.ResponseCode = Balance;
                response.ReturnMsg = "Could not process your request: " + Balance.ToString();
            }

            return response;

        }
        [Route("VerifyPinCode")]
        [HttpGet]
        [Authorize]
        public async Task<JsonResponse> VerifyPinCode(int PIN)
        {
            var response = new JsonResponse();
            if (PIN.ToString().Length == 6)
            {
                var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                var tranResponse = await payment.VerifyPin(PIN, CurrId);
                switch (tranResponse)
                {
                    case FanBucketEntity.Model.ENUM.TransactionResponseCode.OK:
                        response.success = true;
                        response.ReturnMsg = "Pincode Matched!";
                        break;
                    case FanBucketEntity.Model.ENUM.TransactionResponseCode.IncorrectPIN:
                        response.success = false;
                        response.ReturnMsg = "Incorrect Pincode !";
                        break;
                    case FanBucketEntity.Model.ENUM.TransactionResponseCode.PinLocked:
                        response.success = false;
                        response.ReturnMsg = "Incorrect Pincode, Your PIN Has been Locked !";
                        break;
                    case FanBucketEntity.Model.ENUM.TransactionResponseCode.ServerError:
                        response.success = false;
                        response.ReturnMsg = "Internal Server Error !";
                        break;
                    default:
                        response.success = false;
                        response.ReturnMsg = "Something Went Wrong, Try Again Later !";
                        break;


                }
            }
            else
            {
                response.success = false;
                response.ReturnMsg = "PIN should be 6 digits !";
            }

            return response;
        }
        [Route("PaymentForPost")]
        [HttpPost]
        [Authorize]
        public async Task<JsonResponse> PaymentForPost([FromBody] PaymentValidationAPI model)
        {
            var apiModel = new PaymentValidationModel();
            apiModel.PostId = Guid.Parse(model.PostId);
            apiModel.CurrId = Guid.Parse(this.userManager.GetUserId(User));
            apiModel.PIN = Convert.ToInt32(model.PIN);

            var response = new JsonResponse();

            var tranResponse = await payment.PaymentForPost(apiModel);
            switch (tranResponse)
            {
                case FanBucketEntity.Model.ENUM.TransactionResponseCode.OK:
                    response.success = true;
                    response.ReturnMsg = "Transaction Successfull, Post Unlocked!";
                    break;
                case FanBucketEntity.Model.ENUM.TransactionResponseCode.IncorrectPIN:
                    response.success = false;
                    response.ReturnMsg = "Incorrect Pincode !";
                    break;
                case FanBucketEntity.Model.ENUM.TransactionResponseCode.PinLocked:
                    response.success = false;
                    response.ReturnMsg = "Incorrect Pincode, Your PIN Has been Locked !";
                    break;
                case FanBucketEntity.Model.ENUM.TransactionResponseCode.ServerError:
                    response.success = false;
                    response.ReturnMsg = "Internal Server Error !";
                    break;
                case FanBucketEntity.Model.ENUM.TransactionResponseCode.UnAuthorized:
                    response.success = false;
                    response.ReturnMsg = "You Are Not Authorized to perform transaction !";
                    break;
                case FanBucketEntity.Model.ENUM.TransactionResponseCode.InsufficientBalance:
                    response.success = false;
                    response.ReturnMsg = "Insufficient Balance, Please load funds to perform transactions !";
                    break;
                case FanBucketEntity.Model.ENUM.TransactionResponseCode.PinRequired:
                    response.success = false;
                    response.ReturnMsg = "You have not SET PIN For your transaction";
                    break;
                default:
                    response.success = false;
                    response.ReturnMsg = "Something Went Wrong, Try Again Later !";
                    break;


            }
            return response;
        }


    }
}
