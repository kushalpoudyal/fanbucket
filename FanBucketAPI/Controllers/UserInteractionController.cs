﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Operations;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Client")]
    public class UserInteractionController : ControllerBase
    {
        private readonly IAdminRepository adminRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        public UserInteractionController(IAdminRepository adminRepository,
            UserManager<ApplicationUser> userManager)
        {
            this.adminRepository = adminRepository;
            this._userManager = userManager;
        }
        // GET: api/UserInteraction
        [HttpGet]
        [Route("GetUserStatistics")]
        public async Task<UserInteractionViewModel> GetUserStatistics()
        {
            var CurrId = Guid.Parse(_userManager.GetUserId(User));
            return await adminRepository.GetUserMediaData(CurrId);
        }


    }
}
