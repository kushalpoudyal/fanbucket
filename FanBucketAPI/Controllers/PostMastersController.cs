﻿using FanBucketAPI.APIModels;
using FanBucketAPI.Helper;
using FanBucketAPI.Models;
using FanBucketAPI.Services;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.Model.PostDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostMastersController : ControllerBase
    {
        private readonly PostMasterIRepository _postmaster;
        private readonly IBadgeService badgeService;
        private readonly IBadgeRepository badgeRepository;
        private readonly IUserBadgesRepository userBadgesRepository;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IConfiguration configuration;
        private readonly IPaymentRepository paymentRepository;
        private readonly FbDbContext context;

        private string FilePath { get; }

        public PostMastersController(FbDbContext context,
            UserManager<ApplicationUser> userManager,
            PostMasterIRepository postmaster,
            IBadgeService badgeService,
            IUserBadgesRepository userBadgesRepository,
            IBadgeRepository badgeRepository,
           IWebHostEnvironment hostingEnvironment,
           IConfiguration configuration, IPaymentRepository paymentRepository)
        {
            this.userManager = userManager;
            _postmaster = postmaster;
            this.badgeService = badgeService;
            this.userBadgesRepository = userBadgesRepository;
            this.badgeRepository = badgeRepository;
            this.hostingEnvironment = hostingEnvironment;
            this.configuration = configuration;
            FilePath = this.configuration.GetValue<string>("PostFilePath");
            this.paymentRepository = paymentRepository;
            this.context = context;
            //FilePath = @"C:\inetpub\wwwroot\wwwroot"; //live
            //FilePath = @"C:\inetpub\wwwroot\fanbucket\FanBucketUI\wwwroot";
        }

        // GET: PostMasters
        [Route("Search")]
        [HttpGet]
        public async Task<IActionResult> Search(string hashtag)
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));

            var fbDbContext = await _postmaster.Search(currId, hashtag);
            return Ok(fbDbContext);
        }

        // GET: PostMasters
        /// <summary>
        /// Current User feed
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [Route("GetPost")]
        [HttpGet]
        public async Task<IActionResult> Index(int PageNumber = 1, int PageSize = 10)
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            if ((await userManager.GetUserAsync(User)).NSFW)
            {
                var fbDbContext = await _postmaster.GetFeedByUserNSFW(currId, PageNumber, PageSize);
                return Ok(fbDbContext);
            }
            else
            {
                var fbDbContext = await _postmaster.GetFeedByUser(currId, PageNumber, PageSize);
                return Ok(fbDbContext);
            }
        }


        [Route("GetHiddenPost")]
        [HttpGet]
        public async Task<IActionResult> GetHiddenPost()
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            if ((await userManager.GetUserAsync(User)).NSFW)
            {
                var fbDbContext = await _postmaster.GetHiddenPost(currId);
                return Ok(fbDbContext);
            }
            else
            {
                var fbDbContext = await _postmaster.GetHiddenPost(currId);
                return Ok(fbDbContext);
            }
        }

        [Route("VoteForPoll")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [Authorize]
        public async Task<JsonResponse> VoteForPoll(string PostId, string ContentId)
        {
            var response = new JsonResponse();
            try
            {
                var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                response.JsonObj = await _postmaster.RegisterPoll(Guid.Parse(PostId), Guid.Parse(ContentId), CurrId);
                //response.ReturnMsg = (await _postmaster.RegisterPoll(Guid.Parse(PostId), Guid.Parse(ContentId), CurrId)).ToString();
                response.ReturnMsg = "Voted";
                response.success = true;
            }
            catch
            {
                response.success = false;
            }
            return response;
        }

        // GET: PostMasters
        [Route("PostMoments")]
        [HttpPost]
        public async Task<JsonResponse> PostMoments(string PostId)
        {
            var response = new JsonResponse();

            var currId = Guid.Parse(this.userManager.GetUserId(User));

            if (_postmaster.PostExists(Guid.Parse(PostId)))
            {
                var moments = new PostMoments();
                moments.Id = Guid.NewGuid();
                moments.PostId = Guid.Parse(PostId);
                moments.CreatedById = currId;
                moments.CreatedOn = DateTime.Now;
                moments.StartDate = DateTime.Now;
                moments.FileLocation = null;

                response.success = await _postmaster.AddPostToMoments(moments);
                if (response.success)
                {
                    response.ReturnMsg = "Successfully Added In the Moments";
                }
                else
                {
                    response.ReturnMsg = "Could Not Add In the Moments";
                }
            }
            else
            {
                response.success = false;
                response.ReturnMsg = "Post Does Not Exists";
            }

            return response;
        }

        // GET: DeleteMoments
        [Route("DeleteMoments")]
        [HttpPost]
        public async Task<JsonResponse> DeleteMoments(string MomentsId)
        {
            var response = new JsonResponse();
            var currId = Guid.Parse(this.userManager.GetUserId(User));

            if (_postmaster.MomentsExists(Guid.Parse(MomentsId)))
            {
                var entity = await _postmaster.MomentsGetById(Guid.Parse(MomentsId));
                if (entity.CreatedById == currId)
                {
                    try
                    {
                        await _postmaster.RemoveMomentsAsync(entity);
                        response.success = false;
                        response.ReturnMsg = "Successfully Deleted the Moments";
                    }
                    catch
                    {
                        response.success = false;
                        response.ReturnMsg = "Could Not Delete the Moments";
                    }
                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Un-Authorized Access Attempt!";
                }
            }
            else
            {
                response.success = false;
                response.ReturnMsg = "Moments Does Not Exists";
            }
            return response;
        }

        [Route("GetMedia")]
        [HttpGet]
        [Authorize]
        public async Task<List<GetUserMediaViewModel>> GetMediaForUser()
        {
            var CurrId = Guid.Parse(userManager.GetUserId(User));
            var response = await _postmaster.GetMediaByUser(CurrId);
            return response;
        }
        [Route("GetUserImageContents")]
        [HttpGet]
        [Authorize]
        public async Task<List<GetUserMediaViewModel>> GetUserImageContents(Guid UserId)
        {
            var CurrId = Guid.Parse(userManager.GetUserId(User));

            var response = await _postmaster.GetUserImageContents(UserId, CurrId);
            return response;
        }
        // GET: PostMasters
        [Route("GetMoments")]
        [HttpGet]
        public async Task<List<PostMomentsViewModel>> GetMoments()
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            return await _postmaster.GetMoments(currId);
        }

        [Route("GetTopAccounts")]
        [HttpGet]
        public async Task<IActionResult> TopAccounts()
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            var fbDbContext = await _postmaster.GetTopAccounts(currId);

            return Ok(fbDbContext);
        }
        /// <summary>
        ///  user profile page 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [Route("GetPostById")]
        [HttpGet]
        public async Task<IActionResult> GetPostById(Guid UserId, int PageNumber = 1, int PageSize = 10)
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            if (UserId != currId)// && !await paymentRepository.HasSubscribed(UserId, currId))
            {
                var fbDbContext = await _postmaster.GetByUser(UserId, currId, PageNumber, PageSize);
                return Ok(fbDbContext);
            }
            else
            {
                var fbDbContext = await _postmaster.GetByUserSubscribed(UserId, currId, PageNumber, PageSize);
                return Ok(fbDbContext);
            }
        }

        [Route("GetPostByVideo")]
        [HttpGet]
        public async Task<IActionResult> GetPostByVideo(Guid UserId)
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            var fbDbContext = await _postmaster.GetPostByUsersVideo(UserId, currId);

            return Ok(fbDbContext);
        }

        [Route("GetPostByImages")]
        [HttpGet]
        public async Task<IActionResult> GetPostByImages(Guid UserId)
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            var fbDbContext = await _postmaster.GetPostByUsersImage(UserId, currId);

            return Ok(fbDbContext);
        }

        [Route("GetScheduledPost")]
        [HttpGet]
        public async Task<IActionResult> GetScheduledPost()
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            var fbDbContext = await _postmaster.GetScheduledPostUser(currId);

            return Ok(fbDbContext);
        }

        [Route("GetSinglePostByIdAjax")]
        [HttpGet]
        public async Task<IActionResult> GetSinglePostByIdAjax(Guid PostId, Guid NotifyId = default)
        {
            try
            {
                var currId = Guid.Parse(this.userManager.GetUserId(User));
                var fbDbContext = await _postmaster.GetSinglePostById(PostId, NotifyId, currId);

                return Ok(fbDbContext);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// For the post only accesible to current users hides the locked post if not paid and not subscribed
        /// </summary>
        /// <param name="PostId"></param>
        /// <returns></returns>
        [Route("GetSinglePostById")]
        [HttpGet]
        public async Task<JsonResponse> GetSinglePostById(Guid PostId)
        {
            var response = new JsonResponse();
            try
            {
                var currId = Guid.Parse(this.userManager.GetUserId(User));
                var fbDbContext = await _postmaster.GetSingleCollectionById(PostId, currId);

                response.JsonObj = fbDbContext;
                response.success = true;
                response.ReturnMsg = "Successfully Extracted";

                return response;
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Could Not Generated: " + ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Hides the post of current user
        /// </summary>
        /// <param name="PostId">PostId to unhide</param>
        /// <returns></returns>
        [Route("HidePost")]
        [HttpGet]
        public async Task<JsonResponse> HidePost(Guid PostId)
        {
            var response = new JsonResponse();
            try
            {
                var currId = Guid.Parse(this.userManager.GetUserId(User));
                var post = await _postmaster.GetById(PostId);
                if (currId != post.CreatedById)
                {
                    response.success = false;
                    response.ReturnMsg = "Server Error";
                }
                post.Record_Status = Record_Status.Inactive;
                context.Entry(post).State = EntityState.Modified;
                await context.SaveChangesAsync();
                //response.JsonObj = fbDbContext;
                response.success = true;
                response.ReturnMsg = "Successfully Hidden";

                return response;
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Could Not be hidden: " + ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Unhides the post of current user
        /// </summary>
        /// <param name="PostId">PostId to unhide</param>
        /// <returns></returns>
        [Route("UnHidePost")]
        [HttpGet]
        public async Task<JsonResponse> UnHidePost(Guid PostId)
        {
            var response = new JsonResponse();
            try
            {
                var currId = Guid.Parse(this.userManager.GetUserId(User));
                var post = await _postmaster.GetById(PostId);
                if (currId != post.CreatedById)
                {
                    response.success = false;
                    response.ReturnMsg = "Server Error";
                }
                post.Record_Status = Record_Status.Active;
                context.Entry(post).State = EntityState.Modified;

                await context.SaveChangesAsync();
                //response.JsonObj = fbDbContext;
                response.success = true;
                response.ReturnMsg = "Successfully unhidden";

                return response;
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Could Not be unhidden: " + ex.Message;
            }
            return response;
        }

        // GET: PostMasters/Details/5
        [Route("GetPostDetail")]
        [HttpGet]
        public async Task<IActionResult> Details(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postMaster = await _postmaster.GetById(id);
            if (postMaster == null)
            {
                return NotFound();
            }

            return Ok(postMaster);
        }

        [Route("PostUpload")]
        [HttpPost]
        [DisableRequestSizeLimit]
        [AllowAnonymous]
        public async Task<JsonResponse> PostUpload(IFormFile files)
        {
            var response = new JsonResponse();
            var Id = Guid.NewGuid().ToString();
            try
            {
                var fileName = Id + Path.GetExtension(files.FileName);
                double FileLength = files.Length;
                if (!Directory.Exists(Path.Combine(FilePath, "Main")))
                {
                    Directory.CreateDirectory(Path.Combine(FilePath, "Main"));
                }
                string VideoFolder = Path.Combine(FilePath, "Main", fileName);
                await CreateFile.UploadToServer(files, VideoFolder);
                var returnPath = Path.Combine("/Main/", fileName);
                response.success = true;
                response.returnId = Id;
                response.returnPath = returnPath;
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Error:" + ex.Message.ToString();
            }

            return response;
        }

        [Route("DeletePostUpload")]
        [HttpPost]
        [DisableRequestSizeLimit]
        [Authorize]
        public JsonResponse DeletePostUpload(string Id)
        {
            var response = new JsonResponse();
            try
            {
                var fileName = Path.Combine(FilePath, Id.ToString());
                if ((System.IO.File.Exists(fileName)))
                {
                    System.IO.File.Delete(fileName);
                }
                response.success = true;
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Error:" + ex.Message.ToString();
            }

            return response;
        }

        [Route("PropicUpload")]
        [HttpPost]
        public async Task<JsonResponse> PropicUpload(List<IFormFile> files)
        {
            var response = new JsonResponse();
            var UserId = this.userManager.GetUserId(User);
            try
            {
                foreach (IFormFile source in files)
                {
                    var fileName = UserId + Path.GetExtension(source.FileName);
                    double FileLength = source.Length;
                    if (!Directory.Exists(Path.Combine(FilePath, "ProfilePicture")))
                    {
                        Directory.CreateDirectory(Path.Combine(FilePath, "ProfilePicture"));
                    }
                    string VideoFolder = Path.Combine(FilePath, "ProfilePicture", fileName);
                    await CreateFile.UploadToServer(source, VideoFolder);
                    var returnPath = Path.Combine("/ProfilePicture/", fileName);
                    var user = await this.userManager.FindByIdAsync(UserId);
                    user.ProfileImgLocation = returnPath;
                    await this.userManager.UpdateAsync(user);

                    response.success = true;
                    response.ReturnMsg = "Successfully Uploaded User's Profile Picture";
                    response.returnId = UserId;
                    response.returnPath = returnPath;
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Error " + ex.Message.ToString();
            }

            return response;
        }

        [Route("CreatePost")]
        [HttpPost]
        public async Task<JsonResponse> Create([FromBody] PostMasterAPIModel apiModel)
        {
            var response = new JsonResponse();
            PaidPostType PaidPostType;
            var postCollections = new List<PostCollection>();
            if (apiModel.PostCollections.Where(x => x.PostType != ContentTypes.Polls.ToString()).ToList().Count > 4)//Max 4 contents Except Poll
            {
                var postCollectionApiModel = apiModel.PostCollections.Where(x => x.PostType != ContentTypes.Polls.ToString()).Take(4).ToList();
                List<PostCollectionAPIModel> pollContents = apiModel.PostCollections.Where(x => x.PostType == ContentTypes.Polls.ToString()).ToList();
                postCollectionApiModel.AddRange(pollContents);
                apiModel.PostCollections = postCollectionApiModel;
            }


            try
            {
                if (apiModel.PostCollections.Count > 0)
                {
                    if (apiModel.PostCollections.Where(x => Convert.ToBoolean(x.PaidPost) == true).Count() == apiModel.PostCollections.Count())
                    {
                        PaidPostType = PaidPostType.Full;
                    }
                    else if (apiModel.PostCollections.Where(x => Convert.ToBoolean(x.PaidPost) == true).Count() > 0)
                    {
                        PaidPostType = PaidPostType.Partial;
                    }
                    else
                    {
                        PaidPostType = PaidPostType.Free;
                        apiModel.PostPrice = "0";
                    }
                }
                else
                {
                    PaidPostType = PaidPostType.Free;
                    apiModel.PostPrice = "0";
                }
                var postMaster = new PostMaster();
                postMaster.Id = Guid.NewGuid();
                postMaster.CreatedById = Guid.Parse(this.userManager.GetUserId(User));
                postMaster.CreatedOn = Convert.ToDateTime(apiModel.CreatedDate);
                postMaster.ModifiedOn = DateTime.Now;
                postMaster.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active;
                postMaster.Deleted_Status = false;
                postMaster.PostTitle = apiModel.PostTitle;
                postMaster.PostPrivacy = Privacy.Public;
                postMaster.PaidPostType = PaidPostType;
                postMaster.PostPrice = string.IsNullOrEmpty(apiModel.PostPrice) ? 0 : Convert.ToDecimal(apiModel.PostPrice);
                postMaster.Scheduled = string.IsNullOrEmpty(apiModel.IsScheduled) ? false : Convert.ToBoolean(apiModel.IsScheduled);
                postMaster.NSFW = Convert.ToBoolean(apiModel.NSFW);
                if (postMaster.Scheduled)
                {
                    postMaster.ScheduledTime = Convert.ToDateTime(apiModel.ScheduledFor);
                }

                if (!string.IsNullOrEmpty(apiModel.ParentPostId)) // shared post
                {
                    PaidPostType = PaidPostType.Free;

                    postMaster.PostPrice = string.IsNullOrEmpty(apiModel.PostPrice) ? 0 : 0;

                    postMaster.ParentPostId = Guid.Parse(apiModel.ParentPostId);
                    apiModel.PostCollections = new List<PostCollectionAPIModel>();// since no content in collections of shared post
                }

                var success = false;

                await _postmaster.AddAsync(postMaster);
                //
                success = true;
                if (string.IsNullOrEmpty(apiModel.ParentPostId))
                {
                    //if (Convert.ToBoolean(apiModel.IsMedia))
                    if (apiModel.PostCollections.Any())
                    {
                        foreach (var collection in apiModel.PostCollections)
                        {
                            var postCollection = new PostCollection();
                            //postCollection.ContentId = Guid.Parse(collection.ContentId);
                            postCollection.ContentId = Guid.NewGuid();
                            postCollection.Record_Status = Record_Status.Active;
                            postCollection.PostId = postMaster.Id;
                            postCollection.PaidContent = Convert.ToBoolean(collection.PaidPost);
                            postCollection.ContentTitle = collection.ContentTitle;
                            postCollection.PostType = Enum.TryParse(collection.PostType, out ContentTypes post) ? (ContentTypes)Enum.Parse(typeof(ContentTypes), collection.PostType, true) : 0;
                            postCollection.FileLocation = collection.FileLocation;
                            postCollections.Add(postCollection);
                        }
                    }
                    else
                    {
                        var postCollection = new PostCollection();
                        postCollection.Record_Status = Record_Status.Active;

                        postCollection.ContentId = Guid.NewGuid();
                        postCollection.PostId = postMaster.Id;
                        postCollection.PaidContent = false;
                        postCollection.ContentTitle = apiModel.PostTitle;
                        postCollection.PostType = ContentTypes.Text;
                        postCollection.FileLocation = null;
                        postCollections.Add(postCollection);
                    }

                    success = await _postmaster.AddPostCollection(postCollections);
                }
                var tags = postMaster.PostTitle.Split(" ").Where(x => x.StartsWith("#")).Select(x => x.Replace("#", "")).ToList();

                bool successs = await _postmaster.AddPostTagsAsync(postMaster.Id, tags);

                var currentuserID = Guid.Parse(this.userManager.GetUserId(User));
                //if (_postmaster.GetUserPaidPostCount(currentuserID) == 1)
                //{
                //    //add content creator badge
                //    await badgeService.AddBadgeToUserByBadgeCode(currentuserID, BadgeCodes.ContentCreator.ToString());

                //}
                //if (_postmaster.GetUserPaidPostCount(currentuserID) == 10)
                //{
                //    //add contributor badge
                //    await badgeService.AddBadgeToUserByBadgeCode(currentuserID, BadgeCodes.Contributor.ToString());

                //}
                if (success)
                {
                    response.success = true;
                    response.ReturnMsg = "Succesfully Created a Post";
                    response.JsonObj = await _postmaster.GetSinglePostById(postMaster.Id, Guid.Empty, Guid.Parse(this.userManager.GetUserId(User)));
                    return response;
                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Something went wrong, Please Try Again!";
                    return response;
                }
            }
            catch (Exception ex)

            {
                response.success = false;
                response.ReturnMsg = ex.Message;
                return response;
            }
        }

        //// GET: PostMasters/Edit/5
        //[Route("EditPost")]
        //[HttpGet]
        //public async Task<IActionResult> Edit(Guid id)
        //{
        //    if (id == default)
        //    {
        //        return NotFound();
        //    }

        //    var postMaster = await _postmaster.GetById(id);
        //    if (postMaster == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(postMaster);
        //}

        // POST: PostMasters/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("EditPost")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([FromBody] PostMasterAPIModel apiModel)
        {
            Guid id = apiModel.Id;
            if (ModelState.IsValid)
            {
                PaidPostType PaidPostType;
                var postCollections = new List<PostCollection>();
                if (apiModel.PostCollections.Where(x => x.PostType != ContentTypes.Polls.ToString()).ToList().Count > 4)//Max 4 contents
                {
                    var postCollectionApiModel = apiModel.PostCollections.Where(x => x.PostType != ContentTypes.Polls.ToString()).Take(4).ToList();
                    List<PostCollectionAPIModel> pollContents = apiModel.PostCollections.Where(x => x.PostType == ContentTypes.Polls.ToString()).ToList();
                    postCollectionApiModel.AddRange(pollContents);
                    apiModel.PostCollections = postCollectionApiModel;
                }
                try
                {
                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));

                    var postMaster = await _postmaster.GetById(id);
                    if (postMaster == null)
                    {
                        return BadRequest($"Invalid Post Id: {id}");
                    }
                    if (postMaster.CreatedById != CurrId)
                    {
                        return Forbid("Unauthoorized User");
                    }

                    var OldPostCollections = await _postmaster.GetPostCollectionsById(id);
                    //check creator
                    //check null
                    if (apiModel.PostCollections.Count > 0)
                    {
                        if (apiModel.PostCollections.Where(x => Convert.ToBoolean(x.PaidPost) == true).Count() == apiModel.PostCollections.Count())
                        {
                            PaidPostType = PaidPostType.Full;
                        }
                        else if (apiModel.PostCollections.Where(x => Convert.ToBoolean(x.PaidPost) == true).Count() > 0)
                        {
                            PaidPostType = PaidPostType.Partial;
                        }
                        else
                        {
                            PaidPostType = PaidPostType.Free;
                            apiModel.PostPrice = "0";
                        }
                        apiModel.IsMedia = "true";
                    }
                    else
                    {
                        PaidPostType = PaidPostType.Free;
                        apiModel.PostPrice = "0";
                    }
                    postMaster.PostTitle = apiModel.PostTitle;
                    postMaster.PostPrivacy = Privacy.Public;
                    postMaster.PaidPostType = PaidPostType;
                    postMaster.PostPrice = string.IsNullOrEmpty(apiModel.PostPrice) ? 0 : Convert.ToDecimal(apiModel.PostPrice);
                    postMaster.Scheduled = string.IsNullOrEmpty(apiModel.IsScheduled) ? false : Convert.ToBoolean(apiModel.IsScheduled);
                    postMaster.NSFW = Convert.ToBoolean(apiModel.NSFW);
                    if (postMaster.Scheduled)
                    {
                        postMaster.ScheduledTime = Convert.ToDateTime(apiModel.ScheduledFor);
                    }

                    if (!string.IsNullOrEmpty(apiModel.ParentPostId)) // shared post
                    {
                        PaidPostType = PaidPostType.Free;

                        postMaster.PostPrice = string.IsNullOrEmpty(apiModel.PostPrice) ? 0 : 0;

                        postMaster.ParentPostId = Guid.Parse(apiModel.ParentPostId);
                        apiModel.PostCollections = new List<PostCollectionAPIModel>();// since to content in collections
                    }

                    var success = false;

                    await _postmaster.UpdateAsync(postMaster);
                    //
                    success = true;
                    if (string.IsNullOrEmpty(apiModel.ParentPostId))
                    {
                        var oldContentIds = OldPostCollections.Select(x => x.ContentId.ToString()).ToArray();
                        var newContentIds = apiModel.PostCollections.Select(x => x.ContentId.ToString()).ToArray();
                        var removedContentIds = oldContentIds.Except(newContentIds);
                        var UpdatedCollectionIds = oldContentIds.Intersect(newContentIds);
                        if (removedContentIds.Count() > 0)
                        {
                            var removedItemList = OldPostCollections.Where(x => removedContentIds.Contains(x.ContentId.ToString())).ToList();
                            foreach (var item in removedItemList)
                            {
                                //delete froom bucket
                                await _postmaster.RemoveCollectionAsync(item);//call api to remove content
                            }
                        }

                        if (UpdatedCollectionIds.Any())
                        {
                            var updatedCotentIdList = OldPostCollections.Where(x => UpdatedCollectionIds.Contains(x.ContentId.ToString())).ToList();
                            foreach (var item in updatedCotentIdList)
                            {
                                var updated = apiModel.PostCollections.FirstOrDefault(x => x.ContentId == item.ContentId.ToString());
                                if (updated == null)
                                {
                                    continue;
                                }
                                //item.PostId = postMaster.Id;
                                item.PaidContent = Convert.ToBoolean(updated.PaidPost);
                                //item.ContentTitle = updated.ContentTitle;
                                //item.PostType = Enum.TryParse(updated.PostType, out ContentTypes post) ? (ContentTypes)Enum.Parse(typeof(ContentTypes), updated.PostType, true) : 0;
                                //item.FileLocation = updated.FileLocation;
                                //delete froom bucket
                                await _postmaster.UpdateCollectionAsync(item);//call api to remove content
                            }
                        }

                        apiModel.PostCollections = apiModel.PostCollections.Where(x => !UpdatedCollectionIds.Contains(x.ContentId)).ToList();//new contents

                        if (Convert.ToBoolean(apiModel.IsMedia))
                        {
                            foreach (var collection in apiModel.PostCollections)
                            {
                                var postCollection = new PostCollection();
                                //postCollection.ContentId = Guid.Parse(collection.ContentId);
                                postCollection.ContentId = Guid.NewGuid();
                                postCollection.PostId = postMaster.Id;
                                postCollection.PaidContent = Convert.ToBoolean(collection.PaidPost);
                                postCollection.ContentTitle = collection.ContentTitle;
                                postCollection.PostType = Enum.TryParse(collection.PostType, out ContentTypes post) ? (ContentTypes)Enum.Parse(typeof(ContentTypes), collection.PostType, true) : 0;
                                postCollection.FileLocation = collection.FileLocation;
                                postCollection.Record_Status = Record_Status.Active;

                                postCollections.Add(postCollection);
                            }
                        }
                        else
                        {
                            var postCollection = new PostCollection();
                            postCollection.ContentId = Guid.NewGuid();
                            postCollection.PostId = postMaster.Id;
                            postCollection.PaidContent = false;
                            postCollection.ContentTitle = apiModel.PostTitle;
                            postCollection.PostType = ContentTypes.Text;
                            postCollection.FileLocation = null;
                            postCollection.Record_Status = Record_Status.Active;

                            postCollections.Add(postCollection);
                        }

                        success = await _postmaster.AddPostCollection(postCollections);
                    }
                    await _postmaster.RemovePostTagsAsync(postMaster.Id);

                    var tags = postMaster.PostTitle.Split(" ").Where(x => x.StartsWith("#")).Select(x => x.Replace("#", "")).ToList();

                    await _postmaster.AddPostTagsAsync(postMaster.Id, tags);
                    var updatedPost = await _postmaster.GetSinglePostById(postMaster.Id, Guid.Empty, postMaster.CreatedById);
                    return Ok(updatedPost);
                    //await _postmaster.UpdateAsync(postMaster);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostMasterExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Ok(nameof(Index));
            }
            return BadRequest();
        }

        // GET: PostMasters/Delete/5
        //[Route("DeletePost")]
        //[HttpGet]
        //public async Task<IActionResult> Delete(Guid id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var postMaster = await _postmaster.GetById(id);
        //    if (postMaster == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(postMaster);
        //}

        [Route("DeletePost")]
        [HttpPost]
        public async Task<JsonResponse> DeleteConfirmed(Guid id)
        {
            var response = new JsonResponse();
            try
            {
                var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                if (_postmaster.PostExistsByUser(id, CurrId))
                {
                    var postMaster = await _postmaster.GetById(id);
                    await _postmaster.RemoveAsync(postMaster);
                    response.success = true;
                    response.ReturnMsg = "Successfully Deleted";
                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Unauthorized action conducted!";
                }
            }
            catch
            {
                response.success = false;
                response.ReturnMsg = "Unable to delete the post";
            }
            return response;
        }

        [Route("DeleteCollection")]
        [HttpPost]
        public async Task<JsonResponse> DeleteCollection(Guid PostId, Guid CollectionId)
        {
            var response = new JsonResponse();
            try
            {
                var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                if (_postmaster.PostExistsByUser(PostId, CurrId))
                {
                    var postMaster = await _postmaster.GetCollectionById(CollectionId);
                    await _postmaster.RemoveCollectionAsync(postMaster);
                    response.success = true;
                    response.ReturnMsg = "Successfully Deleted";
                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Unauthorized action conducted!";
                }
            }
            catch
            {
                response.success = false;
                response.ReturnMsg = "Unable to delete the post";
            }
            return response;
        }

        [Route("TrendingTags")]
        [HttpGet]
        public async Task<JsonResponse> TrendingTags()
        {
            var response = new JsonResponse();
            try
            {
                var toptag = await context.PostTags.Include(x => x.Tag).GroupBy(x => x.Tag.Title).Select(x => new TopTrendingTagsViewModel()
                {
                    Title = x.Key,
                    Count = x.Count(),
                }).OrderByDescending(x => x.Count).Take(3).ToListAsync();
                response.JsonObj = toptag;
                response.success = true;
                response.ReturnMsg = "Successfully Fetched";
            }
            catch
            {
                response.success = false;
                response.ReturnMsg = "Unable to fetch the trending tags";
            }
            return response;
        }

        [Route("TrendingDailyDose")]
        [HttpGet]
        public async Task<JsonResponse> TrendingDailyDose()
        {
            var response = new JsonResponse();
            try
            {
                var toptag = await context.PostTags.Where(x => x.CreatedOn.Date >= DateTime.UtcNow.Date).Include(x => x.Tag).GroupBy(x => x.Tag.Title).Select(x => new TopTrendingTagsViewModel()
                {
                    Title = x.Key,
                    Count = x.Count(),
                }).OrderByDescending(x => x.Count).Take(3).ToListAsync();
                response.JsonObj = toptag;
                response.success = true;
                response.ReturnMsg = "Successfully Fetched";
            }
            catch
            {
                response.success = false;
                response.ReturnMsg = "Unable to fetch the trending tags";
            }
            return response;
        }

        [Route("PollAnalytics")]
        [HttpGet]
        public async Task<JsonResponse> GetPostPollAnalyticsData(Guid Id)
        {
            var response = new JsonResponse();
            try
            {
                if (!PostMasterExists(Id))
                {
                    response.success = false;
                    response.ReturnMsg = "Invalid Id";
                    return response;
                }
                var PollOptionIds = await context.PostCollection.Where(x => x.PostId == Id && x.PostType == ContentTypes.Polls).Select(x => new { x.ContentId, x.ContentTitle }).ToListAsync();
                if (!PollOptionIds.Any())
                {
                    response.success = false;
                    response.ReturnMsg = "Invalid Media Type";
                    return response;
                }

                var PollOptions = context.PostCollection.Where(x => x.PostId == Id && x.PostType == ContentTypes.Polls).AsQueryable();
                var PollOptionList = PollOptions.Select(x => new
                {
                    x.ContentId,
                    x.ContentTitle
                }).ToList();
                var Users = context.Users.AsQueryable();

                var PollOptionVotes = context.PostOptionByUser.Where(x => x.Record_Status == Record_Status.Active && x.PostId == Id).AsQueryable();
                var PollAnalyticsList = new List<PollAnalyticsViewModel>();

                var AnalyticsSummary = new PollAnalyticsViewModel();

                var OptionPollAnalyticsList = new List<PollAnalyticsViewModel>();

                foreach (var item in PollOptionList)
                {
                    var VotersId = await PollOptionVotes.Where(y => y.OptionId == item.ContentId).Select(x => x.UserId).ToArrayAsync();
                    var pollanalytics = new PollAnalyticsViewModel();
                    pollanalytics.OptionTitle = item.ContentTitle;
                    pollanalytics.PollCount = await context.PostOptionByUser.Where(y => y.Record_Status == Record_Status.Active && y.PostId == Id && y.OptionId == item.ContentId).CountAsync();
                    pollanalytics.Voters = Users.Where(x => VotersId.Contains(x.Id)).Select(x => new CreatorViewModel()
                    {
                        CreatorId = x.Id,
                        Creator = x.Fullname,
                        CreatorUsrName = x.UserName,
                        CreatorImg = x.ProfileImgLocation ?? "/Content/gallery/userimg.png"
                    }).ToList();
                    OptionPollAnalyticsList.Add(pollanalytics);
                }

                AnalyticsSummary.OptionTitle = "All";
                AnalyticsSummary.PollCount = OptionPollAnalyticsList.Sum(x => x.PollCount);
                AnalyticsSummary.Voters = OptionPollAnalyticsList.SelectMany(x => x.Voters).ToList();

                OptionPollAnalyticsList.Insert(0, AnalyticsSummary);

                //var optionVotesList = PollOptions.Select(x => new
                //{
                //    Option=x.ContentTitle,
                //    TotalVotes= context.PostOptionByUser.Where(y =>y.Record_Status==Record_Status.Active && y.PostId == Id && y.OptionId==x.ContentId).Count(),
                //    VotesByUser = PollOptionVotes.Where(y=>y.OptionId==x.ContentId).Select(pov=>
                //    Users.Where(usr => pov.UserId == usr.Id).Select(usr => new CreatorViewModel()
                //    {
                //        CreatorId = pov.UserId,
                //        Creator = usr.Fullname,
                //        CreatorUsrName = usr.UserName,
                //        CreatorImg = usr.ProfileImgLocation
                //    }).FirstOrDefault()),
                //}).ToListAsync();

                //var PollWithUser = await (from povs in PollOptionVotes
                //                          join po in PollOptions on povs.OptionId equals po.ContentId
                //                          select new
                //                          {
                //                              po.ContentId,
                //                              po.ContentTitle,
                //                              CreatorInfo = Users.Where(y => y.Id == povs.UserId).Select(x => new CreatorViewModel()
                //                              {
                //                                  CreatorId=povs.UserId,
                //                                  Creator=x.Fullname,
                //                                  CreatorUsrName = x.UserName,
                //                                  CreatorImg = x.ProfileImgLocation
                //                              }).FirstOrDefault(),
                //                          }).ToListAsync();

                response.JsonObj = OptionPollAnalyticsList;
                response.success = true;
                response.ReturnMsg = "Successfully Fetched";
            }
            catch
            {
                response.success = false;
                response.ReturnMsg = "Unable to fetch the analytics data";
            }
            return response;
        }

        #region PostReport
        /// <summary>
        /// Report Post
        /// </summary>
        /// <param name="PostId">Post Id to report</param>
        /// <param name="ReportedFor">Report Reason</param>
        /// <returns></returns>
        [Route("{PostId}/Report")]
        [HttpPost]
        public async Task<JsonResponse> Report(Guid PostId, string ReportedFor)
        {
            var response = new JsonResponse();
            if (ModelState.IsValid)
            {
                try
                {
                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));

                    var ReporrtType = await context.MessageReportType.FirstOrDefaultAsync(x => x.DisplayValue == ReportedFor);
                    if (ReporrtType != null)
                    {
                        var PostReportingByUser = new PostReportingByUser()
                        {
                            PostId = PostId,
                            CreatedOn = DateTime.UtcNow,
                            Record_Status = Record_Status.Active,
                            ReportingReason = ReportedFor,
                            Remarks = ReportedFor,
                            ReportedById = CurrId,
                            Id = Guid.NewGuid()
                        };

                        context.PostReportingByUser.Add(PostReportingByUser);
                        await context.SaveChangesAsync();
                        response.success = true;
                        response.ReturnMsg = "Successfully Reported";
                        return response;
                    }
                    else
                    {
                        Response.StatusCode = (int)StatusCodes.Status400BadRequest;
                        response.success = false;
                        response.ReturnMsg = "Report Reason invalid";
                        return response;
                    }
                }
                catch (Exception ex)
                {
                    Response.StatusCode = (int)StatusCodes.Status500InternalServerError;

                    response.success = false;
                    response.ReturnMsg = "Could not be saved.Internal Error";
                }
            }
            else
            {
                Response.StatusCode = (int)StatusCodes.Status400BadRequest;
                response.success = false;
                response.ReturnMsg = "Please Fill Up All The Required Details";
            }
            return response;
        }

        #endregion PostReport

        #region ViewAsGuestUser
        [Route("ViewAsGuestUser")]
        [HttpGet]
        public async Task<IActionResult> ViewAsGuestUser(int PageNumber = 1, int PageSize = 10)
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));

            var fbDbContext = await _postmaster.ViewAsGuestUser(currId, PageNumber, PageSize);
            return Ok(fbDbContext);
        }
        [Route("GetUserPostByVideoAsGuestUser")]
        [HttpGet]
        public async Task<IActionResult> GetUserPostByVideoAsGuestUser()
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            var fbDbContext = await _postmaster.GuestUserGetPostByVideo(currId);

            return Ok(fbDbContext);
        }

        [Route("GetPostByImageAsGuestUser")]
        [HttpGet]
        public async Task<IActionResult> GetPostByImageAsGuestUser()
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            var guestUserMedias = await _postmaster.GuestUserGetPostByImage(currId);

            return Ok(guestUserMedias);
        }


        #endregion

        public class PollAnalyticsViewModel
        {
            public string OptionTitle { get; set; }
            public int PollCount { get; set; }
            public List<CreatorViewModel> Voters { get; set; }
        }

        private bool PostMasterExists(Guid id)
        {
            return _postmaster.PostExists(id);
        }
    }
}