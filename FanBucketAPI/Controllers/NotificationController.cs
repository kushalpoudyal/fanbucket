﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationsRepository notificationsRepository;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IChatRepository chatRepository;


        public NotificationController(INotificationsRepository notificationsRepository,
             UserManager<ApplicationUser> userManager,
             IChatRepository chatRepository)
        {
            this.notificationsRepository = notificationsRepository;
            this.userManager = userManager;
            this.chatRepository = chatRepository;
        }
        // GET: api/Notification
        [HttpGet]
        public async Task<List<NotificationViewModel>> Get()
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var data = await notificationsRepository.GetUserNotification(CurrId);
            return data;
        }

        // GET: api/Notification
        [Route("GetNotificationCount")]
        [HttpGet]
        public async Task<int> GetNotificationCount()
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var data = await notificationsRepository.GetNotifCount(CurrId);
            return data;
        }
        // GET: api/Notification
        [Route("GetMessageCount")]
        [HttpGet]
        public async Task<int> GetMessageCount()
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var data = await chatRepository.GetUnreadMessagesCount(CurrId);
            return data;
        }

        // GET: api/Notification/MarkAllAsRead
        [HttpGet("MarkAllAsRead")]
        public async Task<IActionResult> MarkAllAsRead()
        {
            try
            {
                var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                var data = await notificationsRepository.MarkAllAsRead(CurrId);
                return Ok(true);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,false);
            }

        }

        // POST: api/Notification
        [HttpPost]
        [Authorize]
        public async Task<JsonResponse> Post(Guid Id)
        {
            var response = new JsonResponse();
            try
            {
                var data = await notificationsRepository.GetById(Id);
                if (data.PostCreatorId == Guid.Parse(this.userManager.GetUserId(User)))
                {
                    data.Viewed = true;
                    await notificationsRepository.UpdateAsync(data);
                    await notificationsRepository.SaveAsync();
                    response.success = true;
                    response.ReturnMsg = "Successfully Marked as Read";
                    return response;
                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Unathorized Access";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = ex.Message;
                return response;
            }

        }

        // PUT: api/Notification/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
