﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.ViewModel;
using Microsoft.AspNetCore.Identity;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.Model.ENUM;
using FanBucketAPI.Models;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostReactsController : ControllerBase
    {
        private readonly FbDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        public PostReactsController(FbDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        // GET: api/PostReacts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PostReacts>>> GetPostReacts()
        {
            return await _context.PostReacts.ToListAsync();
        }

        // GET: api/PostReacts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PostReacts>> GetPostReacts(Guid id)
        {
            var postReacts = await _context.PostReacts.FindAsync(id);

            if (postReacts == null)
            {
                return NotFound();
            }

            return postReacts;
        }

        // PUT: api/PostReacts/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPostReacts(Guid id, PostReacts postReacts)
        {
            if (id != postReacts.Id)
            {
                return BadRequest();
            }

            _context.Entry(postReacts).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;

            }

            return NoContent();
        }

        // POST: api/PostReacts
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<JsonResponse> PostPostReacts(ReactViewModel reactViewModel)
        {
            var response = new JsonResponse();

            var currId = Guid.Parse(this.userManager.GetUserId(User));
            var PostId = Guid.Parse(reactViewModel.PostId);
            if (!PostReactsExists(currId, PostId))
            {
                try
                {
                    var postReacts = new PostReacts();
                    postReacts.Id = Guid.NewGuid();
                    postReacts.ReactType = Enum.TryParse(reactViewModel.PostReact, out ReactTypes postprr) ? (ReactTypes)Enum.Parse(typeof(ReactTypes), reactViewModel.PostReact, true) : 0;
                    postReacts.PostId = PostId;
                    postReacts.CreatedById = currId;
                    postReacts.CreatedOn = DateTime.Now;
                    postReacts.ModifiedOn = DateTime.Now;
                    postReacts.Record_Status = Enum.TryParse(reactViewModel.record_Status, out Record_Status postpr) ? (Record_Status)Enum.Parse(typeof(Record_Status), reactViewModel.record_Status, true) : 0;

                    _context.PostReacts.Add(postReacts);

                    var notifications = new AppNotifications();
                    notifications.Id = Guid.NewGuid();
                    notifications.ActivityId = postReacts.Id;
                    notifications.PKId = postReacts.PostId;
                    notifications.nType = FanBucketEntity.Model.ENUM.NotificationType.Liked;
                    notifications.ActivityUserId = Guid.Parse(this.userManager.GetUserId(User));
                    notifications.PostCreatorId = _context.PostMaster.Where(x => x.Id == postReacts.PostId).Select(x => x.CreatedById).FirstOrDefault();
                    notifications.CreatedDate = DateTime.Now;
                    notifications.Viewed = false;

                    _context.AppNotifications.Add(notifications);


                    await _context.SaveChangesAsync();

                    response.success = true;
                    response.ReturnMsg = "Succesfully Reacted";
                }
                catch
                {
                    response.success = false;
                    response.ReturnMsg = "Invalid Input";

                }


            }
            else
            {
                var postReacts = _context.PostReacts.Where(e => e.PostId == PostId && e.CreatedById == currId).FirstOrDefault();
                postReacts.Record_Status = postReacts.Record_Status == Record_Status.Active ? Record_Status.Inactive : Record_Status.Active;
                _context.Entry(postReacts).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                    response.success = true;
                    response.ReturnMsg = "Succesfully Updated the Reacts";
                }
                catch
                {
                    response.success = false;
                    response.ReturnMsg = "Invalid Input";

                }

            }
            return response;
        }

        // DELETE: api/PostReacts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PostReacts>> DeletePostReacts(Guid id)
        {
            var postReacts = await _context.PostReacts.FindAsync(id);
            if (postReacts == null)
            {
                return NotFound();
            }

            _context.PostReacts.Remove(postReacts);
            await _context.SaveChangesAsync();

            return postReacts;
        }

        private bool PostReactsExists(Guid UserId, Guid PostId)
        {
            return _context.PostReacts.Any(e => e.PostId == PostId && e.CreatedById == UserId);
        }
    }
}
