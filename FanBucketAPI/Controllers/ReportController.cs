﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using FanBucketAPI.Models;
using Microsoft.AspNetCore.Identity;
using FanBucketEntity.Models.UserDetails;
using AutoMapper;
using FanBucketEntity.Model;
using FanBucketEntity.Model.UserDetails;
using System.IO;
using FanBucketAPI.Helper;
using System.Security.Cryptography.X509Certificates;
using FanBucketEntity.Model.ENUM;
using Microsoft.VisualStudio.Web.CodeGeneration;
using Microsoft.Extensions.Configuration;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReportController : ControllerBase
    {
        private readonly FbDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> userManager;
        public IConfiguration configuration;

        public ReportController(FbDbContext context,
            IMapper mapper,
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration)
        {

            _context = context;
            this.userManager = userManager;
            this.configuration = configuration;
            this._mapper = mapper;

        }
        
        #region Reasons
        /// <summary>
        /// Report Reasons
        /// </summary>
        /// <returns> Report Reasons </returns>
        [Route("Reasons")]
        [HttpGet]
        public async Task<JsonResponse> Reasons()
        {
            var response = new JsonResponse();
            response.success = false;
            try
            {
                response.JsonObj = await _context.MessageReportType.Where(x => x.IsActive).OrderBy(x => x.Order).Select(x => new MessageReportTypeViewModel
                {

                    Description = x.Description,
                    Value = x.DisplayValue,
                    Title = x.Title
                })
           .ToListAsync();
                response.success = true;

            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Internal Error";
            }

            return response;

        }


        #endregion
    }
}
