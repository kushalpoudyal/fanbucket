﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model;
using FanBucketAPI.Models;
using FanBucketEntity.Models.UserDetails;
using Microsoft.AspNetCore.Identity;
using FanBucketRepo.Interface;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Model.PostActivity;
using FanBucketAPI.Options;
using FanBucketAPI.ViewModel;
using FanBucketEntity.Enum;
using Hangfire;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubscriptionsController : ControllerBase
    {
        private readonly FbDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IEmailSender _emailSender;
        private readonly IPaymentRepository paymentRepository;
        private readonly IBackgroundJobClient _backgroundJobClient;

        public SubscriptionsController(FbDbContext context,
            UserManager<ApplicationUser> userManager,
            IEmailSender emailSender,
            IPaymentRepository paymentRepository,
            IBackgroundJobClient backgroundJobClient)
        {
            _context = context;
            this.userManager = userManager;
            _emailSender = emailSender;
            this.paymentRepository = paymentRepository;
            _backgroundJobClient = backgroundJobClient;
        }

        // GET: api/Subscriptions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Subscriptions>>> GetSubscriptions()
        {
            return await _context.Subscriptions.ToListAsync();
        }

        // GET: api/Subscriptions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Subscriptions>> GetSubscriptions(Guid id)
        {
            var subscriptions = await _context.Subscriptions.FindAsync(id);

            if (subscriptions == null)
            {
                return NotFound();
            }

            return subscriptions;
        }

        // PUT: api/Subscriptions/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubscriptions(Guid id, Subscriptions subscriptions)
        {
            if (id != subscriptions.Id)
            {
                return BadRequest();
            }

            _context.Entry(subscriptions).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubscriptionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Subscriptions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Route("PostSubscriptions")]
        [HttpPost]
        public async Task<JsonResponse> PostSubscriptions(string SubscriptionId, string PIN)
        {
            var response = new JsonResponse();

            var Id = Guid.Parse(SubscriptionId);
            var CurrId = Guid.Parse(userManager.GetUserId(User));
            var data = await _context.ApplicationUserSubscriptionInfo.Where(x => x.Id == Id).FirstOrDefaultAsync();
            if (data != null)
            {
                response.ResponseCode = await paymentRepository.PaymentForSubscription(Id, CurrId, Convert.ToInt32(PIN), data);
                //if (response.ResponseCode==FanBucketEntity.Model.ENUM.TransactionResponseCode.OK)
                //{
                //    var CurrUsername = userManager.GetUserName(User);

                //    var subscribeduser = await _context.Users.Where(x => x.Id == data.UserId).Select(s => new
                //    {
                //        s.Fullname,
                //        s.UserName,
                //        s.Email,
                //        s.HasSubscriptionAlert
                //    }).FirstOrDefaultAsync();
                //    var emailUserModel = new EmailUserModel()
                //    {
                //        Email = subscribeduser.Email,
                //        FullName = subscribeduser.Fullname,
                //        UserName = CurrUsername,
                //        Subject = "Subscribed",
                //        Type = EmailTemplate.Subscription

                //    };

                //    _backgroundJobClient.Enqueue(() => _emailSender.SendEmailAsync(emailUserModel));

                //}
                response.success = true;
                response.ReturnMsg = response.ResponseCode.ToString();

            }
            else
            {
                response.success = false;
                response.ReturnMsg = "Invalid Subscription ID, Try again";


            }

            return response;
        }
        // POST: api/Subscriptions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Route("CancelSubscriptions")]
        [HttpPost]
        public async Task<JsonResponse> CancelSubscriptions(Guid SubscriptionId)
        {
            var response = new JsonResponse();

            var CurrId = Guid.Parse(userManager.GetUserId(User));
            var CurrUsername = userManager.GetUserName(User);
            var data = await _context.ApplicationUserSubscriptionInfo.Where(x => x.Id == SubscriptionId).FirstOrDefaultAsync();
            if (data != null)
            {
                var result = await paymentRepository.CancelSubscription(SubscriptionId, CurrId);
                if (result != null)
                {
                    try
                    {
                        var unsubscribeduser = await _context.Users.Where(x => x.Id == data.UserId).Select(s => new
                        {
                            s.Fullname,
                            s.UserName,
                            s.Email,
                            s.HasSubscriptionAlert
                        }).FirstOrDefaultAsync();
                        if (unsubscribeduser!=null && unsubscribeduser.HasSubscriptionAlert)
                        {
                            //send mail on unsbscription
                            //var SubscriperUserEmail = "";

                            var emailUserModel = new EmailUserModel()
                            {
                                Email = unsubscribeduser.Email,
                                FullName = unsubscribeduser.UserName,
                                UserName = CurrUsername,
                                Subject = "Unsubscribed",
                                Type = EmailTemplate.Unsubscription

                            };
                            emailUserModel.CallbackUrl = $"https://fanbucket.com/${CurrUsername}";

                            _backgroundJobClient.Enqueue(() => _emailSender.SendEmailAsync(emailUserModel));
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    response.success = true;
                    response.ReturnMsg = "Subsription has been cancelled.";
                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Could not be cancelled, Invalid Request";
                }


            }
            else
            {
                response.success = false;
                response.ReturnMsg = "Invalid Subscription ID, Try again";


            }

            return response;
        }

        // DELETE: api/Subscriptions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Subscriptions>> DeleteSubscriptions(Guid id)
        {
            var subscriptions = await _context.Subscriptions.FindAsync(id);
            if (subscriptions == null)
            {
                return NotFound();
            }

            _context.Subscriptions.Remove(subscriptions);
            await _context.SaveChangesAsync();

            return subscriptions;
        }

        private bool SubscriptionsExists(Guid id)
        {
            return _context.Subscriptions.Any(e => e.Id == id);
        }
    }
}
