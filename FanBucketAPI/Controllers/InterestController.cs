﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using FanBucketAPI.Models;
using Microsoft.AspNetCore.Identity;
using FanBucketEntity.Models.UserDetails;
using AutoMapper;
using FanBucketEntity.Model;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class InterestController : ControllerBase
    {
        private readonly FbDbContext _context;
        private readonly IInterestRepository repo;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> userManager;

        public InterestController(FbDbContext context,
            IMapper mapper,
            IInterestRepository repo,
            UserManager<ApplicationUser> userManager)
        {

            _context = context;
            this.repo = repo;
            this.userManager = userManager;
            this._mapper = mapper;
        }

        // GET: api/Interest
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InterestViewModel>>> GetInterest()
        {
            var InterestList = await repo.GetAll().ToListAsync();
            var InterestInfoList = _mapper.Map<List<InterestViewModel>>(InterestList);
            return InterestInfoList;
        }

        // GET: api/Interest/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InterestViewModel>> GetInterest(Guid id)
        {
            var Interest = await repo.GetById(id);

            if (Interest == null)
            {
                return NotFound();
            }
            var InteresttDTO = _mapper.Map<InterestViewModel>(Interest);

            return InteresttDTO;
        }


        // PUT: api/Interest/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInterest(Guid id, InterestViewModel Interest)
        {
            if (id != Interest.Id)
            {
                return BadRequest();
            }
            var oldData = await repo.GetById(id);
            if (oldData == null)
            {
                return NotFound();
            }
            oldData.Name = Interest.Name;
            oldData.Description = Interest.Description;
            oldData.Code = Interest.Code;
            oldData.ModifiedOn = DateTime.Now;
            _context.Entry(oldData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InterestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Interest
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<JsonResponse>> PostInterest([FromBody]InterestViewModel Interest)
        {
            var response = new JsonResponse();
            try
            {
                var entity = new Interest();
                entity.Id = Guid.NewGuid();
                entity.Name = Interest.Name;
                entity.Description = Interest.Description;
                entity.Code = Interest.Code;
                entity.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active;
                entity.CreatedById = Guid.Parse(this.userManager.GetUserId(User));
                entity.CreatedOn = DateTime.Now;
                entity.ModifiedOn = DateTime.Now;
                entity.Deleted_Status = false;

                _context.Interest.Add(entity);

                await _context.SaveChangesAsync();

                response.success = true;
                response.ReturnMsg = "Successfully Commented";
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = ex.Message;
            }

            return response;

        }

        // DELETE: api/Interest/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<InterestViewModel>> DeleteInterest(Guid id)
        {
            var Interest = await repo.GetById(id);
            if (Interest == null)
            {
                return NotFound();
            }
            var InterestDto = _mapper.Map<InterestViewModel>(Interest);
            _context.Interest.Remove(Interest);
            await _context.SaveChangesAsync();

            return InterestDto;
        }

        private bool InterestExists(Guid id)
        {
            return _context.Interest.Any(e => e.Id == id);
        }
    }
}
