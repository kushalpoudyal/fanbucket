﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.Chat;
using FanBucketEntity.ViewModel;
using FanBucketAPI.Models;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Microsoft.AspNetCore.Identity;
using FanBucketEntity.Models.UserDetails;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using FanBucketEntity.CustomExceptions;
using FanBucketAPI.Options;
using FanBucketAPI.ViewModel;
using FanBucketEntity.Enum;
using Hangfire;
using FanBucketSignalR.Hubs;
using Microsoft.AspNetCore.SignalR;
using System.Xml.Linq;
using FanBucketEntity.Extension;
using FanBucketSignalR.API_Context;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MessageController : ControllerBase
    {

        private readonly IChatRepository chatRepository;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly FbDbContext _context;
        private readonly IPaymentRepository _payment;
        private readonly IEmailSender _emailSender;
        private readonly IBackgroundJobClient _backgroundJobClient;
        private readonly IHubContext<ChatHub> chat;

        public MessageController(IChatRepository chatRepository,
            UserManager<ApplicationUser> userManager,
            FbDbContext _context,
            IPaymentRepository payment,
            IEmailSender emailSender,
            IBackgroundJobClient backgroundJobClient,
             IHubContext<ChatHub> chat)
        {
            this.chatRepository = chatRepository;
            this.userManager = userManager;
            this._context = _context;
            _payment = payment;
            _emailSender = emailSender;
            _backgroundJobClient = backgroundJobClient;
            this.chat = chat;
        }

        // GET: api/Message
        [Route("GetMessage")]
        [HttpGet]
        public async Task<List<ChatUsersViewModel>> GetMessage()
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            return await chatRepository.GetChats(CurrId);
        }
        // GET: api/Message
        [Route("GetMessageByUser")]
        [HttpGet]
        public async Task<ChatViewModel> GetMessageByUser(Guid ChatId, Guid UserId)
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var MessageUnreadCount = await GetUnreadMessagesCount(CurrId);

            await chat.Clients.User(CurrId.ToString()).SendAsync("RecieveMessageUnreadCount", MessageUnreadCount);

            return await chatRepository.GetMessagesOfChat(ChatId, CurrId, UserId);
        }
        private async Task<int> GetUnreadMessagesCount(Guid CurrId)
        {
            var chatIds = await _context.ChatUsers.Where(z => z.UserId == CurrId).Select(z => z.ChatId).Distinct().ToListAsync();
            var chatId = _context.ChatUsers.Where(x => chatIds.Contains(x.ChatId));
            var ChatIds = await chatId.Select(x => x.ChatId).Distinct().ToListAsync();
            var data = await _context.Message.Where(x => ChatIds.Contains(x.ChatId) && x.DeletedStatus == false).ToListAsync();

            var info = data.Where(x => x.HasRead == false && x.UserId != CurrId).GroupBy(x => x.ChatId).Count();

            return info;
        }
        [Route("GetChatUsers")]
        [HttpGet]
        public async Task<List<ChatUsersViewModel>> GetChatUsers()
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            return await chatRepository.GetUserChats(CurrId);
        }
        [Route("GetOnlineUser")]
        [HttpGet]
        public async Task<List<OnlineUserViewMOdel>> GetOnlineUser()
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            return await chatRepository.GetOnlineUserById(CurrId);
        }

        [Route("UpdateMessageStatus")]
        [HttpGet]
        public async Task<IActionResult> UpdateMessageStatus(Guid ChatId)
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            await chatRepository.UpdateChatStatus(CurrId, ChatId);

            return Ok();
        }

        [Route("MarkAsUnread")]
        [HttpGet]
        public async Task<IActionResult> MarkAsUnread(Guid ChatId)
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            await chatRepository.MarkAsUnread(CurrId, ChatId);

            return Ok();
        }


        [Route("DeleteConversation")]
        [HttpGet]
        public async Task<IActionResult> DeleteConversation(Guid ChatId)
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var result = await chatRepository.DeleteConversation(CurrId, ChatId);

            return Ok();
        }

        [Route("HideConversation")]
        [HttpGet]
        public async Task<IActionResult> HideConversation(Guid ChatId)
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var result = await chatRepository.HideConversation(CurrId, ChatId);

            return Ok();
        }
        [Route("Delete")]
        [HttpGet]
        public async Task<IActionResult> Delete(Guid MessageId)
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            await chatRepository.Delete(CurrId, MessageId);

            return Ok();
        }

        // POST: api/Message
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Route("PostMessages")]
        [HttpPost]
        public async Task<JsonResponse> PostMessages([FromBody] PostChatViewModel messages)
        {
            var response = new JsonResponse()
            {
                success = true
            };
            if (ModelState.IsValid)
            {
                try
                {
                    if (!string.IsNullOrEmpty(messages.TipAmount))
                    {
                        var PayingAmount = Convert.ToDecimal(messages.TipAmount);
                        if (PayingAmount <= 0)
                        {
                            response.success = false;
                            response.ReturnMsg = "Sever Error";
                            return response;
                        }

                    }

                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                    var CurrUsername = this.userManager.GetUserName(User);
                    var Usr = await this.userManager.GetUserAsync(User);

                    var MessageId = Guid.NewGuid();
                    await chatRepository.UpdateChatStatus(CurrId, Guid.Parse(messages.ChatId));
                    var userMessaged = _context.Users.Where(x => x.Id == Guid.Parse(messages.targetId)).Select(x =>
                                        new
                                        {
                                            x.Email,
                                            x.Fullname,
                                            x.UserName,
                                            x.HasMessageAlert,
                                            x.HasTipAlert,
                                            x.ProfileImgLocation
                                        }).FirstOrDefault();
                    if (string.IsNullOrEmpty(messages.TipAmount))
                    {

                        await chatRepository.CreateMessage(Guid.Parse(messages.ChatId), messages.Message, messages.FileLocation, null, CurrId, Guid.Parse(messages.targetId), MessageId, messages.Attachments);
                        if (userMessaged.HasMessageAlert)
                        {
                            try
                            {

                                var emailUserModel = new EmailUserModel()
                                {
                                    Email = userMessaged.Email,
                                    FullName = userMessaged.UserName,
                                    UserName = CurrUsername,
                                    Subject = "Message Recieved",
                                    Type = EmailTemplate.MessageRecieved

                                };
                                emailUserModel.CallbackUrl = $"https://fanbucket.com/Messenger/Messenger?ChatId={messages.ChatId}&UserId=${CurrId}";
                                _backgroundJobClient.Enqueue(() => _emailSender.SendEmailAsync(emailUserModel));

                                //await _emailSender.SendEmailAsync(emailUserModel);
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        }

                    }
                    else
                    {
                        var respCode = await _payment.TipUser(new PaymentValidationModel()
                        {
                            CurrId = CurrId,
                            PayingAmount = Convert.ToDecimal(messages.TipAmount),
                            PIN = Convert.ToInt32(messages.PIN),
                            PostId = Guid.Parse(messages.targetId)
                        }, MessageId);

                        if (respCode == FanBucketEntity.Model.ENUM.TransactionResponseCode.OK)
                        {
                            await chatRepository.CreateMessage(Guid.Parse(messages.ChatId), messages.Message, messages.FileLocation, Convert.ToDecimal(messages.TipAmount), CurrId, Guid.Parse(messages.targetId), MessageId, messages.Attachments);
                            if (userMessaged.HasMessageAlert)
                            {
                                try
                                {

                                    var emailUserModel = new EmailUserModel()
                                    {
                                        Email = userMessaged.Email,
                                        FullName = userMessaged.UserName,
                                        UserName = CurrUsername,
                                        Subject = "Message Recieved",
                                        Type = EmailTemplate.MessageRecieved

                                    };
                                    emailUserModel.CallbackUrl = $"https://fanbucket.com/Messenger/Messenger?ChatId={messages.ChatId}&UserId=${CurrId}";

                                    _backgroundJobClient.Enqueue(() => _emailSender.SendEmailAsync(emailUserModel));
                                }
                                catch (Exception ex)
                                {
                                    throw;
                                }
                            }
                            if (userMessaged.HasTipAlert)
                            {
                                try
                                {

                                    var emailUserModel = new EmailUserModel()
                                    {
                                        Email = userMessaged.Email,
                                        FullName = userMessaged.UserName,
                                        UserName = CurrUsername,
                                        Subject = "Tips Recieved",
                                        Type = EmailTemplate.TipRecieved

                                    };
                                    emailUserModel.CallbackUrl = $"https://fanbucket.com/PaymentHistory";

                                    await _emailSender.SendEmailAsync(emailUserModel);
                                }
                                catch (Exception ex)
                                {
                                    throw;
                                }
                            }

                            //return response;

                        }
                        else if (respCode == FanBucketEntity.Model.ENUM.TransactionResponseCode.PinNotSet)
                        {
                            Response.StatusCode = StatusCodes.Status500InternalServerError;
                            response.success = false;
                            response.ReturnMsg = "Could not send the Tip due to " + respCode.ToString();
                        }
                        else
                        {
                            Response.StatusCode = StatusCodes.Status500InternalServerError;
                            response.success = false;
                            response.ReturnMsg = "Could not send the Tip due to " + respCode.ToString();
                        }
                    }

                    if (response.success)
                    {
                        await chat.Clients.User(messages.targetId).SendAsync("MessageNotification", "Send");

                        await chat.Clients.Group(messages.ChatId.ToString())
                                      .SendAsync("ReceiveMessage",new
                                      {
                                          Text = messages.Message,
                                          Success = true,
                                          User = CurrId,
                                          Time = DateTime.Now.Stringify(),
                                          //ChatWithName = User.Identity.GetName(),
                                          //profileImgLocation = string.IsNullOrEmpty(User.Identity.GetProfileImg()) ? "/Content/gallery/userimg.png" : User.Identity.GetProfileImg(),
                                          ChatWithName = Usr.Fullname,
                                          profileImgLocation = string.IsNullOrEmpty(Usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : Usr.ProfileImgLocation,
                                          ChatId = messages.ChatId,
                                          FileLocation = messages.FileLocation,
                                          TipAmount = messages.TipAmount,
                                          Attachments = messages.Attachments.Select(x => x.FileLocation).ToArray()
                                      });

                        //var MessageUnreadCount = await GetUnreadMessagesCount(Guid.Parse(messages.targetId));
                        //await chat.Clients.User(messages.targetId.ToString()).SendAsync("RecieveMessageUnreadCount", MessageUnreadCount);
                    }


                }
                catch (CommonException ex)
                {
                    Response.StatusCode = StatusCodes.Status500InternalServerError;
                    response.success = false;
                    response.ReturnMsg = ex.Message.ToString();
                }
                catch (Exception ex)
                {
                    Response.StatusCode = StatusCodes.Status500InternalServerError;
                    response.success = false;
                    response.ReturnMsg = "Could not be saved.Internal Error";
                }

            }
            else
            {
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                response.success = false;
                response.ReturnMsg = "Please Fill Up All The Required Details";
            }
            return response;

        }
        [Route("JoinRoom")]
        [HttpPost]
        public async Task<JsonResponse> JoinRoom([FromBody] PostChatViewModel messages)
        {
            var response = new JsonResponse();
            if (ModelState.IsValid)
            {
                try
                {
                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                    await chatRepository.JoinRoom(Guid.Parse(messages.ChatId), CurrId);

                    response.success = true;
                }
                catch (Exception ex)
                {
                    response.success = false;
                    response.ReturnMsg = "Could not be saved.Internal Error";
                }

            }
            else
            {
                response.success = false;
                response.ReturnMsg = "Please Fill Up All The Required Details";
            }
            return response;

        }
        //[Route("CreateRoom")]
        //[HttpPost]
        //public async Task<JsonResponse> CreateRoom(string TargetId)
        //{
        //    var response = new JsonResponse();
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
        //            var ChatId = await chatRepository.CreatePrivateRoom(CurrId, Guid.Parse(TargetId));

        //            response.success = true;
        //            response.ReturnMsg = ChatId.ToString();
        //        }
        //        catch (Exception ex)
        //        {
        //            response.success = false;
        //            response.ReturnMsg = "Could not be saved.Internal Error";
        //        }

        //    }
        //    else
        //    {
        //        response.success = false;
        //        response.ReturnMsg = "Please Fill Up All The Required Details";
        //    }
        //    return response;

        //}

        // DELETE: api/Message/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Messages>> DeleteMessages(Guid id)
        //{
        //    var messages = await chatRepository.GetById(id);
        //    if (messages == null)
        //    {
        //        return NotFound();
        //    }
        //    if (messages.UserId != Guid.Parse(this.userManager.GetUserId(User)))
        //    {
        //        return Unauthorized();
        //    }

        //    await chatRepository.RemoveAsync(messages);

        //    return messages;
        //}

        private bool MessagesExists(Guid ChatId)
        {
            return _context.Message.Any(e => e.ChatId == ChatId);
        }
        // GET: api/Message/BlockConversation
        [Route("BlockConversation")]
        [HttpPost]
        public async Task<bool> BlockConversation([FromBody] ChatBlockViewModel model)
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            return await chatRepository.BlockMessage(model.ChatId, CurrId, model.UserId);
        }
        // GET: api/Message/UnBlockConversation
        [Route("UnblockConversation")]
        [HttpPost]
        public async Task<bool> UnblockConversation([FromBody] ChatBlockViewModel model)
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            return await chatRepository.UnblockMessage(model.ChatId, CurrId, model.UserId);
        }
        #region ReportMessage
        [Route("ReportTypes")]
        [HttpGet]
        public async Task<JsonResponse> MessageReportTypes()
        {
            var response = new JsonResponse();
            response.success = false;
            response.ReturnMsg = "Internal Error";
            try
            {
                response.JsonObj = await chatRepository.GetReportTypesAsync();
                response.success = true;
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Internal Error";
            }

            return response;

        }

        [Route("ReportConversation")]
        [HttpPost]
        public async Task<JsonResponse> ReportConversation([FromBody] ReportConversationViewModel report)
        {
            var response = new JsonResponse();
            if (ModelState.IsValid)
            {
                try
                {
                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                    response.success = await chatRepository.ReportConversationAsync(report.ChatId, CurrId, report.ReportedFor);
                    response.ReturnMsg = "Conversation has been reported successfully.";

                    if (response.success == false)
                    {
                        response.ReturnMsg = "Could not be saved.Internal Error";

                    }
                }
                catch (Exception ex)
                {
                    response.success = false;
                    response.ReturnMsg = "Could not be saved.Internal Error";
                }

            }
            else
            {
                response.success = false;
                response.ReturnMsg = "Please Fill Up All The Required Details";
            }
            return response;

        }
        #endregion
    }
}
