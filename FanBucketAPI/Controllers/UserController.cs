﻿using FanBucketAPI.Models;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly FbDbContext _context;
        private readonly IUriService uriService;
        private readonly UserManager<ApplicationUser> userManager;

        public UserController(
            FbDbContext context,
            IUriService uriService,
            UserManager<ApplicationUser> userManager
            )
        {
            _context = context;
            this.uriService = uriService;
            this.userManager = userManager;
        }

        #region Settings

        /// <summary>
        /// Get All Setting Details
        /// </summary>
        [Route("Settings")]
        [HttpGet]
        public async Task<IActionResult> Settings()
        {
            var response = new JsonResponse();
            response.success = true;
            try
            {
                var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                var usr = _context.Users;
                //var socialMediaLinks=

                var user = await _context.Users.Where(x => x.Id == CurrId).FirstOrDefaultAsync();
                if (user != null)
                {
                    var settings = new SettingsViewModel();
                    var abc = from SocialMediaType in _context.SocialMediaType.OrderBy(x => x.SortOrder)
                              join UserSocialMediaLink in _context.UserSocialMediaLinks.Where(x => x.UserId == CurrId)
                              on SocialMediaType.Id equals UserSocialMediaLink.SocialMediaTypeId into SM
                              from UserSocialMediaLink in SM.DefaultIfEmpty()
                              select new SocialMediaLinksViewModel()
                              {
                                  BaseUrl = SocialMediaType.BaseUrl,
                                  IsPublic = UserSocialMediaLink.IsPublic,
                                  Name = SocialMediaType.DisplayText,
                                  UserName = UserSocialMediaLink.UserName
                              };
                    var social = await ((abc).ToListAsync());
                    var UserDetails = _context.ApplicationUserDetails.Where(x => x.UserId == CurrId).FirstOrDefault();
                    int MaxNoOfUserNameChange = 2;
                    int remaining = NoOfTimesUsernameCanBeChanged(user, MaxNoOfUserNameChange);
                    settings.ProfileSettingViewModel = new ProfileSettingViewModel()
                    {
                        Fullname = user.Fullname,
                        ProfileImgLocation = user.ProfileImgLocation,
                        ShortBio = UserDetails?.ShortBio,
                        Profession = UserDetails?.Profession,
                        UserName = user.UserName,
                        RemainingUserNameChanges = remaining,
                        MaxNoOfUserNameChange = MaxNoOfUserNameChange,
                    };
                    if (settings.ProfileSettingViewModel.ShortBio == null)
                    {
                        settings.ProfileSettingViewModel.ShortBio = string.Empty;
                    }
                    if (settings.ProfileSettingViewModel.Profession == null)
                    {
                        settings.ProfileSettingViewModel.Profession = string.Empty;
                    }
                    settings.ApperanceSetting = new ApperanceSettingViewModel()
                    {
                        CoverImgPic = user.CoverImgLocation,
                        FeatureImageLeft = user.FeaturedImageLeft,
                        FeatureImageRight = user.FeaturedImageRight,
                        //ProfilePicUrl = user.ProfileImgLocation,
                        SocialMediaLinks = social
                    };
                    var kycDetails =await _context.ApplicationUserKYC.Where(x => x.UserId == CurrId)
                        .Select(x => new EditKycViewModel()
                        {
                            FirstName=x.FirstName,
                            MiddleName=x.MiddleName,
                            LastName=x.LastName,
                            Address=x.Address,
                            IdNO = x.IdNO,
                            Country = x.Country,
                            Gender = x.Gender,
                            ContactNumber = x.ContactNumber,
                            DOB = x.DOB.ToShortDateString(),
                            Identification = x.Identification,
                            IdIssuedFrom = x.IdIssuedFrom,
                            IdentificationFileLocation=x.IdentificationFileLocation,
                        }).FirstOrDefaultAsync();
                    settings.KYCSetting = kycDetails ?? new EditKycViewModel(); ;

                    settings.NotificationSetting = new NotificationSettingViewModel()
                    {
                        EmailNotification = user.EmailNotification,
                        HasLoginAlert = user.HasLoginAlert,
                        HasInformationUpdateAlert = user.HasInformationUpdateAlert,
                        HasMessageAlert = user.HasMessageAlert,
                        HasTipAlert = user.HasTipAlert,
                        HasSubscriptionAlert = user.HasSubscriptionAlert,
                        HasFollowerAlert = user.HasFollowerAlert,
                        HasShareAlert = user.HasShareAlert,
                        HasMentionAlert = user.HasMentionAlert,
                        HasBookmarkAlert = user.HasBookmarkAlert,
                        HasLoveReactAlert = user.HasLoveReactAlert
                    };

                    settings.SubscriptionSettingViewModel = new SubscriptionSettingViewModel()
                    {
                        AccountType = user.AccountType,
                        NSFW = user.NSFW,
                        SubscriptionInfos = _context.ApplicationUserSubscriptionInfo
                                  .Where(z => z.UserId == user.Id && z.Record_Status != FanBucketEntity.Model.ENUM.Record_Status.Inactive)
                                  .Select(z => new ApplicationUserSubscriptionINfo
                                  {
                                      Id = z.Id,
                                      UserId = z.UserId,
                                      CreatedOn = z.CreatedOn,
                                      Record_Status = z.Record_Status,
                                      SubscriptionAmount = z.SubscriptionAmount,
                                      Tenure = z.Tenure
                                  }).ToList(),

                    };
                    return Ok(settings);
                }
                else
                {
                    return NotFound("User Not Found");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { message = "Server error occurred" });
            }
        }

        private static int NoOfTimesUsernameCanBeChanged(ApplicationUser user, int NoOfTimesThatCanBeChanged)
        {
            var noOfTimeChanged = 0;
            var remaining = 0;
            //var remaining = NoOfTimesThatCanBeChanged;

            if (user.LastUserNameModifiedDate.HasValue)
            {
                var lastUsernameChangedDate = user.LastUserNameModifiedDate.Value;
                noOfTimeChanged = user.UserNameModifiedTimes;
                var currentyear = DateTime.UtcNow.Year;
                var LastModifiedYear = lastUsernameChangedDate.Year;

                if (currentyear == LastModifiedYear)
                {
                    remaining = NoOfTimesThatCanBeChanged - noOfTimeChanged;
                    ////can not be changed
                }
                if (currentyear != LastModifiedYear)
                {
                    remaining = NoOfTimesThatCanBeChanged;//reset if not changed
                }

            }

            return remaining;
        }

        /// <summary>
        /// Change appereance Setting
        /// </summary>
        /// <param name="apperanceViewModel"></param>
        [Route("EditApperanceSettings")]
        [HttpPost]
        public async Task<JsonResponse> EditApperanceSettings([FromBody] ApperanceSettingViewModel apperanceViewModel)
        {
            var response = new JsonResponse();
            response.success = true;
            if (ModelState.IsValid)
            {
                try
                {
                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                    var user = await _context.Users.Where(x => x.Id == CurrId).FirstOrDefaultAsync();
                    if (user != null)
                    {
                        //user.ProfileImgLocation = apperanceViewModel.ProfilePicUrl;
                        user.CoverImgLocation = apperanceViewModel.CoverImgPic;
                        user.FeaturedImageLeft = apperanceViewModel.FeatureImageLeft;
                        user.FeaturedImageRight = apperanceViewModel.FeatureImageRight;
                        var socialmediaTypes = await _context.SocialMediaType.Select(x => new
                        {
                            x.Id,
                            x.Value
                        }).ToListAsync();
                        var mediaLinkList = await _context.UserSocialMediaLinks.Where(x => x.UserId == CurrId).ToListAsync();

                        foreach (var item in apperanceViewModel.SocialMediaLinks)
                        {
                            var socialMediaType = socialmediaTypes.FirstOrDefault(x => x.Value == item.Name);
                            if (socialMediaType == null)
                            {
                                continue;
                            }
                            var oldData = mediaLinkList.Where(x => x.SocialMediaTypeId == socialMediaType.Id).FirstOrDefault();
                            if (oldData == null)
                            {
                                _context.UserSocialMediaLinks.Add(new UserSocialMediaLinks()
                                {
                                    Id = Guid.NewGuid(),
                                    IsPublic = item.IsPublic,
                                    SocialMediaTypeId = socialMediaType.Id,
                                    UserId = CurrId,
                                    UserName = item.UserName
                                });
                            }
                            else
                            {
                                oldData.UserName = item.UserName;
                                oldData.IsPublic = item.IsPublic;
                                _context.UserSocialMediaLinks.Update(oldData);
                            }
                        }
                        _context.Users.Update(user);

                        //update social media links
                        await _context.SaveChangesAsync();
                        response.success = true;
                        response.ReturnMsg = "Successfully Updated.";
                        return response;
                    }
                    else
                    {
                        Response.StatusCode = (int)StatusCodes.Status400BadRequest;
                        response.success = false;
                        response.ReturnMsg = "User Not Found";
                        return response;
                    }
                }
                catch (Exception ex)
                {
                    Response.StatusCode = (int)StatusCodes.Status500InternalServerError;

                    response.success = false;
                    response.ReturnMsg = "Could not be saved.Internal Error";
                }
            }
            else
            {
                Response.StatusCode = (int)StatusCodes.Status400BadRequest;
                response.success = false;
                response.ReturnMsg = "Please Fill Up All The Required Details";
            }
            return response;
        }

        #endregion NotificationSetting

        #region NotificationSetting

        /// <summary>
        /// Used for updating Notification Setting
        /// </summary>
        /// <param name="notificationSetting"></param>
        [Route("EditNotificationSettings")]
        [HttpPost]
        public async Task<JsonResponse> EditNotificationSettings([FromBody] NotificationSettingViewModel notificationSetting)
        {
            var response = new JsonResponse();
            if (ModelState.IsValid)
            {
                try
                {
                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                    var user = await _context.Users.Where(x => x.Id == CurrId).FirstOrDefaultAsync();
                    if (user != null)
                    {
                        user.EmailNotification = notificationSetting.EmailNotification;
                        user.HasLoginAlert = notificationSetting.HasLoginAlert;
                        user.HasInformationUpdateAlert = notificationSetting.HasInformationUpdateAlert;
                        user.HasMessageAlert = notificationSetting.HasMessageAlert;
                        user.HasTipAlert = notificationSetting.HasTipAlert;
                        user.HasSubscriptionAlert = notificationSetting.HasSubscriptionAlert;
                        user.HasFollowerAlert = notificationSetting.HasFollowerAlert;
                        user.HasShareAlert = notificationSetting.HasShareAlert;
                        user.HasMentionAlert = notificationSetting.HasMentionAlert;
                        user.HasBookmarkAlert = notificationSetting.HasBookmarkAlert;
                        user.HasLoveReactAlert = notificationSetting.HasLoveReactAlert;
                        await _context.SaveChangesAsync();
                        response.success = true;
                        response.ReturnMsg = "Successfully Updated.";
                        return response;
                    }
                    else
                    {
                        Response.StatusCode = (int)StatusCodes.Status400BadRequest;
                        response.success = false;
                        response.ReturnMsg = "User Not Found";
                        return response;
                    }
                }
                catch (Exception ex)
                {
                    Response.StatusCode = (int)StatusCodes.Status500InternalServerError;

                    response.success = false;
                    response.ReturnMsg = "Could not be saved.Internal Error";
                }
            }
            else
            {
                Response.StatusCode = (int)StatusCodes.Status400BadRequest;
                response.success = false;
                response.ReturnMsg = "Please Fill Up All The Required Details";
            }
            return response;
        }

        #endregion NotificationSetting



        #region PrivacySetting
        /// <summary>
        /// Used for updating privacy settings and user subscriptions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("EditPrivacyProfile")]
        [HttpPost]
        public async Task<JsonResponse> EditPrivacyProfile(EditProfilePrivacyViewModel model)
        {
            var response = new JsonResponse();
            var UserId = this.userManager.GetUserId(User);
            try
            {
                var Users = await this.userManager.FindByIdAsync(UserId);

                Users.AccountType = Enum.TryParse(model.AccountType, out AccountType post) ? (AccountType)Enum.Parse(typeof(AccountType), model.AccountType, true) : 0;
                Users.NSFW = Convert.ToBoolean(model.NSFW);
                UpdateUserSubscriptionInfo(model, UserId);

                await _context.SaveChangesAsync();
                response.JsonObj = _context.ApplicationUserSubscriptionInfo.Where(x => x.UserId == Guid.Parse(UserId) && x.Record_Status == Record_Status.Active).Select(x => new
                {
                    Id = x.Id,
                    Tenure = x.Tenure.ToString(),
                    SubscriptionAmount = x.SubscriptionAmount.ToString()
                }).ToList();

                response.success = true;
                response.ReturnMsg = "Successfully Updated Privacy details.";

            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = "Error " + ex.Message.ToString();
            }

            return response;

        }

        private void UpdateUserSubscriptionInfo(EditProfilePrivacyViewModel model, string UserId)
        {
            var oldSubscriptioonInfo = _context.ApplicationUserSubscriptionInfo.Where(x => x.UserId == Guid.Parse(UserId) && x.Record_Status == Record_Status.Active).ToList();

            var oldSubscriptioonInfoIds = oldSubscriptioonInfo.Select(x => x.Id.ToString()).ToList();
            var newIds = model.SubscriptionType.Select(x => x.Id).ToList();


            var removedList = oldSubscriptioonInfo.Where(x => !newIds.Contains(x.Id.ToString())).ToList();
            foreach (var data in removedList)
            {
                data.Record_Status = Record_Status.Inactive;
                _context.ApplicationUserSubscriptionInfo.Update(data);
            }
            var newSubscription = model.SubscriptionType.Where(x => !oldSubscriptioonInfoIds.Contains(x.Id)).ToList();
            foreach (var subData in newSubscription)
            {

                bool isNumerical = int.TryParse(subData.Tenure, out int Tenure);
                if (!isNumerical || Tenure == -1)//skip if not number 
                {
                    continue;
                }
                var subs = new ApplicationUserSubscriptionINfo();
                subs.Id = Guid.NewGuid();
                subs.SubscriptionAmount = Convert.ToDecimal(subData.SubscriptionAmount);
                subs.Tenure = Tenure;
                subs.UserId = Guid.Parse(UserId);
                subs.CreatedOn = DateTime.Now;
                subs.Record_Status = Record_Status.Active;

                _context.ApplicationUserSubscriptionInfo.Add(subs);
            }

            var existingSubscription = model.SubscriptionType.Where(x => oldSubscriptioonInfoIds.Contains(x.Id)).ToList();
            foreach (var subData in existingSubscription)
            {
                var subs = oldSubscriptioonInfo.FirstOrDefault(x => x.Id == Guid.Parse(subData.Id));
                bool isNumerical = int.TryParse(subData.Tenure, out int Tenure);
              
                subs.SubscriptionAmount = Convert.ToDecimal(subData.SubscriptionAmount);
                if (!isNumerical || Tenure == -1|| subs.SubscriptionAmount <= 0)//skip if not number 
                {
                    continue;
                }
                subs.Tenure = Tenure;

                _context.ApplicationUserSubscriptionInfo.Update(subs);
            }
        }
        #endregion
        #region UserReport

        /// <summary>
        /// Report user
        /// </summary>
        /// <param name="UserId">User Id to report</param>
        /// <param name="ReportedFor">Report Reason</param>
        [Route("{UserId}/Report")]
        [HttpPost]
        public async Task<JsonResponse> Report(Guid UserId, string ReportedFor)
        {
            var response = new JsonResponse();
            if (ModelState.IsValid)
            {
                try
                {
                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));

                    var ReporrtType = await _context.MessageReportType.FirstOrDefaultAsync(x => x.DisplayValue == ReportedFor);
                    if (ReporrtType != null)
                    {
                        var UserReportedByUser = new UserReportedByUser()
                        {
                            UserId = UserId,
                            CreatedOn = DateTime.UtcNow,
                            Record_Status = Record_Status.Active,
                            ReportingReason = ReportedFor,
                            Remarks = ReportedFor,
                            ReportedById = CurrId,
                            Id = Guid.NewGuid(),
                        };

                        _context.UserReportedByUser.Add(UserReportedByUser);
                        await _context.SaveChangesAsync();
                        response.success = true;
                        response.ReturnMsg = "Successfully Reported";
                        return response;
                    }
                    else
                    {
                        Response.StatusCode = (int)StatusCodes.Status400BadRequest;
                        response.success = false;
                        response.ReturnMsg = "Report Reason invalid";
                        return response;
                    }
                }
                catch (Exception ex)
                {
                    Response.StatusCode = (int)StatusCodes.Status500InternalServerError;

                    response.success = false;
                    response.ReturnMsg = "Could not be saved.Internal Error";
                }
            }
            else
            {
                Response.StatusCode = (int)StatusCodes.Status400BadRequest;
                response.success = false;
                response.ReturnMsg = "Please Fill Up All The Required Details";
            }
            return response;
        }


        #endregion PostReport


        #region KycSetting
        /// <summary>
        /// Used for updating KYC details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("EditKYCDetails")]
        [HttpPost]
        public async Task<JsonResponse> EditKYCDetails(EditKycViewModel model)
        {
            var response = new JsonResponse();
            var UserId = this.userManager.GetUserId(User);
            try
            {
                var data = _context.ApplicationUserKYC.Where(x => x.UserId == Guid.Parse(UserId)).FirstOrDefault();
                if (data == null)
                {
                    var kyc = new ApplicationUserKYC();
                    kyc.UserId = Guid.Parse(UserId);
                    kyc.FirstName = model.FirstName;
                    kyc.IdentificationFileLocation = model.IdentificationFileLocation;
                    kyc.MiddleName = model.MiddleName;
                    kyc.LastName = model.LastName;
                    kyc.Address = model.Address;
                    kyc.Identification = model.Identification;
                    //kyc.IdNO = model.IdNO;
                    //kyc.IdIssuedFrom = model.IdIssuedFrom;
                    kyc.ContactNumber = model.ContactNumber;
                    //kyc.DOB = Convert.ToDateTime(model.DOB);
                    kyc.Country = model.Country;
                    //kyc.Gender = model.Gender;
                    kyc.CreatedOn = DateTime.UtcNow;
                    kyc.ModifiedOn = DateTime.UtcNow;

                    _context.ApplicationUserKYC.Add(kyc);

                }
                else
                {
                    data.IdentificationFileLocation = model.IdentificationFileLocation;

                    data.FirstName = model.FirstName;
                    data.MiddleName = model.MiddleName;
                    data.LastName = model.LastName;
                    data.Address = model.Address;
                    data.Identification = model.Identification;
                    //data.IdNO = model.IdNO;
                    //data.IdIssuedFrom = model.IdIssuedFrom;
                    data.ContactNumber = model.ContactNumber;
                    data.Country = model.Country;
                    //data.Gender = model.Gender;
                    //data.DOB = Convert.ToDateTime(model.DOB);
                    data.ModifiedOn = DateTime.UtcNow;

                }
                if (model.CardNumber != null)
                {
                    var dataCard = _context.CustomerPaymentDetails.Where(x => x.UserId == Guid.Parse(UserId)).FirstOrDefault();
                    if (dataCard == null)
                    {
                        var card = new CustomerPaymentDetails()
                        {
                            Id = Guid.NewGuid(),
                            CardName = model.CardName,
                            CardNumber = model.CardNumber,
                            CreatedOn = DateTime.UtcNow,
                            CVV = model.CVV,
                            Expiry = model.Expiry,
                            UserId = Guid.Parse(UserId)
                        };
                        _context.CustomerPaymentDetails.Add(card);
                    }
                    else
                    {
                        dataCard.CardName = model.CardName;
                        dataCard.CardNumber = model.CardNumber;
                        dataCard.CVV = model.CVV;
                        dataCard.Expiry = model.Expiry;
                    }
                }

                await _context.SaveChangesAsync();
                response.success = true;
                response.ReturnMsg = "Successfully Updated KYC details.";

            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = "Error " + ex.Message.ToString();
            }
            return response;
        }
        #endregion
    }

}