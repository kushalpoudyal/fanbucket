﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketAPI.Options;
using FanBucketAPI.ViewModel;
using FanBucketEntity.DBContext;
using FanBucketEntity.Enum;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserFollowController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly FbDbContext _context;
        private readonly IEmailSender _emailSender;
        private readonly IUserRepository userRepository;
        private readonly IBackgroundJobClient _backgroundJobClient;

        public UserFollowController(UserManager<ApplicationUser> userManager, FbDbContext _context, IEmailSender emailSender,
            IUserRepository userRepository, IBackgroundJobClient backgroundJobClient)
        {
            this.userManager = userManager;
            this._context = _context;
            _emailSender = emailSender;
            this.userRepository = userRepository;
            _backgroundJobClient = backgroundJobClient;
        }

        [HttpPost]
        public async Task<JsonResponse> FollowUser(string UserId)
        {
            var UsrId = Guid.Parse(UserId);
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var CurrUsername = this.userManager.GetUserName(User);
            var response = new JsonResponse();
            if (UserExist(UsrId))
            {
                try
                {

                    var Chats = _context.ChatUsers.Where(x => x.UserId == CurrId).Select(x => x.ChatId);
                    var ChatId = _context.ChatUsers.Where(x => Chats.Contains(x.ChatId) && x.UserId == UsrId).Select(x => x.ChatId).FirstOrDefault();
                    if (ChatId == default)
                    {
                        ChatId = Guid.NewGuid();
                    }
                    if (!UserFollowExist(UsrId, CurrId))
                    {
                        var FollowReq = new ApplicationUserFollowlist();
                        FollowReq.Id = Guid.NewGuid();
                        FollowReq.SubId = Guid.Parse("05018EF5-72B9-4549-BC25-B68F6D36F7F6");
                        FollowReq.FollowedBy = CurrId;
                        FollowReq.UserId = UsrId;
                        FollowReq.FollowedFrom = DateTime.Now;
                        FollowReq.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active;
                        FollowReq.ModifiedOn = DateTime.Now;

                        _context.ApplicationUserFollowlist.Add(FollowReq);


                        var notifications = new AppNotifications();
                        notifications.Id = Guid.NewGuid();
                        notifications.ActivityId = FollowReq.Id;
                        notifications.PKId = FollowReq.Id;
                        notifications.nType = FanBucketEntity.Model.ENUM.NotificationType.Following;
                        notifications.ActivityUserId = CurrId;
                        notifications.PostCreatorId = FollowReq.UserId;
                        notifications.CreatedDate = DateTime.Now;
                        notifications.Viewed = false;

                        _context.AppNotifications.Add(notifications);

                        await _context.SaveChangesAsync();
                        response.JsonObj = ChatId;
                        response.success = true;
                        response.ReturnMsg = "User Followed";
                    }
                    else
                    {
                        var FollowReq = _context.ApplicationUserFollowlist.Where(x => x.FollowedBy == CurrId && x.UserId == UsrId).FirstOrDefault();
                        FollowReq.Record_Status = FollowReq.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active ? FanBucketEntity.Model.ENUM.Record_Status.Inactive : FanBucketEntity.Model.ENUM.Record_Status.Active;
                        FollowReq.ModifiedOn = DateTime.Now;
                        _context.ApplicationUserFollowlist.Update(FollowReq);
                        await _context.SaveChangesAsync();
                        if (FollowReq.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active)
                        {
                            response.JsonObj = ChatId;

                            response.success = true;
                            response.ReturnMsg = "User Followed";

                        }
                        else
                        {
                            var unfollowedUser = _context.Users.Where(x => x.Id == UsrId).Select(x =>
                            new
                            {
                                x.Email,
                                x.Fullname,
                                x.UserName,
                                x.HasFollowerAlert
                            }).FirstOrDefault();
                            if (unfollowedUser.HasFollowerAlert)
                            {
                                try
                                {

                                    var emailUserModel = new EmailUserModel()
                                    {
                                        Email = unfollowedUser.Email,
                                        FullName = unfollowedUser.UserName,
                                        UserName = CurrUsername,
                                        CallbackUrl = "https://fanbucket.com/" + CurrUsername,
                                        Subject = "Unfollowed by User",
                                        Type = EmailTemplate.Unfollowed

                                    };
                                    _backgroundJobClient.Enqueue(() => _emailSender.SendEmailAsync(emailUserModel));
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }


                            response.success = true;
                            response.ReturnMsg = "User Unfollowed";
                        }

                    }

                }
                catch
                {
                    response.success = false;
                    response.ReturnMsg = "Something Went Wrong";
                }

            }
            else
            {
                response.success = false;
                response.ReturnMsg = "User Does not Exist";
            }

            return response;

        }
        [Route("GetRelatedUser")]
        [HttpGet]
        public async Task<List<OnlineUserViewMOdel>> GetRelatedUser(string UserId, string Type)
        {
            var CurrId = Guid.Parse(UserId);

            if (Type.ToUpper() == "FOLLOWERS")
            {
                return await userRepository.GetUserRelatedFollowers(CurrId);
            }
            else if (Type.ToUpper() == "FOLLOWING")
            {
                return await userRepository.GetUserRelatedFollowing(CurrId);
            }
            else if (Type.ToUpper() == "SUBSCRIBERS")
            {
                return await userRepository.GetUserRelatedSubscribers(CurrId);
            }
            else
            {
                return null;
            }

        }

        private bool UserExist(Guid UserId)
        {
            return _context.Users.Any(x => x.Id == UserId);
        }
        private bool UserFollowExist(Guid UserId, Guid CurrId)
        {
            return _context.ApplicationUserFollowlist.Any(x => x.FollowedBy == CurrId && x.UserId == UserId);
        }
    }
}