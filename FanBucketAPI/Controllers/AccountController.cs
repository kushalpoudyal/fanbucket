﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketEntity.DBContext;
using FanBucketEntity.Models.UserDetails;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace FanBucketAPI.Controllers
{

    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly FbDbContext _context;
        private readonly SignInManager<ApplicationUser> signInManager;
        public AccountController(UserManager<ApplicationUser> userManager,
            FbDbContext context, SignInManager<ApplicationUser> signInManager)
        {
            this.userManager = userManager;
            this._context = context;
            this.signInManager = signInManager;
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {

                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);


                if (result.Succeeded)
                {
                    var usrdetails = await _context.Users.Where(x => x.UserName == model.Email).FirstOrDefaultAsync();
                    var usrRole = await this.userManager.GetRolesAsync(usrdetails);

                    if (HttpContext.User != null)
                    {
                        var claims = new List<Claim>
                     {
                               new Claim(ClaimTypes.Role,usrRole.FirstOrDefault()),
                               new Claim(ClaimTypes.NameIdentifier,usrdetails.Id.ToString()),

                    };

                        var appIdentity = new ClaimsIdentity(claims);
                        HttpContext.User.AddIdentity(appIdentity);
                    }
                    // this.emailService.Mail_Alert(model.Email, "Login Detected", "<h4>You recently have been found to be logged in the Boxklip Application.</h4>");

                    return RedirectToAction("Index", "Home");


                }
                else
                {
                    //if (usrdetails.IsPasswordUpdated)
                    //{
                    //    return RedirectToAction("Index", "Client");
                    //}
                    //else
                    //{
                    //    return RedirectToAction("ChangePassword", "Account");
                    //}

                }

            }

            ModelState.AddModelError("", "Invalid Login Attempt");
            return View(model);
        }



        //[ContentAuthorize(ControllerName = "Account", UserType = Models.Enum.UserType.Admin)]
        public IActionResult Register(string BuyId)
        {
            var model = new RegistrationViewModel();
            model.BuyId = BuyId;

            return View(model);
        }


        [HttpPost]

        public async Task<IActionResult> Register(RegistrationViewModel model)
        {
            var UserType = string.Empty;
            if (ModelState.IsValid)
            {


                var user = new ApplicationUser() { Id = Guid.NewGuid(), UserName = model.Email, Email = model.Email };
                var result = await userManager.CreateAsync(user, model.Password);


                if (result.Succeeded)
                {


                    user.Fullname = model.Fullname;
                    user.Contact = model.Contact;
                    user.Country = model.Country;
                    user.Organization = model.Organization;
                    user.WorkTitle = model.WorkTitle;
                    user.CreatedOn = DateTime.Now;
                    user.HasSubscribed = User.Identity.IsAuthenticated == true ? true : false;

                    user.IsPasswordUpdated = User.Identity.IsAuthenticated == true ? false : true;

                    _context.Users.Update(user);
                    await _context.SaveChangesAsync();

                    if (!User.Identity.IsAuthenticated)

                    {
                        await this.userManager.AddToRoleAsync(user, "Admin");
                        //this.emailService.Mail_Alert(model.Email, "Successfully Registered", "<h1>Welcome to Boxklip</h1><p>Your own video privacy solution<p>");
                        await signInManager.SignInAsync(user, isPersistent: false);
                        return RedirectToAction("Index", "Home", new { id = model.BuyId });
                    }


                }
                foreach (var item in result.Errors)
                {
                    ModelState.AddModelError("", item.Description);
                }
            }
            return View(model);
        }


       
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }
        private async Task<bool> IsValidUsernameAndPassword(string username, string password)

        {
            var user = await this.userManager.FindByEmailAsync(username);
            return await this.userManager.CheckPasswordAsync(user, password);
        }

        private async Task<dynamic> GenerateToken(string Username)
        {
            var usr = await this.userManager.FindByEmailAsync(Username);
            var roles = from ur in _context.UserRoles
                        join r in _context.Roles on ur.RoleId equals r.Id
                        where ur.UserId == usr.Id
                        select new { ur.UserId, ur.RoleId, r.Name };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,Username),
                new Claim(ClaimTypes.NameIdentifier,usr.Id.ToString()),
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Nbf,new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Exp,new DateTimeOffset(DateTime.Now.AddDays(1)).ToUnixTimeSeconds().ToString())
            };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            }

            var token = new JwtSecurityToken(
                new JwtHeader(
                    new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes("MyBoxklipAPISecretKeyIsThis")),
                        SecurityAlgorithms.HmacSha256)),
                new JwtPayload(claims));

            var output = new
            {
                Access_Token = new JwtSecurityTokenHandler().WriteToken(token),
                UserName = Username
            };

            return output;

        }
    }
}