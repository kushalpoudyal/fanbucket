﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using FanBucketAPI.Models;
using Microsoft.AspNetCore.Identity;
using FanBucketEntity.Models.UserDetails;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostCommentsController : ControllerBase
    {
        private readonly FbDbContext _context;
        private readonly PostMasterIRepository repo;
        private readonly UserManager<ApplicationUser> userManager;

        public PostCommentsController(FbDbContext context, PostMasterIRepository repo,
            UserManager<ApplicationUser> userManager)
        {

            _context = context;
            this.repo = repo;
            this.userManager = userManager;
        }



        [Route("GetCommentsByPost")]
        [HttpGet]
        public async Task<List<PostCommentViewModel>> GetCommentsByPost(Guid PostId)
        {
            var postComments = await repo.GetCommentByPost(PostId);

            return postComments;
        }

        // PUT: api/PostComments/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Route("EditPostComments")]
        [HttpPost]
        public async Task<JsonResponse> EditPostComments([FromBody] PutCommentViewModel postComments)
        {
            var response = new JsonResponse();
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var comments = await _context.PostComments.Where(x => x.Id == postComments.Id && x.CreatedById == CurrId).FirstOrDefaultAsync();
            if (comments == null)
            {
                response.success = false;
                response.ReturnMsg = "Comment Not Found";
            }
            comments.Comment = postComments.Comment;
            comments.ModifiedOn = DateTime.UtcNow;

            _context.Entry(comments).State = EntityState.Modified;


            try
            {
                await _context.SaveChangesAsync();
                response.success = true;
                response.ReturnMsg = "Your Coment Has been Edited";

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostCommentsExists(postComments.Id))
                {
                    response.success = false;
                    response.ReturnMsg = "Comment Not Found";
                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Could not save your comment at this time. Please try again later.";
                }
            }
            return response;

        }
       
        // POST: api/PostComments
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Route("PostPostComments")]
        [HttpPost]
        public async Task<ActionResult<JsonResponse>> PostPostComments([FromBody] CommentViewModel postComments)
        {
            var response = new JsonResponse();
            try
            {
                var entity = new PostComments();
                entity.Id = Guid.NewGuid();
                entity.PostId = postComments.PostId;
                entity.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active;
                entity.CreatedById = Guid.Parse(this.userManager.GetUserId(User));
                entity.CreatedOn = DateTime.UtcNow;
                entity.ModifiedOn = DateTime.UtcNow;
                entity.Comment = postComments.Comment;
                entity.Deleted_Status = false;

                _context.PostComments.Add(entity);

                var notifications = new AppNotifications();
                notifications.Id = Guid.NewGuid();
                notifications.ActivityId = entity.Id;
                notifications.PKId = entity.PostId;
                notifications.nType = FanBucketEntity.Model.ENUM.NotificationType.Commented;
                notifications.ActivityUserId = Guid.Parse(this.userManager.GetUserId(User));
                notifications.PostCreatorId = _context.PostMaster.Where(x => x.Id == entity.PostId).Select(x => x.CreatedById).FirstOrDefault();
                notifications.CreatedDate = DateTime.UtcNow;
                notifications.Viewed = false;

                _context.AppNotifications.Add(notifications);


                await _context.SaveChangesAsync();

                response.success = true;
                response.ReturnMsg = "Successfully Commented";
                response.JsonObj = await repo.GetCommentById(entity.Id);
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = ex.Message;
            }

            return response;

        }

        // DELETE: api/PostComments/5
        [Route("DeleteComment")]
        [HttpPost]
        public async Task<JsonResponse> DeletePostComments(Guid id)
        {
            var response = new JsonResponse();
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            var postComments = await _context.PostComments.Where(x => x.Id == id && x.CreatedById == CurrId).FirstOrDefaultAsync();
            if (postComments == null)
            {
                response.success = false;
                response.ReturnMsg = "Unable to delete the comment.";
                return response;
            }

            try
            {
                _context.PostComments.Remove(postComments);
                await _context.SaveChangesAsync();
                response.success = true;
                response.ReturnMsg = "Successfully Deleted the comment";
            }
            catch
            {
                response.success = false;
                response.ReturnMsg = "Unable to delete the comment.";
            }


            return response;
        }

        private bool PostCommentsExists(Guid id)
        {
            return _context.PostComments.Any(e => e.Id == id);
        }
    }
}
