﻿using FanBucketAPI.Helper;
using FanBucketAPI.Models;
using FanBucketAPI.ViewModel;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketEntity.Wrappers;
using FanBucketRepo.Interface;
using FanBucketRepo.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AdminController : ControllerBase
    {
        private readonly IAdminRepository adminRepository;
        private readonly IUserBadgesRepository userBadgesRepository;
        private readonly IBadgeRepository badgeRepository;
        private readonly FbDbContext _context;
        private readonly IUriService uriService;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IWebHostEnvironment hostingEnvironment;

        public AdminController(IAdminRepository adminRepository,
            IUserBadgesRepository userBadgesRepository,
            IBadgeRepository badgeRepository,
            FbDbContext context,
            IUriService uriService,
            UserManager<ApplicationUser> userManager
            )
        {
            this.adminRepository = adminRepository;
            this.userBadgesRepository = userBadgesRepository;
            this.badgeRepository = badgeRepository;
            _context = context;
            this.uriService = uriService;
            this.userManager = userManager;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAll([FromQuery] PaginationFilter filter, [FromQuery] string filterString = "")
        {
            var qUsers = _context.Users.AsQueryable();
            if (filterString != "")
            {
                qUsers = qUsers.Where(x => x.Email.ToLower().Contains(filterString.ToLower()) || x.UserName.ToLower().Contains(filterString.ToLower()));
            }
            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var index = validFilter.PageNumber;
            var qPageData = index > 1 ? qUsers.Skip((index - 1) * validFilter.PageSize).Take(validFilter.PageSize) : qUsers.Take(validFilter.PageSize);
            var pagedData = await qPageData
                .Select(x => new UserDetailsViewModel()
                {
                    Id = x.Id,
                    Fullname = x.Fullname,
                    Email = x.Email,
                    ProfileImgLocation = string.IsNullOrEmpty(x.ProfileImgLocation) ? "/Content/gallery/userimg.png" : x.ProfileImgLocation,
                    Username = x.UserName,
                    UserFollowed = _context.ApplicationUserFollowlist.Any(z => z.UserId == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                    TotalFollowers = _context.ApplicationUserFollowlist.Where(z => z.UserId == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                    TotalFollowing = _context.ApplicationUserFollowlist.Where(z => z.FollowedBy == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                    TotalSubscribers = _context.ApplicationUserFollowlist.Where(z => z.UserId == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                    Badges = _context.UserBadges.Where(x => x.UserId == x.Id).Include(c => c.BadgeInfo).Select(x => new UserBadgesViewModel()
                    {
                        Name = x.BadgeInfo.Name,
                        ImageUrl = x.BadgeInfo.ImageUrl
                    }).ToList(),//userBadgesRepository.GetBagesById(x.Id),
                }).ToListAsync();
            var totalRecords = await qUsers.CountAsync();
            var pagedReponse = PaginationHelper.CreatePagedReponse<UserDetailsViewModel>(pagedData, validFilter, totalRecords, uriService, route);
            return Ok(pagedReponse);
        }

        [Route("GetUserDetails")]
        [HttpGet]
        public async Task<ActionResult> GetUserDetails(Guid Id)
        {

            var qUsers = _context.Users.Where(x => x.Id == Id).AsQueryable();
            var qPaymentCreditUser = _context.PaymentCreditByUser.Where(x => x.UserId == Id).AsQueryable();

            var userData = await (from u in qUsers
                                  join p in qPaymentCreditUser
                                  on u.Id equals p.UserId

                                  select new UserDetailsViewModel()
                                  {
                                      Id = u.Id,
                                      Fullname = u.Fullname,
                                      Email = u.Email,
                                      ProfileImgLocation = string.IsNullOrEmpty(u.ProfileImgLocation) ? "/Content/gallery/userimg.png" : u.ProfileImgLocation,
                                      Username = u.UserName,
                                      Status = u.Status,
                                      UserFollowed = _context.ApplicationUserFollowlist.Any(z => z.UserId == u.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                      TotalFollowers = _context.ApplicationUserFollowlist.Where(z => z.UserId == u.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                      TotalFollowing = _context.ApplicationUserFollowlist.Where(z => z.FollowedBy == u.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                      TotalSubscribers = _context.ApplicationUserFollowlist.Where(z => z.UserId == u.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                      //Badges = _context.UserBadges.Where(z => z.UserId == u.Id).Include(c => c.BadgeInfo).Select(x => x.BadgeInfo.Name).ToArray(),
                                      Badges = _context.UserBadges.Where(x => x.UserId == Id).Include(c => c.BadgeInfo).Select(x => new UserBadgesViewModel()
                                      {
                                          Name = x.BadgeInfo.Name,
                                          ImageUrl = x.BadgeInfo.ImageUrl
                                      }).ToList(),//userBadgesRepository.GetBagesById(x.Id),
                                      KYC_Details = _context.ApplicationUserKYC.Where(x => x.UserId == u.Id).Select(x => new EditKycViewModel()
                                      {
                                          IdNO = x.IdNO,
                                          Country = x.Country,
                                          Gender = x.Gender,
                                          ContactNumber = x.ContactNumber,
                                          DOB = x.DOB.ToShortDateString(),
                                          Identification = x.Identification,
                                          IdIssuedFrom = x.IdIssuedFrom,
                                      }).FirstOrDefault(),
                                      BalanceInfo = _context.PaymentCreditByUser.Where(x => x.UserId == u.Id).Select(x =>
                                            new PaymentCreditByUserViewModel()
                                            {
                                                CreditSum = x.CreditSum,
                                                Currency = x.Currency,
                                                CurrentBalance = x.CurrentBalance,
                                                DebitSum = x.DebitSum,
                                                EarningPercentage = x.EarningPercentage,
                                            }
                                ).FirstOrDefault(),
                                      BlockTill = u.BlockTill
                                  }
                          ).FirstOrDefaultAsync();
            if (userData != null)
            {
                userData.ActiveReports = await adminRepository.ReportedUserWithDetail(userData.Id);
            }
            var pagedReponse = userData;
            return Ok(pagedReponse);

        }

        [Route("GetUserDetailsByUsername")]
        [HttpGet]
        public async Task<ActionResult> GetUserDetailsByUsername(string username)
        {

            var qUsers = _context.Users.Where(x => x.UserName == username).AsQueryable();

            var userData = await (from u in qUsers
                                  select new UserDetailsViewModel()
                                  {
                                      Id = u.Id,
                                      Fullname = u.Fullname,
                                      Email = u.Email,
                                      ProfileImgLocation = string.IsNullOrEmpty(u.ProfileImgLocation) ? "/Content/gallery/userimg.png" : u.ProfileImgLocation,
                                      Username = u.UserName,
                                      UserFollowed = _context.ApplicationUserFollowlist.Any(z => z.UserId == u.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                      TotalFollowers = _context.ApplicationUserFollowlist.Where(z => z.UserId == u.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                      TotalFollowing = _context.ApplicationUserFollowlist.Where(z => z.FollowedBy == u.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                      TotalSubscribers = _context.ApplicationUserFollowlist.Where(z => z.UserId == u.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                      Badges = _context.UserBadges.Where(x => x.UserId == u.Id).Include(c => c.BadgeInfo).Select(x => new UserBadgesViewModel()
                                      {
                                          Name = x.BadgeInfo.Name,
                                          ImageUrl = x.BadgeInfo.ImageUrl
                                      }).ToList(),//userBadgesRepository.GetBagesById(x.Id),
                                      KYC_Details = _context.ApplicationUserKYC.Where(x => x.UserId == u.Id).Select(x => new EditKycViewModel()
                                      {
                                          IdNO = x.IdNO,
                                          Country = x.Country,
                                          Gender = x.Gender,
                                          ContactNumber = x.ContactNumber,
                                          DOB = x.DOB.ToShortDateString(),
                                          Identification = x.Identification,
                                          IdIssuedFrom = x.IdIssuedFrom,
                                      }).FirstOrDefault(),
                                      BalanceInfo = _context.PaymentCreditByUser.Where(x => x.UserId == u.Id).Select(x =>
                                            new PaymentCreditByUserViewModel()
                                            {
                                                CreditSum = x.CreditSum,
                                                Currency = x.Currency,
                                                CurrentBalance = x.CurrentBalance,
                                                DebitSum = x.DebitSum,
                                                EarningPercentage = x.EarningPercentage,
                                            }
                                     ).FirstOrDefault()
                                  }
                          ).FirstOrDefaultAsync();

            var pagedReponse = userData;
            return Ok(pagedReponse);

        }
        [Route("GetDashboardInfo")]
        [HttpGet]
        public async Task<DashboardViewModel> GetDashboardInfo(int Days = 7)
        {
            var From = new DateTime();
            var To = new DateTime();
            if (Days == 0)
            {
                From = DateTime.Now;
                To = DateTime.Now;
            }
            else if (Days == 30)
            {
                From = DateTime.Now.AddMonths(-1);
                To = DateTime.Now;
            }
            else
            {
                From = DateTime.Now.AddDays(-7);
                To = DateTime.Now;
            }
            return await adminRepository.GetDashBoardValues(From, To);
        }

        [Route("GetUsers")]
        [HttpGet]
        public async Task<List<UserDetailsViewModel>> GetUsers()
        {
            return await adminRepository.GetUsers();
        }

        [Route("GetNewUsers")]
        [HttpGet]
        public async Task<List<UserDetailsViewModel>> GetNewUsers(int Days = 7)
        {
            return await adminRepository.GetNewUsers(Days);
        }

        [Route("GetReportedPosts")]
        [HttpGet]
        public async Task<List<PostReportedViewModel>> GetReportedPosts()
        {
            return await adminRepository.GetReportedPosts();
        }

        [Route("ReportedPosts")]
        [HttpGet]
        public async Task<List<PostReportSummaryViewModel>> ReportedPosts()
        {
            return await adminRepository.ReportedPosts();
        }

        [Route("ReportedPostDetail")]
        [HttpGet]
        public async Task<PostReportSummaryViewModel> ReportedPostDetail(Guid Id)
        {
            return await adminRepository.ReportedPostWithDetail(Id);
        }

        [Route("ReportedUsers")]
        [HttpGet]
        public async Task<List<UserReportSummaryViewModel>> ReportedUsers()
        {
            return await adminRepository.ReportedUsers();
        }

        [Route("ReportedUserDetail")]
        [HttpGet]
        public async Task<UserReportSummaryViewModel> ReportedUserDetail(Guid Id)
        {
            return await adminRepository.ReportedUserWithDetail(Id);
        }

        [Route("VerifyUserById")]
        [HttpPost]
        public async Task<bool> VerifyUserById([FromBody] Guid UserId)
        {
            var user = await userManager.FindByIdAsync(UserId.ToString());
            if (user == null)
            {
                throw new Exception("Invalid user Id " + UserId);
            }
            var badge = await badgeRepository.GetByCode(BadgeCodes.Verified.ToString());
            if (badge == null)
            {
                throw new Exception("Unable to retrieve badge with code: " + BadgeCodes.Verified.ToString());
            }
            var userBadge = new UserBadges()
            {
                BadgeId = badge.Id,
                UserId = UserId,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedById = Guid.Parse(this.userManager.GetUserId(User)),
                Deleted_Status = false,
                Record_Status = Record_Status.Active,
                Id = Guid.NewGuid(),
            };
            await userBadgesRepository.AddAsync(userBadge);
            return true;
        }

        [Route("SetUserCommision")]
        [HttpPost]
        public async Task<JsonResponse> SetUserCommision(string UserId, string EarningCumm)
        {
            var response = new JsonResponse();
            try
            {
                var user = await userManager.FindByIdAsync(UserId);
                if (user == null)
                {
                    response.success = false;
                    response.ReturnMsg = "Invalid User ";
                }
                var data = _context.PaymentCreditByUser.Where(x => x.UserId == Guid.Parse(UserId)).FirstOrDefault();
                data.EarningPercentage = Convert.ToDecimal(EarningCumm);

                await _context.SaveChangesAsync();
                response.success = true;
                response.ReturnMsg = "Successfully Placed the Commision";
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Error " + ex.ToString();
            }
            return response;
        }

        [Route("Commision/Review")]
        [HttpPost]
        public async Task<JsonResponse> CommisionReview(CommissionReviewViewModel commissionReviewViewModel)
        {
            var response = new JsonResponse();
            try
            {
                var user = await userManager.FindByIdAsync(commissionReviewViewModel.UserId.ToString());
                if (user == null)
                {
                    response.success = false;
                    response.ReturnMsg = "Invalid User ";
                }

                await adminRepository.UserCommissionReview(commissionReviewViewModel, Guid.Parse(userManager.GetUserId(User)));//return true
                response.success = true;
                response.ReturnMsg = "Successfully Placed the Commision";
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Error " + ex.ToString();
            }
            return response;
        }

        [Route("ModerateUser")]
        [HttpPost]
        public async Task<JsonResponse> ModerateUser(ReportActionViewModel reportActionViewModel)
        {
            var response = new JsonResponse();
            try
            {
                var user = await userManager.FindByIdAsync(reportActionViewModel.UserId.ToString());
                if (user == null)
                {
                    response.success = false;
                    response.ReturnMsg = "Invalid User ";
                }

                await adminRepository.UserReportAction(reportActionViewModel, Guid.Parse(userManager.GetUserId(User)));//return true
                response.success = true;
                response.ReturnMsg = "Successfully moderated user.";
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Error " + ex.ToString();
            }
            return response;
        }

        [Route("PostModeration")]
        [HttpPost]
        public async Task<JsonResponse> PostModeration(PostReviewViewModel PostReviewViewModel)
        {
            var response = new JsonResponse();
            try
            {
                await adminRepository.PostReview(PostReviewViewModel, Guid.Parse(userManager.GetUserId(User)));//return true
                response.success = true;
                response.ReturnMsg = "Successfully reviewed successfully.";
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Error " + ex.ToString();
            }
            return response;
        }

        // POST: api/Admin
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Admin/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}