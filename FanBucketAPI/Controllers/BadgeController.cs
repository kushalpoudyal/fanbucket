﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using FanBucketAPI.Models;
using Microsoft.AspNetCore.Identity;
using FanBucketEntity.Models.UserDetails;
using AutoMapper;
using FanBucketEntity.Model;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BadgeController : ControllerBase
    {
        private readonly FbDbContext _context;
        private readonly IBadgeRepository repo;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> userManager;

        public BadgeController(FbDbContext context,
            IMapper mapper,
            IBadgeRepository repo,
            UserManager<ApplicationUser> userManager)
        {

            _context = context;
            this.repo = repo;
            this.userManager = userManager;
            this._mapper = mapper;
        }

        // GET: api/Badge
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BadgeViewModel>>> GetBadge()
        {
            var badgeList = await repo.GetAll().ToListAsync();
            var badgeInfoList = _mapper.Map<List<BadgeViewModel>>(badgeList);
            return badgeInfoList;
        }

        // GET: api/Badge/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BadgeViewModel>> GetBadge(Guid id)
        {
            var Badge = await repo.GetById(id);

            if (Badge == null)
            {
                return NotFound();
            }
            var badgetDTO = _mapper.Map<BadgeViewModel>(Badge);

            return badgetDTO;
        }


        // PUT: api/Badge/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBadge(Guid id, BadgeViewModel Badge)
        {
            if (id != Badge.Id)
            {
                return BadRequest();
            }
            var oldData = await repo.GetById(id);
            if (oldData == null)
            {
                return NotFound();
            }
            oldData.Name = Badge.Name;
            oldData.TriggeredOn = Badge.TriggeredOn;
            oldData.TriggerType = Badge.TriggerType;
            oldData.Description = Badge.Description;
            oldData.Code = Badge.Code;
            oldData.ModifiedOn = DateTime.Now;
            _context.Entry(oldData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BadgeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Badge
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<JsonResponse>> PostBadge([FromBody]BadgeViewModel Badge)
        {
            var response = new JsonResponse();
            try
            {
                var entity = new BadgeInfo();
                entity.Id = Guid.NewGuid();
                entity.Name = Badge.Name;
                entity.TriggeredOn = Badge.TriggeredOn;
                entity.TriggerType = Badge.TriggerType;
                entity.Description = Badge.Description;
                entity.Code = Badge.Code;
                entity.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active;
                entity.CreatedById = Guid.Parse(this.userManager.GetUserId(User));
                entity.CreatedOn = DateTime.Now;
                entity.ModifiedOn = DateTime.Now;
                entity.Deleted_Status = false;

                _context.BadgeInfo.Add(entity);

                await _context.SaveChangesAsync();

                response.success = true;
                response.ReturnMsg = "Successfully Commented";
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = ex.Message;
            }

            return response;

        }

        // DELETE: api/Badge/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<BadgeViewModel>> DeleteBadge(Guid id)
        {
            var Badge = await repo.GetById(id);
            if (Badge == null)
            {
                return NotFound();
            }
            var badgeDto = _mapper.Map<BadgeViewModel>(Badge);
            _context.BadgeInfo.Remove(Badge);
            await _context.SaveChangesAsync();

            return badgeDto;
        }

        private bool BadgeExists(Guid id)
        {
            return _context.BadgeInfo.Any(e => e.Id == id);
        }
    }
}
