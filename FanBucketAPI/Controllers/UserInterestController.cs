﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using FanBucketAPI.Models;
using Microsoft.AspNetCore.Identity;
using FanBucketEntity.Models.UserDetails;
using AutoMapper;
using FanBucketEntity.Model;
using FanBucketEntity.Model.UserDetails;
using System.IO;
using FanBucketAPI.Helper;
using System.Security.Cryptography.X509Certificates;
using FanBucketEntity.Model.ENUM;
using Microsoft.VisualStudio.Web.CodeGeneration;
using Microsoft.Extensions.Configuration;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserInterestController : ControllerBase
    {
        private readonly FbDbContext _context;
        private readonly IUserInterestsRepository repo;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> userManager;
        private string FilePath { get; }
        public IConfiguration configuration;

        public UserInterestController(FbDbContext context,
            IMapper mapper,
            IUserInterestsRepository repo,
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration)
        {

            _context = context;
            this.repo = repo;
            this.userManager = userManager;
            this.configuration = configuration;
            this._mapper = mapper;
            //FilePath = @"C:\inetpub\wwwroot\wwwroot"; //live
            //FilePath = @"C:\inetpub\wwwroot\fanbucket\FanBucketUI\wwwroot";
            FilePath = this.configuration.GetValue<string>("PostFilePath");

        }
        [Route("EditProfilePersonal")]
        [HttpPost]
        public async Task<JsonResponse> EditProfilePersonal(EditPersonalViewModel model)
        {
            var response = new JsonResponse();
            var UserId = this.userManager.GetUserId(User);
            try
            {
                var User = await this.userManager.FindByIdAsync(UserId);
                if (model.Username != User.UserName)
                {

                    if (User.LastUserNameModifiedDate.HasValue
                   && User.LastUserNameModifiedDate.Value.Year == DateTime.UtcNow.Year
                    )
                    {
                        if (User.UserNameModifiedTimes < 2)
                        {
                            User.UserName = model.Username;
                            User.NormalizedUserName = model.Username.ToUpper();
                            User.UserNameModifiedTimes = User.UserNameModifiedTimes + 1;//modiifed times in this year
                            User.LastUserNameModifiedDate = DateTime.UtcNow;
                        }
                        else
                        {
                            //dont edit
                        }

                    }
                    else
                    {
                        User.UserName = model.Username;
                        User.NormalizedUserName = model.Username.ToUpper();

                        User.UserNameModifiedTimes = 1;//modiifed times in this year
                        User.LastUserNameModifiedDate = DateTime.UtcNow;

                    }
                }
                var UserDetails = _context.ApplicationUserDetails.Where(x => x.UserId == Guid.Parse(UserId)).FirstOrDefault();
                if (UserDetails == null)
                {
                    var NewUserDetails = new ApplicationUserDetails();
                    NewUserDetails.UserId = Guid.Parse(UserId);
                    NewUserDetails.Profession = model.Profession;
                    NewUserDetails.ShortBio = model.ShortBio;
                    NewUserDetails.Location = model.Location;
                    NewUserDetails.LatitudeCoordinate = "";
                    NewUserDetails.LongitudeCoordinate = "";

                    await _context.AddAsync(NewUserDetails);
                }
                else
                {
                    UserDetails.Profession = model.Profession;
                    UserDetails.ShortBio = model.ShortBio;
                    UserDetails.Location = model.Location;
                    UserDetails.LatitudeCoordinate = "";
                    UserDetails.LongitudeCoordinate = "";

                   

                }
                if (!string.IsNullOrEmpty(model.ProfileImgLocation))
                {
                    User.ProfileImgLocation = model.ProfileImgLocation;
                }
                User.Fullname = model.Fullname;
                await _context.SaveChangesAsync();

                response.success = true;
                response.ReturnMsg = "Succesfully Updated User Details";

            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = "Error " + ex.Message.ToString();
            }

            return response;

        }
        [Route("EditPrivacyProfile")]
        [HttpPost]
        public async Task<JsonResponse> EditPrivacyProfile(EditProfilePrivacyViewModel model)
        {
            var response = new JsonResponse();
            var UserId = this.userManager.GetUserId(User);
            try
            {
                var Users = await this.userManager.FindByIdAsync(UserId);

                Users.AccountType = Enum.TryParse(model.AccountType, out AccountType post) ? (AccountType)Enum.Parse(typeof(AccountType), model.AccountType, true) : 0;
                Users.NSFW = Convert.ToBoolean(model.NSFW);
                var oldSubscriptioonInfo = _context.ApplicationUserSubscriptionInfo.Where(x => x.UserId == Guid.Parse(UserId) && x.Record_Status==Record_Status.Active).ToList();

                var oldSubscriptioonInfoIds = oldSubscriptioonInfo.Select(x => x.Id.ToString()).ToList();
                var newIds = model.SubscriptionType.Select(x => x.Id).ToList();


               var removedList= oldSubscriptioonInfo.Where(x => !newIds.Contains(x.Id.ToString())).ToList();
                foreach (var data in removedList)
                {
                    data.Record_Status = Record_Status.Inactive;
                    _context.ApplicationUserSubscriptionInfo.Update(data);
                }


                var newSubscription=model.SubscriptionType.Where(x=>!oldSubscriptioonInfoIds.Contains(x.Id)).ToList();

                foreach (var subData in newSubscription)
                {

                    bool isNumerical = int.TryParse(subData.Tenure, out int Tenure);
                    if (!isNumerical || Tenure == -1)//skip if not number 
                    {
                        continue;
                    }
                    var subs = new ApplicationUserSubscriptionINfo();
                    subs.Id = Guid.NewGuid();
                    subs.SubscriptionAmount = Convert.ToDecimal(subData.SubscriptionAmount);
                    subs.Tenure = Tenure;
                    subs.UserId = Guid.Parse(UserId);
                    subs.CreatedOn = DateTime.Now;
                    subs.Record_Status = Record_Status.Active;

                    _context.ApplicationUserSubscriptionInfo.Add(subs);
                }

                var existingSubscription = model.SubscriptionType.Where(x => oldSubscriptioonInfoIds.Contains(x.Id)).ToList();
                foreach (var subData in existingSubscription)
                {
                    var subs = oldSubscriptioonInfo.FirstOrDefault(x => x.Id == Guid.Parse(subData.Id));
                    bool isNumerical = int.TryParse(subData.Tenure, out int Tenure);
                    if (!isNumerical || Tenure == -1)//skip if not number 
                    {
                        continue;
                    }
                    subs.SubscriptionAmount = Convert.ToDecimal(subData.SubscriptionAmount);
                    subs.Tenure = Tenure;

                    _context.ApplicationUserSubscriptionInfo.Update(subs);
                }



                await _context.SaveChangesAsync();
                response.JsonObj =  _context.ApplicationUserSubscriptionInfo.Where(x => x.UserId == Guid.Parse(UserId) && x.Record_Status == Record_Status.Active).Select(x=>new
                {
                    Id=x.Id,
                    Tenure=x.Tenure.ToString(),
                    SubscriptionAmount=x.SubscriptionAmount.ToString()
                }).ToList();

                response.success = true;
                response.ReturnMsg = "Successfully Updated Privacy details.";

            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = "Error " + ex.Message.ToString();
            }

            return response;

        }
        [Route("EditKYCDetails")]
        [HttpPost]
        public async Task<JsonResponse> EditKYCDetails(EditKycViewModel model)
        {
            var response = new JsonResponse();
            var UserId = this.userManager.GetUserId(User);
            try
            {
                var data = _context.ApplicationUserKYC.Where(x => x.UserId == Guid.Parse(UserId)).FirstOrDefault();
                if (data == null)
                {
                    var kyc = new ApplicationUserKYC();
                    kyc.UserId = Guid.Parse(UserId);
                    kyc.Identification = model.Identification;
                    kyc.IdNO = model.IdNO;
                    kyc.IdIssuedFrom = model.IdIssuedFrom;
                    kyc.ContactNumber = model.ContactNumber;
                    kyc.DOB = Convert.ToDateTime(model.DOB);
                    kyc.Country = model.Country;
                    kyc.Gender = model.Gender;
                    kyc.CreatedOn = DateTime.Now;
                    kyc.ModifiedOn = DateTime.Now;

                    _context.ApplicationUserKYC.Add(kyc);

                }
                else
                {
                    data.Identification = model.Identification;
                    data.IdNO = model.IdNO;
                    data.IdIssuedFrom = model.IdIssuedFrom;
                    data.ContactNumber = model.ContactNumber;
                    data.Country = model.Country;
                    data.Gender = model.Gender;
                    data.DOB = Convert.ToDateTime(model.DOB);
                    data.ModifiedOn = DateTime.Now;

                }
                if (model.CardNumber != null)
                {
                    var dataCard = _context.CustomerPaymentDetails.Where(x => x.UserId == Guid.Parse(UserId)).FirstOrDefault();
                    if (dataCard == null)
                    {
                        var card = new CustomerPaymentDetails()
                        {
                            Id = Guid.NewGuid(),
                            CardName = model.CardName,
                            CardNumber = model.CardNumber,
                            CreatedOn = DateTime.Now,
                            CVV = model.CVV,
                            Expiry = model.Expiry,
                            UserId = Guid.Parse(UserId)
                        };
                        _context.CustomerPaymentDetails.Add(card);
                    }
                    else
                    {
                        dataCard.CardName = model.CardName;
                        dataCard.CardNumber = model.CardNumber;
                        dataCard.CVV = model.CVV;
                        dataCard.Expiry = model.Expiry;
                    }
                }

                await _context.SaveChangesAsync();
                response.success = true;
                response.ReturnMsg = "Successfully Updated KYC details.";

            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = "Error " + ex.Message.ToString();
            }
            return response;
        }
        [Route("PropicUpload")]
        [HttpPost]
        public async Task<JsonResponse> PropicUpload(List<IFormFile> files)
        {
            var response = new JsonResponse();
            var UserId = this.userManager.GetUserId(User);
            try
            {
                foreach (IFormFile source in files)
                {
                    var fileName = UserId + Path.GetExtension(source.FileName);
                    double FileLength = source.Length;
                    if (!Directory.Exists(Path.Combine(FilePath, "ProfilePicture")))
                    {
                        Directory.CreateDirectory(Path.Combine(FilePath, "ProfilePicture"));
                    }
                    string VideoFolder = Path.Combine(FilePath, "ProfilePicture", fileName);
                    if (System.IO.File.Exists(VideoFolder))
                    {
                        System.IO.File.Delete(VideoFolder);
                    }
                    await CreateFile.UploadToServer(source, VideoFolder);
                    var returnPath = Path.Combine("/ProfilePicture/", fileName);
                    var user = await this.userManager.FindByIdAsync(UserId);
                    user.ProfileImgLocation = returnPath;
                    await this.userManager.UpdateAsync(user);

                    response.success = true;
                    response.ReturnMsg = "Successfully Uploaded User's Profile Picture";
                    response.returnId = UserId;
                    response.returnPath = returnPath;
                }
            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = "Error " + ex.Message.ToString();
            }

            return response;

        }
        [Route("CoverpicUpload")]
        [HttpPost]
        public async Task<JsonResponse> CoverpicUpload(List<IFormFile> files)
        {
            var response = new JsonResponse();
            var UserId = this.userManager.GetUserId(User);
            try
            {
                foreach (IFormFile source in files)
                {
                    var fileName = UserId + Path.GetExtension(source.FileName);
                    double FileLength = source.Length;
                    if (!Directory.Exists(Path.Combine(FilePath, "CoverPicture")))
                    {
                        Directory.CreateDirectory(Path.Combine(FilePath, "CoverPicture"));
                    }
                    string VideoFolder = Path.Combine(FilePath, "CoverPicture", fileName);
                    if (System.IO.File.Exists(VideoFolder))
                    {
                        System.IO.File.Delete(VideoFolder);
                    }
                    await CreateFile.UploadToServer(source, VideoFolder);
                    var returnPath = Path.Combine("/CoverPicture/", fileName);
                    var user = await this.userManager.FindByIdAsync(UserId);
                    user.CoverImgLocation = returnPath;
                    await this.userManager.UpdateAsync(user);

                    response.success = true;
                    response.ReturnMsg = "Successfully Uploaded User's Profile Picture";
                    response.returnId = UserId;
                    response.returnPath = returnPath;
                }
            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = "Error " + ex.Message.ToString();
            }

            return response;

        }
        // GET: api/Interest
        [Route("KYCExists")]
        [HttpGet]
        public async Task<bool> KYCExists()
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            return await _context.ApplicationUserKYC.AnyAsync(x => x.UserId == CurrId);
        }
        // GET: api/Interest
        [HttpPost]
        public async Task<ActionResult<JsonResponse>> PostUserInterest([FromBody] Guid[] InterestIds)
        {
            var response = new JsonResponse();
            try
            {
                var interestList = new List<UserInterests>();
                foreach (var item in InterestIds)
                {
                    var entity = new UserInterests();
                    entity.Id = Guid.NewGuid();
                    entity.InterestId = item;
                    entity.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active;
                    entity.UserId = Guid.Parse(this.userManager.GetUserId(User));
                    entity.CreatedOn = DateTime.Now;
                    entity.ModifiedOn = DateTime.Now;
                    entity.Deleted_Status = false;
                    interestList.Add(entity);
                }
                await repo.AddInterests(interestList);

                response.success = true;
                response.ReturnMsg = "Successfully Interest Added";
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = ex.Message;
            }

            return response;

        }

        // GET: api/Interest
        [Route("UserMailSubscribe")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<JsonResponse>> UserMailSubscribe(string Email)
        {
            var response = new JsonResponse();
            try
            {
                if (_context.UserInfo.Any(x => x.Email.ToLower() == Email.ToLower()))
                {
                    response.success = true;
                    response.ReturnMsg = "Hey looks like you've already subscribed, Please wait for your email regarding the Inaugration  of the website.";
                    return response;
                }
                if (!IsValidEmail(Email))
                {
                    response.success = false;
                    response.ReturnMsg = "Email Validation Error.";
                    return response;
                }
                var entity = new UserInfo();
                entity.Id = Guid.NewGuid();
                entity.Email = Email;
                entity.CreatedOn = DateTime.Now;


                _context.UserInfo.Add(entity);

                await _context.SaveChangesAsync();

                response.success = true;
                response.ReturnMsg = "Thank you for subscribing with us! You Will receive a notification regarding the Inauguration of the website. Stick with us for creating contents sharing them and earning on the way! ";
            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = ex.Message;
            }

            return response;

        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }



        private bool InterestExists(Guid id)
        {
            return _context.Interest.Any(e => e.Id == id);
        }
    }
}
