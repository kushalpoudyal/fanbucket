﻿using FanBucketAPI.Helper;
using FanBucketAPI.Models;
using FanBucketAPI.Options;
using FanBucketAPI.Services;
using FanBucketAPI.ViewModel;
using FanBucketEntity.DBContext;
using FanBucketEntity.Enum;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using FanBucketUI.ResponseModel;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using OpenGraphNet;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketAPI.Controllers
{
    


    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IPasswordValidator<ApplicationUser> passwordValidator;
        private readonly FbDbContext _context;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IUserRepository userRepository;
        private readonly IPaymentRepository payment;
        private readonly IBadgeService badgeService;
        private readonly IEmailService emailService;
        private readonly IBackgroundJobClient _backgroundJobClient;
        private readonly IEmailSender _emailSender;

        public AuthenticationController(UserManager<ApplicationUser> userManager,
            FbDbContext context,
            SignInManager<ApplicationUser> signInManager,
            IUserRepository userRepository,
            IPaymentRepository payment,
            IBadgeService badgeService,
            IPasswordValidator<ApplicationUser> passwordValidator,
            IEmailService emailService,
            IBackgroundJobClient backgroundJobClient,
            IEmailSender emailSender)
        {
            this.userManager = userManager;
            this._context = context;
            this.signInManager = signInManager;
            this.userRepository = userRepository;
            this.payment = payment;
            this.badgeService = badgeService;
            this.passwordValidator = passwordValidator;
            this.emailService = emailService;
            this._backgroundJobClient = backgroundJobClient;
            _emailSender = emailSender;
        }


        [Route("ipaddress")]
        [HttpGet]
        public async Task<IActionResult> IpAddress()
        {
            var ipaddress = HttpContext.GetIpAddress();
            return Ok(ipaddress);
        }

        [Route("GetIpInfo")]
        [HttpGet]
        public async Task<IActionResult> GetIpInfo()
        {
            IpInfo ipInfo = await GetIpInfoFromIpAddress();
            return base.Ok(ipInfo);

        }

        [Route("token")]
        [HttpPost]
        public async Task<IActionResult> CreateToken([FromBody] UserCred userCred)
        {
            try
            {

                var localIp = HttpContext?.Connection?.LocalIpAddress.ToString();
                var remoteIpAddress = HttpContext?.Connection?.RemoteIpAddress.ToString();

                if (await IsValidUsernameAndPassword(userCred.username, userCred.password))
                {
                    TokenModel tokenModel = await GenerateToken(userCred.username);

                    return base.Ok(tokenModel);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException.ToString());
            }
        }

        [Route("ScrapeData")]
        [HttpPost]
        public async Task<IActionResult> ScrapeData(string Url)
        {
            try
            {
                OpenGraph graph = await OpenGraph.ParseUrlAsync(Url, "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0");
                string desc = null;
                string sitename = null;
                if (graph.Metadata.ContainsKey("og:description"))
                {
                    desc = graph.Metadata["og:description"][0].Value;
                }
                if (graph.Metadata.ContainsKey("og:site_name"))
                {
                    sitename = graph.Metadata["og:site_name"][0].Value;
                }
                var scrapedata = new
                {
                    SiteName = sitename,
                    Title = graph.Title,
                    Image = graph.Image,
                    Description = desc,
                    Url = graph.Url,
                    Type = graph.Type
                };
                return Ok(scrapedata);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException.ToString());
            }
        }

        [Route("CheckToken")]
        [HttpPost]
        public async Task<bool> CheckToken([FromBody] UserCred userCred)
        {
            try
            {
                if (await IsValidUsernameAndPassword(userCred.username, userCred.password))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [Route("CheckPasswordComplexity")]
        [HttpPost]
        public async Task<IdentityResult> CheckPasswordComplexity(string password)
        {
            try
            {
                var Userr = new ApplicationUser();
                var result = await this.passwordValidator.ValidateAsync(userManager, Userr, password);
                return result;
            }
            catch
            {
                return null;
            }
        }

        private async Task<bool> IsValidUsernameAndPassword(string username, string password)
        {
            var user = await this.userManager.FindByEmailAsync(username);
            if (user == null)
            {
                user = await this.userManager.FindByNameAsync(username);
            }
            return await this.userManager.CheckPasswordAsync(user, password);
        }

        [Route("UsernameCheck")]
        [HttpPost]
        public async Task<bool> UsernameCheck(string username)
        {
            var user = await this.userManager.FindByNameAsync(username);
            if (user == null)
            {
                return false;
            }
            return true;
        }

        [Route("EmailCheck")]
        [HttpPost]
        public async Task<bool> EmailCheck(string Email)
        {
            var user = await this.userManager.FindByEmailAsync(Email);
            if (user == null)
            {
                return false;
            }
            return true;
        }

        private async Task<TokenModel> GenerateToken(string Username)
        {
            var usr = await this.userManager.FindByEmailAsync(Username);
            if (usr == null)
            {
                usr = await this.userManager.FindByNameAsync(Username);
            }

            if (usr.Status == Record_Status.Reported)
            {
                if (usr.BlockTill.HasValue && usr.BlockTill >= DateTime.UtcNow)
                {
                    throw new Exception("User has been temporary blocked by admin till " + usr.BlockTill.Value.ToLongDateString() + " " + usr.BlockTill.Value.ToLongTimeString());
                }
                else
                {
                    throw new Exception("User has been blocked by admin");
                }
            }

            var roles = from ur in _context.UserRoles
                        join r in _context.Roles on ur.RoleId equals r.Id
                        where ur.UserId == usr.Id
                        select new { ur.UserId, ur.RoleId, r.Name };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,Username),
                new Claim(ClaimTypes.Actor,usr.Fullname),
                new Claim(ClaimTypes.Email,usr.Email),
                new Claim("ProfileImage", string.IsNullOrEmpty(usr.ProfileImgLocation)?"/Content/gallery/userimg.png":usr.ProfileImgLocation),
                new Claim(ClaimTypes.NameIdentifier,usr.Id.ToString()),
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Nbf,new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp,new DateTimeOffset(DateTime.Now.AddDays(1)).ToUnixTimeSeconds().ToString())
            };

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            }

            var token = new JwtSecurityToken(
                new JwtHeader(
                    new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes("MyFanBucketAPISecretKeyIsThis")),
                        SecurityAlgorithms.HmacSha256)),
                new JwtPayload(claims));

            var output = new TokenModel();

            output.access_token = new JwtSecurityTokenHandler().WriteToken(token);
            output.username = usr.UserName;
            output.FullName = usr.Fullname;
            output.ProfileImgLocation = usr.ProfileImgLocation;
            output.UserId = usr.Id;
            output.Role = roles.FirstOrDefault().Name;
            output.CoverImgLocation = usr.CoverImgLocation;
            if (usr.HasLoginAlert)
            {
                await SendLoginNotification(usr);

            }

            return output;
        }


        [Route("GetUserBySearch")]
        [HttpGet]
        [Authorize]
        public async Task<List<SearchUserViewModel>> GetUserBySearch([MinLength(3)] string StringText)
        {
            if (StringText.StartsWith('@'))
            {
                var strRep = StringText.Replace("@", "");
                var user = await _context.Users.Where(x => x.UserName == strRep && x.AccountType != FanBucketEntity.Model.ENUM.AccountType.Superadmin).ToListAsync();

                var userdetails = user.Select(x => new SearchUserViewModel()
                {
                    Id = x.Id,
                    Fullname = x.Fullname,
                    Email = x.UserName,
                    Username = x.UserName,
                    ProfileImgLocation = string.IsNullOrEmpty(x.ProfileImgLocation) ? "/Content/gallery/userimg.png" : x.ProfileImgLocation,
                }).ToList();
                return userdetails;
            }
            else
            {
                var user = await _context.Users.Where(x => (x.Email.Contains(StringText) || x.UserName.Contains(StringText) || x.Fullname.Contains(StringText)) && x.AccountType != FanBucketEntity.Model.ENUM.AccountType.Superadmin).ToListAsync();
                var userdetails = user.Select(x => new SearchUserViewModel()
                {
                    Id = x.Id,
                    Fullname = x.Fullname,
                    Email = x.UserName,
                    Username = x.UserName,
                    ProfileImgLocation = string.IsNullOrEmpty(x.ProfileImgLocation) ? "/Content/gallery/userimg.png" : x.ProfileImgLocation,
                }).ToList();
                return userdetails;
            }
        }

        [Route("GetUserById")]
        [HttpGet]
        public async Task<UserDetailsViewModel> GetUserById(string Username, Guid NotifyId = default)
        {
            try
            {
                var UserId = (await this.userManager.FindByNameAsync(Username)).Id;
                var userdetails = new UserDetailsViewModel();
                if (User.Identity.IsAuthenticated)
                {
                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                    userdetails = await this.userRepository.GetUserById(UserId, CurrId);
                    if (NotifyId != default)
                    {
                        var activenotification = await _context.AppNotifications.FirstOrDefaultAsync(x => x.PostCreatorId == CurrId && !x.Viewed && x.Id == NotifyId);
                        if (activenotification != null)
                        {
                            activenotification.Viewed = true;
                            await _context.SaveChangesAsync();
                        }
                    }
                }
                else
                {
                    userdetails = await this.userRepository.GetUserById(UserId);
                }
                userdetails.HasPinSet = await _context.PaymentUPIN.AnyAsync(x => x.UserId == UserId);
                return userdetails;
            }
            catch
            {
                return null;
            }
        }

        [Route("ViewAsOtherUser")]
        [HttpGet]
        public async Task<UserDetailsViewModel> ViewAsOtherUser()
        {
            try
            {
                var userdetails = new UserDetailsViewModel();
                if (User.Identity.IsAuthenticated)
                {
                    var CurrId = Guid.Parse(this.userManager.GetUserId(User));
                    userdetails = await this.userRepository.GetUserById(CurrId);
                }

                return userdetails;
            }
            catch
            {
                return null;
            }
        }

        [Route("RegisterClient")]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegistrationViewModel model)
        {
            var responseF = new JsonResponse();
            await RegisterUser(model, responseF);

            if (responseF.success)
            {
                return Ok(responseF);
            }

            return BadRequest(responseF);


        }

        private async Task RegisterUser(RegistrationViewModel model, JsonResponse responseF)
        {
            var ExistingUserWithUserName = await userManager.FindByNameAsync(model.Username);
            if (ExistingUserWithUserName != null)
            {
                responseF.success = false;
                responseF.ReturnMsg = "Username is already in use.";
                return;
            }
            var ExistingUser = await userManager.FindByEmailAsync(model.Email);
            if (ExistingUser != null)
            {
                responseF.success = false;
                responseF.ReturnMsg = "User with Email Already Exists";
                return;
            }
            var user = new ApplicationUser()
            {
                Id = Guid.NewGuid(),
                UserName = model.Username,
                Email = model.Email,
                Fullname = model.Fullname,
                CreatedOn = DateTime.Now,
                EmailNotification = true,
                HasLoginAlert = true,
                HasInformationUpdateAlert = true,
                HasMessageAlert = true,
                HasTipAlert = true,
                HasSubscriptionAlert = true,
                HasFollowerAlert = true,
                HasShareAlert = true,
                HasMentionAlert = true,
                HasBookmarkAlert = true,
                HasLoveReactAlert = true,
                HasSubscribed = false,
                IsPasswordUpdated = true,
            };
            var result = await userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {

                user.HasSubscribed = User.Identity.IsAuthenticated == true ? true : false;

                user.IsPasswordUpdated = User.Identity.IsAuthenticated == true ? false : true;
                _context.Update(user);

                if (model.Latitude.HasValue && model.Longitude.HasValue)
                {
                    _context.ApplicationUserDetails.Add(
                        new ApplicationUserDetails()
                        {
                            UserId = user.Id,
                            LatitudeCoordinate = model.Latitude?.ToString(),
                            LongitudeCoordinate = model.Longitude?.ToString(),
                            Location = model.Latitude.ToString() + " , " + model.Longitude.ToString()
                        });
                }

                await _context.SaveChangesAsync();
                //add new user badge to user
                await badgeService.AddBadgeToUserByBadgeCode(user.Id, BadgeCodes.NewUser.ToString());

                if (!User.Identity.IsAuthenticated)
                {
                    await this.userManager.AddToRoleAsync(user, "Client");
                    var loadBal = new PaymentValidationModel();
                    loadBal.CurrId = user.Id;
                    loadBal.PayingAmount = 500;

                    await payment.LoadFund(loadBal, true);
                    try
                    {
                        var emailUserModel = new EmailUserModel()
                        {
                            Email = model.Email,
                            FullName = model.Fullname,
                            UserName = model.Username,
                            CallbackUrl = "",
                            Subject = "Successfully Registered",
                            Type = EmailTemplate.CreateNewUser
                        };
                        await _emailSender.SendEmailAsync(emailUserModel);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
                responseF.success = true;
                responseF.ReturnMsg = "User Registered! Please try Loging In";
                //return Ok(responseF);
            }
            else
            {
                var sb = new StringBuilder();
                foreach (var item in result.Errors)
                {
                    sb.Append(item.Description);
                }
                responseF.success = false;
                responseF.ReturnMsg = "Error while registering. " + sb.ToString();
            }
        }

        [Route("SignUpQueue")]
        [HttpPost]
        public async Task<IActionResult> SignUpQueue([FromBody] SignUpQueueViewModel model)
        {
            var responseF = new JsonResponse();
            try
            {
                var registrationViewModel = new RegistrationViewModel()
                {
                    Password = model.Password,
                    Email = model.EmailAddress,
                    ConfirmPassword = model.Password,
                    Fullname = model.FirstName,
                    Latitude = model.Latitude,
                    Longitude = model.Longitude,
                    Username = model.Username,
                };

                await RegisterUser(registrationViewModel, responseF);

                if (!responseF.success)
                {
                    return BadRequest(responseF);
                }

                //create user with password
                var SignUpQueue = new SignUpQueue()
                {
                    ContentStyle = model.ContentStyle,
                    CSMReason = model.CSMReason,
                    EmailAddress = model.EmailAddress,
                    FirstName = model.FirstName,
                    FollowersAndPlatfrom = model.FollowersAndPlatfrom,
                    LastName = model.LastName,
                    SocialMediaLinks = model.SocialMediaLinks,
                    Status = Record_Status.Active,
                    Deleted = false,
                    CreatedOn = DateTime.UtcNow,
                    ModifiedOn = DateTime.UtcNow
                };
                await _context.SignUpQueue.AddAsync(SignUpQueue);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                responseF.success = false;
                responseF.ReturnMsg = "Error while applying for queue";
                return BadRequest(responseF);
            }
            responseF.success = true;
            responseF.ReturnMsg = "User Registered! Please try Loging In";
            return new JsonResult(responseF);
        }
        [Route("GetUserDetail")]
        [HttpGet]
        [Authorize]
        public async Task<UserDetailsViewModel> GetUserDetail()
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            return await userRepository.GetUserDetails(CurrId);
        }

        [Route("GetUserBadgesById")]
        [HttpGet]
        [Authorize]
        public async Task<List<BadgeViewModel>> GetUserBadgesById(Guid Id)
        {
            //var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            return await this.userRepository.GetUserBadgesById(Id);
        }

        [Route("UpdateFeatureBadge")]
        [HttpPost]
        [Authorize]
        public async Task<bool> UpdateFeatureBadge(string NewFeaturedBadgeCode)
        {
            var CurrId = Guid.Parse(this.userManager.GetUserId(User));
            return await this.userRepository.UpdateUserBadge(CurrId, NewFeaturedBadgeCode);
        }

        [Route("ChangePassword")]
        [HttpPost]
        [Authorize]
        public async Task<JsonResponse> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            var response = new JsonResponse();
            if (ModelState.IsValid)
            {
                var CurrId = Guid.Parse(this.userManager.GetUserId(User));

                var UserT = await _context.Users.Where(x => x.Id == CurrId).FirstOrDefaultAsync();
                if (UserT == null)
                {
                    response.success = false;
                    response.ReturnMsg = "User Not Found";

                    return response;
                }
                var result = await this.userManager.ChangePasswordAsync(UserT, model.OldPassword, model.NewPassword);
                if (!result.Succeeded)
                {
                    response.success = false;
                    response.JsonObj = result.Errors.ToList();
                    response.ReturnMsg = "Error while changing password";
                    return response;
                }
                try
                {
                    var emailUserModel = new EmailUserModel()
                    {
                        Email = UserT.Email,
                        FullName = UserT.UserName,
                        UserName = UserT.UserName,
                        Subject = "Password Changed",
                        Type = EmailTemplate.PasswordChanged
                    };
                    emailUserModel.CallbackUrl = "";

                    await _emailSender.SendEmailAsync(emailUserModel);
                }
                catch (Exception ex)
                {
                    throw;
                }
                response.success = true;
                response.ReturnMsg = "Password Successfully Changed";
                return response;
            }
            else
            {
                response.success = false;
                response.ReturnMsg = "Please enter all the required fields.";
            }

            return response;
        }

        [Route("ResetPassword")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResponse> ResetPassword(ResetPasswordViewModel model)
        {
            var response = new JsonResponse();

            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    // reset the user password
                    //var r =await this.userManager.VerifyUserTokenAsync(user, this.userManager.Options.Tokens.PasswordResetTokenProvider, "ResetPassword", model.Token);
                    var result = await userManager.ResetPasswordAsync(user, model.Token, model.Password);
                    if (result.Succeeded)
                    {
                        response.success = true;
                        response.ReturnMsg = "Password Successfully Changed";
                        return response;
                    }
                    else
                    {
                        response.success = false;
                        response.ReturnMsg = "The password reset link has already expired or has already been used or invalid .";
                        return response;
                    }
                    // Display validation errors. For example, password reset token already
                    // used to change the password or password complexity rules not met
                    var Errors = new List<string>();
                    foreach (var item in result.Errors)
                    {
                        Errors.Add(item.Description);
                    }
                    var jsonErr = JsonConvert.SerializeObject(Errors);
                    response.success = false;
                    response.ReturnMsg = jsonErr;
                    return response;
                }

                // To avoid account enumeration and brute force attacks, don't
                // reveal that the user does not exist
            }
            else
            {
                string modelError = "";
                foreach (var item in ModelState.Values)
                {
                    foreach (ModelError error in item.Errors)
                    {
                        var ErrorStr = modelError.ToString();
                        modelError = modelError + error.ErrorMessage;
                    }
                }
                response.success = false;
                response.ReturnMsg = modelError;
                return response;
            }
            // Display validation errors if model state is not valid
            return response;
        }

        [Route("ForgotPassword")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResponse> ForgotPassword(string Email)
        {
            var response = new JsonResponse();
            if (!string.IsNullOrEmpty(Email) && IsValidEmail(Email))
            {
                var user = await userManager.FindByEmailAsync(Email);
                // If the user is found AND Email is confirmed
                if (user != null)//&& await userManager.IsEmailConfirmedAsync(user))
                {
                    // Generate the reset password token
                    var token = await userManager.GeneratePasswordResetTokenAsync(user);

                    // Build the password reset link
                    var passwordResetLink = "https://www.fanbucket.com/User/ResetPassword?email=" + Email + "&token=" + Uri.EscapeDataString(token);

                    // Build the password reset link
                    // var passwordResetLink = "https://localhost:44300/User/ResetPassword?email=" + Email + "&token=" + Uri.EscapeDataString( token);

                    // Log the password reset link
                    // _logger.Log(LogLevel.Warning, passwordResetLink);

                    this.emailService.Mail_Alert(Email, "Reset Password Link", "<h3>Dear User,</h3><p>Please Use the following link to reset your password for Fan Bucket.</p><p><a href=" + passwordResetLink + ">Reset Password Link</a></p>");
                    //this.emailService.Mail_Alert("sujandhungel2015@gmail.com", "Reset Password Link", "<h3>Dear User,</h3><p>Please Use the following link to reset your password for Fan Bucket.</p><p><a href=" + passwordResetLink + ">Reset Password Link</a></p>");
                    // Send the user to Forgot Password Confirmation view
                    response.success = true;
                    response.ReturnMsg = "Email has been sent to the valid user.";
                }
            }
            else
            {
                response.success = false;
                response.ReturnMsg = "Please enter a valid email address.";
            }

            return response;
        }




        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        #region Private methods

        //private async void SendEmailConfirmation(RegistrationViewModel model, ApplicationUserViewModel applicationUser)
        //{
        //    string code = await _userManager.GenerateEmailConfirmationTokenAsync(_mapper.Map<ApplicationUser>(applicationUser));

        //    var callbackUrl = new Uri(Url.Link("ConfirmEmailRoute", new { user = applicationUser, code }));

        //    try
        //    {
        //        var emailUserModel = new EmailUserModel()
        //        {
        //            UserName = model.UserName,
        //            Password = model.Password,
        //            FullName = model.FullName,
        //            Email = model.Email,
        //            Role = model.Role,
        //            CallbackUrl = callbackUrl.ToString(),
        //            Subject = "HamroBazaar Account Confirmation"
        //        };
        //        await _emailSender.SendEmailAsync(emailUserModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        private async Task<IpInfo> GetIpInfoFromIpAddress()
        {
            var ipAddress = HttpContext.GetIpAddress();
            var httpclient = new HttpClient()
            {
                BaseAddress = new Uri($"http://ip-api.com/")
            };
            var response = await httpclient.GetAsync($"json/{ipAddress}");
            var result = await response.Content.ReadAsStringAsync();
            IpInfo ipInfo = JsonConvert.DeserializeObject<IpInfo>(result);
            return ipInfo;
        }
        private async Task SendLoginNotification(ApplicationUser usr)
        {
            try
            {
                var ipinfo = await GetIpInfoFromIpAddress();

                var emailUserModel = new EmailUserModel()
                {
                    Email = usr.Email,
                    FullName = usr.Fullname,
                    UserName = usr.UserName,
                    Subject = "New Login",
                    Type = EmailTemplate.Login,
                    Message = $"IP Address : {ipinfo.query}",
                    On = DateTime.UtcNow

                };
                if (ipinfo.status == "success")
                {
                    emailUserModel.Message = $"IP Address :{ipinfo.query},{ipinfo.city}.{ipinfo.regionName}.{ipinfo.country}";

                }
                _backgroundJobClient.Enqueue(() => _emailSender.SendEmailAsync(emailUserModel));

                //await _emailSender.SendEmailAsync(emailUserModel);
            }
            catch (Exception ex)
            {

            }
        }

        #endregion Private methods
    }
}