﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Helper;
using FanBucketAPI.Models;
using FanBucketAPI.Services;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BucketController : ControllerBase
    {
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IBucketService bucketService;
        private readonly IConfiguration _configuration;
        private readonly UserManager<ApplicationUser> userManager;
        public BucketController(IWebHostEnvironment hostingEnvironment, IBucketService bucketService,
            IConfiguration configuration,
            UserManager<ApplicationUser> userManager)
        {
            this.hostingEnvironment = hostingEnvironment;
            this.bucketService = bucketService;
            _configuration = configuration;
            this.userManager = userManager;
        }
        [HttpGet]
        public void Get()
        {

        }
        [HttpPost]
        [Authorize]
        [DisableRequestSizeLimit]
        public async Task<JsonResponse> Post(List<IFormFile> files, string contentType)
        {
            var ContenType = (ContentTypes)Enum.Parse(typeof(ContentTypes), contentType);
            var response = new JsonResponse();
            var Keyname = GetKeyName(ContenType);
            var fileextensions = files.Select(x => Path.GetExtension(x.FileName).ToLower()).ToArray();

            try
            {

                string[] validImageTypes = new string[] { ".gif", ".jpeg", ".png", ".jpg", ".bmp" };
                var validVideoTypes = new string[] { ".mp4", ".avi", ".m4v", ".mpg" };
                var validDocumentTypes = new string[] { ".pdf", ".doc", ".xlsx", ".ppt" };
                var validAudioTypes = new string[] { ".mp4", ".mpeg", ".mp3", ".x-m4a" };

                switch (ContenType)
                {
                    case ContentTypes.File:
                        foreach (var item in fileextensions)
                        {
                            if (!validDocumentTypes.Contains(item))
                            {
                                return CommonInvalidContentTypeResponse(response);
                            }
                        }
                        break;
                    case ContentTypes.Image:
                        foreach (var item in fileextensions)
                        {
                            if (!validImageTypes.Contains(item))
                            {
                                return CommonInvalidContentTypeResponse(response);
                            }
                        }
                        break;
                    case ContentTypes.Audio:
                        foreach (var item in fileextensions)
                        {
                            if (!validAudioTypes.Contains(item))
                            {
                                return CommonInvalidContentTypeResponse(response);
                            }
                        }
                        break;
                    case ContentTypes.Video:
                        foreach (var item in fileextensions)
                        {
                            if (!validVideoTypes.Contains(item))
                            {
                                return CommonInvalidContentTypeResponse(response);
                            }
                        }
                        break;
                    case ContentTypes.Polls:
                        foreach (var item in fileextensions)
                        {
                            if (!validVideoTypes.Contains(item) && !validImageTypes.Contains(item))
                            {
                                return CommonInvalidContentTypeResponse(response);
                            }
                        }
                        break;
                    default:
                        response.success = false;
                        response.ReturnMsg = "Invalid Content Type.";
                        return response;
                        // break;

                }


                if (!string.IsNullOrEmpty(Keyname))
                {
                    var Id = Guid.NewGuid().ToString();

                    foreach (IFormFile source in files)
                    {

                        var fileName = Id + Path.GetExtension(source.FileName);
                        long FileLength = source.Length;

                        if (!Directory.Exists(Path.Combine(hostingEnvironment.ContentRootPath, "Main")))
                        {
                            Directory.CreateDirectory(Path.Combine(hostingEnvironment.ContentRootPath, "Main"));
                        }
                        string VideoFolder = Path.Combine(hostingEnvironment.ContentRootPath, "Main", fileName);
                        await CreateFile.UploadToServer(source, VideoFolder);
                        var requestModel = new FileUploadModel()
                        {
                            fileSize = FileLength,
                            bucketName = _configuration.GetSection("AWS")["bucketName"],
                            file = source,
                            fileExt = source.ContentType,
                            filename = fileName,
                            FilePath = VideoFolder,
                            AccessKey = _configuration.GetSection("AWS")["AccessKey"],
                            SecretKey = _configuration.GetSection("AWS")["SecretKey"],
                            KeyName = Keyname
                        };
                        if (ContenType == ContentTypes.Image)
                        {

                            using (Image image = Image.FromFile(VideoFolder))
                            {
                                using (MemoryStream m = new MemoryStream())
                                {
                                    image.Save(m, image.RawFormat);
                                    image.Dispose();
                                    ImageCompressor.CompressImage(Image.FromStream(m), 35, VideoFolder);
                                }
                            }
                        }
                        response = await this.bucketService.UploadFileAsync(requestModel);
                        response.returnId = Id;

                        System.IO.File.Delete(VideoFolder);

                    }

                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Invalid Content Type";
                }



            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = ex.Message.ToString();
            }

            return response;

        }

        private static JsonResponse CommonInvalidContentTypeResponse(JsonResponse response)
        {
            response.success = false;
            response.ReturnMsg = "Contains Invalid Content Type";
            return response;
        }

        [Route("UploadImage")]
        [HttpPost]
        [Authorize]
        public async Task<JsonResponse> UploadImage(IFormFile source, string Type)
        {
            string[] validImageTypes = new string[] { ".gif", ".jpeg", ".png", ".jpg", ".bmp" };

            var response = new JsonResponse();
            var getExtension = Path.GetExtension(source.FileName);
            if (!validImageTypes.Contains(getExtension))
            {
                return CommonInvalidContentTypeResponse(response);
            }

            string Keyname;
            if (Type == "CoverPicture")
            {
                Keyname = "CoverPicture/";
            }
            else if (Type == "ProfilePicture")
            {
                Keyname = "ProfilePicture/";
            }
            else if (Type == "FeaturedImage")
            {
                Keyname = "FeaturedImage/";
            }
            else if (Type == "Identification")
            {
                Keyname = "Identification/";
            }
            else
            {
                return CommonInvalidContentTypeResponse(response);

            }

            //var UserId = userManager.GetUserId(User);
            try
            {

                var FileId = Guid.NewGuid();


                var fileName = FileId + Path.GetExtension(source.FileName);
                long FileLength = source.Length;

                if (!Directory.Exists(Path.Combine(hostingEnvironment.ContentRootPath, "Main")))
                {
                    Directory.CreateDirectory(Path.Combine(hostingEnvironment.ContentRootPath, "Main"));
                }
                string VideoFolder = Path.Combine(hostingEnvironment.ContentRootPath, "Main", fileName);
                await CreateFile.UploadToServer(source, VideoFolder);
                var requestModel = new FileUploadModel()
                {
                    fileSize = FileLength,
                    bucketName = _configuration.GetSection("AWS")["bucketName"],
                    fileExt = source.ContentType,
                    filename = fileName,
                    FilePath = VideoFolder,
                    AccessKey = _configuration.GetSection("AWS")["AccessKey"],
                    SecretKey = _configuration.GetSection("AWS")["SecretKey"],
                    KeyName = Keyname
                };
                using (Image image = Image.FromFile(VideoFolder))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        image.Save(m, image.RawFormat);
                        image.Dispose();
                        ImageCompressor.CompressImage(Image.FromStream(m), 30, VideoFolder);
                    }
                }
                response = await this.bucketService.UploadFileAsync(requestModel);
                response.returnId = FileId.ToString(); ;
                System.IO.File.Delete(VideoFolder);

                return response;
            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = ex.Message.ToString();
            }

            return response;

        }


        [Route("UploadUserPic")]
        [HttpPost]
        [Authorize]
        public async Task<JsonResponse> UploadUserPic(List<IFormFile> files, string Type)
        {

            var response = new JsonResponse();
            var Keyname = Type == "CoverPicture" ? "CoverPicture/" : "ProfilePicture/";
            var UserId = userManager.GetUserId(User);
            try
            {

                var FileId = UserId;

                foreach (IFormFile source in files)
                {
                    var fileName = FileId + Path.GetExtension(source.FileName);
                    long FileLength = source.Length;

                    if (!Directory.Exists(Path.Combine(hostingEnvironment.ContentRootPath, "Main")))
                    {
                        Directory.CreateDirectory(Path.Combine(hostingEnvironment.ContentRootPath, "Main"));
                    }
                    string VideoFolder = Path.Combine(hostingEnvironment.ContentRootPath, "Main", fileName);
                    await CreateFile.UploadToServer(source, VideoFolder);
                    var requestModel = new FileUploadModel()
                    {
                        fileSize = FileLength,
                        bucketName = _configuration.GetSection("AWS")["bucketName"],
                        fileExt = source.ContentType,
                        filename = fileName,
                        FilePath = VideoFolder,
                        AccessKey = _configuration.GetSection("AWS")["AccessKey"],
                        SecretKey = _configuration.GetSection("AWS")["SecretKey"],
                        KeyName = Keyname
                    };
                    using (Image image = Image.FromFile(VideoFolder))
                    {
                        using (MemoryStream m = new MemoryStream())
                        {
                            image.Save(m, image.RawFormat);
                            image.Dispose();
                            ImageCompressor.CompressImage(Image.FromStream(m), 30, VideoFolder);
                        }
                    }
                    response = await this.bucketService.UploadFileAsync(requestModel);
                    response.returnId = FileId;
                    System.IO.File.Delete(VideoFolder);
                    var user = await userManager.FindByIdAsync(UserId.ToString());
                    if (Type == "CoverPicture")
                        user.CoverImgLocation = response.returnPath;
                    else
                        user.ProfileImgLocation = response.returnPath;
                    await userManager.UpdateAsync(user);


                }
            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = ex.Message.ToString();
            }

            return response;

        }
        private string GetKeyName(ContentTypes contentType)
        {
            var ContentType = string.Empty;
            switch (contentType)
            {

                case ContentTypes.File:
                    ContentType = "Contents/Document/";
                    break;
                case ContentTypes.Image:
                    ContentType = "Contents/Image/";
                    break;
                case ContentTypes.Audio:
                    ContentType = "Contents/Audio/";
                    break;
                case ContentTypes.Video:
                    ContentType = "Contents/Video/";
                    break;
                case ContentTypes.Polls:
                    ContentType = "Contents/Polls/";
                    break;

                default:
                    ContentType = "";
                    break;
            }
            return ContentType;

        }
    }
}