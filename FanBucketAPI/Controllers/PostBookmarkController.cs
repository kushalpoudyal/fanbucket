﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.Models.UserDetails;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FanBucketAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostBookmarkController : ControllerBase
    {
        private readonly PostMasterIRepository _postmaster;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly FbDbContext context;
        private string FilePath { get; }
        public PostBookmarkController(FbDbContext context, UserManager<ApplicationUser> userManager,
            PostMasterIRepository postmaster,
           IWebHostEnvironment hostingEnvironment)
        {
            this.context = context;
            this.userManager = userManager;
            _postmaster = postmaster;
            this.hostingEnvironment = hostingEnvironment;
            //FilePath = @"C:\inetpub\wwwroot\wwwroot"; //live
            FilePath = @"C:\inetpub\wwwroot\fanbucket\FanBucketUI\wwwroot";
        }


        // GET: api/PostBookmark/5
        [Route("GetBookmarkByUser")]
        [HttpGet]
        public async Task<IActionResult> GetBookmarkByUser()
        {
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            var data = await _postmaster.GetBookmarkByUser(currId);
            return Ok(data);
        }

        [Route("BookmarkPost")]
        [HttpPost]
        public async Task<JsonResponse> BookmarkPost(Guid PostId)
        {
            var response = new JsonResponse();
            var currId = Guid.Parse(this.userManager.GetUserId(User));
            try
            {

                var bookmarkExist = context.PostBookmark.Any(x => x.PostId == PostId && x.UserId == currId);

                if (bookmarkExist)
                {
                    var bookmark = context.PostBookmark.Where(x => x.PostId == PostId && x.UserId == currId).FirstOrDefault();
                    context.Remove(bookmark);
                    await context.SaveChangesAsync();

                    response.success = true;
                    response.ReturnMsg = "Successfully Removed from Bookmarks!";

                    return response;
                }
                else
                {
                    var bookmark = new PostBookmark();

                    bookmark.Id = Guid.NewGuid();
                    bookmark.PostId = PostId;
                    bookmark.UserId = currId;
                    bookmark.Status = FanBucketEntity.Model.ENUM.Record_Status.Active;
                    bookmark.BookmarkedOn = DateTime.Now;

                    context.PostBookmark.Add(bookmark);
                    await context.SaveChangesAsync();

                    response.success = true;
                    response.ReturnMsg = "Successfully Bookmarked the Post!";

                    return response;
                }

            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Could Not Bookmark the post. Please try again Later";

            }

            return response;
        }


    }
}
