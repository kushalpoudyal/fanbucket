﻿using FanBucketEntity.DBContext;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FanBucketAPI.BackgroundJobs
{
    //https://stacksecrets.com/dot-net-core/scheduled-repeating-task-with-net-core
    public class BlockStatusUpdateService : IHostedService
    {
        private Timer _timer;
        private readonly IServiceScopeFactory scopeFactory;

        public BlockStatusUpdateService(IServiceScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {

            // timer repeates call to UnblockTheTemporaryBlockedUser every 5 minutes.
            _timer = new Timer(
                UnblockTheTemporaryBlockedUser,
                null,
                TimeSpan.Zero,
                TimeSpan.FromDays(30)
            );

            return Task.CompletedTask;


        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        private void UnblockTheTemporaryBlockedUser(object state = null)
        {
            return;

            using (var scope = scopeFactory.CreateScope())
            {
                var _context = scope.ServiceProvider.GetRequiredService<FbDbContext>();
                var userList = _context.Users.Where(x => x.BlockTill <= DateTime.UtcNow).ToList();//Get List of user when has exceed the blocked date
                foreach (var item in userList)
                {
                    item.BlockTill = null;//set the block till date to null
                    item.Status = FanBucketEntity.Model.ENUM.Record_Status.Active;//set status to active
                }
                _context.Users.UpdateRange(userList);
                _context.SaveChanges();
            }
        }
    }
}
