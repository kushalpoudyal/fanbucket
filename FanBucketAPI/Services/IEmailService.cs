﻿using FanBucketEntity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Services
{
    public interface IEmailService
    {
       void Mail_Alert(string Email, string subject, string HTMLMessage);
    }
}
