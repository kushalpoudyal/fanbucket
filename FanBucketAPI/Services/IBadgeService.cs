﻿using FanBucketEntity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Services
{
    public interface IBadgeService
    {
        Task<bool> AddBadgeToUserByBadgeCode(Guid CurrId, string BadgeCode);
        Task<bool> AddNewBadgeToUser(Guid CurrId, Guid BadgeId);
        int GetUserPaidPostCount(Guid CurrId);
        Task<bool> IsVerified(Guid CurrId);
        bool UserIsContentCreator(Guid CurrId);
        Task<BadgeInfo> GetBadgeByCode(string BadgeCode);
        Task<bool> VerifyUser(Guid CurrId);


    }
}
