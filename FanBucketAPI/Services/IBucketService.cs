﻿using FanBucketAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Services
{
    public interface IBucketService
    {
        Task<JsonResponse> UploadFileAsync(FileUploadModel model);
    }
}
