﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Model;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketRepo.Interface;
using FanBucketRepo.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Services
{


    public class BadgeService : IBadgeService
    {
        private readonly IBadgeRepository badgeRepository;
        private readonly IUserBadgesRepository userBadgesRepository;
        private readonly PostMasterIRepository postMasterRepository;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly FbDbContext context;

        public BadgeService(IBadgeRepository badgeRepository,
            PostMasterIRepository postMasterRepository,
            UserManager<ApplicationUser> userManager,
            IUserBadgesRepository userBadgesRepository,
            FbDbContext context
            )
        {
            this.badgeRepository = badgeRepository;
            this.postMasterRepository = postMasterRepository;
            this.userManager = userManager;
            this.userBadgesRepository = userBadgesRepository;
            this.context = context;
        }
        public int GetUserPaidPostCount(Guid CurrId)
        {
            return postMasterRepository.GetAll().Where(x => x.CreatedById == CurrId && x.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free).Count();
        }

        public bool UserIsContentCreator(Guid CurrId)
        {
            return postMasterRepository.GetAll().Where(x => x.CreatedById == CurrId && x.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free).Any();
        }

        public async Task<bool> IsVerified(Guid CurrId)
        {
            var VerifiedCode = BadgeCodes.Verified.ToString();
            var GetVerfiedBadge = await badgeRepository.GetByCode(BadgeCodes.Verified.ToString());
            if (GetVerfiedBadge == null)
            {
                throw new Exception("Unable to retrieve badge with code: " + VerifiedCode);
            }
            return context.UserBadges.Where(x => x.UserId == CurrId && x.BadgeId == GetVerfiedBadge.Id).Any();
        }
        public async Task<BadgeInfo> GetBadgeByCode(string BadgeCode)
        {
            return await badgeRepository.GetByCode(BadgeCode);
        }
        public async Task<bool> AddNewBadgeToUser(Guid CurrId, Guid BadgeId)
        {
            //var NewUserCode = BadgeCodes.NewUser.ToString();
            //var NewUserBadge = await badgeRepository.GetByCode(BadgeCodes.NewUser.ToString());
            //if (NewUserBadge == null)
            //{
            //    throw new Exception("Unable to retrieve badge with code: " + NewUserBadge);
            //}
            var userBadge = new UserBadges()
            {
                BadgeId = BadgeId,
                UserId = CurrId,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedById = CurrId,
                Deleted_Status = false,
                Record_Status = Record_Status.Active,
                Id = Guid.NewGuid()

            };
            await userBadgesRepository.AddAsync(userBadge);
            return true;
        }
        public async Task<bool> AddBadgeToUserByBadgeCode(Guid CurrId, string BadgeCode)
        {

            var badge = await badgeRepository.GetByCode(BadgeCode);
            if (badge == null)
            {
                throw new Exception("Unable to retrieve badge with code: " + BadgeCode);
            }
            var userBadge = new UserBadges()
            {
                BadgeId = badge.Id,
                UserId = CurrId,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedById = CurrId,
                Deleted_Status = false,
                Record_Status = Record_Status.Active,
                Id = Guid.NewGuid()

            };
            try
            {
                await badgeRepository.AddUserBadgeAsync(userBadge);
            }
            catch (Exception ex)
            {

            }

            return true;
        }

        public async Task<bool> VerifyUser(Guid CurrId)
        {
            //var user = await userManager.FindByIdAsync(CurrId.ToString());
            //if (user==null)
            //{
            //    throw new Exception("Invalid user Id "+CurrId);
            //}
            //var badge = await badgeRepository.GetByCode(BadgeCodes.Verified.ToString());
            //if (badge == null)
            //{
            //    throw new Exception("Unable to retrieve badge with code: " + BadgeCodes.Verified.ToString());
            //}
            //var userBadge = new UserBadges()
            //{
            //    BadgeId = badge.Id,
            //    UserId = CurrId,
            //    CreatedOn = DateTime.Now,
            //    ModifiedOn = DateTime.Now,
            //    CreatedById = user,
            //    Deleted_Status = false,
            //    Record_Status = Record_Status.Active,
            //    Id = Guid.NewGuid()

            //};
            //await badgeRepository.AddAsync(badge);
            return true;
        }
    }
}
