﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class InterestViewModel
    {
        public Guid Id { get; set; }
        [StringLength(150)]
        [Required]
        public string Name { get; set; }
        [StringLength(20)]
        [Required]
        public string Code { get; set; }
        [StringLength(1000)]
        public string Description { get; set; }

    }


}
