﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.APIModels
{

    public class PostMasterAPIModel
    {
        public Guid Id { get; set; }
        public string PostTitle { get; set; }
        public string PostPrivacy { get; set; }
        public string PostPrice { get; set; }
        public string IsMedia { get; set; }

        public string IsScheduled { get; set; }

        public string NSFW { get; set; }

        public string ScheduledFor { get; set; }
        public string ParentPostId { get; set; }

        public DateTime CreatedDate { get; set; }
        public List<PostCollectionAPIModel> PostCollections { get; set; }
    }
    public class PostCollectionAPIModel
    {
        public string ContentId { get; set; }
        public string FileLocation { get; set; }
        public string ContentTitle { get; set; }
        public string PostType { get; set; }
        public string PaidPost { get; set; }

    }

}
