﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class CreatorViewModel
    {
        public Guid CreatorId { get; set; }

        public string Creator { get; set; }
        public string CreatorImg { get; set; }

        public string CreatorUsrName { get; set; }


    }
}
