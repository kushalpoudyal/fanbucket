﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class PostReportedViewModel
    {
        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        public string ReportingReason { get; set; }

        public string Remarks { get; set; }

        public Guid ReportedById { get; set; }

        public DateTime CreatedOn { get; set; }

        public Record_Status Record_Status { get; set; }

        public UserDetailsViewModel UserDetailsViewModel { get; set; }
    }
}
