﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class GetUserMediaViewModel
    {
        public string FileLocation { get; set; }
        public Guid CollectionId { get; set; }

        public Guid PostId { get; set; }
        public bool HasPaid { get; set; }
        public string PostTitle { get; set; }
        public bool PaidContent { get; set; }
    }
}
