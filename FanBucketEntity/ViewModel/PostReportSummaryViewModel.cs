﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;

namespace FanBucketAPI.ViewModel
{
    public class PostReportSummaryViewModel
    {
        public PostReportSummaryViewModel()
        {
            Reports = new List<ProductReportDetailViewModel>();
        }

        public Guid PostId { get; set; }
        public string PostTitle { get; set; }
        public int NoOfReports { get; set; }
        public DateTime LastReportedDate { get; set; }
        public string PostCreatedBy { get; set; }
        public Guid PostCreatedById { get; set; }
        public IEnumerable<ProductReportDetailViewModel> Reports { get; set; }
        public Record_Status Status { get; set; }
    }

    public class ProductReportDetailViewModel
    {
        public ProductReportDetailViewModel()
        {
        }

        public string Remarks { get; set; }
        public string ReportingReason { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid ReportedById { get; set; }
        public string ReportedByFullname { get; set; }
    }
}