﻿using System;

namespace FanBucketAPI.ViewModel
{
    public class CommissionReviewViewModel
    {
        public Guid UserId { get; set; }
        public decimal NewCommissionPer { get; set; }
        public string Remarks { get; set; }
    }
}