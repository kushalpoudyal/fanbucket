﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class PostChatViewModel
    {
        public PostChatViewModel()
        {
            Attachments = new List<PostMessagesAttachmentViewModel>();
        }
        [StringLength(1000)]
        public string Message { get; set; }
        [StringLength(200)]
        public string FileLocation { get; set; }

        public string ChatId { get; set; }
        public string targetId { get; set; }
        public string TipAmount { get; set; }

        public string PIN { get; set; }
        public List<PostMessagesAttachmentViewModel> Attachments { get; set; }
    }
    public class PostMessagesAttachmentViewModel
    {
        //[Key]
        //public Guid Id { get; set; }

        //public Guid MessageId { get; set; }
        //public Guid UserId { get; set; }

        //public ContentTypes ContentType { get; set; } = ContentTypes.Image;
        public Guid ContentId { get; set; }

        //[StringLength(200)]
        public string FileLocation { get; set; }

        //public DateTime When { get; set; }
        //[ForeignKey("MessageId")]
        //public virtual Messages Messages { get; set; }
    }
    public class MessagesAttachmentViewModel
    {
        //[Key]
        //public Guid Id { get; set; }

        //public Guid MessageId { get; set; }
        //public Guid UserId { get; set; }

        //public ContentTypes ContentType { get; set; } = ContentTypes.Image;

        //[StringLength(200)]
        public string FileLocation { get; set; }

        //public DateTime When { get; set; }
        //[ForeignKey("MessageId")]
        //public virtual Messages Messages { get; set; }
    }
}
