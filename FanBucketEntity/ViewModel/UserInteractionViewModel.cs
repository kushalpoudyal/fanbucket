﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class UserInteractionViewModel
    {
        public int TotalSubscribers { get; set; }

        public decimal TotalEarnings { get; set; }

        public decimal EarningByPost { get; set; }

        public decimal EarningBySubscription { get; set; }

        public int NewUser { get; set; }

        public int ProfileViews { get; set; }

        public int TrendingPost { get; set; }

        public int TotalLikes { get; set; }

        public int TotalReported { get; set; }
    }
}
