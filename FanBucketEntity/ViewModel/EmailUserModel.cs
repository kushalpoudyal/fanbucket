﻿using FanBucketEntity.Enum;
using System;

namespace FanBucketAPI.ViewModel
{

    public class EmailUserModel : ApplicationUserModel
    {
        public string CallbackUrl { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public EmailTemplate Type { get; set; }
        public DateTime? On { get; set; }
    }
}
