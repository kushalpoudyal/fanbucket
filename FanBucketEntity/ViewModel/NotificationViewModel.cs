﻿using FanBucketEntity.Model.ENUM;
using Microsoft.EntityFrameworkCore.Query.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class NotificationViewModel
    {

        public Guid Id { get; set; }
        public string Fullname { get; set; }

        public string CreatorImg { get; set; }


        public string Username { get; set; }

        public Guid UserId { get; set; }

        public Guid PostId { get; set; }

        public Guid ActivityId { get; set; }

        public NotificationType nType { get; set; }

        public string Message { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool Viewed { get; set; }

        public string DateString { get; set; }
    }

}
