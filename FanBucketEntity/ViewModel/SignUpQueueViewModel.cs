﻿using System.ComponentModel.DataAnnotations;

namespace FanBucketAPI.ViewModel
{
    public class SignUpQueueViewModel
    {
        #region BasicInfo
        [StringLength(350)]
        [Required]
        public string FirstName { get; set; }
        [StringLength(350)]
        [Required]

        public string LastName { get; set; }

        [StringLength(150)]
        [Required]
        public string EmailAddress { get; set; }

        [StringLength(500)]
        [Required]
        //Multiple content creation types are joined by commas
        public string ContentStyle { get; set; }


        #endregion

        #region SocialMediaPresence
        [StringLength(1000)]
        [Required]
        //Subscriber/Followers count? and Platform?
        public string FollowersAndPlatfrom { get; set; }
        [StringLength(1000)]
        [Required]
        //Social Media Links
        public string SocialMediaLinks { get; set; }
        [StringLength(1000)]
        [Required]
        //Customer Success Managers (CSM) are limited, let us know why you would like one?       
        public string CSMReason { get; set; }
        #endregion

        public string Password { get; set; }
        public string Username { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }

}