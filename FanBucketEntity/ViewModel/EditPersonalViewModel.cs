﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class EditPersonalViewModel
    {
        public string ShortBio { get; set; }
        public string Profession { get; set; }

        public string Username { get; set; }

        public string Location { get; set; }

        // public List<SelectListItem> Interests { get; set; }

        public string Fullname { get; set; }
        public string ProfileImgLocation { get; set; }
        //  public bool PrivateAccount { get; set; }
    }
}
