﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class UserSuscriptionViewModel
    {
        public string Id { get; set; }
        public string SubscriptionAmount { get; set; }

        public string Tenure { get; set; }

    }
}
