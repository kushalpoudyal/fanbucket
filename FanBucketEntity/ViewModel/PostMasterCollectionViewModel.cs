﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketAPI.Models
{
    public class PostMasterCollectionViewModel
    {
        public Guid PostId { get; set; }
        public string PostTitle { get; set; }

        public Privacy PostPrivacy { get; set; }
        public PaidPostType PaidPostType { get; set; }
        public decimal PostPrice { get; set; }

        public CreatorViewModel Creator { get; set; }
        public DateTime CreatedOn { get; set; }

        public string DateString { get; set; }
        public bool HasPaid { get; set; }

        public bool HasSubscribed { get; set; }

        public bool NSFW { get; set; }

        public bool HasReacted { get; set; }
        public bool HasBookMarked { get; set; }
        public int TotalReacts { get; set; }

        public int TotalComments { get; set; }

        public int TotalVotes { get; set; }

        public bool Scheduled { get; set; }

        public Nullable<DateTime> ScheduledFor { get; set; }

        public List<PostCollectionViewModel> collection { get; set; }

        public Guid? ParentPostId { get; set; }
        public PostMasterCollectionViewModel ParentPostMasterCollectionViewModel { get; set; }
    }
    public class PostCollectionViewModel
    {
        public Guid ContentId { get; set; }

        public string ContentTitle { get; set; }

        public string FileLocation { get; set; }
        public bool HasPaid { get; set; }

        public bool PaidPost { get; set; }
        public bool HasReacted { get; set; }

        public bool HasVoted { get; set; }
        public bool HasBookMarked { get; set; }
        public int TotalReacts { get; set; }

        public int TotalComments { get; set; }

        public int TotalVote { get; set; }
        public ContentTypes PostType { get; set; }
        public List<CreatorViewModel> Voters { get; set; } = new List<CreatorViewModel>();
    }
    public class PollVoteViewModel
    {
        public Guid ContentId { get; set; }
        
        public int TotalVote { get; set; }
        public List<CreatorViewModel> Voters { get; set; } = new List<CreatorViewModel>();
    }
}
