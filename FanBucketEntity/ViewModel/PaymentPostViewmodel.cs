﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class PaymentPostViewmodel
    {
        public decimal PaidAmount { get; set; }

        public Guid PaidById { get; set; }

        public Guid PostId { get; set; }

        public int PIN { get; set; }
    }
}
