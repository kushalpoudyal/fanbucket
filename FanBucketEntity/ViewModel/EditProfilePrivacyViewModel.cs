﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class EditProfilePrivacyViewModel
    {
        public string AccountType { get; set; }

        public string NSFW { get; set; }
        public List<UserSuscriptionViewModel> SubscriptionType { get; set; }
    }
}
