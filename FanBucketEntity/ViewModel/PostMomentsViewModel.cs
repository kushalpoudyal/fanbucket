﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class PostMomentsViewModel
    {

        public List<MomentsViewModel> MomentDetail { get; set; }

        public CreatorViewModel Creator { get; set; }

        public DateTime MaxDate { get; set; }
    }

    public class MomentsViewModel
    {

        public Guid PostId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string DateString { get; set; }
    }

}
