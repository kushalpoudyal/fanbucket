﻿namespace FanBucketAPI.ViewModel
{
    public class PaymentCreditByUserViewModel
    {
        public PaymentCreditByUserViewModel()
        {
        }

        public decimal CreditSum { get; set; }
        public string Currency { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal DebitSum { get; set; }
        public decimal EarningPercentage { get; set; }
    }

}