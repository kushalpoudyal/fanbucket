﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class EarningSplit
    {
        public decimal UserAmount { get; set; }

        public decimal AdminAmount { get; set; }
    }
}
