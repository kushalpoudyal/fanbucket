﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class BadgeViewModel
    {
        public Guid Id { get; set; }
        [StringLength(150)]
        [Required]
        public string Name { get; set; }
        [StringLength(50)]
        [Required]
        public string Code { get; set; }
        [StringLength(1000)]
        [Required]
        public string Description { get; set; }
        public BadgeTriggerType TriggerType { get; set; }
        [StringLength(1000)]
        public string TriggeredOn { get; set; }
        public string ImageUrl { get; set; }
    }


}
