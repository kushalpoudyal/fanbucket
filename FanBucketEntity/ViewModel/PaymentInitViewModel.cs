﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class PaymentInitViewModel
    {
        public string PostId { get; set; }

        public string PostTitle { get; set; }

        public decimal PostAmount { get; set; }
    }

    public class LoadInitViewModel
    {
        public decimal Amount { get; set; }
    }

}
