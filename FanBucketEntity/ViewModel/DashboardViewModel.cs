﻿using FanBucketEntity.Model.Rendering;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class DashboardViewModel
    {
        public int NewUsersTotal { get; set; }
        public List<SelectListVal> NewUsers { get; set; }
        public int TotalUsers { get; set; }
        public List<SelectListVal> ReportedUsers { get; set; }
        public int ReportedPostsTotal { get; set; }
        public decimal TotalEarnings { get; set; }
        public int ReportedUsersCount { get; set; }
    }
}
