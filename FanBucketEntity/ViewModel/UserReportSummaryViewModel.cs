﻿using System;
using System.Collections.Generic;

namespace FanBucketRepo.ViewModel
{
    public class UserReportSummaryViewModel
    {
        public UserReportSummaryViewModel()
        {
            Reports = new List<UserReportDetailViewModel>();
        }

        public Guid UserId { get; set; }
        public string UserFullname { get; set; }
        public int NoOfReports { get; set; }
        public DateTime LastReportedDate { get; set; }
        public List<UserReportDetailViewModel> Reports { get; set; }
    }
    public class UserReportDetailViewModel
    {
        public UserReportDetailViewModel()
        {
        }

        public string Remarks { get; set; }
        public string ReportingReason { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid ReportedById { get; set; }
        public string ReportedByUsername { get; set; }
        public string ReportedbyFullname { get; set; }
    }
}