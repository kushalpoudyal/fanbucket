﻿using FanBucketEntity.Model.ENUM;
using System;

namespace FanBucketAPI.ViewModel
{
    public class ReportActionViewModel
    {
        public Guid UserId { get; set; }
        public EnumReportAction Action { get; set; }
        public EnumDays BlockDays { get; set; }

        public string Remarks { get; set; }
    }
}