﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class PostCommentViewModel
    {
        public Guid Id { get; set; }
        public string Comment { get; set; }
        public Guid PostId { get; set; }

        public Guid CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string CreatorImg { get; set; }

        public string CreatorUsrName { get; set; }

        public string DateString { get; set; }


    }
}
