﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class ChatUsersViewModel
    {
        public Guid ChatId { get; set; }
        public Guid UserId { get; set; }

        public string LastMessage { get; set; }

        public DateTime recentTime { get; set; }

        public string DateString { get; set; }

        public string ChatWithName { get; set; }

        public string ProfileImgLocation { get; set; }

        public bool HasRead { get; set; }
        public bool UserStatus { get; set; }
        public string ChatWithUserName { get; set; }
        public bool HasFile { get; set; }
        public bool Blocking { get; set; }
        public bool IsBlocked { get; set; }
    }
}
