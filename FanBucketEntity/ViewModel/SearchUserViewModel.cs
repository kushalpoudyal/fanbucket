﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class SearchUserViewModel
    {
        public string ProfileImgLocation { get; set; }
        public string Fullname { get; set; }

        public Guid Id { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }
    }
}
