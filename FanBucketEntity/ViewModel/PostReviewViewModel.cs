﻿using FanBucketEntity.Model.ENUM;
using System;

namespace FanBucketAPI.ViewModel
{
    public class PostReviewViewModel
    {
        public Guid PostId { get; set; }
        public Record_Status Status { get; set; }
        public string Remarks { get; set; }
    }
}