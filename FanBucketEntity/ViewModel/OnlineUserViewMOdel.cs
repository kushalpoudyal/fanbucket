﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class OnlineUserViewMOdel
    {
        public string ProfileImg { get; set; }
        public Guid UserId { get; set; }

        public string Fullname { get; set; }

        public Guid ChatId { get; set; }

        public string Username { get; set; }
    }
}
