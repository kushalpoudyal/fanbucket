﻿namespace FanBucketEntity.ViewModel
{
    public class TopTrendingTagsViewModel
    {
        public TopTrendingTagsViewModel()
        {
        }

        public string Title { get; set; }
        public int Count { get; set; }
    }
}