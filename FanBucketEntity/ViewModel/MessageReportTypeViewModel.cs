﻿using System;

namespace FanBucketEntity.ViewModel
{
    public class MessageReportTypeViewModel
    {
        public string Value { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

    }

    public class ReportConversationViewModel
    {
        public Guid ChatId { get; set; }
        public string ReportedFor { get; set; }

    }
}