﻿using Microsoft.AspNetCore.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketEntity.ViewModel
{
    public class PaymentValidationModel
    {
        public Guid PostId { get; set; } 
        public decimal PayingAmount { get; set; }
        public int PIN { get; set; }
        public Guid CurrId { get; set; } 
    }
}
