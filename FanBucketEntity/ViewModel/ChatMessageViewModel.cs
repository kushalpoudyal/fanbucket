﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class ChatViewModel
    {
        public Guid ChatId { get; set; }
        public Guid UserId { get; set; }
        public string ChatWithName { get; set; }

        public string ProfileImgLocation { get; set; }

        public List<ChatMessageViewModel> ChatMessages { get; set; }
        public bool UserStatus { get; set; }
        public string ChatWithUserName { get; set; }
        public bool IsBlocked { get; set; }
        public bool Blocking { get; set; }
    }
    public class ChatMessageViewModel
    {
        public ChatMessageViewModel()
        {
            Attachments = new List<MessagesAttachmentViewModel>();
        }
        public Guid UserId { get; set; }
        public string Message { get; set; }

        public DateTime When { get; set; }

        public string FileLocation { get; set; }

        public string DateString { get; set; }
        public string Username { get; set; }

        public bool HasRead { get; set; }

        public decimal? TipAmount { get; set; }
        public Guid Id { get; set; }
        public List<MessagesAttachmentViewModel> Attachments { get; set; }
    }

}
