﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class CommentViewModel
    {

        public string Comment { get; set; }

        public Guid PostId { get; set; } 
    }

    public class PutCommentViewModel
    {

        public Guid Id { get; set; }
        public string Comment { get; set; }
    }

}
