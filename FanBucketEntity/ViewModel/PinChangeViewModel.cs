﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class PinChangeViewModel
    {
        public int CurrentPin { get; set; }
        public int NewPin { get; set; }
        public string CurrentPassword { get; set; }
    }
}
