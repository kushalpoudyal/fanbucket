﻿using System;

namespace FanBucketEntity.ViewModel
{
    public class EditKycViewModel
    {
        public string Identification { get; set; }
        public string IdNO { get; set; }

        public string IdIssuedFrom { get; set; }

        public string Country { get; set; }

        public string ContactNumber { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }

        public string CardName { get; set; }

        public string CardNumber { get; set; }
        public DateTime Expiry { get; set; }

        public string CVV { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string IdentificationFileLocation { get; set; }
    }
}