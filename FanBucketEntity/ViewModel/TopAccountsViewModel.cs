﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class TopAccountsViewModel
    {
        public Guid UserId { get; set; }

        public string FullName { get; set; }

        public string Username { get; set; }

        public string ProfileImgLocation { get; set; }

        public string FeaturedImageLeft { get; set; }

        public string FeaturedImageRight { get; set; }
    }
}
