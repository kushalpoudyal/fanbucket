﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.PostActivity;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.ViewModel
{
    public class ReactViewModel
    {

        public string PostReact { get; set; }

        public string PostId { get; set; }

        public string record_Status { get; set; }
    }
}
