﻿using FanBucketAPI.ViewModel;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.UserDetails;
using FanBucketRepo.ViewModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace FanBucketEntity.ViewModel
{

    public class UserDetailsViewModel
    {
        public UserDetailsViewModel()
        {
            Badges = new List<UserBadgesViewModel>();
            AllBadges = new List<UserBadgesViewModel>();
            SocialMediaLinks = new List<SocialMediaLinksViewModel>();
        }
        public string Location { get; set; }
        public string LatitudeCoordinate { get; set; }
        public string LongitudeCoordinate { get; set; }
        public string ShortBio { get; set; }
        public string Profession { get; set; }
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public AccountType AccountType { get; set; }

        public List<SelectListItem> Interests { get; set; }

        public string ProfileImgLocation { get; set; }

        public string CoverImgLocation { get; set; }

        public bool UserFollowed { get; set; }

        public bool HasSubscribed { get; set; }

        public Guid SubscriptionId { get; set; }

        public int TotalSubscribers { get; set; }

        public int TotalFollowers { get; set; }
        public int TotalFollowing { get; set; }
        //public string[] Badges { get; set; }
        public List<UserBadgesViewModel> Badges { get; set; }
        public int UserInterestCount { get; set; }
        public bool HideNewUserBadge { get; set; }

        public bool NSFW { get; set; }

        public EditKycViewModel KYC_Details { get; set; }

        public List<ApplicationUserSubscriptionINfo> SubsInfo { get; set; }

        public List<GetUserMediaViewModel> Medias { get; set; }
        public string FeaturedBadgeCode { get; set; }

        public Guid ChatId { get; set; }
        public PaymentCreditByUserViewModel BalanceInfo { get; set; }
        public UserReportSummaryViewModel ActiveReports { get; set; }
        public Record_Status Status { get; set; }
        public DateTime? BlockTill { get; set; }
        public int UserNameModifiedTimes { get; set; }
        public DateTime? LastUserNameModifiedDate { get; set; }
        public List<UserBadgesViewModel> AllBadges { get; set; }
        public bool HasPinSet { get; set; }
        public int RemainingSubscriptionDays { get; set; }
        public string FeaturedImageLeft { get; set; }
        public string FeaturedImageRight { get; set; }
        public List<SocialMediaLinksViewModel> SocialMediaLinks { get; set; }
    }
    public class UserBadgesViewModel
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }

    public class SettingsViewModel
    {
        public NotificationSettingViewModel NotificationSetting { get; set; }
        public ApperanceSettingViewModel ApperanceSetting { get; set; }
        public ProfileSettingViewModel ProfileSettingViewModel { get; set; }
        public SubscriptionSettingViewModel SubscriptionSettingViewModel { get; set; }
        public EditKycViewModel KYCSetting { get; set; }
    }
    public class NotificationSettingViewModel
    {
        public bool EmailNotification { get; set; }
        public bool HasLoginAlert { get; set; }
        public bool HasInformationUpdateAlert { get; set; }
        public bool HasMessageAlert { get; set; }
        public bool HasTipAlert { get; set; }
        public bool HasSubscriptionAlert { get; set; }
        public bool HasFollowerAlert { get; set; }
        public bool HasShareAlert { get; set; }
        public bool HasMentionAlert { get; set; }
        public bool HasBookmarkAlert { get; set; }
        public bool HasLoveReactAlert { get; set; }
    }
    public class ApperanceSettingViewModel
    {
        //[JsonPropertyName(nameof(ProfilePicUrl))]
        //public string ProfilePicUrl { get; set; }
        [JsonPropertyName(nameof(CoverImgPic))]

        public string CoverImgPic { get; set; }
        [JsonPropertyName(nameof(FeatureImageLeft))]

        public string FeatureImageLeft { get; set; }
        [JsonPropertyName(nameof(FeatureImageRight))]

        public string FeatureImageRight { get; set; }
        [JsonPropertyName(nameof(SocialMediaLinks))]

        public List<SocialMediaLinksViewModel> SocialMediaLinks { get; set; }

        public ApperanceSettingViewModel()
        {
            SocialMediaLinks = new List<SocialMediaLinksViewModel>();
        }
    }

    public class ProfileSettingViewModel
    {
        public ProfileSettingViewModel()
        {
        }

        public string Fullname { get; set; }
        public string ProfileImgLocation { get; set; }
        public string ShortBio { get; set; }
        public string Profession { get; set; }
        public string UserName { get; set; }
        public int RemainingUserNameChanges { get; set; }
        public int MaxNoOfUserNameChange { get; set; }
    }
    public class SocialMediaLinksViewModel
    {
        [JsonPropertyName(nameof(Name))]

        public string Name { get; set; }
        [JsonPropertyName(nameof(BaseUrl))]

        public string BaseUrl { get; set; }
        [JsonPropertyName(nameof(UserName))]

        public string UserName { get; set; }
        [JsonPropertyName(nameof(IsPublic))]

        public bool IsPublic { get; set; }
        public string LogoUrl { get; set; }
    }

    public class SubscriptionSettingViewModel
    {
        public SubscriptionSettingViewModel()
        {
            SubscriptionInfos = new List<ApplicationUserSubscriptionINfo>();
        }

        public AccountType AccountType { get; set; }
        public bool NSFW { get; set; }
        public List<ApplicationUserSubscriptionINfo> SubscriptionInfos { get; set; }
    }

    public class IpInfo
    {
        public string query { get; set; }
        public string status { get; set; }
        public string country { get; set; }
        public string countryCode { get; set; }
        public string region { get; set; }
        public string regionName { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public float lat { get; set; }
        public float lon { get; set; }
        public string timezone { get; set; }
        public string isp { get; set; }
        public string org { get; set; }
        public string _as { get; set; }
    }

}
