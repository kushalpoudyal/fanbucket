﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.CustomExceptions
{
    public class CommonException : Exception
    {
        public CommonException() : base()
        {
        }

        public CommonException(string message) : base(message)
        {
        }

        public CommonException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
