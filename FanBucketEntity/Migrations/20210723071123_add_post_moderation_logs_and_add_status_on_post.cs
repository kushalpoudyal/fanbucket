﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_post_moderation_logs_and_add_status_on_post : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "PostPrice",
                table: "PostMaster",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "PostMaster",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.CreateTable(
                name: "PostModerationLogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PostId = table.Column<Guid>(nullable: false),
                    OldStatus = table.Column<int>(nullable: false),
                    NewStatus = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    CreatedById = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostModerationLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostModerationLogs_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PostModerationLogs_PostMaster_PostId",
                        column: x => x.PostId,
                        principalTable: "PostMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PostReportingByUser_PostId",
                table: "PostReportingByUser",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostReportingByUser_ReportedById",
                table: "PostReportingByUser",
                column: "ReportedById");

            migrationBuilder.CreateIndex(
                name: "IX_PostModerationLogs_CreatedById",
                table: "PostModerationLogs",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PostModerationLogs_PostId",
                table: "PostModerationLogs",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_PostReportingByUser_PostMaster_PostId",
                table: "PostReportingByUser",
                column: "PostId",
                principalTable: "PostMaster",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_PostReportingByUser_ApplicationUser_ReportedById",
                table: "PostReportingByUser",
                column: "ReportedById",
                principalTable: "ApplicationUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PostReportingByUser_PostMaster_PostId",
                table: "PostReportingByUser");

            migrationBuilder.DropForeignKey(
                name: "FK_PostReportingByUser_ApplicationUser_ReportedById",
                table: "PostReportingByUser");

            migrationBuilder.DropTable(
                name: "PostModerationLogs");

            migrationBuilder.DropIndex(
                name: "IX_PostReportingByUser_PostId",
                table: "PostReportingByUser");

            migrationBuilder.DropIndex(
                name: "IX_PostReportingByUser_ReportedById",
                table: "PostReportingByUser");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "PostMaster");

            migrationBuilder.AlterColumn<long>(
                name: "PostPrice",
                table: "PostMaster",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");
        }
    }
}
