﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_conversation_report_types_and_message_report_option : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "WalletAmount",
                table: "PaymentTransactionByUser",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "MessageReportType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    DisplayValue = table.Column<string>(maxLength: 30, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Order = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageReportType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChatReportedByUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ChatId = table.Column<Guid>(nullable: false),
                    ReportedByUserId = table.Column<Guid>(nullable: false),
                    MessageReportTypeId = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(maxLength: 150, nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    IsAcive = table.Column<bool>(nullable: false),
                    DeletedStatus = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatReportedByUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChatReportedByUser_Chats_ChatId",
                        column: x => x.ChatId,
                        principalTable: "Chats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChatReportedByUser_MessageReportType_MessageReportTypeId",
                        column: x => x.MessageReportTypeId,
                        principalTable: "MessageReportType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChatReportedByUser_ApplicationUser_ReportedByUserId",
                        column: x => x.ReportedByUserId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "MessageReportType",
                columns: new[] { "Id", "Description", "DisplayValue", "IsActive", "Order", "Title" },
                values: new object[,]
                {
                    { 1, null, "Spam", true, 1, "Spam" },
                    { 2, null, "Hate Speech", true, 2, "HateSpeech" },
                    { 3, null, "Harassment", true, 3, "Harassment" },
                    { 4, null, "Illegal Activity", true, 4, "Illegal" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChatReportedByUser_ChatId",
                table: "ChatReportedByUser",
                column: "ChatId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatReportedByUser_MessageReportTypeId",
                table: "ChatReportedByUser",
                column: "MessageReportTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatReportedByUser_ReportedByUserId",
                table: "ChatReportedByUser",
                column: "ReportedByUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChatReportedByUser");

            migrationBuilder.DropTable(
                name: "MessageReportType");

            migrationBuilder.DropColumn(
                name: "WalletAmount",
                table: "PaymentTransactionByUser");
        }
    }
}
