﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_master_subscription_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubscribedNoOfDays",
                table: "SubscriptionByUser",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "UserSubscribedStatus",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    CreatedById = table.Column<Guid>(nullable: false),
                    SubscribedFrom = table.Column<DateTime>(nullable: false),
                    SubscribedTill = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSubscribedStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserSubscribedStatus_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_UserSubscribedStatus_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserSubscribedStatus_CreatedById",
                table: "UserSubscribedStatus",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UserSubscribedStatus_UserId",
                table: "UserSubscribedStatus",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserSubscribedStatus");

            migrationBuilder.DropColumn(
                name: "SubscribedNoOfDays",
                table: "SubscriptionByUser");
        }
    }
}
