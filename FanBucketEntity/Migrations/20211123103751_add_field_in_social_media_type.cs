﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_field_in_social_media_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LogoUrl",
                table: "SocialMediaType",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LogoUrl",
                table: "SocialMediaType");
        }
    }
}
