﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class UserDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationUserDetails",
                table: "ApplicationUserDetails");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationUserDetails_UserId",
                table: "ApplicationUserDetails");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ApplicationUserDetails");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationUserDetails",
                table: "ApplicationUserDetails",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationUserDetails",
                table: "ApplicationUserDetails");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "ApplicationUserDetails",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationUserDetails",
                table: "ApplicationUserDetails",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserDetails_UserId",
                table: "ApplicationUserDetails",
                column: "UserId");
        }
    }
}
