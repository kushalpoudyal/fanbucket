﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class OtherEntityChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SubscriptionByUser",
                table: "SubscriptionByUser");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubscriptionByUser",
                table: "SubscriptionByUser",
                columns: new[] { "SubscriptionId", "PaidByUserId", "TranId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SubscriptionByUser",
                table: "SubscriptionByUser");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubscriptionByUser",
                table: "SubscriptionByUser",
                columns: new[] { "SubscriptionId", "PaidByUserId" });
        }
    }
}
