﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_notification_settings_and_featured_image_column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EmailNotification",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "FeaturedImageLeft",
                table: "ApplicationUser",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FeaturedImageRight",
                table: "ApplicationUser",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasBookmarkAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasFollowerAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasInformationUpdateAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasLoginAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasLoveReactAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasMentionAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasMessageAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasShareAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasSubscriptionAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasTipAlert",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmailNotification",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "FeaturedImageLeft",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "FeaturedImageRight",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasBookmarkAlert",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasFollowerAlert",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasInformationUpdateAlert",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasLoginAlert",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasLoveReactAlert",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasMentionAlert",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasMessageAlert",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasShareAlert",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasSubscriptionAlert",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "HasTipAlert",
                table: "ApplicationUser");
        }
    }
}
