﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class paymentresponse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ResponseCode",
                table: "PaymentTransactionByUser",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TranRemarks",
                table: "PaymentTransactionByUser",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "PaidAmount",
                table: "PaymentPostByUser",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AddColumn<Guid>(
                name: "TranId",
                table: "PaymentPostByUser",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ResponseCode",
                table: "PaymentTransactionByUser");

            migrationBuilder.DropColumn(
                name: "TranRemarks",
                table: "PaymentTransactionByUser");

            migrationBuilder.DropColumn(
                name: "TranId",
                table: "PaymentPostByUser");

            migrationBuilder.AlterColumn<decimal>(
                name: "PaidAmount",
                table: "PaymentPostByUser",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");
        }
    }
}
