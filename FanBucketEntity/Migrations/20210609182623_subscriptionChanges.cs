﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class subscriptionChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DeactivatedOn",
                table: "SubscriptionByUser",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PaidForUserId",
                table: "SubscriptionByUser",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "Record_Status",
                table: "SubscriptionByUser",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeactivatedOn",
                table: "SubscriptionByUser");

            migrationBuilder.DropColumn(
                name: "PaidForUserId",
                table: "SubscriptionByUser");

            migrationBuilder.DropColumn(
                name: "Record_Status",
                table: "SubscriptionByUser");
        }
    }
}
