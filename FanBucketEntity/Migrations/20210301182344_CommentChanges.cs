﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class CommentChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PostComments_PostMaster_PostId",
                table: "PostComments");
 
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PostComments_PostId",
                table: "PostComments",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_PostComments_PostMaster_PostId",
                table: "PostComments",
                column: "PostId",
                principalTable: "PostMaster",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
