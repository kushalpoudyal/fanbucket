﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class Subscription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubscriptionByUser_PaymentTransactionByUser_TranId",
                table: "SubscriptionByUser");

            migrationBuilder.DropIndex(
                name: "IX_SubscriptionByUser_TranId",
                table: "SubscriptionByUser");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionByUser_TranId",
                table: "SubscriptionByUser",
                column: "TranId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubscriptionByUser_PaymentTransactionByUser_TranId",
                table: "SubscriptionByUser",
                column: "TranId",
                principalTable: "PaymentTransactionByUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
