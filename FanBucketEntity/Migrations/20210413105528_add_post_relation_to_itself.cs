﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_post_relation_to_itself : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<Guid>(
                name: "ParentPostId",
                table: "PostMaster",
                nullable: true);

          
            migrationBuilder.CreateIndex(
                name: "IX_PostMaster_ParentPostId",
                table: "PostMaster",
                column: "ParentPostId");

           
            migrationBuilder.AddForeignKey(
                name: "FK_PostMaster_PostMaster_ParentPostId",
                table: "PostMaster",
                column: "ParentPostId",
                principalTable: "PostMaster",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PostMaster_PostMaster_ParentPostId",
                table: "PostMaster");

            migrationBuilder.DropIndex(
                name: "IX_PostMaster_ParentPostId",
                table: "PostMaster");

            migrationBuilder.DropColumn(
                name: "ParentPostId",
                table: "PostMaster");
        }
    }
}
