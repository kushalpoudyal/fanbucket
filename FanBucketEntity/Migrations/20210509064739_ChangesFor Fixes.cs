﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class ChangesForFixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "DeletedStatus",
                table: "PostCollection",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Record_Status",
                table: "PostCollection",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Record_Status",
                table: "AppNotifications",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletedStatus",
                table: "PostCollection");

            migrationBuilder.DropColumn(
                name: "Record_Status",
                table: "PostCollection");

            migrationBuilder.DropColumn(
                name: "Record_Status",
                table: "AppNotifications");
        }
    }
}
