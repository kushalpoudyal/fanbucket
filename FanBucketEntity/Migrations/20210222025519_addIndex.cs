﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class addIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
               name: "IX_Message_ChatId",
               table: "Message",
               column: "ChatId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
