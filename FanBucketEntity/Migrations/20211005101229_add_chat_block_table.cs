﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_chat_block_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ChatUserBlockedByUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    ChatId = table.Column<Guid>(nullable: false),
                    BlockedDate = table.Column<DateTime>(nullable: false),
                    BlockedByUserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatUserBlockedByUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChatUserBlockedByUser_ApplicationUser_BlockedByUserId",
                        column: x => x.BlockedByUserId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChatUserBlockedByUser_Chats_ChatId",
                        column: x => x.ChatId,
                        principalTable: "Chats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChatUserBlockedByUser_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChatUserBlockedByUser_BlockedByUserId",
                table: "ChatUserBlockedByUser",
                column: "BlockedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatUserBlockedByUser_ChatId",
                table: "ChatUserBlockedByUser",
                column: "ChatId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatUserBlockedByUser_UserId",
                table: "ChatUserBlockedByUser",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChatUserBlockedByUser");
        }
    }
}
