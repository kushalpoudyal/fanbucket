﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class PaymentUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Stripe",
                table: "PaymentTransactionByUser",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "DuesRemaining",
                table: "PaymentCreditByUser",
                type: "decimal(18,4)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalEarnings",
                table: "PaymentCreditByUser",
                type: "decimal(18,4)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "PaymentEarningsByUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TranId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    TranDate = table.Column<DateTime>(nullable: false),
                    TranRemarks = table.Column<string>(maxLength: 1000, nullable: true),
                    Stripe = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentEarningsByUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StripePaymentByUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TranId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    TranDate = table.Column<DateTime>(nullable: false),
                    DebitCredit = table.Column<string>(maxLength: 1, nullable: true),
                    TranRemarks = table.Column<string>(maxLength: 1000, nullable: true),
                    ResponseCode = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StripePaymentByUser", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaymentEarningsByUser");

            migrationBuilder.DropTable(
                name: "StripePaymentByUser");

            migrationBuilder.DropColumn(
                name: "Stripe",
                table: "PaymentTransactionByUser");

            migrationBuilder.DropColumn(
                name: "DuesRemaining",
                table: "PaymentCreditByUser");

            migrationBuilder.DropColumn(
                name: "TotalEarnings",
                table: "PaymentCreditByUser");
        }
    }
}
