﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_featured_badge : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "FeaturedBadgeId",
                table: "ApplicationUser",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUser_FeaturedBadgeId",
                table: "ApplicationUser",
                column: "FeaturedBadgeId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUser_BadgeInfo_FeaturedBadgeId",
                table: "ApplicationUser",
                column: "FeaturedBadgeId",
                principalTable: "BadgeInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUser_BadgeInfo_FeaturedBadgeId",
                table: "ApplicationUser");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationUser_FeaturedBadgeId",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "FeaturedBadgeId",
                table: "ApplicationUser");
        }
    }
}
