﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class UserSubscriptionModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SubscriptionByUser",
                table: "SubscriptionByUser");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "SubscriptionByUser");

            migrationBuilder.AddColumn<DateTime>(
                name: "SubscribedOn",
                table: "SubscriptionByUser",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "SubscribedTill",
                table: "SubscriptionByUser",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubscriptionByUser",
                table: "SubscriptionByUser",
                columns: new[] { "SubscriptionId", "PaidByUserId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SubscriptionByUser",
                table: "SubscriptionByUser");

            migrationBuilder.DropColumn(
                name: "SubscribedOn",
                table: "SubscriptionByUser");

            migrationBuilder.DropColumn(
                name: "SubscribedTill",
                table: "SubscriptionByUser");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "SubscriptionByUser",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubscriptionByUser",
                table: "SubscriptionByUser",
                column: "Id");
        }
    }
}
