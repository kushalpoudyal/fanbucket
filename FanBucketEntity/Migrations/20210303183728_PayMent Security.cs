﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class PayMentSecurity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PaymentCreditByUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Currency = table.Column<string>(maxLength: 3, nullable: true),
                    CurrentBalance = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    CreditSum = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    DebitSum = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentCreditByUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentCreditByUser_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentTransactionByUser",
                columns: table => new
                {
                    TranId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    TranDate = table.Column<DateTime>(nullable: false),
                    DebitCredit = table.Column<string>(maxLength: 1, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTransactionByUser", x => x.TranId);
                });

            migrationBuilder.CreateTable(
                name: "PaymentUPIN",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    PINHash = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentUPIN", x => x.UserId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PaymentCreditByUser_UserId",
                table: "PaymentCreditByUser",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaymentCreditByUser");

            migrationBuilder.DropTable(
                name: "PaymentTransactionByUser");

            migrationBuilder.DropTable(
                name: "PaymentUPIN");
        }
    }
}
