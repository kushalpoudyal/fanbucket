﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class pReactChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PostReacts_PostMaster_PostId",
                table: "PostReacts");

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PostReacts_PostId",
                table: "PostReacts",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_PostReacts_PostMaster_PostId",
                table: "PostReacts",
                column: "PostId",
                principalTable: "PostMaster",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
