﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class postactivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PostBookmark",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PostId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    BookmarkedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostBookmark", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PostCollection",
                columns: table => new
                {
                    ContentId = table.Column<Guid>(nullable: false),
                    PostId = table.Column<Guid>(nullable: false),
                    FileLocation = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostCollection", x => x.ContentId);
                    table.ForeignKey(
                        name: "FK_PostCollection_PostMaster_PostId",
                        column: x => x.PostId,
                        principalTable: "PostMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostComments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedById = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    Record_Status = table.Column<int>(nullable: false),
                    Deleted_Status = table.Column<bool>(nullable: false),
                    Comment = table.Column<string>(maxLength: 500, nullable: true),
                    PostId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostComments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostComments_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PostComments_PostMaster_PostId",
                        column: x => x.PostId,
                        principalTable: "PostMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostOption",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    OptionName = table.Column<string>(maxLength: 500, nullable: true),
                    Answer = table.Column<bool>(nullable: false),
                    PostId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostOption", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostOption_PostMaster_PostId",
                        column: x => x.PostId,
                        principalTable: "PostMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostOptionByUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    OptionId = table.Column<Guid>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostOptionByUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PostReacts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedById = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    Record_Status = table.Column<int>(nullable: false),
                    Deleted_Status = table.Column<bool>(nullable: false),
                    ReactType = table.Column<int>(nullable: false),
                    PostId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostReacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostReacts_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PostReacts_PostMaster_PostId",
                        column: x => x.PostId,
                        principalTable: "PostMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostSharing",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedById = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    Record_Status = table.Column<int>(nullable: false),
                    Deleted_Status = table.Column<bool>(nullable: false),
                    SharedComment = table.Column<string>(maxLength: 500, nullable: true),
                    PostId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostSharing", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostSharing_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_PostSharing_PostMaster_PostId",
                        column: x => x.PostId,
                        principalTable: "PostMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PostCollection_PostId",
                table: "PostCollection",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostComments_CreatedById",
                table: "PostComments",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PostComments_PostId",
                table: "PostComments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostOption_PostId",
                table: "PostOption",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostReacts_CreatedById",
                table: "PostReacts",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PostReacts_PostId",
                table: "PostReacts",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostSharing_CreatedById",
                table: "PostSharing",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PostSharing_PostId",
                table: "PostSharing",
                column: "PostId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PostBookmark");

            migrationBuilder.DropTable(
                name: "PostCollection");

            migrationBuilder.DropTable(
                name: "PostComments");

            migrationBuilder.DropTable(
                name: "PostOption");

            migrationBuilder.DropTable(
                name: "PostOptionByUser");

            migrationBuilder.DropTable(
                name: "PostReacts");

            migrationBuilder.DropTable(
                name: "PostSharing");
        }
    }
}
