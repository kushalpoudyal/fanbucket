﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class bookmark : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PostBookmark",
                table: "PostBookmark");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PostBookmark",
                table: "PostBookmark",
                columns: new[] { "PostId", "UserId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PostBookmark",
                table: "PostBookmark");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PostBookmark",
                table: "PostBookmark",
                column: "Id");
        }
    }
}
