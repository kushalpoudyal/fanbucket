﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class PaymentSecuritys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ErrorCount",
                table: "PaymentUPIN",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "LockTime",
                table: "PaymentUPIN",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Locked",
                table: "PaymentUPIN",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ErrorCount",
                table: "PaymentUPIN");

            migrationBuilder.DropColumn(
                name: "LockTime",
                table: "PaymentUPIN");

            migrationBuilder.DropColumn(
                name: "Locked",
                table: "PaymentUPIN");
        }
    }
}
