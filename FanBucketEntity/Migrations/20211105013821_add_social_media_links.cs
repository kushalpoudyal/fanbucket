﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_social_media_links : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SocialMediaType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DisplayText = table.Column<string>(maxLength: 100, nullable: true),
                    Value = table.Column<string>(maxLength: 100, nullable: true),
                    BaseUrl = table.Column<string>(maxLength: 100, nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    IsHidden = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SocialMediaType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserSocialMediaLinks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    SocialMediaTypeId = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(maxLength: 100, nullable: true),
                    IsPublic = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSocialMediaLinks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserSocialMediaLinks_SocialMediaType_SocialMediaTypeId",
                        column: x => x.SocialMediaTypeId,
                        principalTable: "SocialMediaType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserSocialMediaLinks_SocialMediaTypeId",
                table: "UserSocialMediaLinks",
                column: "SocialMediaTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserSocialMediaLinks");

            migrationBuilder.DropTable(
                name: "SocialMediaType");
        }
    }
}
