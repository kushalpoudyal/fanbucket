﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class ScheduledNsfw : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUser_BadgeInfo_FeaturedBadgeId",
                table: "ApplicationUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PostOptionByUser",
                table: "PostOptionByUser");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "PostOptionByUser");

            migrationBuilder.DropColumn(
                name: "Comment",
                table: "PostOptionByUser");

            migrationBuilder.AlterColumn<Guid>(
                name: "OptionId",
                table: "PostOptionByUser",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PostId",
                table: "PostOptionByUser",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "Record_Status",
                table: "PostOptionByUser",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "NSFW",
                table: "PostMaster",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<Guid>(
                name: "FeaturedBadgeId",
                table: "ApplicationUser",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PostOptionByUser",
                table: "PostOptionByUser",
                columns: new[] { "UserId", "OptionId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUser_BadgeInfo_FeaturedBadgeId",
                table: "ApplicationUser",
                column: "FeaturedBadgeId",
                principalTable: "BadgeInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUser_BadgeInfo_FeaturedBadgeId",
                table: "ApplicationUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PostOptionByUser",
                table: "PostOptionByUser");

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "PostOptionByUser");

            migrationBuilder.DropColumn(
                name: "Record_Status",
                table: "PostOptionByUser");

            migrationBuilder.DropColumn(
                name: "NSFW",
                table: "PostMaster");

            migrationBuilder.AlterColumn<Guid>(
                name: "OptionId",
                table: "PostOptionByUser",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "PostOptionByUser",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "PostOptionByUser",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "FeaturedBadgeId",
                table: "ApplicationUser",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PostOptionByUser",
                table: "PostOptionByUser",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUser_BadgeInfo_FeaturedBadgeId",
                table: "ApplicationUser",
                column: "FeaturedBadgeId",
                principalTable: "BadgeInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
