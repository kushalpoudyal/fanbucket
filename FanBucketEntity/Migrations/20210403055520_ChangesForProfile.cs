﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class ChangesForProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AccountType",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "CoverImgLocation",
                table: "ApplicationUser",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ApplicationUserDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Location = table.Column<string>(maxLength: 500, nullable: true),
                    LatitudeCoordinate = table.Column<string>(maxLength: 50, nullable: true),
                    LongitudeCoordinate = table.Column<string>(maxLength: 50, nullable: true),
                    ShortBio = table.Column<string>(maxLength: 1000, nullable: true),
                    Profession = table.Column<string>(maxLength: 100, nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUserDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicationUserDetails_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationUserSubscriptionInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    SubscriptionAmount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    Tenure = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Record_Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUserSubscriptionInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicationUserSubscriptionInfo_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostMentions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PostId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostMentions", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserDetails_UserId",
                table: "ApplicationUserDetails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserSubscriptionInfo_UserId",
                table: "ApplicationUserSubscriptionInfo",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationUserDetails");

            migrationBuilder.DropTable(
                name: "ApplicationUserSubscriptionInfo");

            migrationBuilder.DropTable(
                name: "PostMentions");

            migrationBuilder.DropColumn(
                name: "AccountType",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "CoverImgLocation",
                table: "ApplicationUser");
        }
    }
}
