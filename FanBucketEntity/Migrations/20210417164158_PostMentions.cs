﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class PostMentions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PostMoments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PostId = table.Column<Guid>(nullable: true),
                    FileLocation = table.Column<string>(maxLength: 100, nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    CreatedById = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostMoments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostMoments_PostMaster_PostId",
                        column: x => x.PostId,
                        principalTable: "PostMaster",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateIndex(
                name: "IX_PostMoment_PostId",
                table: "PostMoments",
                column: "PostId");

            migrationBuilder.CreateIndex(
               name: "IX_PostMoments_PostId",
               table: "PostMoments",
               column: "StartDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationUserKYC");

            migrationBuilder.DropTable(
                name: "ApplicationUserSubscriptionInfo");

            migrationBuilder.DropTable(
                name: "PostMentions");

            migrationBuilder.DropTable(
                name: "PostMoments");

            migrationBuilder.DropTable(
                name: "SubscriptionByUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationUserDetails",
                table: "ApplicationUserDetails");

            migrationBuilder.DropColumn(
                name: "EarningPercentage",
                table: "PaymentCreditByUser");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "ApplicationUserDetails",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationUserDetails",
                table: "ApplicationUserDetails",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserDetails_UserId",
                table: "ApplicationUserDetails",
                column: "UserId");
        }
    }
}
