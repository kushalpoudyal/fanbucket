﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class ChangesforPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PaymentTransactionByUser",
                table: "PaymentTransactionByUser");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "PaymentTransactionByUser",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_PaymentTransactionByUser",
                table: "PaymentTransactionByUser",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PaymentTransactionByUser",
                table: "PaymentTransactionByUser");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "PaymentTransactionByUser");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PaymentTransactionByUser",
                table: "PaymentTransactionByUser",
                column: "TranId");
        }
    }
}
