﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_signup_queue_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SignUpQueue",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 350, nullable: false),
                    LastName = table.Column<string>(maxLength: 350, nullable: false),
                    EmailAddress = table.Column<string>(maxLength: 150, nullable: false),
                    ContentStyle = table.Column<string>(maxLength: 500, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    FollowersAndPlatfrom = table.Column<string>(maxLength: 1000, nullable: false),
                    SocialMediaLinks = table.Column<string>(maxLength: 1000, nullable: false),
                    CSMReason = table.Column<string>(maxLength: 1000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SignUpQueue", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SignUpQueue");
        }
    }
}
