﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class PostChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaidPost",
                table: "PostMaster");

            migrationBuilder.DropColumn(
                name: "PostType",
                table: "PostMaster");

            migrationBuilder.AddColumn<bool>(
                name: "IsMedia",
                table: "PostMaster",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PaidPostType",
                table: "PostMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "PaidContent",
                table: "PostCollection",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PostType",
                table: "PostCollection",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "AppNotifications",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ActivityId = table.Column<Guid>(nullable: false),
                    ActivityUserId = table.Column<Guid>(nullable: false),
                    PKId = table.Column<Guid>(nullable: false),
                    nType = table.Column<int>(nullable: false),
                    PostCreatorId = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Viewed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppNotifications", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppNotifications");

            migrationBuilder.DropColumn(
                name: "IsMedia",
                table: "PostMaster");

            migrationBuilder.DropColumn(
                name: "PaidPostType",
                table: "PostMaster");

            migrationBuilder.DropColumn(
                name: "PaidContent",
                table: "PostCollection");

            migrationBuilder.DropColumn(
                name: "PostType",
                table: "PostCollection");

            migrationBuilder.AddColumn<bool>(
                name: "PaidPost",
                table: "PostMaster",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PostType",
                table: "PostMaster",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
