﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_user_status_and_commision_per_change_log : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "BlockTill",
                table: "ApplicationUser",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "ApplicationUser",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.CreateTable(
                name: "UserCommissionReviewLogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    OldCommissionPer = table.Column<decimal>(nullable: false),
                    NewCommissionPer = table.Column<decimal>(nullable: false),
                    Remarks = table.Column<string>(maxLength: 255, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ReviewedBy = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCommissionReviewLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserCommissionReviewLogs_ApplicationUser_ReviewedBy",
                        column: x => x.ReviewedBy,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_UserCommissionReviewLogs_ApplicationUser_UserId",
                        column: x => x.UserId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserCommissionReviewLogs_ReviewedBy",
                table: "UserCommissionReviewLogs",
                column: "ReviewedBy");

            migrationBuilder.CreateIndex(
                name: "IX_UserCommissionReviewLogs_UserId",
                table: "UserCommissionReviewLogs",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserCommissionReviewLogs");

            migrationBuilder.DropColumn(
                name: "BlockTill",
                table: "ApplicationUser");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "ApplicationUser");
        }
    }
}
