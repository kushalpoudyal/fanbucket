﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class payment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.CreateTable(
                name: "SubscriptionByUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SubscriptionId = table.Column<Guid>(nullable: false),
                    PaidByUserId = table.Column<Guid>(nullable: false),
                    TranId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubscriptionByUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubscriptionByUser_PaymentTransactionByUser_TranId",
                        column: x => x.TranId,
                        principalTable: "PaymentTransactionByUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });


            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionByUser_TranId",
                table: "SubscriptionByUser",
                column: "TranId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubscriptionByUser");

            migrationBuilder.DropTable(
                name: "UserBadges");

            migrationBuilder.DropTable(
                name: "UserInterests");

            migrationBuilder.DropTable(
                name: "BadgeInfo");

            migrationBuilder.DropTable(
                name: "Interest");
        }
    }
}
