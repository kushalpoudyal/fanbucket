﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FanBucketEntity.Migrations
{
    public partial class add_fields_in_kyc_details : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "ApplicationUserKYC",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "ApplicationUserKYC",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "ApplicationUserKYC",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MiddleName",
                table: "ApplicationUserKYC",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "ApplicationUserKYC");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "ApplicationUserKYC");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "ApplicationUserKYC");

            migrationBuilder.DropColumn(
                name: "MiddleName",
                table: "ApplicationUserKYC");
        }
    }
}
