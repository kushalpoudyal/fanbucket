﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.Rendering
{
    public class SelectListVal
    {
        public string title { get; set; }
        public string value { get; set; }
    }
}
