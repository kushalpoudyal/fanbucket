﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace FanBucketEntity.Model
{
    public class Subscriptions
    {
        public Guid Id { get; set; }

        public string Code { get; set; }
        public string Description { get; set; }

        public SubscriptionType SubscriptionType { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public Record_Status Record_Status { get; set; }

        public bool Deleted_Status { get; set; }


    }
}
