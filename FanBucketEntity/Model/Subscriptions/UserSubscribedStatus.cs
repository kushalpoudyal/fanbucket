﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.Payments;
using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model
{
    public class UserSubscribedStatus
    {
        [Key]
        public Guid Id { get; set; }
        /// <summary>
        /// Subscribed To UserId
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// Subscribed By Id
        /// </summary>
        public Guid CreatedById { get; set; }
        public DateTime SubscribedFrom { get; set; }
        public DateTime SubscribedTill { get; set; }
        public Record_Status Status { get; set; }
        [ForeignKey(nameof(UserId))]

        public virtual ApplicationUser SubscribedUser { get; set; }
        [ForeignKey(nameof(CreatedById))]
        public virtual ApplicationUser SubscribedByUser { get; set; }

    }
}
