﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.Payments;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model
{
    public class SubscriptionByUser
    {

        public Guid SubscriptionId { get; set; }
        public Guid PaidByUserId { get; set; }
        public Guid PaidForUserId { get; set; }
        public Guid TranId { get; set; }
        public DateTime SubscribedOn { get; set; }
        public int SubscribedNoOfDays { get; set; }

        public DateTime SubscribedTill { get; set; }
        public Record_Status Record_Status { get; set; }

        public DateTime? DeactivatedOn { get; set; }

    }
}
