﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketUI.RequestModel
{
    public class RegisterClient
    {
        public string Email { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string Fullname { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
    }
}
