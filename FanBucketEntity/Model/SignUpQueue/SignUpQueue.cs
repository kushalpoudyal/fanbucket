﻿using FanBucketEntity.Model;
using FanBucketEntity.Model.Chat;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.UserDetails;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Models.UserDetails
{
    public class SignUpQueue
    {
        [Key]
        public long Id { get; set; }
        #region BasicInfo
        [StringLength(350)]
        [Required]
        public string FirstName { get; set; }
        [StringLength(350)]
        [Required]

        public string LastName { get; set; }

        [StringLength(150)]
        [Required]
        public string EmailAddress { get; set; }

        [StringLength(500)]
        [Required]
        //Multiple content creation types are joined by commas
        public string ContentStyle { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public bool Deleted { get; set; }

        public Record_Status Status { get; set; }
        #endregion

        #region SocialMediaPresence
        [StringLength(1000)]
        [Required]
        //Subscriber/Followers count? and Platform?
        public string FollowersAndPlatfrom { get; set; }
        [StringLength(1000)]
        [Required]
        //Social Media Links
        public string SocialMediaLinks { get; set; }
        [StringLength(1000)]
        [Required]
        //Customer Success Managers (CSM) are limited, let us know why you would like one?       
        public string CSMReason { get; set; }
        #endregion


    }
}