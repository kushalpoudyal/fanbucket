﻿using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.UserDetails
{
    public class ApplicationUserDetails
    {

        public Guid UserId { get; set; }
        [StringLength(500)]
        public string Location { get; set; }
        [StringLength(50)]

        public string LatitudeCoordinate { get; set; }
        [StringLength(50)]

        public string LongitudeCoordinate { get; set; }
        [StringLength(1000)]

        public string ShortBio { get; set; }

        [StringLength(100)]
        public string Profession { get; set; }



        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }



    }
}
