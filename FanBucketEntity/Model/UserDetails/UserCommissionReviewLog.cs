﻿using FanBucketEntity.Models.UserDetails;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Model.UserDetails
{
    public class UserCommissionReviewLog
    {
        public Guid Id { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public decimal OldCommissionPer { get; set; }

        [Required]
        public decimal NewCommissionPer { get; set; }

        [Required]
        [StringLength(255)]
        public string Remarks { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        [Required]
        public Guid ReviewedBy { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("UserCommissionReviewLog")]
        public ApplicationUser ReviewedUser { get; set; }

        [ForeignKey(nameof(ReviewedBy))]
        public ApplicationUser CreatedByUser { get; set; }
    }
}