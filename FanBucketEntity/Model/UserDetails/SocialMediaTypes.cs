﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketEntity.Models.UserDetails
{

    public class SocialMediaType
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string DisplayText { get; set; }
        [StringLength(100)]
        public string Value { get; set; }
        [StringLength(100)]
        public string BaseUrl { get; set; }
        public int SortOrder { get; set; }
        public bool IsHidden { get; set; }
        [StringLength(100)]

        public string LogoUrl { get; set; }
    }
    public class UserSocialMediaLinks
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int SocialMediaTypeId { get; set; }
        [StringLength(100)]
        public string UserName { get; set; }
        public bool IsPublic { get; set; }
        [ForeignKey(nameof(SocialMediaTypeId))]
        public virtual SocialMediaType SocialMediaType { get; set; }
        //[ForeignKey(nameof(UserId))]
        //public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
