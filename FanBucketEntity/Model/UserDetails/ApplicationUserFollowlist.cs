﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.UserDetails
{
    public class ApplicationUserFollowlist

    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Guid FollowedBy { get; set; }

        public DateTime FollowedFrom { get; set; }

        [Required]
        public Guid SubId { get; set; }

        public Record_Status Record_Status { get; set; }

        public DateTime ModifiedOn { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
        [ForeignKey("SubId")]
        public virtual Subscriptions Subscriptions { get; set; }
    }


}
