﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.UserDetails
{
    public class ApplicationUserSubscriptionINfo
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }

        public decimal SubscriptionAmount { get; set; }

        public int Tenure { get; set; }

        public DateTime CreatedOn { get; set; }

        public Record_Status Record_Status { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }

}
