﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FanBucketEntity.Model
{
    public class BadgeInfo : BaseEntity
    {
        public BadgeInfo()
        {
            UserBadges = new List<UserBadges>();
        }
        [StringLength(150)]
        [Required]
        public string Name { get; set; }
        [StringLength(50)]
        [Required]
        public string Code { get; set; }
        [StringLength(1000)]
        [Required]
        public string Description { get; set; }
        public BadgeTriggerType TriggerType { get; set; }
        [StringLength(1000)]
        public string TriggeredOn { get; set; }

        [StringLength(500)]
        public string ImageUrl { get; set; }

        public ICollection<UserBadges> UserBadges { get; set; }

    }
}
