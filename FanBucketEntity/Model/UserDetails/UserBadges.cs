﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.UserDetails
{
    public class UserBadges
    {
        [Key]
        [Required]
        public Guid Id { get; set; }

        public Guid CreatedById { get; set; }


        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public Record_Status Record_Status { get; set; }

        public bool Deleted_Status { get; set; }

        [Required]
        public Guid UserId { get; set; }
        [Required]
        public Guid BadgeId { get; set; }


        [ForeignKey("BadgeId")]
        public virtual BadgeInfo BadgeInfo { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser1 { get; set; }
        [ForeignKey("CreatedById")]
        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}
