﻿using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.UserDetails
{
    public class ApplicationUserKYC
    {

        public Guid UserId { get; set; }
        [StringLength(100)]
        public string Identification { get; set; }
        [StringLength(100)]
        public string IdNO { get; set; }
        [StringLength(100)]
        public string IdIssuedFrom { get; set; }
        [StringLength(100)]
        public string Country { get; set; }
        [StringLength(100)]
        public string ContactNumber { get; set; }
        public DateTime DOB { get; set; }
        [StringLength(10)]
        public string Gender { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string MiddleName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(150)]
        public string Address { get; set; }
        [StringLength(1000)]
        public string IdentificationFileLocation { get; set; }
    }
}
