﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.UserDetails
{
    public class UserInfo
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public DateTime CreatedOn { get; set; }

    }
}
