﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FanBucketEntity.Model
{
    public class Interest : BaseEntity
    {
        public Interest()
        {
            UserBadges = new List<UserBadges>();
            UserInterests = new List<UserInterests>();
        }
        [StringLength(150)]
        [Required]
        public string Name { get; set; }
        [StringLength(20)]
        [Required]
        public string Code { get; set; }
        [StringLength(1000)]
        public string Description { get; set; }

        public ICollection<UserBadges> UserBadges { get; set; }
        public ICollection<UserInterests> UserInterests { get; set; }

    }
}
