﻿using FanBucketEntity.Model;
using FanBucketEntity.Model.Chat;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.UserDetails;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Models.UserDetails
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public ApplicationUser() : base()
        {
            UserCommissionReviewLog = new List<UserCommissionReviewLog>();
            UserModerationLog = new List<UserModerationLog>();
            ChatUser = new List<ChatUser>();
            this.ModifiedOn = DateTime.Now;
        }

        [StringLength(350)]
        public string Fullname { get; set; }

        [StringLength(150)]
        public string Contact { get; set; }

        [StringLength(150)]
        public string Country { get; set; }

        [StringLength(150)]
        public string Organization { get; set; }

        [StringLength(150)]
        public string WorkTitle { get; set; }

        [StringLength(1000)]
        public string ProfileImgLocation { get; set; }

        [StringLength(1000)]
        public string CoverImgLocation { get; set; }

        public bool IsPasswordUpdated { get; set; }

        public bool HasSubscribed { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public AccountType AccountType { get; set; }
        public Guid? FeaturedBadgeId { get; set; }

        public bool NSFW { get; set; }

        public Record_Status Status { get; set; }

        public DateTime? BlockTill { get; set; }

        public int UserNameModifiedTimes { get; set; }
        public DateTime? LastUserNameModifiedDate { get; set; }
        #region UserPrefrences
        public bool EmailNotification { get; set; }
        public bool HasLoginAlert { get; set; }
        public bool HasInformationUpdateAlert { get; set; }
        public bool HasMessageAlert { get; set; }
        public bool HasTipAlert { get; set; }
        public bool HasSubscriptionAlert { get; set; }
        public bool HasFollowerAlert { get; set; }
        public bool HasShareAlert { get; set; }
        public bool HasMentionAlert { get; set; }
        public bool HasBookmarkAlert { get; set; }
        public bool HasLoveReactAlert { get; set; }

        #endregion
        [StringLength(1000)]

        public string FeaturedImageLeft { get; set; }
        [StringLength(1000)]

        public string FeaturedImageRight { get; set; }
        //[InverseProperty("UserId")]
        public ICollection<UserCommissionReviewLog> UserCommissionReviewLog { get; set; }

        public ICollection<UserModerationLog> UserModerationLog { get; set; }

        public ICollection<ChatUser> ChatUser { get; set; }

        [ForeignKey("FeaturedBadgeId")]
        public BadgeInfo BadgeInfo { get; set; }
    }
}