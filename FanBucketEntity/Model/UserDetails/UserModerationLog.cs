﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.UserDetails
{
    public class UserModerationLog
    {
        public Guid Id { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public EnumReportAction Action { get; set; }
        public EnumDays BlockDays { get; set; }

        public int BlockedNumberOfDays { get; set; }

        [Required]
        [StringLength(255)]
        public string Remarks { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        [Required]
        public Guid CreatedById { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("UserModerationLog")]
        public ApplicationUser ModeratedUser { get; set; }

        [ForeignKey(nameof(CreatedById))]
        public ApplicationUser CreatedBy { get; set; }
    }

}
