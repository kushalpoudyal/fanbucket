﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FanBucketEntity.Model.UserDetails
{
    public class CustomerPaymentDetails
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        [StringLength(100)]
        public string CardName { get; set; }
        [StringLength(16)]
        public string CardNumber { get; set; }
        public DateTime Expiry { get; set; }
        [StringLength(3)]
        public string CVV { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
