﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Text;

namespace FanBucketEntity.Model
{
    public class BaseEntity
    {
        [Key]
        [Required]
        public Guid Id { get; set; }

        public Guid CreatedById { get; set; }

         
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public Record_Status Record_Status { get; set; }

        public bool Deleted_Status { get; set; }

        [ForeignKey("CreatedById")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
