﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.ENUM
{
    public enum TransactionResponseCode
    {
        OK = 100,
        IncorrectPIN = 200,
        InsufficientBalance = 300,
        ServerError = 400,
        UnAuthorized = 500,
        PinLocked = 600,
        PinRequired = 700,
        PinNotSet=800
    }
}
