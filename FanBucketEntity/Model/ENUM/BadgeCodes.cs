﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.ENUM
{
    public enum BadgeCodes
    {
        NewUser = 1,
        Verified = 2,
        ContentCreator = 3,
        Contributor = 4
    }
}
