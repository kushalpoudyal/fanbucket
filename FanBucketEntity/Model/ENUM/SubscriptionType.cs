﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.ENUM
{
    public enum SubscriptionType
    {
        Free = 1,
        Paid,
        Trial
    }
}
