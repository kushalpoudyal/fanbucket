﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.ENUM
{
    public enum NotificationType
    {
        Commented = 1,
        Liked,
        Mentioned,
        Subscribed,
        Paid,
        Following,
        Promotions,
        Auto,Tips
    }
}
