﻿using System.ComponentModel.DataAnnotations;

namespace FanBucketEntity.Model.ENUM
{
    public enum Record_Status
    {
        Active = 1,
        Inactive,
        Hidden,
        Reported
    }

    public enum EnumUserStatus
    {
        Active = 1,
        Inactive,
        Blocked,
    }

    public enum EnumReportAction
    {
        Ignore = 1,
        TemporaryBlock,
        PermanentBlock,
    }

    public enum EnumDays
    {
        [Display(Name = "1 Day")]
        OneDay = 1,

        [Display(Name = "7 Days")]
        SevenDays,

        [Display(Name = "15 Days")]
        FifthteenDays,

        [Display(Name = "1 Month")]
        OneMonth,

        [Display(Name = "3 Months")]
        ThreeMonths,

        [Display(Name = "6 Months")]
        SixMonths,

        Permanent
    }
}