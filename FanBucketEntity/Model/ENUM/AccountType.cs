﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.ENUM
{
    public enum AccountType
    {
        Public = 1,
        Private,
        Superadmin
    }
}
