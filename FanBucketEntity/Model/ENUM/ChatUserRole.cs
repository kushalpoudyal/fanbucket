﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.ENUM
{
    public enum ChatUserRole
    {

        Admin,
        Member,
        Guest

    }
}
