﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.ENUM
{
    public enum Privacy
    {
        Public=1,
        OnlyMe,
        Paid
    }
}
