﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.ENUM
{
    public enum ContentTypes
    {
        Text = 1,
        File,
        Image,
        Audio,
        Video,
        Embedded,
        Polls,
        Quiz,
        Questions


    }
}
