﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.ENUM
{
    public enum BadgeTriggerType
    {
        Post = 1,
        Activity = 2,
        Earning = 3
    }
}
