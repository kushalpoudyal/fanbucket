﻿namespace FanBucketEntity.Enum
{
    public enum EmailTemplate
    {
        CreateNewUser,
        MessageRecieved,
        TipRecieved,
        Unfollowed,
        Unsubscription,
        UserInformationUpdated,
        PasswordChanged,
        Subscription,
        Login
    }
}
