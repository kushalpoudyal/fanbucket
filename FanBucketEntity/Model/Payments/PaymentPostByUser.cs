﻿using FanBucketEntity.Model.PostDetails;
using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.Payments
{
    public class PaymentPostByUser : BaseEntity
    {
        public decimal PaidAmount { get; set; }

        public Guid PaidById { get; set; }

        public Guid PostId { get; set; }

        public Guid TranId { get; set; } 
    }
}
