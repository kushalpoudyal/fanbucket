﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FanBucketEntity.Model.Payments
{
    public class StripePaymentByUser
    {
        public Guid Id { get; set; }
        public Guid TranId { get; set; }
        public Guid UserId { get; set; }

        public decimal Amount { get; set; }

        public DateTime TranDate { get; set; }
        [StringLength(1)]
        public string DebitCredit { get; set; }
        [StringLength(1000)]

        public string TranRemarks { get; set; }

        public TransactionResponseCode ResponseCode { get; set; } 
    }
}
