﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.Payments
{
    public class PaymentUPIN
    {
        public Guid UserId { get; set; }

        public string PINHash { get; set; }

        public int ErrorCount { get; set; }

        public bool Locked { get; set; }

        public DateTime? LockTime { get; set; }
    }
}
