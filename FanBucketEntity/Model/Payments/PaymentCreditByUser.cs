﻿using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.Payments
{
    public class PaymentCreditByUser
    {
        public Guid Id { get; set; }

        [StringLength(3)]
        public string Currency { get; set; }

        public decimal CurrentBalance { get; set; }

        public decimal TotalEarnings { get; set; }


        public decimal DuesRemaining { get; set; }

        public decimal CreditSum { get; set; }

        public decimal DebitSum { get; set; }
        public Guid UserId { get; set; }

        public decimal EarningPercentage { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
