﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketUI.ResponseModel
{
    public class RegistryModel
    {
        public bool Registered { get; set; }

        public string ReturnMsg { get; set; }
    }
}
