﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketUI.ResponseModel
{
    public class TokenModel
    {
        public string Role { get; set; }
        public string access_token { get; set; }

        public string username { get; set; }

        public string FullName { get; set; }
        public string ProfileImgLocation { get; set; }

        public string CoverImgLocation { get; set; }

        public Guid UserId { get; set; }
    }
}
