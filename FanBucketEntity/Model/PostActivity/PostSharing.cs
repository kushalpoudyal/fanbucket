﻿using FanBucketEntity.Model.PostDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.PostActivity
{
    public class PostSharing : BaseEntity
    {
        [StringLength(500)]
        public string SharedComment { get; set; }

        public Guid PostId { get; set; }
        [ForeignKey("PostId")]
        public virtual PostMaster PostMaster { get; set; }



    }
}
