﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.PostActivity
{
    public class AppNotifications
    {

        public Guid Id { get; set; }
        public Guid ActivityId { get; set; }

        public Guid ActivityUserId { get; set; }

        public Guid PKId { get; set; }
        public NotificationType nType { get; set; }
        public Guid PostCreatorId { get; set; }

        public DateTime CreatedDate { get; set; }

        public Record_Status Record_Status { get; set; }

        public bool Viewed { get; set; }


    }
}
