﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.PostDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.PostActivity
{
    public class PostReacts : BaseEntity
    {
        public ReactTypes ReactType { get; set; }
        public Guid PostId { get; set; } 
    }
}
