﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.PostActivity
{
    public class PostBookmark
    {
        public Guid Id { get; set; }
        public Guid PostId { get; set; }

        public Guid UserId { get; set; }

        public Record_Status Status { get; set; }

        public DateTime BookmarkedOn { get; set; }
    }
}
