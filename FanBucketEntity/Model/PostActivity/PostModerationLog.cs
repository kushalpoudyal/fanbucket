﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.PostDetails;
using FanBucketEntity.Models.UserDetails;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Model.PostActivity
{
    public class PostModerationLog
    {
        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        public Record_Status OldStatus { get; set; }
        public Record_Status NewStatus { get; set; }

        public string Remarks { get; set; }

        public Guid CreatedById { get; set; }

        public DateTime CreatedOn { get; set; }

        [ForeignKey(nameof(PostId))]
        public PostMaster PostMaster { get; set; }

        [ForeignKey(nameof(CreatedById))]
        public ApplicationUser ApplicationUser { get; set; }
    }
}