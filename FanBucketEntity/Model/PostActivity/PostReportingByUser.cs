﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.PostDetails;
using FanBucketEntity.Models.UserDetails;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Model.PostActivity
{
    public class PostReportingByUser
    {
        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        public string ReportingReason { get; set; }

        public string Remarks { get; set; }

        public Guid ReportedById { get; set; }

        public DateTime CreatedOn { get; set; }

        public Record_Status Record_Status { get; set; }

        [ForeignKey(nameof(PostId))]
        public PostMaster PostMaster { get; set; }

        [ForeignKey(nameof(ReportedById))]
        public ApplicationUser ApplicationUser { get; set; }
    }
}