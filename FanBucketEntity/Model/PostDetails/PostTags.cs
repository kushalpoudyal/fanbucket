﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Model.PostDetails
{
    public class Tag
    {
        public Tag()
        {
            PostTags = new HashSet<PostTags>();
        }
        public Guid Id { get; set; }

        public string Title { get; set; }
        public DateTime CreatedOn { get; set; }

        public ICollection<PostTags> PostTags { get; set; }
    }

    public class PostTags
    {
        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        public Guid TagId { get; set; }
        [ForeignKey(nameof(TagId))]
        public virtual Tag Tag { get; set; }
        [ForeignKey(nameof(PostId))]
        public virtual PostMaster PostMaster { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}