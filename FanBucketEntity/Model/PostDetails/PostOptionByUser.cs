﻿using FanBucketEntity.Model.ENUM;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.PostDetails
{
    public class PostOptionByUser
    {

        public Guid UserId { get; set; }

        public Guid OptionId { get; set; }

        public Guid PostId { get; set; }

        public DateTime CreatedOn { get; set; }

        public Record_Status Record_Status { get; set; }
    }

}
