﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.PostDetails
{
    public class PostOption
    {
        public Guid Id { get; set; }
        [StringLength(500)]
        public string OptionName { get; set; }
        public bool Answer { get; set; }
        public Guid PostId { get; set; }
        [ForeignKey("PostId")]
        public virtual PostMaster PostMaster { get; set; }
    }
}
