﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.PostDetails
{
    public class PostCollection
    {
        [Key]
        public Guid ContentId { get; set; }

        public Guid PostId { get; set; }
        [StringLength(200)]
        public string ContentTitle { get; set; }

        public ContentTypes PostType { get; set; }

        public bool PaidContent { get; set; }

        [StringLength(200)]

        public string FileLocation { get; set; }

        public Record_Status Record_Status { get; set; }

        public bool DeletedStatus { get; set; }
        [ForeignKey("PostId")]
        public virtual PostMaster PostMaster { get; set; }
    }
}
