﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Model.PostDetails
{
    public class PostMaster : BaseEntity
    {
        public string PostTitle { get; set; }

        public Privacy PostPrivacy { get; set; }
        public PaidPostType PaidPostType { get; set; }

        public decimal PostPrice { get; set; }

        public bool IsMedia { get; set; }

        public bool Scheduled { get; set; }

        public bool NSFW { get; set; }

        public DateTime? ScheduledTime { get; set; }


        [ForeignKey("ParentPost")]
        [InverseProperty("Children")]
        public Guid? ParentPostId { get; set; }

        public virtual PostMaster ParentPost { get; set; }

        public virtual ICollection<PostMaster> Children { get; set; }
    }
}