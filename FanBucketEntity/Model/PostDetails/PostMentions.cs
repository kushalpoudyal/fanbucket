﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.PostDetails
{
    public class PostMentions
    {
        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        public Guid UserId { get; set; }
    }
}
