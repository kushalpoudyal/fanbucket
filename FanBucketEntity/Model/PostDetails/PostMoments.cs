﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.PostDetails
{
    public class PostMoments
    {
        public Guid Id { get; set; }

        public Guid? PostId { get; set; }

        [StringLength(100)]
        public string FileLocation { get; set; }
        public DateTime StartDate { get; set; }

        public Guid CreatedById { get; set; }

        public DateTime CreatedOn { get; set; }
        [ForeignKey("PostId")]
        public virtual PostMaster PostMaster { get; set; }
    }
}
