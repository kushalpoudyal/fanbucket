﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.Chat
{
    public class Chat
    {
        public Chat()
        {
            Users = new List<ChatUser>();
            Messages = new List<Messages>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ChatType Type { get; set; }



        public Guid CreatedById { get; set; }

        public DateTime CreatedOn { get; set; }
        public ICollection<ChatUser> Users { get; set; }
        public ICollection<Messages> Messages { get; set; }
    }
}
