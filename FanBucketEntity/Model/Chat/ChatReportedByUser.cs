﻿using FanBucketEntity.Models.UserDetails;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Model.Chat
{
    public class ChatReportedByUser
    {
        [Key]
        public Guid Id { get; set; }

        public Guid ChatId { get; set; }
        public Guid ReportedByUserId { get; set; }
        public int MessageReportTypeId { get; set; }
        [StringLength(150)]
        public string Remarks { get; set; }
        public DateTime CreatedDate { get; set; }

        public bool IsAcive { get; set; }

        public bool DeletedStatus { get; set; }

        [ForeignKey("ReportedByUserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [ForeignKey("ChatId")]
        public virtual Chat Chat { get; set; }

        [ForeignKey("MessageReportTypeId")]
        public virtual MessageReportType MessageReportType { get; set; }
    }


}