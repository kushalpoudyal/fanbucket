﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FanBucketEntity.Model.Chat
{
    public class ChatUser
    {

        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public Guid ChatId { get; set; }
        public Chat Chat { get; set; }
        public ChatUserRole Role { get; set; }

        public bool Hidden { get; set; }

    }

    public class ChatUserBlockedByUser
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
        public Guid ChatId { get; set; }
        public Chat Chat { get; set; }
        public DateTime BlockedDate { get; set; }
        public Guid BlockedByUserId { get; set; }
        [ForeignKey("BlockedByUserId")]
        public ApplicationUser BlockedBy { get; set; }
    }
}
