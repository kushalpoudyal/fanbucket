﻿using FanBucketEntity.Models.UserDetails;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Model.Chat
{
    public class MessageReportType
    {
        [Key]
        public int Id { get; set; }
        [StringLength(100)]
        [Required]
        public string Title { get; set; }

        [StringLength(30)]
        [Required]
        public string DisplayValue { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
    }


}