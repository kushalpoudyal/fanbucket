﻿using FanBucketEntity.Model.ENUM;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Model.Chat
{
    public class MessageRoom
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid ChatId { get; set; }
        public ChatUserRole Role { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
