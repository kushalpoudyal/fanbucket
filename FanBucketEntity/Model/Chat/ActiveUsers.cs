﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FanBucketEntity.Model.Chat
{
    public class ActiveUsers
    {
        [Key]
        public Guid Id { get; set; }
      
        public Guid UserId { get; set; }
        public string ConnectionId { get; set; }
        public string Username { get; set; }
    }
}
