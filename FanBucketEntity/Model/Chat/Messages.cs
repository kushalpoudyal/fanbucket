﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FanBucketEntity.Model.Chat
{
    public class Messages
    {
        public Messages()
        {
            MessagesAttachments = new HashSet<MessagesAttachment>();
        }
        [Key]
        public Guid Id { get; set; }

        public Guid ChatId { get; set; }
        public Guid UserId { get; set; }

        [StringLength(1000)]
        public string Message { get; set; }

        [StringLength(200)]
        public string FileLocation { get; set; }

        public Guid? ParentId { get; set; }
        public decimal? TipAmount { get; set; }
        public DateTime When { get; set; }

        public bool HasRead { get; set; }

        public DateTime? ReadDate { get; set; }

        public bool DeletedStatus { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<MessagesAttachment> MessagesAttachments { get; set; }
    }
    public class MessagesAttachment
    {
        [Key]
        public Guid Id { get; set; }

        public Guid MessageId{ get; set; }
        public Guid UserId { get; set; }

        public ContentTypes ContentType { get; set; } = ContentTypes.Image;
        [Required]
        [StringLength(200)]
        public string FileLocation { get; set; }

        public DateTime When { get; set; }
        [ForeignKey("MessageId")]
        public virtual Messages Message { get; set; }
        public Guid ContentId { get; set; }
    }
    public class MessagesDeletedByUser
    {
        [Key]
        public Guid Id { get; set; }

        public Guid MessageId { get; set; }
        public Guid UserId { get; set; }

        public bool Deleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}