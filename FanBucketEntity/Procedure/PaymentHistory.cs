﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketEntity.Procedure
{
    public class PaymentHistory
    {
        public DateTime TranDate { get; set; }

        public string DebitCredit { get; set; }
        //Net Amount
        public decimal Amount { get; set; }
        //Actual Amount
        public decimal ActualAmount { get; set; }
        //Fee Amount or Commision
        public decimal FeeOrCommision => ActualAmount - Amount;

        public string PaidFor { get; set; }

        public Guid UserId { get; set; }

        public string ProfileImgLocation { get; set; }

        public string PaymentType { get; set; }
        public Guid? EntityId { get; set; }

        public string Action { get; set; }
    }
}
