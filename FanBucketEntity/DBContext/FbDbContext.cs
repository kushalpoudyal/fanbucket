﻿using FanBucketEntity.Model;
using FanBucketEntity.Model.Chat;
using FanBucketEntity.Model.Master;
using FanBucketEntity.Model.Payments;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.Model.PostDetails;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.Procedure;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketEntity.DBContext
{
    public class FbDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public FbDbContext()
        {

        }
        public FbDbContext(DbContextOptions<FbDbContext> options) : base(options)
        {

        }
        public DbSet<SocialMediaType> SocialMediaType { get; set; }
        public DbSet<UserSocialMediaLinks> UserSocialMediaLinks { get; set; }
        public DbSet<PostMaster> PostMaster { get; set; }
        public DbSet<Tag> Tag { get; set; }
        public DbSet<PostTags> PostTags { get; set; }
        public DbSet<PaymentPostByUser> PaymentPostByUser { get; set; }
        public DbSet<SignUpQueue> SignUpQueue { get; set; }
        //public DbSet<MessageTipByUser> MessageTipByUser { get; set; }

        public DbSet<ChangeLog> ChangeLogs { get; set; }

        public DbSet<PostBookmark> PostBookmark { get; set; }
        public DbSet<PostComments> PostComments { get; set; }
        public DbSet<PostReacts> PostReacts { get; set; }
        public DbSet<PostSharing> PostSharing { get; set; }
        public DbSet<PostCollection> PostCollection { get; set; }
        public DbSet<PostOption> PostOption { get; set; }
        public DbSet<PostOptionByUser> PostOptionByUser { get; set; }
        public DbSet<UserInfo> UserInfo { get; set; }

        public DbSet<Subscriptions> Subscriptions { get; set; }
        public DbSet<ApplicationUserFollowlist> ApplicationUserFollowlist { get; set; }
        public DbSet<Messages> Message { get; set; }
        public DbSet<MessagesAttachment> MessagesAttachments { get; set; }
        public DbSet<MessagesDeletedByUser> MessagesDeletedByUser { get; set; }

        public DbSet<Chat> Chats { get; set; }
        public DbSet<ChatUser> ChatUsers { get; set; }
        public DbSet<ChatUserBlockedByUser> ChatUserBlockedByUser { get; set; }
        public DbSet<ActiveUsers> ActiveUsers { get; set; }
        public DbSet<MessageRoom> MessageRoom { get; set; }
        public DbSet<MessageReportType> MessageReportType { get; set; }
        public DbSet<ChatReportedByUser> ChatReportedByUser { get; set; }
        public DbSet<PaymentCreditByUser> PaymentCreditByUser { get; set; }
        public DbSet<PaymentTransactionByUser> PaymentTransactionByUser { get; set; }
        public DbSet<PaymentUPIN> PaymentUPIN { get; set; }

        public DbSet<PostReportingByUser> PostReportingByUser { get; set; }

        public DbSet<AppNotifications> AppNotifications { get; set; }
        public DbSet<BadgeInfo> BadgeInfo { get; set; }
        public DbSet<UserBadges> UserBadges { get; set; }
        public DbSet<Interest> Interest { get; set; }
        public DbSet<UserInterests> UserInterests { get; set; }

        public DbSet<ApplicationUserDetails> ApplicationUserDetails { get; set; }

        public DbSet<PostMentions> PostMentions { get; set; }

        public DbSet<ApplicationUserSubscriptionINfo> ApplicationUserSubscriptionInfo { get; set; }
        public DbSet<SubscriptionByUser> SubscriptionByUser { get; set; }
        public DbSet<UserSubscribedStatus> UserSubscribedStatus { get; set; }

        public DbSet<ApplicationUserKYC> ApplicationUserKYC { get; set; }

        public DbSet<PostMoments> PostMoments { get; set; }
        public DbSet<UserCommissionReviewLog> UserCommissionReviewLogs { get; set; }
        public DbSet<UserModerationLog> UserModerationLogs { get; set; }
        public DbSet<PostModerationLog> PostModerationLogs { get; set; }
        public DbSet<UserReportedByUser> UserReportedByUser { get; set; }

        public DbSet<CustomerPaymentDetails> CustomerPaymentDetails { get; set; }

        public DbSet<StripePaymentByUser> StripePaymentByUser { get; set; }

        public DbSet<PaymentEarningsByUser> PaymentEarningsByUser { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration.GetConnectionString("DbCoreConnectionString");
                optionsBuilder.UseSqlServer(connectionString);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<MessageReportType>().HasData(
                    new MessageReportType { Id = 1, Title = "Spam", DisplayValue = "Spam", IsActive = true, Order = 1 },
                    new MessageReportType { Id = 2, Title = "HateSpeech", DisplayValue = "Hate Speech", IsActive = true, Order = 2 },
                    new MessageReportType { Id = 3, Title = "Harassment", DisplayValue = "Harassment", IsActive = true, Order = 3 },
                    new MessageReportType { Id = 4, Title = "Illegal", DisplayValue = "Illegal Activity", IsActive = true, Order = 4 }
            );
            modelBuilder.Entity<ApplicationUser>().ToTable("ApplicationUser");
            modelBuilder.Entity<ApplicationRole>().ToTable("ApplicationRole");
            modelBuilder.Entity<PaymentHistory>().HasNoKey().ToView(null);
            modelBuilder.Entity<Messages>()
         .Property(p => p.TipAmount)
         .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<StripePaymentByUser>()
       .Property(p => p.Amount)
       .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<PaymentEarningsByUser>()
       .Property(p => p.Amount)
       .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<ApplicationUserSubscriptionINfo>()
         .Property(p => p.SubscriptionAmount)
         .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<PostMaster>()
        .Property(p => p.PostPrice)
        .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<ChatUser>()
                .HasKey(x => new { x.ChatId, x.UserId });
            //modelBuilder.Entity<ChatUser>()
            //    .has(x => new { x.ChatId, x.UserId });
            modelBuilder.Entity<PostOptionByUser>()
                .HasKey(x => new { x.UserId, x.OptionId });

            modelBuilder.Entity<ApplicationUserDetails>()
               .HasKey(x => new { x.UserId });

            modelBuilder.Entity<PostBookmark>()
               .HasKey(x => new { x.PostId, x.UserId });

            modelBuilder.Entity<SubscriptionByUser>()
              .HasKey(x => new { x.SubscriptionId, x.PaidByUserId, x.TranId });

            modelBuilder.Entity<PaymentUPIN>()
               .HasKey(x => new { x.UserId });

            modelBuilder.Entity<ApplicationUserKYC>()
              .HasKey(x => new { x.UserId });

            modelBuilder.Entity<PaymentTransactionByUser>()
              .Property(p => p.Amount)
              .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<PaymentCreditByUser>()
             .Property(p => p.DebitSum)
             .HasColumnType("decimal(18,4)");
            modelBuilder.Entity<PaymentCreditByUser>()
             .Property(p => p.CreditSum)
             .HasColumnType("decimal(18,4)");
            modelBuilder.Entity<PaymentCreditByUser>()
             .Property(p => p.CurrentBalance)
             .HasColumnType("decimal(18,4)");
            modelBuilder.Entity<PaymentCreditByUser>()
             .Property(p => p.TotalEarnings)
             .HasColumnType("decimal(18,4)");
            modelBuilder.Entity<PaymentCreditByUser>()
             .Property(p => p.DuesRemaining)
             .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<PaymentPostByUser>()
             .Property(p => p.PaidAmount)
             .HasColumnType("decimal(18,4)");

            //modelBuilder.Entity<MessageTipByUser>()
            // .Property(p => p.PaidAmount)
            // .HasColumnType("decimal(18,4)");
            modelBuilder.Entity<BadgeInfo>().HasIndex(x => x.Code)
                .IsUnique();
            modelBuilder.Entity<PaymentCreditByUser>()
                        .Property(p => p.EarningPercentage)
                        .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<UserCommissionReviewLog>()
                        .Property(p => p.NewCommissionPer)
                        .HasColumnType("decimal(18,4)");
            modelBuilder.Entity<UserCommissionReviewLog>()
                       .Property(p => p.OldCommissionPer)
                       .HasColumnType("decimal(18,4)");
            modelBuilder.Entity<PostMaster>()
                     .Property(p => p.PostPrice)
                     .HasColumnType("decimal(18,4)");
            //modelBuilder.Entity<PostMaster>().has(b => b.ParentPost)
            //                      .WithMany(b => b.Children)
            //                      .HasForeignKey(b => b.ParentPostId);
        }

        public Task<int> CommitAsync()
        {
            try
            {
                var modifiedEntities = this.ChangeTracker.Entries()
       .Where(p => p.State == EntityState.Modified || p.State == EntityState.Added).ToList();
                var now = DateTime.UtcNow;
                foreach (var change in modifiedEntities)
                {
                    var modified = change.CurrentValues;
                    var state = change.State;
                    if (state == EntityState.Added)
                    {
                        var entityName = change.Entity.GetType().Name;
                        var Id = string.Empty;

                        foreach (var prop in change.CurrentValues.Properties)
                        {

                            if (change.CurrentValues[prop.Name] != null)
                            {
                                var currentValue = change.CurrentValues[prop.Name] != null ? change.CurrentValues[prop.Name].ToString() : null;

                                ChangeLog log = new ChangeLog()
                                {
                                    PKId = Id,
                                    EntityName = entityName,
                                    PropertyName = prop.Name,
                                    OldValue = currentValue,
                                    NewValue = currentValue,
                                    EntityState = state,
                                    DateChanged = now
                                };
                                ChangeLogs.Add(log);
                            }

                        }

                    }
                    else
                    {
                        var dataList = change.OriginalValues;
                        var entityName = change.Entity.GetType().BaseType.Name;
                        if (entityName.ToLower() == "object")
                            entityName = change.Entity.GetType().Name;

                        var Id = string.Empty;
                        if (change.Property("Id") != null)
                            Id = change.Property("Id").CurrentValue.ToString();
                        foreach (var prop in change.OriginalValues.Properties)
                        {


                            if (change.CurrentValues[prop.Name] != null)
                            {
                                var originalValue = change.OriginalValues[prop.Name] != null ? change.OriginalValues[prop.Name].ToString() : null;
                                var currentValue = change.CurrentValues[prop.Name] != null ? change.CurrentValues[prop.Name].ToString() : null;
                                if (originalValue != currentValue)
                                {
                                    ChangeLog log = new ChangeLog()
                                    {

                                        PKId = Id,
                                        EntityName = entityName,
                                        PropertyName = prop.Name,
                                        OldValue = originalValue,
                                        NewValue = currentValue,
                                        EntityState = state,
                                        DateChanged = now
                                    };
                                    ChangeLogs.Add(log);

                                }
                            }
                        }

                    }
                }

                var __ = base.SaveChangesAsync();
                return __;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
