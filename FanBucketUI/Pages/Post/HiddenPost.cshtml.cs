﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Post
{
    public class HiddenPostModel : PageModel
    {
        public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();
        private readonly apiRequest api;
        public HiddenPostModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }


        public async Task<IActionResult> OnGet()
        {

            postMaster = await api.GetHiddenPost(User.Identity.GetJWT());

            return Page();
        }

    }
}