﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Post
{
    public class TopAccountsModel : PageModel
    {
        public List<TopAccountsViewModel> topAccs = new List<TopAccountsViewModel>();
        private readonly apiRequest api;
        public TopAccountsModel(apiRequest _apiRequest)
        {
            api = _apiRequest;
        }
        public async Task<IActionResult> OnGet()
        {
             
            topAccs = await api.GetTopAccounts(User.Identity.GetJWT());
            return Page();
        }
    }
}