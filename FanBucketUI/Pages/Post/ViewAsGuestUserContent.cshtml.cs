﻿using FanBucketAPI.Models;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FanBucketUI.Pages.Post
{
    public class ViewAsGuestUserContentModel : PageModel
    {
        private readonly apiRequest api;

        public ViewAsGuestUserContentModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();

        public int MyProperty { get; set; }

        public DateTime ClientTime;

        public async Task<IActionResult> OnGet(DateTime dateTime, int PageNumber = 1, int PageSize = 10)
        {
            ClientTime = dateTime;

            postMaster = await api.GetContentsAsGuestUser(User.Identity.GetJWT(), PageNumber, PageSize);

            return Page();
        }

        public async Task<IActionResult> OnPost([FromBody] ReactViewModel reactViewModel)
        {
            await api.ReactPost(reactViewModel, User.Identity.GetJWT());

            return Page();
        }
    }
}