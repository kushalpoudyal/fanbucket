﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Post
{
    public class CommentsModel : PageModel
    {
        public List<PostCommentViewModel> postMaster = new List<PostCommentViewModel>();

        private readonly apiRequest api;
        public CommentsModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }


        public async Task<IActionResult> OnGet(Guid PostId)
        {

            postMaster = await api.GetComments(User.Identity.GetJWT(), PostId);
            return Page();
        }
        public async Task<IActionResult> OnPost([FromBody]CommentViewModel param)
        {

            var response = await api.CreateComment(param, User.Identity.GetJWT());
            if (response.Registered)
            {
                return Page();
            }
            else
            {
                return null;
            }
        }
    }
}