﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Post
{
    public class BookmarkModel : PageModel
    {
        public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();
        private readonly apiRequest api;
        public BookmarkModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }


        public async Task<IActionResult> OnGet()
        {

            postMaster = await api.GetBookmark(User.Identity.GetJWT());

            return Page();
        }

    }
}