﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Post
{
    public class SingleContentModel : PageModel
    {
        public PostMasterCollectionViewModel postMaster = new PostMasterCollectionViewModel();
        private readonly apiRequest api;
        public SingleContentModel(apiRequest _apiRequest)
        {
            api = _apiRequest;
        }

        public async Task<IActionResult> OnGet(Guid PostId)
        {

            postMaster = await api.GetSinglePostById(PostId, User.Identity.GetJWT());
            return Page();


        }
    }
}