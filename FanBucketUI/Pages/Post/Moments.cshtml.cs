﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Post
{
    public class MomentsModel : PageModel
    {
        public List<PostMomentsViewModel> moments = new List<PostMomentsViewModel>();

        private readonly apiRequest api;
        public MomentsModel(apiRequest _apiRequest)
        {
            api = _apiRequest;
        }

        public async Task<IActionResult> OnGet()
        {

            moments = await api.GetUserMoments(User.Identity.GetJWT());

            return Page();

        }
    }
}