﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Stripe.Checkout;

namespace FanBucketUI.Pages.Post
{
    public class ContentModel : PageModel
    {
        private readonly apiRequest api;
        public ContentModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();


        public int MyProperty { get; set; }

        public DateTime ClientTime;
        public async Task<IActionResult> OnGet(Guid? Id, DateTime dateTime, int PageNumber = 1, int PageSize = 10)
        {
            ClientTime = dateTime;


            if (Id == null)
            {
                postMaster = await api.GetContents(User.Identity.GetJWT(),PageNumber,PageSize);
            }
            else
            {
                postMaster = await api.GetContents(User.Identity.GetJWT(), Id.Value,PageNumber,PageSize);
            }

            return Page();
        }


        public async Task<IActionResult> OnPost([FromBody] ReactViewModel reactViewModel)
        {


            await api.ReactPost(reactViewModel, User.Identity.GetJWT());



            return Page();
        }
    }
}