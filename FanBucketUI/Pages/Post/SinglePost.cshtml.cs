﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketUI.API_Context;
using FanBucketUI.Extension;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Post
{
    public class SinglePostModel : PageModel
    {
        public PostMasterCollectionViewModel postMaster = new PostMasterCollectionViewModel();
        public string LayoutPath = string.Empty;
        private readonly apiRequest api;
        public SinglePostModel(apiRequest _apiRequest)
        {
            api = _apiRequest;
        }



        public async Task<IActionResult> OnGet(string PostId ,string NotifyId=null)
        {

          
            postMaster = await api.GetSinglePostById(Guid.Parse(PostId),User.Identity.GetJWT(),NotifyId);
            if (!HttpContext.Request.IsAjaxRequest())
            {
                LayoutPath = "../Shared/_LayoutUser.cshtml";
            }

            return Page();


        }
    }
}