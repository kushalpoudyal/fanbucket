﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Stripe.Checkout;

namespace FanBucketUI.Pages.Post
{
    public class SearchModel : PageModel
    {
        private readonly apiRequest api;
        public SearchModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();

        public int MyProperty { get; set; }

        public DateTime ClientTime;
        public async Task<IActionResult> OnGet(Guid? Id, string hashtag)
        {
            //ClientTime = dateTime;

            postMaster = await api.SearchPost(User.Identity.GetJWT(), hashtag);

            return Page();
        }


        public async Task<IActionResult> OnPost([FromBody] ReactViewModel reactViewModel)
        {


            await api.ReactPost(reactViewModel, User.Identity.GetJWT());



            return Page();
        }
    }
}