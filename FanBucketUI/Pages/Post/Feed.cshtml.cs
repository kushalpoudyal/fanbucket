﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.APIModels;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Stripe.Checkout;

namespace FanBucketUI.Pages.Post
{
    [Authorize]
    public class FeedModel : PageModel
    {
        public List<InterestViewModel> interests = new List<InterestViewModel>();

        public SubscriptionViewModel paymentModel = new SubscriptionViewModel();

        private readonly apiRequest apiRequest;
        public FeedModel(apiRequest _apiRequest)
        {
            apiRequest = _apiRequest;
        }

        public UserDetailsViewModel usr { get; set; }
        public async Task<IActionResult> OnGet()
        {

            usr = await apiRequest.GetUsersById(User.Identity.GetUsername(), User.Identity.GetJWT());
            interests = await apiRequest.GetInterests(User.Identity.GetJWT());
            return Page();
        }


        public async Task<IActionResult> OnPost([FromBody] PostMasterAPIModel param)
        {

            var response = await apiRequest.RegisterPost(param, User.Identity.GetJWT());
            if (response.Registered)
            {

                return Page();
            }
            else
            {
                return null;
            }
        }
    }
}