﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Badge
{

    public class EditModel : PageModel
    {

        [BindProperty]
        public BadgeViewModel badgeViewModel { get; set; }

        [TempData]
        public string Message { get; set; }

        private readonly apiRequest api;
        public EditModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }


        public async Task<IActionResult> OnGetAsync(string Id)
        {

            badgeViewModel = await api.GetBadgeById(Id, User.Identity.GetJWT());
            if (badgeViewModel == null)
            {
                Message = $"Badge {Id} not found!";
                return RedirectToPage("./Index");
            }
            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await api.CreateBadge(badgeViewModel, User.Identity.GetJWT());

            Message = "Badge created successfully!";

            return RedirectToPage("./Index");
        }
    }
}