﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Interest
{

    public class IndexModel : PageModel
    {
        public List<InterestViewModel> interestViewModels = new List<InterestViewModel>();
        private readonly apiRequest api;
        public IndexModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task OnGetAsync()
        {

            interestViewModels = await api.GetInterests(User.Identity.GetJWT());
        }
    }
}