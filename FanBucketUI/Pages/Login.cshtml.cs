using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using FanBucketUI.API_Context;
using FanBucketUI.RequestModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Session;

namespace FanBucketUI.Pages
{

    public class LoginModel : PageModel
    {
        public string _Msg;
        public bool _Status;
        [BindProperty(SupportsGet = true)]
        public string _LoginMsg { get; set; } = null;
        public RegisterClient RegisterClient { get; set; } = new RegisterClient();

        private readonly apiRequest api;
        public LoginModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public IActionResult OnGet(bool Status = false, string Msg = null,string LoginMsg=null)
        {
            _Msg = Msg;
            _LoginMsg = LoginMsg;
            _Status = Status;
            if (User.Identity.IsAuthenticated)

            {
                return RedirectToPage("/Post/Feed");
            }
            return Page();
        }


        public async Task<IActionResult> OnPost(UserCred userCred)
        {

            if (ModelState.IsValid)
            {

            }
            var response = await api.GenerateToken(userCred);
            
            if (response != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, userCred.username),
                    new Claim(ClaimTypes.Hash, response.access_token),
                    new Claim(ClaimTypes.Actor, response.FullName),
                    new Claim(ClaimTypes.Email, response.username),
                    new Claim(ClaimTypes.PostalCode, string.IsNullOrEmpty(response.ProfileImgLocation)?"/Content/gallery/userimg.png":response.ProfileImgLocation),
                    new Claim(ClaimTypes.Sid, response.UserId.ToString()),
                    new Claim(ClaimTypes.Role,response.Role),
                    new Claim(ClaimTypes.GivenName,string.IsNullOrEmpty(response.CoverImgLocation)?"/Content/gallery/userBanner.png":response.CoverImgLocation),
                    new Claim(ClaimTypes.NameIdentifier,response.UserId.ToString())
                };
                var appIdentity = new ClaimsIdentity(claims);


                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                HttpContext.User = new ClaimsPrincipal(claimsIdentity);
                //HttpContext.User.AddIdentity(appIdentity);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                if (response.Role == "Admin")
                {
                    return RedirectToPage("/Admin/Dashboard");
                }
                else
                {
                    return RedirectToPage("/Post/Feed");
                }

            }
            else
            {
                return Page();

            }


        }
        public async Task<IActionResult> OnPostRegisterClient(RegisterClient userCred)
        {

            
            userCred.ConfirmPassword = userCred.Password;
            var response = await api.RegisterClient(userCred);
            if (response.success)
            {
                return RedirectToPage("Login", new { Status = true, Msg = response.ReturnMsg });
            }
            RegisterClient = userCred;
            _Status = false;
            _Msg = response.ReturnMsg;
            return Page();
            //else
            //{
            //    return RedirectToPage("Login", new { userCred, Status = false, Msg = response.ReturnMsg + " Try Again." });

            //}
            //if (response.Registered)
            //{
            //    return RedirectToPage("Login", new { Status = true, Msg = response.ReturnMsg });
            //}
            //else
            //{
            //    return RedirectToPage("Login", new { userCred, Status = false, Msg = response.ReturnMsg + " Try Again." });

            //}


        }
        public async Task<IActionResult> OnPostLogoutAsync()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToPage("/login");
        }
    }
}
