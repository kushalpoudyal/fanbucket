﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;

namespace FanBucketUI.Pages.User
{
    public class ViewAsGuestUserModel : PageModel
    {
        public UserDetailsViewModel usr = new UserDetailsViewModel();
        public string Username { get; set; }
        private readonly apiRequest api;
        public ViewAsGuestUserModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task<IActionResult> OnGet(string NotifyId=null)
        {
            Username = User.Identity.GetUsername();
            if (User.Identity.IsAuthenticated)
            {
                usr = await api.GetUsersById(Username, User.Identity.GetJWT());
                return Page();
            }
            else
            {
                return Redirect("/U/" + Username);
            }

        }

        //public async Task<IActionResult> OnGetUpdateFeaturedBadge(string BadgeCode)
        //{
        //    var api = new apiRequest();
        //    var flag= await api.UpdateFeaturedBadge(BadgeCode, User.Identity.GetJWT());
        //    if (flag)
        //    {
        //      return  RedirectToPage("/ProfilePage",new { Id=User.Identity.GetUserId()});
        //    }
        //    else
        //    {

        //    }
        //    return Page();
        //}



    }
}