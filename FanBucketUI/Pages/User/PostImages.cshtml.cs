﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Post
{
    [Authorize]
    public class PostImagesModel : PageModel
    {
        //public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();
        //public List<GetUserImageContents> postMaster = new List<GetUserImageContents>();
        private readonly apiRequest api;
        public List<GetUserMediaViewModel> userMedias=new List<GetUserMediaViewModel>();

        public PostImagesModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }


        public async Task<IActionResult> OnGet(Guid Id)
        {


            userMedias = await api.GetUserImageContents(Id, User.Identity.GetJWT());

            return Page();
        }
    }
}