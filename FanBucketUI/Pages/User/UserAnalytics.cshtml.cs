﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.User
{
    [Authorize]
    public class UserAnalyticsModel : PageModel
    {
        public UserInteractionViewModel usrInt = new UserInteractionViewModel();


        private readonly apiRequest api;
        public UserAnalyticsModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public async Task<ActionResult> OnGet()
        {
            usrInt = await api.GetUserData(User.Identity.GetJWT());

            return Page();
        }
    }
}