using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json.Linq;

namespace FanBucketUI.Pages.User
{
    public class SettingsModel : PageModel
    {
        private readonly apiRequest _apirequest;

        public  SettingsViewModel SettingsViewModel { get; set; }
        public SettingsModel(apiRequest  apirequest)
        {
            _apirequest = apirequest;
        }
        public async Task<IActionResult> OnGetAsync()
        {
            SettingsViewModel = await _apirequest.GetSettings(User.Identity.GetJWT());
            return Page();
            //var claimNew = new Claim(type, value);
            //var claimOld = new Claim(type, oldValue);
            //var result = await UserManager.ReplaceClaimAsync(user, claimOld, claimNew);
        }
    }
}
