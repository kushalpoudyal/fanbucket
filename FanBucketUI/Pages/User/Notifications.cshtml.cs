﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.User
{
    public class NotificationsModel : PageModel
    {
        public List<NotificationViewModel> postMaster = new List<NotificationViewModel>();
        private readonly apiRequest api;
        public NotificationsModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public async Task<IActionResult> OnGet()
        {

            postMaster = await api.GetUserNotifications(User.Identity.GetJWT());
            return Page();
        }
    }
}