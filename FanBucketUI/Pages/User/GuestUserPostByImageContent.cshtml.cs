﻿using FanBucketAPI.Models;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FanBucketUI.Pages.Post
{
    public class GuestUserPostByImageContentModel : PageModel
    {
        //public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();
        public List<GetUserMediaViewModel> userMedias = new List<GetUserMediaViewModel>();
        private readonly apiRequest api;

        public GuestUserPostByImageContentModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task<IActionResult> OnGet()
        {
            userMedias = await api.GetPostByImageAsGuestUser(User.Identity.GetJWT());

            return Page();
        }
    }
}