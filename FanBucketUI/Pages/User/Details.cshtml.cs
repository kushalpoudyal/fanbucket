﻿using FanBucketAPI.Helper;
using FanBucketAPI.ViewModel;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketUI.Pages.User
{
    public class DetailsModel : PageModel
    {
        public UserDetailsViewModel usr = new UserDetailsViewModel();
        [BindProperty]
        public ReportActionViewModel ReportActionViewModel { get; set; }
        [BindProperty]
        public CommissionReviewViewModel CommissionReviewViewModel { get; set; }

        private readonly apiRequest api;

        public List<SelectListItem> Actions { get; set; }
        public List<SelectListItem> Days { get; set; }


        public DetailsModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        private void Common()
        {
            Actions = EnumHelper.GetEnumValuesAndDescriptions<EnumReportAction>().ToList();
            Days = EnumHelper.GetEnumValuesAndDescriptions<EnumDays>().ToList();
        }
        public async Task<IActionResult> OnGet(Guid Id)
        {
            usr = await api.GetUserDetailsById(Id, User.Identity.GetJWT()); 
            Common();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await api.UserCommissionReview(CommissionReviewViewModel, User.Identity.GetJWT());
            Common();
            return RedirectToPage("./Details", new { Id = CommissionReviewViewModel.UserId });
        }

        public async Task<IActionResult> OnPostUserReview()
        {
            var user = User.Identity.GetUserId();
            await api.UserReview(ReportActionViewModel, User.Identity.GetJWT());
            return RedirectToPage("./Details", new { Id = ReportActionViewModel.UserId });
        }
    }
}