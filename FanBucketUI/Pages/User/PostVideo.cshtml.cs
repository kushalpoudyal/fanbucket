﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Post
{
    public class PostVideoModel : PageModel
    {
        public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();
        private readonly apiRequest api;
        public PostVideoModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task<IActionResult> OnGet(Guid Id)
        {

            

            postMaster = await api.GetVideos(Id, User.Identity.GetJWT());

            return Page();
        }

    }
}