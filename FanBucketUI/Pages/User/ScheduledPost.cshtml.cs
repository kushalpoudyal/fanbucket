﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.Models;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.User
{
    public class ScheduledPostModel : PageModel
    {
        public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();
        private readonly apiRequest api;
        public ScheduledPostModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public DateTime ClientsTime;

        public async Task<IActionResult> OnGet(DateTime dateTime)
        {
            ClientsTime = dateTime;
            postMaster = await api.GetScheduled(User.Identity.GetJWT());

            return Page();
        }
    }
}