﻿using FanBucketAPI.Helper;
using FanBucketAPI.ViewModel;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketUI.Pages.User
{
    public class LoadWalletModel : PageModel
    {

        [BindProperty(SupportsGet = true)]
        public decimal amount { get; set; }


        public LoadWalletModel()
        {
            amount = 0;
        }

        public async Task<IActionResult> OnGet(decimal Amount)
        {
            if (amount<=0)
            {
                return RedirectToPage("/Post/Feed");
            }
            amount = Amount;

            return Page();
        }


    }
}