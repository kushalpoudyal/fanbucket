﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.User
{
    public class ResetPasswordModel : PageModel
    {
        public ResetPasswordViewModel resetToken = new ResetPasswordViewModel();
        public async Task<IActionResult> OnGet(string token, string email)
        {
            if (token == null || email == null)
            {
                return BadRequest("Invalid Token");
            }
            resetToken.Token = token;
            resetToken.Email = email;
            return Page();
        }

    }
}