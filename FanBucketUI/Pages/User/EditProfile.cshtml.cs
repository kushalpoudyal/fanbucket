﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.User
{
    public class EditProfileModel : PageModel
    {
        public UserDetailsViewModel usr = new UserDetailsViewModel();

        private readonly apiRequest api;
        public EditProfileModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public async Task<IActionResult> OnGet(Guid Id)
        {


            usr = await api.GetUserDetail(User.Identity.GetJWT());
            return Page();
        }

        public async Task<IActionResult> OnPost(EditPersonalViewModel model)
        {


            usr = await api.GetUserDetail(User.Identity.GetJWT());
            return Page();
        }
    }
}