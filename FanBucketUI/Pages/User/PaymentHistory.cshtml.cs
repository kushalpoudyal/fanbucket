using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.Procedure;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.User
{
    [Authorize]
    public class PaymentHistoryModel : PageModel
    {
        public List<PaymentHistory> postMaster = new List<PaymentHistory>();
        private readonly apiRequest api;
        public PaymentHistoryModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public async Task<IActionResult> OnGet(Guid NotifyId=default)
        {

            postMaster = await api.GetPaymentHistory(NotifyId,User.Identity.GetJWT());
            return Page();
        }
    }
}
