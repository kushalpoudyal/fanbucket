﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.User
{
    public class UserBySearchModel : PageModel
    {
        public List<SearchUserViewModel> usrlist = new List<SearchUserViewModel>();
        private readonly apiRequest api;
        public UserBySearchModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public async Task<IActionResult> OnGet(string msg)
        {

            usrlist = await api.GetUsersBySearch(msg, User.Identity.GetJWT());
            return Page();
        }

    }
}