﻿using FanBucketAPI.Models;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FanBucketUI.Pages.Post
{
    public class GuestUserPostByVideoContentModel : PageModel
    {
        public List<PostMasterCollectionViewModel> postMaster = new List<PostMasterCollectionViewModel>();
        private readonly apiRequest api;

        public GuestUserPostByVideoContentModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task<IActionResult> OnGet()
        {
            postMaster = await api.GetUserPostByVideoAsGuestUser(User.Identity.GetJWT());

            return Page();
        }
    }
}