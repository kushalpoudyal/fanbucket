﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages
{
    public class LockProfileModel : PageModel
    {
        public UserDetailsViewModel usr = new UserDetailsViewModel();

        private readonly apiRequest api;
        public LockProfileModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public async Task<IActionResult> OnGet(string Username)
        {
            usr = await api.GetUsersById(Username);
            return Page();
        }
    }
}