﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FanBucketUI.API_Context;
using FanBucketUI.RequestModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages
{
    public class RegisterModel : PageModel
    {
        private readonly apiRequest api;
        public RegisterModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public void OnGet()
        {

        }
        public async Task<IActionResult> OnPost(RegisterClient userCred)
        {


            userCred.ConfirmPassword = userCred.Password;
            var response = await api.RegisterClient(userCred);
            if (response.success)
            {
                return RedirectToPage("Login", new {Status=true, Msg =  response.ReturnMsg });
            }
            else
            {
                return RedirectToPage("Login", new {userCred,Status=false, Msg = response.ReturnMsg + " Try Again." });

            }


        }
    }
}