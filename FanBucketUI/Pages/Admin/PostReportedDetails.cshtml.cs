﻿using FanBucketAPI.Helper;
using FanBucketAPI.ViewModel;
using FanBucketEntity.Model.ENUM;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketUI.Pages.Admin
{
    public class PostReportedDetailsModel : PageModel
    {
        public PostReportSummaryViewModel reportedPost = new PostReportSummaryViewModel();
        private readonly apiRequest api;

        [BindProperty]
        public PostReviewViewModel PostReviewViewModel { get; set; }

        [ViewData]
        public List<SelectListItem> NewStatus { get; set; }

        private void Common()
        {
            NewStatus = EnumHelper.GetEnumValuesAndDescriptions<Record_Status>().ToList();
        }

        public PostReportedDetailsModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task<IActionResult> OnGet(Guid Id)
        {
            reportedPost = await api.ReportedPostsDetails(Id, User.Identity.GetJWT());
            Common();
            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            var user = User.Identity.GetUserId();
            await api.PostReview(PostReviewViewModel, User.Identity.GetJWT());
            return RedirectToPage("./PostsReported");
        }
    }
}