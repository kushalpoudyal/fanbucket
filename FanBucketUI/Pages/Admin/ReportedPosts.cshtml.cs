﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.APIModels;
using FanBucketAPI.ViewModel;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Admin
{
    
    public class ReportedPostsModel : PageModel
    {
        public List<PostReportSummaryViewModel> reportedPostList = new List<PostReportSummaryViewModel>();
        private readonly apiRequest api;
        public ReportedPostsModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task<IActionResult> OnGet()
        {
            reportedPostList = await api.ReportedPosts(User.Identity.GetJWT());
            return Page();
        }
       
    }
}