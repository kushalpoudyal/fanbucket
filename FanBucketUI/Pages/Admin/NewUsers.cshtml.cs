﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Admin
{
    public class NewUsersModel : PageModel
    {
        public List<UserDetailsViewModel> newUsrlist = new List<UserDetailsViewModel>();
        private readonly apiRequest api;
        public NewUsersModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task<IActionResult> OnGet(string msg)
        {

            newUsrlist = await api.GetNewUsers(User.Identity.GetJWT(), 7);
            return Page();
        }
    }
}
