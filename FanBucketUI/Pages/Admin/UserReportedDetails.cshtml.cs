﻿using FanBucketAPI.Helper;
using FanBucketAPI.ViewModel;
using FanBucketEntity.Model.ENUM;
using FanBucketRepo.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketUI.Pages.Admin
{
    public class UserReportedDetailsModel : PageModel
    {
        public UserReportSummaryViewModel reportedUser = new UserReportSummaryViewModel();
        private readonly apiRequest api;

        [BindProperty]
        public ReportActionViewModel ReportActionViewModel { get; set; }

        public List<SelectListItem> Actions { get; set; }
        public List<SelectListItem> Days { get; set; }

        private void Common()
        {
            Actions = EnumHelper.GetEnumValuesAndDescriptions<EnumReportAction>().ToList();
            Days = EnumHelper.GetEnumValuesAndDescriptions<EnumDays>().ToList();
        }

        public UserReportedDetailsModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task<IActionResult> OnGet(Guid Id)
        {
            reportedUser = await api.ReportedUserDetails(Id, User.Identity.GetJWT());
            Common();
            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            var user = User.Identity.GetUserId();
            await api.UserReview(ReportActionViewModel, User.Identity.GetJWT());
            return RedirectToPage("./UsersReported");
        }
    }
}