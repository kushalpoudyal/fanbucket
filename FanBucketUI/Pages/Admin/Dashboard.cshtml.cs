﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Admin
{

    public class DashboardModel : PageModel
    {
        public DashboardViewModel dashboardViewModel = new DashboardViewModel();
        private readonly apiRequest api;
        public DashboardModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }


        public async Task<IActionResult> OnGet()
        {
           
            dashboardViewModel = await api.GetDashboardInfo(User.Identity.GetJWT(), 7);
            return Page();
        }
    }
}