﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketAPI.APIModels;
using FanBucketAPI.ViewModel;
using FanBucketEntity.ViewModel;
using FanBucketRepo.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Admin
{

    public class ReportedUsersModel : PageModel
    {
        public List<UserReportSummaryViewModel> reportedPostList = new List<UserReportSummaryViewModel>();
        private readonly apiRequest api;
        public ReportedUsersModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }

        public async Task<IActionResult> OnGet()
        {
            reportedPostList = await api.ReportedUsers(User.Identity.GetJWT());
            return Page();
        }
       
    }
}