﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketEntity.Wrappers;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.Admin
{
    public class UsersModel : PageModel
    {
        public PagedResponse<List<UserDetailsViewModel>> pagedUsrlist = new PagedResponse<List<UserDetailsViewModel>>();
        bool response = false;
        [BindProperty]
        public string UserId { get; set; }

        private readonly apiRequest api;
        public UsersModel(apiRequest apiRequest)
        {
            api = apiRequest;
        }
        public async Task<IActionResult> OnGet(string msg, int PageNumber = 1, int PageSize = 10, string searchText = "")
        {

            pagedUsrlist = await api.GetUsers(User.Identity.GetJWT(), PageNumber, PageSize,searchText);
            return Page();
        }
        public async Task<IActionResult> OnPostVerify()
        {

            var user = User.Identity.GetUserId();
            response = await api.VerifyUserById(UserId, User.Identity.GetJWT());
            return RedirectToPage("Users");
        }

        public async Task<IActionResult> OnGetPartial(string msg, int PageNumber = 1, int PageSize = 10, string searchText = "")
        {

           var pagedList1 = await api.GetUsers(User.Identity.GetJWT(), PageNumber, PageSize, searchText);
            return Partial("_UserList",pagedList1);
        }
    }
}