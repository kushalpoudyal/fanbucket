﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using FanBucketUI.Extension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FanBucketUI.Pages.User
{
    [Authorize]
    public class MessengerModel : PageModel
    {
        public List<ChatUsersViewModel> ChatUsers = new List<ChatUsersViewModel>();
        public string LayoutPath = string.Empty;
        public Guid? extChatId;
        public Guid? extUserId;
        public string extUserImg;
        public string extName;

        private readonly SignalR data;
        public MessengerModel(SignalR _data)
        {
            data = _data;
        }

        public async Task<IActionResult> OnGet(Guid? ChatId, Guid? UserId)
        {
            if (!HttpContext.Request.IsAjaxRequest())
            {
                LayoutPath = "../Shared/_LayoutChat.cshtml";
            }

            ChatUsers = await data.GetChatUsers(User.Identity.GetJWT());
            foreach (var item in ChatUsers)
            {
                if (string.IsNullOrEmpty(item.ProfileImgLocation))
                {
                    item.ProfileImgLocation = "/Content/gallery/userimg.png";
                }
            }
            extChatId = ChatId;
            extUserId = UserId;
            //extUserId =Guid.Parse( User.Identity.GetUserId());
           foreach (var item in ChatUsers)
            {
                //item.UserStatus = FanBucketSignalR.Hubs.ChatHub.CurrentConnections.Any(x => x.UserId == item.UserId.ToString());
            }
            if (ChatId.HasValue && UserId.HasValue)
            {
                extUserImg = ChatUsers.Where(x => x.UserId == extUserId.Value).Select(x => x.ProfileImgLocation).FirstOrDefault();
                extName = ChatUsers.Where(x => x.UserId == extUserId.Value).Select(x => x.ChatWithName).FirstOrDefault();

            }
            return Page();
        }
    }
}