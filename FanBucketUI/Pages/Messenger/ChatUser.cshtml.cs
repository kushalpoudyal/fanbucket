﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;

namespace FanBucketUI.Pages.Messenger
{
    [Authorize]
    public class ChatUserModel : PageModel
    {
        public List<ChatUsersViewModel> postMaster = new List<ChatUsersViewModel>();
        private readonly SignalR data;
        public ChatUserModel(SignalR _data)
        {
            data = _data;
        }
        public async Task<IActionResult> OnGet(string ChatId)
        {

            postMaster = await data.GetChatUsers(User.Identity.GetJWT());
            foreach (var item in postMaster)
            {
                if (string.IsNullOrEmpty(item.ProfileImgLocation))
                {
                    item.ProfileImgLocation = "/Content/gallery/userimg.png";
                }
            }

            //await chat.Clients.Group()
            //   .SendAsync("ReceiveUser", new
            //   {
            //       Text = ReturnMsg.Message,
            //       Name = ReturnMsg.UserId.ToString(),
            //       Timestamp = ReturnMsg.When.ToString("dd/MM/yyyy hh:mm:ss")
            //   });

            return Page();

        }
        public async Task<IActionResult> OnPost(PostChatViewModel model)
        {

            //var ReturnMsg = await data.PostMessage(model, User.Identity.GetJWT());
            //await chat.Clients.Group(model.ChatId)
            //    .SendAsync("ReceiveMessage", new
            //    {
            //        Text = ReturnMsg.Message,
            //        Name = ReturnMsg.UserId.ToString(),
            //        Timestamp = ReturnMsg.When.ToString("dd/MM/yyyy hh:mm:ss")
            //    });

            return Page();
        }
    }
}