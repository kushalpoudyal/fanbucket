﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanBucketEntity.Extension;
using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using FanBucketUI.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Templates;

namespace FanBucketUI.Pages.User
{
    [Authorize]
    public class ChatContentModel : PageModel
    {
        private readonly IRazorViewToStringRenderer _renderer;
        public ChatViewModel chatViewModel = new ChatViewModel();
        private readonly SignalR data;

        public ChatContentModel(IRazorViewToStringRenderer renderer,
            SignalR _data)
        {
            _renderer = renderer;
            data = _data;
        }
        public async Task<IActionResult> OnGet(string ChatId, string UserId)
        {

            chatViewModel = await data.GetMessageByUser(ChatId, UserId, User.Identity.GetJWT());
            if (string.IsNullOrEmpty(chatViewModel.ProfileImgLocation))
            {
                chatViewModel.ProfileImgLocation = "/Content/gallery/userimg.png";
            }
            //var MessageUnreadCount = await data.GetMessageUnreadCount( User.Identity.GetJWT());
            //var CurId = User.Identity.GetUserId();

            //await hubContext.Clients.User(CurId).SendAsync("RecieveMessageUnreadCount", MessageUnreadCount);

            //chatViewModel.UserStatus = FanBucketSignalR.Hubs.ChatHub.CurrentConnections.Any(x => x.UserId == chatViewModel.UserId.ToString());
            return Page();

        }
        public async Task<IActionResult> OnPost([FromBody] PostChatViewModel model)//
        {
            var response = new JsonResponse()
            {
                success = true,
                ReturnMsg = "Successfull"
            };
            try
            {


                var UserId = User.Identity.GetUserId();
                var ProfileImg = User.Identity.GetProfileImg();
                var Name = User.Identity.GetName();
                var Token = User.Identity.GetJWT();
                var dataRtrn = await data.PostMessage(model, User.Identity.GetJWT());

                if (dataRtrn.success)
                {
                    var UserModel = await data.GetChatUsers(Token);
                    //var htmlBody = await _renderer.RenderViewToStringAsync("/Pages/Messenger/ChatUser.cshtml", UserModel);

                    //await chat.Clients.User(model.targetId).SendAsync("MessageNotification", "Send");

                    //await chat.Clients.Group(model.ChatId.ToString())
                    //              .SendAsync("ReceiveMessage", new
                    //              {
                    //                  Text = model.Message,
                    //                  Success = true,
                    //                  User = UserId,
                    //                  Time = DateTime.Now.Stringify(),
                    //                  ChatWithName = Name,
                    //                  profileImgLocation = ProfileImg,
                    //                  ChatId = model.ChatId,
                    //                  FileLocation = model.FileLocation,
                    //                  TipAmount = model.TipAmount,
                    //                  Attachments = model.Attachments.Select(x => x.FileLocation).ToArray()
                    //              });
                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = dataRtrn.ReturnMsg;
                    //await chat.Clients.Group(model.ChatId.ToString())
                    //         .SendAsync("ReceiveMessage", new
                    //         {
                    //             Text = model.Message,
                    //             Success = false
                    //         });
                }


            }
            catch (Exception ex)
            {
                response.success = false;
                response.ReturnMsg = "Error Occured.Please Try Again Later.";
                // await chat.Clients.Group(model.ChatId.ToString())
                //.SendAsync("ReceiveMessage", new
                //{
                //    Text = ex.Message,
                //    Success = false
                //});
            }
            return new JsonResult(response);

        }
    }
}