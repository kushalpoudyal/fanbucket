﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketUI.Routing
{
    public class UsernameConstraint : IRouteConstraint
    {
        public bool Match(HttpContext httpContext, IRouter route,
        string routeKey, RouteValueDictionary values,
        RouteDirection routeDirection)
        {
            if (values.TryGetValue(routeKey, out var routeValue))
            {
                var parameterValueString = Convert.ToString(routeValue,
                CultureInfo.InvariantCulture);
                return IsUsernameValid(parameterValueString);
            }
            return false;
        }
        private bool IsUsernameValid(string Username)
        {
            return true;
        }
    }
}
