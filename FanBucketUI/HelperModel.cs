﻿using FanBucketEntity.ViewModel;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FanBucketUI
{
    public class HelperModel
    {
        private readonly apiRequest api;
        public readonly ApplicationSettings applicationSettings;
        public HelperModel(apiRequest apiRequest, IOptions<ApplicationSettings> appsetings)
        {
            api = apiRequest;
            applicationSettings = appsetings.Value;
        }
        public async Task<List<TopAccountsViewModel>> GetRecommendersUser(ClaimsPrincipal UserC)
        {

            var recomUserr = await api.GetTopAccounts(UserC.Identity.GetJWT());
            return recomUserr;
        }

        public async Task<UserDetailsViewModel> GetCurrUserStatt(ClaimsPrincipal UserC)
        {

            var usr = await api.GetUsersById(UserC.Identity.GetUsername(), UserC.Identity.GetJWT());
            usr.Medias = await api.GetMediaByUser(UserC.Identity.GetJWT());
            return usr;
        }
        public async Task<int> GetNotifCount(ClaimsPrincipal UserC)
        {

            var usr = await api.GetNotificationCount(UserC.Identity.GetJWT());
            return usr;
        }
        public async Task<int> GetMessageCount(ClaimsPrincipal UserC)
        {

            var usr = await api.GetMessageCount(UserC.Identity.GetJWT());
            return usr;
        }

        public async Task<List<MessageReportTypeViewModel>> GetConversationReportTypes(ClaimsPrincipal UserC)
        {

            var usr = await api.GetConversationReportTypes(UserC.Identity.GetJWT());
            return usr;
        }
        public bool IsProfilePage(string Page)
        {


            if (Page == "ProfilePage")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
