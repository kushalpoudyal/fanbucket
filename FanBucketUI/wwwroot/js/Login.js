﻿if (!window.app) {
    window.app = {};
}

app.Login = (function ($) {

    var signupQueueModel = {
        FirstName: '',
        LastName: '',
        ContentStyle: '',
        FollowersAndPlatfrom: '',
        SocialMediaLinks: '',
        CSMReason: '',
        Password: '',
        Username: '',
        Latitude: null,
        Longitude: null
    };
    var initUI = function () {
        $("#flexCheckDefault").click(function () {
            if ($(this).is(":checked")) {
                $(".signup--bottom").find("button").removeAttr("disabled");
            } else {
                $(".signup--bottom").find("button").attr("disabled", true);
            }
        });

        //// Tests

        //// Test where paswords don't match
        //RunTest("hello", "hell"); // Too short
        //RunTest("helloThere", "helloTher"); // No numbers or symbols

        //// Test where passwords match, but are valid or invalid
        //RunTest("helloThere");         // No numbers or symbols
        //RunTest("helloThere6");        // Missing a symbol or space
        //RunTest("hello There 6");      // VALID
        //RunTest("helloThere6!");       // VALID
        //RunTest("6!6!6!6!6!6!6!6!");   // No letters
        //RunTest("6!6!6!6!A6!6!6!6!");  // VALID
        //RunTest("!@#$%^&*()-=+_");     // No numbers or letters
        //RunTest("!@#$%^&*()-=+_A4");   // VALID - Note that the test contains symbols that are not in the validation list
        //RunTest("!A12345678901234567890123456789012345678901234567890");   // valid contents but too long
        //RunTest("!A123456789012345678901234567890123456789012345678");     // valid, max length
        //RunTest("9Now is the time for all good men to come to the aide of their country!");     // Too long
        $('.signup--bottom button.signup--btn').click(function (e) {
            e.preventDefault();
            $(".btn--toggle").hide(300);
            $('.sign-up--form--1').show(300);

        })


        $('.sign-up--form--1 .signup--bottom .btn--common.back ').click(function (e) {
            e.preventDefault();
            $('.sign-up--form--1').hide(300);
            $(".btn--toggle").show(300);
        })

        // fan--btn

        $('.signup--bottom button.fan--btn').click(function (e) {
            e.preventDefault();
            $('.step-1').hide();

            $(".btn--toggle").hide(300);
            $('.join--form').show(300);
        })

        $('.step-0 .signup--bottom button.back').click(function (e) {
            e.preventDefault();
            $(".btn--toggle").show(300);
            $('.sign-up--form').hide(300);
            // $('.join--form').hide(300);
        })
        // step form logic
        $('.step-1 .signup--bottom button.next').click(function (e) {
            e.preventDefault();
            $(".step-1").hide(300);
            $('.step-2').show(300);
        })
        //// step form logic
        //$('.step-0 .signup--bottom button.next').click(function (e) {
        //    e.preventDefault();
        //    $(".step-0").hide(300);
        //    $('.step-1').show(300);
        //})
        //$('.step-1 .signup--bottom button.backk').click(function (e) {
        //    e.preventDefault();
        //    $(".step-1").hide(300);
        //    $('.step-0').show(300);
        //})
        $('.step-2 .signup--bottom button.backk').click(function (e) {
            e.preventDefault();
            $(".step-2").hide(300);
            $('.step-1').show(300);
        })
        $('.step-1 .signup--bottom button.back').click(function (e) {
            e.preventDefault();
            $(".step-1").hide(300);
            $('.step-0').show(300);
        })


        $("#submit_signup_queue").click(submitSignupQueue);


        $(".signup--wrapper__half .sign-up--form.step1").submit(function (e) {
            let parentFormHide = $(".signup--wrapper__half .sign-up--form:hidden")[0];
            let parentFormVisible = $(
                ".signup--wrapper__half .sign-up--form:visible"
            )[0];
            $(parentFormVisible).hide("300");
            $(parentFormHide).show("500");
        });

        $(".mob-login.mob-login--top .link").click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(".signup--wrapper").is(":visible")
                ? $(this).text("Sign Up")
                : $(this).text("Sign In");

            $(".signup--wrapper").slideToggle("300");
        });
        $(".mob-login.mob-login--top").click(function (e) {
            e.preventDefault();

            if ($(".signup--wrapper").is(":visible")) {
                $(".mob-login.mob-login--top .link").text("Sign Up");
                $(".signup--wrapper").slideToggle("300");
            }
        });
        document.addEventListener("keydown", function (event) {
            if (event.ctrlKey && event.key === "b") {
                $("#loginForm").find(".fieldShake").addClass("errorAnimate");
                $("#loginForm").find(".fieldShake").addClass("errorAnimate");
            }
        });
        $('.sign-up--form.sign-up--form--1 .signup--bottom .common').click(submitSignup);

        $('.Password').keyup(function () {
            console.log("PAssword");
            CheckPassword($(this));
        });
        $("#loginForm").find("button").click(checkLogin);

        $(".Email").change(function () {
            console.log("Email");
            checkEmail($(this));
        });

        //$(".Username").change(checkUsername($(this)));
        $(".Username").change(function () {
            checkUsername($(this));
        });
    };
    var submitSignup = function (e) {
        e.preventDefault();
        console.log("Sign Up called");
        console.log($(this));
        $(this).attr("disabled", true);
        $(this).append(`<img src="/Content/gallery/rings.svg" alt="" >`);

        var valid = CheckPassword($(this).closest("form").find(".Password"));
        if (!valid) {
            return false;

        }
        valid = checkEmail($(this).closest("form").find(".Email"));
        if (!valid) {
            return false;
        }
        valid = checkUsername($(this).closest("form").find(".Username"));
        if (!valid) {
            return false;
        }


        //return false;
        $(this).closest("form").submit();
        // $(this).removeAttr("disabled");
        //return true;

    }
    var CheckPassword = function ($this) {
        var THIS = $this;
        var findErrorLabel = THIS.closest(".form-group").find(".text-danger").attr('id');
        var password = THIS.val();
        var valid = IsValidatePassword(password, password, "#" + findErrorLabel);
        if (valid === false) {
            return false;
        }
        return true;
    };
    //signup_queue_password

    var checkLogin = function (e) {
        e.preventDefault();
        var userCred = {
            username: "",
            password: "",
        };
        var form = $("#loginForm");

        userCred.username = form.find('input[name="username"]').val();
        userCred.password = form.find('input[name="password"]').val();

        //start
        $(".progress--loader").show();
        if (userCred.username && userCred.password) {
            var url = apiURL + "/Authentication/CheckToken";
            $.ajax({
                url: url,
                type: "POST",
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(userCred),
            })
                .done(function (result) {
                    if (result) {
                        //end
                        $(".progress--loader").show();

                        form.submit();
                    } else {
                        $(".progress--loader").hide();
                        $("#loginForm").find("input").addClass("errorAnimate");
                        setTimeout(() => {
                            $("#loginForm").find("input").removeClass("errorAnimate");
                        }, 1000);
                    }
                })
                .fail(function (error) {
                    $(".progress--loader").hide();
                    $("#loginForm").find("input").addClass("errorAnimate");
                    setTimeout(() => {
                        $("#loginForm").find("input").removeClass("errorAnimate");
                    }, 1000);
                });
        } else {
            $(".progress--loader").hide();
            $("#loginForm").find("input").addClass("errorAnimate");
            setTimeout(() => {
                $("#loginForm").find("input").removeClass("errorAnimate");
            }, 1000);
        }
    };

    var checkUsername = function ($this) {
        //var THIS = $(".sign-up--form").find('input[name="Username"]');
        //var THIS = $(this);
        debugger;
        var THIS = $this;
        var Username = THIS.val();
        if (Username == '') {
            return;
        }
        var valid = false;
        if (Username) {
            var url = apiURL + "/Authentication/UsernameCheck?username=" + Username;
            $.ajax({
                url: url,
                type: "POST",
                crossDomain: true,
                async: false,
                contentType: "application/json; charset=utf-8",
            })
                .done(function (exists) {
                    if (exists) {
                        //valid = true;
                        //THIS.val("");
                        //THIS.next().removeClass("hide");
                        THIS.closest(".form-group").find("p.text-danger").removeClass("hide");
                    } else {
                        //THIS.next().addClass("hide");
                        THIS.closest(".form-group").find("p.text-danger").addClass("hide");

                        valid = true;

                    }
                })
                .fail(function (error) { });
        }
        return valid;
    };

    var submitSignupQueue = function () {
        var parentForm = $(this).closest(".join--form");
        var contentstyle = "";
        var arr = [];
        parentForm.find('.form-check-input:checked').each(function () {
            arr.push($(this).val());
            console.log(this.value);
        });
        var OtherContentStle = $('#Other').val();
        if (OtherContentStle) {
            arr.push(OtherContentStle);

        }
        signupQueueModel.FirstName = parentForm.find('[name="FirstName"]').val();
        signupQueueModel.LastName = parentForm.find('[name="LastName"]').val();
        signupQueueModel.LastName = "default";
        signupQueueModel.EmailAddress = parentForm.find('[name="EmailAddress"]').val();
        signupQueueModel.ContentStyle = arr.join(",");
        signupQueueModel.FollowersAndPlatfrom = parentForm.find('[name="FollowersAndPlatfrom"]').val();
        signupQueueModel.SocialMediaLinks = parentForm.find('[name="SocialMediaLinks"]').val();
        signupQueueModel.CSMReason = parentForm.find('[name="CSMReason"]').val();
        signupQueueModel.Password = parentForm.find('[name="Password"]').val();
        signupQueueModel.Username = parentForm.find('[name="Username"]').val();

        signupQueueModel.FirstName = parentForm.find('[name="Fullname"]').val();
        signupQueueModel.EmailAddress = parentForm.find('[name="Email"]').val();


        var valid = CheckPassword($(this).closest("form").find(".Password"));
        if (!valid) {
            return false;

        }
        valid = checkEmail($(this).closest("form").find(".Email"));
        if (!valid) {
            return false;
        }
        valid = checkUsername($(this).closest("form").find(".Username"));
        if (!valid) {
            return false;
        }
        console.log(signupQueueModel);
        var url = apiURL + "/Authentication/SignUpQueue";
        $.ajax({
            url: url,
            type: "POST",
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(signupQueueModel)
        })
            .done(function (response) {
                console.log(response);
                if (response.success) {

                    location.href = '/Login?Status="true"&Msg="Successfully Submitted."';

                } else {
                    console.error("error" + response.returnMsg);
                    alert("Error" + response.returnMsg);
                    //    location.href = '/Login';
                }
            })
            .fail(function (error) { console.log(error) });

    };


    var checkEmail = function ($this) {
        //var THIS = $(".sign-up--form").find('input[name="Email"]');
        //var THIS = $(this);
        var THIS = $this;
        var Email = THIS.val();
        var valid = false;

        if (Email) {
            var url = apiURL + "/Authentication/EmailCheck?Email=" + Email;
            $.ajax({
                url: url,
                type: "POST",
                crossDomain: true,
                async: false,
                contentType: "application/json; charset=utf-8",
            })
                .done(function (exists) {
                    if (exists) {
                        //THIS.val("");
                        THIS.closest(".form-group").find("p.text-danger").removeClass("hide");

                        //    THIS.next().removeClass("hide");
                    } else {
                        valid = true;
                        //THIS.next().addClass("hide");
                        THIS.closest(".form-group").find("p.text-danger").addClass("hide");
                    }
                })
                .fail(function (error) { });
        } else {
        }

        return valid;
    };

    return {
        initUI: initUI,
    };
})(jQuery);
jQuery(app.Login.initUI);
