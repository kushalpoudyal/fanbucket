﻿if (!window.app) {
    window.app = {};
}

app.Notitifications = (function ($) {

    var CommonSite = app.CommonSite;

    var initUI = function () {

        $(".btnMarkRead").click(MarkAsRead);

        $(".btnHide").click(HideNotification);
        $("#__GetHiddenPost").click(HiddenPost);
       

    }
    window.addEventListener('DOMContentLoaded', (event) => {

        let marker = document.querySelector('.indicator');
        //let marker = $(this).ind;
        let item = document.querySelectorAll('.nav.nav-tabs .nav-link');
        let itemActive = document.querySelector('.nav.nav-tabs .nav-link.active');

        function indicator(e) {
            marker.style.left = e.offsetLeft + "px";
            marker.style.width = e.offsetWidth + "px";
        }
        indicator(itemActive);
        item.forEach(link => {
            link.addEventListener('click', (e) => {
                console.log($(this));
                console.log($(this).closest("ul").find(".indicator"));
                console.log($(this).closest(".nav.nav-tabs").find(".indicator"));
                //    indicator(e.target);
                $(e.target).closest(".nav.nav-tabs").find(".indicator").css({ left: e.target.offsetLeft + "px", width: e.target.offsetWidth + "px" });

                //     marker.style.left = e.offsetLeft + "px";
                //marker.style.width = e.offsetWidth + "px";
            })
        })
    });


    var HiddenPost = function (e) {
        var $this = $(this);
        //$(this).addClass("active");
        //e.preventDefault();
        //$(this).toggleClass('active');
        //$(this).toggleClass('show');

        $('#HiddenPost').empty();
        var url = CommonSite.appURL + "/Post/HiddenPost";
        //var auth = $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'GET',
            //contentType: 'application/json; charset=utf-8',
            //headers: {
            //    Authorization:
            //        "Bearer " + auth
            //}
        }).done(function (response) {
            var Feed = document.getElementById("HiddenPost");
            Feed.innerHTML = response;
            CommonSite.ContentAddons();

        }).fail(function (error) {
            alert(error);
        });

    };
    var HideNotification = function () {
        var evt = $(this);
        evt.parents('.notification--card').remove();
    };
    var MarkAsRead = function () {
        var evt = $(this);
        $(this).append(`<img src="/Content/gallery/rings.svg" alt="" >`);

        var id = $(this).attr('data-id');

        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: CommonSite.apiURL + '/Notification?Id=' + id,
            type: 'POST',
            crossDomain: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (data) {
            evt.find('img').remove();
            $d.resolve(data);
            CommonSite.EventNotification(data);
            evt.parents('.notification--card').find('time i').remove();
        }).fail(function (data) {
            evt.find('img').remove();
            $d.reject(data);
            CommonSite.EventNotification(data);
        });

        return $d.promise();
    };

    return {
        initUI: initUI
    };
})(jQuery);
jQuery(app.Notitifications.initUI);