﻿if (!window.app) {
    window.app = {};
}

app.CommonSite = (function ($) {
    var Payments = app.Payments;
    //const appURL = 'https://localhost:44300';
    //const apiURL = 'http://localhost:56556/api';
    var fullImageImg = "";
    var ContentType = 'Text';

    var validImageTypes = ["image/gif", "image/jpeg", "image/png", "image/bmp"];
    var validVideoTypes = ["video/mp4", "video/avi", "video/m4v", "video/mpg"];
    var validDocumentTypes = ["application/pdf"];
    var validAudioTypes = ["audio/mp4", "audio/mpeg", "audio/mp3", "audio/x-m4a"];
    //var allTypes = "image/*,image/heif,image/heic,video/*,video/mp4,video/x-m4v,video/x-matroska,.mkv";

    var CurrentPictureModel = {
        Creator: '',
        CreatorId: '',
        CreatorImg: '',
        PostId: '',
        CurrentElem: '',
        CreatedOn: '',
        DateString: '',
        Collection: []
    };

    var apiModel = {
        Id: '',
        PostTitle: '',
        PostPrivacy: '',
        PaidPost: '',
        PostPrice: '',
        NSFW: 'false',
        IsMedia: 'false',
        ParentPostId: '',
        IsScheduled: 'false',
        ScheduledFor: '',
        CreatedDate: '',
        PostCollections: []


    };

    var apiEditModel = {
        Id: '',
        PostTitle: '',
        PostPrivacy: '',
        PaidPost: '',
        PostPrice: '',
        NSFW: 'false',
        IsMedia: 'false',
        ParentPostId: '',
        IsScheduled: 'false',
        ScheduledFor: '',
        CreatedDate: '',
        PostCollections: []


    };
    var momentsModal = {
        PostId: [],
        CurrentId: '',
        UserId: '',
        CreatorName: '',
        CreatorImg: '',
        CurrentLoop: 0
    }
    function validAcceptedTypes() {
        var validTypes = "";

        var val = $('#MediaType').val();

        if (val == "Video") {
            validTypes = validVideoTypes.join(",");
        }
        else if (val == "Audio") {
            validTypes = validAudioTypes.join(",");
        }
        else if (val == "Document") {
            validTypes = validDocumentTypes.join(",");
        }
        else if (val == "Image") {
            validTypes = validImageTypes.join(",");
        }
        else {
            validTypes = validImageTypes.join(",");
        }
        return validTypes;
    }
    var scrollContentsLoad = true;

    const darModeElem = $('#darkmode');

    initUI = function () {

        //window.onfocus = function () {
        //    window.onclick = function () {
        //        console.log('on');
        //        $('body').find('img').show();
        //    }

        //};
        //window.onblur = function () {
        //    console.log('off');
        //    $('body').find('img').hide();
        //};

        //window.onblur = function () {
        //    console.log('off');
        //    $('body').find('img').hide();
        //};
        $(".post__card__link__input").on('change', function (e) {
            let timestamp = e.timeStamp;
            uploadImage(this, "#post--modal .modal--post__ph--upload", timestamp);
        });
        $(".otp .eye").click(function (e) {
            debugger;
            let elem = $(this).find("i");
            if (elem.hasClass("bi-eye")) {
                elem.removeClass("bi-eye");
                elem.addClass("bi-eye-slash");
                $(".otp> input").removeAttr("type");
                $(".otp> input").attr("type", "password");
            } else {
                elem.addClass("bi-eye");
                elem.removeClass("bi-eye-slash");
                $(".otp> input").removeAttr("type");
                $(".otp> input").attr("type", "number");
            }
        });
        $('.right--aside #postSearch').keyup(SearchVideos);
        $('.profile__card__wallpaper').find('.card__top__link').find('.btnSubscribeUser').click(SubscribeToUser);
        $('.profile__card__wallpaper').find('.card__top__link').find('.cancelSubs').click(cancelSubscriptionPrompt);
        $('.user--relation--trigger').click(GetUserRelation);
        //$('.pollfile').on('change',uploadPollFile(this));
        $('.pollfile').on('change', function () {
            uploadPollFile(this);
        });
        let options = {
            root: null,
            rootMargin: '0px',
            threshold: 0.5
        }

        let callback = (entries, observer) => {
            entries.forEach(entry => {
                // Each entry describes an intersection change for one observed
                // target element:
                //   entry.boundingClientRect
                //   entry.intersectionRatio
                //   entry.intersectionRect
                //   entry.isIntersecting
                //   entry.rootBounds
                //   entry.target
                //   entry.time
                if (entry.isIntersecting && scrollContentsLoad) {
                    scrollContentsLoad = false;
                    loadContents();
                    //    scrollContentsLoad = true;
                }


                console.log(entry.isIntersecting);
            });
        };
        let observer = new IntersectionObserver(callback, options);
        let target = document.querySelector('.main--feed #End');
        if (target != null) {
            observer.observe(target);

        }

        //      Gif On Click
        $(document).on("click", ".grid img", GifOnClick);


        //LiveNotification();


        //$('.moments--modal .play').click(function (e) {
        //    $(this).find('i').toggleClass('bi-play-fill');
        //    $(this).parents('.modal-content').find('.progress-bar').toggleClass('paused');
        //})
        $('.modal--post .poll--trash').click(function (e) {
            debugger;
            $('.modal--post .poll__wrapper').toggle(300);
            if (ContentType == "Polls") {
                ResetPoll();
                ContentType = "Text";
                $('#MediaType').removeAttr('disabled');
            }
            else {
                ContentType = "Polls";
                $('#MediaType').attr('disabled');
            }
        });
        // poll added
        const pollWrapper = $('.modal--post  .poll__wrapper .poll--inside');
        let elem = $('#poll--inside--count');
        elem.val(pollWrapper[0].childElementCount - 1);

        // function remove cross if there is only two list item
        const crossCheck = () => {
            // console.log(pollWrapper[0].childElementCount)
            if (pollWrapper[0].childElementCount > 2) {
                pollWrapper.removeClass('removeCross');
            }
            else {
                pollWrapper.addClass('removeCross');
            }
        }
        crossCheck();
        $(document).on('click', 'a.file_link', function (e) {
            e.preventDefault();
            var url = $(this).data('src');
            window.open(url, '_blank');
        });
        $('.modal--post .poll__wrapper .poll--buttom button.poll--add').click(function (e) {

            e.preventDefault();
            // init
            let pollMax = elem[0].max;

            // universal input value
            let pollCount = parseInt(elem.val());

            // for placeholder option 1234 ..
            let placeholderCount = pollCount + 1;
            pollWrapper.removeClass('removeCross');

            if (pollCount < pollMax) {
                // clone template of poll and its child
                let pollTemplate = pollWrapper.find('.custom-control:last-child').clone();
                let pollTemplateInput = pollTemplate.find('input');
                pollTemplateInput.val('');

                let pollTemplateInputWrapper = pollWrapper.find('.custom-control:last-child');


                // grab recent id
                let elemId = pollTemplateInputWrapper.attr('id');

                // set new id for template using replace
                //elemId = (elemId.replace(`_${pollCount}`, `_${++pollCount}`));

                elemId = `pollwrapper_${++pollCount}`;
                // grab recent placeholder and replace bew one
                let newPlaceholder = pollTemplateInput.attr('placeholder').replace(`Option ${placeholderCount}`, `Option ${++placeholderCount}`);

                // add new placeholder and id from above
                pollTemplateInput.attr('placeholder', newPlaceholder)
                pollTemplateInputWrapper.attr('id', elemId)

                // set elem value 
                elem.val(pollCount);

                // after all these finally appen the poll template phew 
                pollTemplate.appendTo(pollWrapper);
                $('.pollfile').unbind().on('change', function () {
                    uploadPollFile(this);
                });

                pollTemplateInput.focus();
                if (pollCount >= pollMax) {
                    $(this).hide();
                }

            }
            else {
                $(this).hide();
            }

        })

        $('.modal--post .poll__wrapper .poll--inside').on('click', 'a.count', function (e) {
            debugger;

            let elemC = $(this);

            let elemParents = elemC.parents('.custom-control');
            let elemIndex = elemParents.index();
            // let placeholderCount = elemIndex + 2;
            let elemVal = parseInt(elem.val());



            elem.val(--elemVal)

            elemParents.nextAll().each((index, elem) => {

                let elemCC = $(elem).find('input');
                // console.log(elemCC)

                // grab recent id
                let elemId = elemCC.attr('id');

                // set new id for template using replace
                elemId = (elemId.replace(`_${elemIndex + index + 1}`, `_${elemIndex + index}`));

                // grab recent placeholder and replace bew one
                let newPlaceholder = elemCC.attr('placeholder').replace(`Option ${elemIndex + index + 2}`, `Option ${elemIndex + index + 1}`);

                // add new placeholder and id from above
                elemCC.attr('placeholder', newPlaceholder)
                elemCC.attr('id', elemId)

            })
            elemParents.hide(300);
            elemParents.remove();
            $('.modal--post .poll__wrapper .poll--buttom button.poll--add').show();
            crossCheck();



        });

        //$('.modal--post form').submit(PostContent);
        //$('#edit--post--modal form').submit(SubmitEditPost);
        $('#post--modal form').submit(PostContent);
        $(".public--or--paid").change(function (e) {
            let paidElem = $(this).parent().next();
            if ($(this)[0].selectedIndex) {
                $(".modal--post .lock").removeClass("unlocked");
                apiModel.PostCollections.forEach(function (val, index) {
                    apiModel.PostCollections[index].PaidPost = "true";
                });
                paidElem.removeClass("hide");
            } else {
                paidElem.addClass("hide");
                apiModel.PostCollections.forEach(function (val, index) {
                    apiModel.PostCollections[index].PaidPost = "false";
                });
                $(".modal--post .lock").addClass("unlocked");
            }
        });
        $('.pass--wrapper .eye').click(function (e) {
            debugger;
            let elem = $(this).find("i");
            console.log($(this).prev('input'))
            if (elem.hasClass("bi-eye")) {
                elem.removeClass("bi-eye");
                elem.addClass("bi-eye-slash");

                $(this).prev("input").removeAttr("type");
                $(this).prev("input").attr("type", "password");
            } else {
                elem.addClass("bi-eye");
                elem.removeClass("bi-eye-slash");
                $(this).prev("input").removeAttr("type");
                $(this).prev("input").attr("type", "text");
            }
        });
        $(document).on("submit", ".modal--shareFeed form", PostContent);
        $('#post__card__link__input').unbind().change(function (e) {
            let timestamp = e.timeStamp;
            if (!($("#post--modal").data('bs.modal') || {})._isShown) {
                $("#post--modal").modal('toggle');
            }
            uploadImage(this, '#post--modal .modal--post__ph--upload', timestamp);
        });
        $('#MediaType').change(function () {
            var validTypes = validAcceptedTypes();
            ContentType = $(this).val();
            //alert(ContentType);
            $(this).closest("form").find('.modal--post__ph--upload>[class="image"]').remove();
            $(this).closest("form").find('.modal--post__ph--upload>[class="image uploading"]').remove();
            $(this).closest("form").find('.image.empty--input').addClass("hide");
            apiModel.PostCollections = [];
            debugger;
            $('.post__card__link__input').each(function () {
                $(this).attr('accept', validTypes);
            });
        });



        $(".public--or--paid").change(function () {
            if ($(this).val() === "true") {
                checkForKyc();
            }
        });
        $(document).on("click", ".poll-analytics", function () {
            $("ul#myTab").empty();
            $("#myTabContent").empty();
            $('#myTab.nav.nav-tabs').scrollingTabs('destroy');
            $('#poll-analytics-modal').modal('show');

            var auth = $('#token___Auth').val();
            var id = $(this).data("id");
            var url = apiURL + "/PostMasters/PollAnalytics?Id=" + id;
            $.ajax({
                url: url,
                type: 'GET',
                crossDomain: true,
                contentType: 'application/json; charset=utf-8',
                headers: {
                    Authorization:
                        "Bearer " + auth
                }
            }).done(function (result) {

                console.log(result);
                result.jsonObj.map(function1);

                //// Refreshing the tab as we have just added new tab  
                //$("#tabs").tabs("refresh");  
                $('#myTab.nav.nav-tabs').scrollingTabs({
                    tabs: null,
                    ArrowsOnFullyScrolled: true,
                    disableScrollArrowsOnFullyScrolled: true,
                    enableSwiping: false,
                    scrollToTabEdge: true,

                    bootstrapVersion: 4,
                    cssClassLeftArrow: 'fa fa-chevron-left',
                    cssClassRightArrow: 'fa fa-chevron-right'
                });
                //reset the modal
                //show modal
                //set the analytics tab data by tempalte
                //an append to html
                //show modal
            });
        });
        function function1(currentValue, index) {
            console.log(currentValue);
            var active = index === 0 ? "active" : "";
            var show = index === 0 ? "show" : "";
            $("ul#myTab").append(
                `<li class="nav-item">
                            <a class="nav-link ${active}" data-toggle="tab" href="#${currentValue.optionTitle}_voters" role="tab" aria-controls="all_voters" aria-selected="true">${currentValue.optionTitle}</a>
                        </li>`);
            var temp = "";

            currentValue.voters.map(function (voter, index) {
                temp += `<a href='/${voter.creatorUsrName}' title='Send message to Person Name' class='card chat--card'> <div class='card__top'>
                                    <div class='card__img'>
                                        <img src='${voter.creatorImg}' alt=''>
                                      
                                    </div>
                                    <div class='card__top__body'>
                                        <h2 class='card__title'>${voter.creator}</h2>
                                    </div>
                                </div>
                            </a>`;

            });
            $("#myTabContent").append(
                `<div class='tab-pane fade ${show} ${active}' id='${currentValue.optionTitle}_voters' role='tabpanel' aria-labelledby='home-tab'>`
                + temp +

                `</div>`
                //    "<div style='display:none;' id='tab-" + newid + "'><p>Content for Tab: " + $("#divDialog input").val() + "</p></div>"  
                //    "<li class='nav-item' id='" + newid + "'><a href='#tab-" + newid + "'>" + OptionName + "</a></li>"  
            );
            //$('#myTab').scrollingTabs('refresh', {
            //    forceActiveTab: true
            //});


        }

        //$(document).unbind().ajaxError(function () {
        //    var result = {
        //        returnMsg: 'Something Went Wrong. Please Try Again!'
        //    };
        //    EventNotification(result);
        //});
        //$.ajax({
        //    url: "http://52.15.150.6:8090" + "/swagger",
        //    dataType: "jsonp",
        //    statusCode: {
        //        200: function (response) {
        //            console.log('Connected!');
        //        },
        //        404: function (response) {
        //            var result = {
        //                returnMsg: 'Server offline detected!!'
        //            };
        //            EventNotification(result);
        //        }
        //    }
        //});

        darModeElem.click(function (e) {
            e.preventDefault();
            let text;
            if ($(this).hasClass('active')) {
                $('body').removeClass('darkmode')
                $(this).removeClass('active')
                $(this).find('i').removeClass('bi-brightness-high');
                text = $(this).html().replace('Light', 'Dark');
                setCookie('darkmode', false, 0)
            } else {
                $('body').addClass('darkmode')
                $(this).addClass('active')
                $(this).find('i').addClass('bi-brightness-high')
                text = $(this).html().replace('Dark', 'Light');
                setCookie('darkmode', true, 30)
            }

            $(this).html(text)
        })

        let darkMode = getCookie('darkmode');
        if (darkMode) {
            $('body').addClass('darkmode')
            darModeElem.addClass('active')
            darModeElem.find('i').addClass('bi-brightness-high')
            text = darModeElem.html().replace('Dark', 'Light');
            darModeElem.html(text)
        }
        else {
            $('body').removeClass('darkmode')
        }


        if (darkMode) {
            $('body').addClass('darkmode')
        }
        else {
            $('body').removeClass('darkmode')
        }


    };



    var gifIndex = 0;
    // on clicking on gif, add its url to input value;
    var GifOnClick = function (e) {
        let src = $(this).attr("src");
        // let title = $(this).attr("title");
        // const urlToObject = async () => {
        //   const response = await fetch(src);
        //   const blob = await response.blob();
        //   const file = new File([blob], title, { type: blob.type });
        //   console.log(file);
        // };
        var data = {
        };
        data.returnId = gifIndex.toString();
        data.returnPath = src;
        let imageAppend = $("#post--modal .modal--post__ph--upload");
        let timestamp = e.timeStamp;

        let paidElem = $("#public--or--paid")[0].selectedIndex;
        //<div class="watermark">watermark</div>

        let image = ` <div class="image uploading">
				   <img src="${src}" alt="">
				   <input type="file" accept="image/*,image/heif,image/heic,video/*,video/mp4,video/x-m4v,video/x-matroska,.mkv" class="input__hide" name="uploadedImage" id="uploadedImage__${timestamp}" value="${src}">
				   <a href="#!" class="lock  ${paidElem ? " " : " unlocked "}">
				     <svg xmlns="http://www.w3.org/2000/svg" width="502.571" height="562.771" viewBox="0 0 502.571 562.771">
				       <path id="Subtraction_1" data-name="Subtraction 1" d="M-9212.429,706.77H-9715V347.906h47.059a205.856,205.856,0,0,1,4.235-41.035,203.366,203.366,0,0,1,11.938-38.224,204.484,204.484,0,0,1,18.827-34.6,205.982,205.982,0,0,1,24.9-30.155,206.034,206.034,0,0,1,30.159-24.9,204.617,204.617,0,0,1,34.6-18.825,203.485,203.485,0,0,1,38.234-11.938A205.907,205.907,0,0,1-9463.993,144a205.7,205.7,0,0,1,42.227,4.485,203.391,203.391,0,0,1,39.218,12.63A204.665,204.665,0,0,1-9347.23,181a206.061,206.061,0,0,1,30.527,26.251l-31.746,27.321a161.253,161.253,0,0,0-115.845-48.8c-89.36.085-162.129,72.816-162.214,162.129l324.388.044c0-.195,0-.388,0-.582v-.023h42.033v.025c0,.177,0,.359,0,.536h47.658V706.769Zm-251.865-221.811a46.223,46.223,0,0,0-46.156,46.071,45.909,45.909,0,0,0,28.683,42.592v56.721h35.036V573.715l-.047.019-.041.017.088-.129v.093a45.931,45.931,0,0,0,28.509-42.685A46.123,46.123,0,0,0-9464.294,484.959Z" transform="translate(9715 -143.999)" fill="#fff"/>

				       <path id="Subtraction_2" data-name="Subtraction 1" d="M-9212.429,706.77H-9715V347.906h47.059a205.856,205.856,0,0,1,4.235-41.035,203.366,203.366,0,0,1,11.938-38.224,204.484,204.484,0,0,1,18.827-34.6,205.982,205.982,0,0,1,24.9-30.155,206.034,206.034,0,0,1,30.159-24.9,204.617,204.617,0,0,1,34.6-18.825,203.485,203.485,0,0,1,38.234-11.938A205.907,205.907,0,0,1-9463.993,144a205.8,205.8,0,0,1,40.95,4.217,203.406,203.406,0,0,1,38.152,11.888,204.534,204.534,0,0,1,34.545,18.749,206.089,206.089,0,0,1,30.127,24.8,206.05,206.05,0,0,1,24.9,30.042,204.534,204.534,0,0,1,18.86,34.474,203.365,203.365,0,0,1,12.014,38.1,205.649,205.649,0,0,1,4.356,40.909l-42.033.031c-.482-88.885-73.231-161.3-162.171-161.43-89.36.085-162.129,72.816-162.214,162.129l324.388.044c0-.195,0-.388,0-.582v-.023h42.033v.025c0,.177,0,.359,0,.536h47.658V706.769Zm-251.865-221.811a46.223,46.223,0,0,0-46.156,46.071,45.909,45.909,0,0,0,28.683,42.592v56.721h35.036V573.715l-.047.019-.041.017.088-.129v.093a45.931,45.931,0,0,0,28.509-42.685A46.123,46.123,0,0,0-9464.294,484.959Z" transform="translate(9715 -143.999)" fill="#fff"/>


				     </svg>
				   </a>
				   <div class="edit--wrapper">
				     <label for="uploadedImage__${timestamp}">change</label>
				     <a href="#" class="new--trash trash" data-val="${data.returnId}"><i class="bi bi-trash"></i></a>
				   </div>
				   <div class="loader__wrapper">
				     <div class="loader">
				       <div class="box">
				         <img src="./gallery/logoWhite.png" alt="fanbucket">
				       </div>
				       <div class="shadow"></div>
				     </div>
				   </div>
				 </div>`;
        gifIndex++;
        AddContentToPostCollection(data, paidElem, "Image");
        imageAppend.append(image);
        setTimeout(function (e) {
            imageAppend.find(".uploading").removeClass("uploading");
        }, 3000);
        $('.modal--post__ph--upload .lock').unbind().click(LockUnlockPost);
        $('#post--modal .new--trash.trash').unbind().click(DeleteContent);
        $(".image.empty--input.hide").removeClass("hide");
    }
    var VotePoll = function () {
        var THIS = $(this);
        //checkboxwrapper
        var pollwrapper = $(this).closest(".poll__wrapper");
        var checkboxwrapper = pollwrapper.find(".custom-checkbox");//.find(".count--wrapper");
        THIS.parents('.poll__wrapper').prop('disabled', true);
        THIS.parents('.poll__wrapper').css('opacity', '0.5');
        var ContentId = THIS.attr('data-id');
        var PostId = THIS.attr('data-val');
        var TotalVoteCount = $('#voteCount_' + PostId);
        //var OptionVoteCount = $('#voteCount_' + ContentId);

        var url = apiURL + '/PostMasters/VoteForPoll?PostId=' + PostId + '&ContentId=' + ContentId;
        var auth = $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            if (result.success) {

                var data = result.jsonObj;
                checkboxwrapper.each(function () {
                    var cnt = $(this).find(".count--wrapper");
                    cnt.empty();
                    var id = $(this).attr("id");
                    // alert(id);
                    var index = data.findIndex(x => x.contentId === id);
                    var temp = data[index];
                    var length = temp.totalVote;
                    for (var i = 0; i < length; i++) {
                        if (i == 3) {
                            var voter = `<span class="count" data-toggle="tooltip" data-placement="top">
                             ${temp.totalVote - temp.voters[i]}
                                        </span>`;
                            cnt.append(voter);

                            break;
                        }
                        var voter = `<span class="count" data-toggle="tooltip" data-placement="top" title="${temp.voters[i].creator}">
                            <img src="${temp.voters[i].creatorImg}" alt="">
                        </span>`;
                        cnt.append(voter);
                    }

                });


                TotalVoteCount.val(result.returnMsg);
                THIS.parents('.poll__wrapper').prop('disabled', false);
                THIS.parents('.poll__wrapper').css('opacity', '1');
            }
            else {
                THIS.parents('.poll__wrapper').prop('disabled', false);
                THIS.parents('.poll__wrapper').css('opacity', '1');
            }

        }).fail(function (error) {
            THIS.parents('.poll__wrapper').prop('disabled', false);
            THIS.parents('.poll__wrapper').css('opacity', '1');
        });

    };

    var checkForKyc = function () {

        var auth = $('#token___Auth').val();
        var url = apiURL + "/UserInterest/KYCExists";
        $.ajax({
            url: url,
            type: 'GET',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            result = true;
            if (!result) {
                var Response = {
                    returnMsg: "Hey looks like you want to be a content Creator. Please Fill Your KYC First."
                }
                $('#post--modal').find(".public--or--paid").val('false');
                $('#post--modal').find("#input__paid").parent('div').addClass('hide');
                $('#post--modal').modal('hide');
                EventNotification(Response);
                $('#kyc--modal').modal('show');
                $('#kyc--modal').find("input[name='CardNumber']").on('keyup', function () {
                    event = this;
                    if ($(this).val().length == 16) {
                        if (cardnumber($(this).val())) {

                            $(event).css('border-color', '');
                            $('#kyc--modal').find('#btnKYC').css('display', 'block');
                        }
                        else {
                            Response = {
                                returnMsg: "Invalid Card No."
                            }
                            EventNotification(Response);

                            $(event).css('border-color', 'red');
                            $('#kyc--modal').find('#btnKYC').css('display', 'none');
                        }
                    }
                    else {
                        $(event).css('border-color', 'red');
                        $('#kyc--modal').find('#btnKYC').css('display', 'none');
                        return false;
                    }
                });
                $('#kyc--modal').find('input[name="Expiry"]').on('keyup', function () {
                    expiryMask();
                });

                $('#kyc--modal').find('input[name="Expiry"]').on('focusout', function () {
                    splitDate($(this), $(this).val());
                });
                const parent = $('#kyc--modal .steps--wrapper');
                let count = parent.find('.card__link .count');
                $('#steps--next').click(function (e) {
                    e.preventDefault();
                    let elem = parent.find('.steps.active');
                    if (elem.next().hasClass('steps')) {
                        elem.removeClass('active');
                        elem.next().addClass('active');
                        $('#steps--prev').removeClass('d-none');
                        let count = elem.index() + 2;
                        calcWidth(count);
                        elem.next()[0].scrollIntoView({
                            behavior: "smooth",
                            block: "center",
                            inline: "start"
                        });
                        if (elem.next().hasClass('step-5')) {
                            $('#steps--next').hide();
                            $('#btnKYC').removeAttr('hidden');
                        }
                    }
                    else {
                        $(this).parents('form').submit();
                    }

                })
                $('#steps--prev').click((e) => {
                    e.preventDefault();
                    let elem = parent.find('.steps.active');
                    console.log(elem.index())
                    if (elem.index() > 0) {
                        $('#steps--next').show();
                        $('#btnKYC').attr('hidden', 'hidden');
                        elem.removeClass('active');
                        elem.prev().addClass('active');
                        let count = elem.index();
                        calcWidth(count);
                        elem.prev()[0].scrollIntoView({
                            behavior: "smooth",
                            block: "center",
                            inline: "start"
                        });
                        if (elem.index() === 1)
                            $('#steps--prev').addClass('d-none')


                    }

                })
                $('#btnKYC').click(EditKYC);

            }

        }).fail(function (error) {
            console.log(error);
        });
    };
    const calcWidth = (count) => {
        // console.log(count * 33);

        $('#kyc--modal .steps--bottom .progress-bar').width(count * 20 + '%');

    }
    var cardnumber = function (inputtxt) {
        var cardVisa = [/^(?:4[0-9]{12}(?:[0-9]{3})?)$/, /^(?:5[1-5][0-9]{14})$/];
        var val = false;
        for (i = 0; i < cardVisa.length; i++) {
            if (inputtxt.match(cardVisa[i])) {
                val = true;
                break;
            }
        }

        return val;

    };
    var expiryMask = function () {
        var inputChar = String.fromCharCode(event.keyCode);
        var code = event.keyCode;
        var allowedKeys = [8];
        if (allowedKeys.indexOf(code) !== -1) {
            return;
        }

        event.target.value = event.target.value.replace(
            /^([1-9]\/|[2-9])$/g, '0$1/'
        ).replace(
            /^(0[1-9]|1[0-2])$/g, '$1/'
        ).replace(
            /^([0-1])([3-9])$/g, '0$1/$2'
        ).replace(
            /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2'
        ).replace(
            /^([0]+)\/|[0]+$/g, '0'
        ).replace(
            /[^\d\/]|^[\/]*$/g, ''
        ).replace(
            /\/\//g, '/'
        );
    }

    var splitDate = function ($domobj, value) {
        var regExp = /(1[0-2]|0[1-9]|\d)\/(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)/;
        var matches = regExp.exec(value);
        $domobj.siblings('input[name$="expiryMonth"]').val(matches[1]);
        $domobj.siblings('input[name$="expiryYear"]').val(matches[2]);
    }


    var PostContent = function (e) {
        e.preventDefault();
        //change to show lodaer text if it is for edit
        let hideElem = $(this).find('.form__wrapper');
        let showElem = $(this).next('.loader__wrapper');
        let modalParent = $(this).closest('.modal')
        hideElem.hide(400);
        showElem.addClass('active');
        apiModel.NSFW = $(this).find('[name="NSFW"]:checked').val();
        apiModel.ParentPostId = $(this).find("[name='ParentPostId']").val();
        apiModel.PostTitle = $(this).find("[name='PostTitle']").val();
        apiModel.CreatedDate = new Date().toJSON();
        apiModel.ScheduledFor = $("#post--date--value").find("#ScheduledFor").val();
        function ResetLoader() {
            modalParent.modal('hide');
            hideElem.show('slow');
            showElem.removeClass('active');
        }
        if (apiModel.ScheduledFor) {
            apiModel.IsScheduled = "true";
        }
        var IsPaidPost = $(this).find("#public--or--paid").val();
        apiModel.IsPaidPost = IsPaidPost;
        apiModel.PostPrivacy = "Public";

        if (ContentType == "Polls") {
            pollCapture();
        }


        var price = parseFloat($(this).find("[name='PostPrice']").val());
        if (IsPaidPost == "true" && isNaN(price)) {
            var result = { returnMsg: 'Post Price should be number.' };
            EventNotification(result);
            console.log("price validation failed");
            ResetLoader();
            return false;
        }
        apiModel.PostPrice = $(this).find("[name='PostPrice']").val();
        apiModel.IsMedia = ContentType == 'Text' ? 'false' : 'true';


        var url = appURL + '/Post/Feed/';
        if (modalParent.hasClass("edit--post--modal"))// edit post if true
        {
            url = apiURL + '/PostMasters/EditPost/';
        }
        if (apiModel.PostCollections.length == 0 && IsPaidPost == "true") {
            var result = { returnMsg: 'Content is required for paid post.' };
            showElem.removeClass('active');
            hideElem.show('slow');
            $('#post--modal').modal('hide');
            EventNotification(result);
            console.log("validation failed");
            ResetLoader();

            return false;

        }
        if (apiModel.PostPrice <= 0 && IsPaidPost == "true") {
            var result = { returnMsg: 'Paid Post Price should be greater than 0.' };
            showElem.removeClass('active');
            hideElem.show('slow');
            EventNotification(result);
            console.log("price validation failed");
            ResetLoader();

            return false;

        }
        var param = JSON.stringify(apiModel);
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();
        var token_auth = "Bearer " + $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token,
                Authorization: token_auth

            },
            data: param,
        }).done(function (result) {

            ResetLoader();

            ContentType = 'Text';
            PageNumber = 1;

            $("#viewFeed article").remove();
            debugger;
            //loadContents();
            //EventNotification(result);
        }).fail(function (error) {

            ResetLoader();

            var result = {
                returnMsg: 'Something Went Wrong. Please Try Again!'
            };
            EventNotification(result);
        });

        $('#my-post--value').val("");

        $(this).find('.modal--post__ph--upload>[class="image"]').remove();
        $(this).find('.modal--post__ph--upload>[class="image uploading"]').remove();
        $(this).find('.image.empty--input').addClass("hide");
        apiModel.PostCollections = [];

        $(this).find('#input__paid').closest(".input--wrapper").addClass("hide");
        $(this).find("#public--or--paid").val("false");
        //$(this).find('#input__paid').closest(".input--wrapper").addClass("hide");


        $(this).closest('form').trigger("reset");

    };
    $('#post--modal').on('hidden.bs.modal', function (e) {
        //    alert("hidden");
        ResetForm();
    });
    //$("#post--modal").on("hidden.bs.modal", ResetForm);
    var ResetForm = function () {
        $('#my-post--value').val("");
        $('#modal--post__input').val("");
        $(".post__card__link__input").attr("accept", validImageTypes.join(","));
        $("#post--modal").find('.modal--post__ph--upload>[class="image"]').remove();
        $("#post--modal").find('.modal--post__ph--upload>[class="image uploading"]').remove();
        $("#post--modal").find('.image.empty--input').addClass("hide");
        apiModel.PostCollections = [];

        $("#post--modal").find('#input__paid').closest(".input--wrapper").addClass("hide");
        $("#post--modal").find("#public--or--paid").val("false");
        ResetPoll();
        ResetScheduleDate();
        $("#post--modal").find('form').trigger("reset");
        $("#post--modal").find('button[type=submit]').removeAttr("disabled");
        $("#post--modal").find('button[type=submit]').attr("disabled", "true");
    };
    var ResetPoll = function () {
        var template = $("#poll_default_template").html();
        $("#post--modal .poll__wrapper").hide();

        $(".poll--inside").empty().append(template);
        $('.pollfile').on('change', function () {
            uploadPollFile(this);
        });
    }
    var ResetScheduleDate = function () {
        const schedulewrapper = $("#post--date--value");
        const sheduleLink = $(".shedule--link");
        schedulewrapper.find("time").remove();
        schedulewrapper.find("#ScheduledFor").remove();
        $("#datetimepicker-demo>input").val(null);
        sheduleLink.removeClass("active");
        $("#reset").addClass("hide");
        $('#post--modal form button[type="submit"]').text("Post");

    };
    $("#modal--post__input").keyup(function () {
        var currentText = $(this).val();
        if (currentText) {
            $("#post--modal").find('button[type=submit]').removeAttr("disabled");

        } else {
            $("#post--modal").find('button[type=submit]').attr("disabled", "true");

        }
    });
    $("#reset").click(function (e) {
        ResetScheduleDate();
    });
    //Page Number and Page Size changes
    var PageNumber = 1;
    var PageSize = 10;

    var loadContents = function () {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var Feed = document.getElementById("viewFeed");
                console.log(this.responseText);
                //Feed.appendChild(this.responseText);
                if (this.responseText.replace(' ', '').length == 0) {
                    $('.skeleton--loader').addClass("d-none");
                }
                Feed.innerHTML += this.responseText;
                ContentAddons();
                scrollContentsLoad = true;
                PageNumber++;
            }
        };
        var Dat = new Date().toJSON();
        xhttp.open("GET", appURL + "/Post/Content?dateTime=" + Dat + "&PageNumber=" + PageNumber + "&PageSize=" + PageSize, true);
        xhttp.send();
    };
    var pollCapture = function () {
        debugger;
        var pollCount = $('#poll--inside--count').val();
        for (i = 0; i <= pollCount; i++) {
            debugger;
            var OptionText = $('.poll__wrapper').find('#pollwrapper_' + i).find(".poll-text").val();
            var OptionFileLocation = $('.poll__wrapper').find('#pollwrapper_' + i).find(".poll-file-location").val();
            if (OptionText || OptionFileLocation) {

                var uploadModel = {
                    ContentId: i.toString(),
                    FileLocation: OptionFileLocation,
                    PaidPost: 'false',
                    ContentTitle: OptionText,
                    PostType: ContentType
                };

                apiModel.PostCollections.push(uploadModel);
            }
            else {
                return false;
            }

        }


    };
    $('.modal--post .poll--post').click(function (e) {
        debugger;
        if (apiModel.PostCollections.filter(x => x.PostType == "Image").length > 3) {
            NoticeNotification("Please select only 3 images for poll.");
            return false;
        }
        ContentType = "Polls";
        $('.modal--post .poll__wrapper').toggle(300);

        $('#MediaType').attr('disabled');
    });
    var uploadBlob = function (formData) {
        var $d = $.Deferred();
        var auth = $('#token___Auth').val();

        $.ajax({

            url: apiURL + '/Bucket?contentType=' + ContentType,
            crossDomain: true,
            data: formData,
            type: 'POST',
            processData: false,
            contentType: false,
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (data) {

            $d.resolve(data);

        }).fail(function (data) {

            $d.reject(data);

        });

        return $d.promise();


    };
    var DeleteBlob = function (Id) {
        var auth = $('#token___Auth').val();
        var $d = $.Deferred();
        $.ajax({

            url: apiURL + '/PostMasters/DeletePostUpload?Id=' + Id,
            crossDomain: true,
            type: 'POST',
            processData: false,
            contentType: false,
            headers: {
                Authorization:
                    "Bearer " + auth
            }

        }).done(function (data) {

            $d.resolve(data);
        }).fail(function (data) {

            $d.reject(data);

        });

        return $d.promise();



    }

    var getFileExtension = function (filename) {
        return '.' + filename.split('.').pop();
    }
    function ValidateMaxContents(noOfNewFiles) {
        let noOfAlreadyUploadFiles = apiModel.PostCollections.length;
        if ((noOfNewFiles + noOfAlreadyUploadFiles) > 4) {
            ResetFileInput();

            NoticeNotification("Max 4 contents allowed per post");
            return false;
        }
        return true;
    }
    var uploadImage = function (input, imageAppend, timestamp) {

        function isVideo(filename) {
            var ext = getFileExtension(filename);
            switch (ext.toLowerCase()) {
                case '.m4v':
                case '.avi':
                case '.mpg':
                case '.mp4':
                    // etc
                    return true;
            }
            return false;
        }
        function isDoc(filename) {
            var ext = getFileExtension(filename);
            switch (ext.toLowerCase()) {
                case '.pdf':
                case '.doc':
                case '.xlsx':
                case '.ppt':
                    // etc
                    return true;
            }
            return false;
        }

        if (input.files) {

            var files = input.files;


            let filesAmount = input.files.length;
            ValidateMaxContents(filesAmount);

            for (i = 0; i < filesAmount; i++) {
                var formData = new FormData();
                formData.append("files", files[i]);
                //var fileExt = getFileExtension(files[i].name);
                var file = files[i];
                var filenm = files[i].name
                var fileType = file["type"];

                var validTypes = "";

                var val = $('#MediaType').val();
                //ContentType = val;
                debugger;
                if (val == "Video") {
                    validTypes = validVideoTypes.join(",");
                    if (!isVideo(filenm)) {
                        ResetFileInput();

                        NoticeNotification("Invalid File Type.Please select valid media types files.");
                        return false;
                    }
                }
                else if (val == "Audio") {
                    validTypes = validAudioTypes.join(",");
                    ContentType = 'Audio';

                    if (($.inArray(fileType, validAudioTypes) <= 0)) {
                        ResetFileInput();

                        NoticeNotification("Invalid File Type.Please select valid media types files");
                        return false;
                    }
                }
                else if (val == "Document") {
                    validTypes = validDocumentTypes.join(",");
                    ContentType = 'File';

                    if (!isDoc(filenm)) {
                        ResetFileInput();
                        NoticeNotification("Invalid File Type.Please select valid media types files");
                        return false;

                    }
                }
                //else if (val == "Image") {
                //    validTypes = validImageTypes.join(",");

                //}

                else if (val == "Polls") {
                    validTypes = validImageTypes.join(",");
                    ContentType = 'Polls';

                    if (($.inArray(fileType, validImageTypes) <= 0)) {
                        NoticeNotification("Invalid File Type.Please select valid media types files");
                        return false;
                    }
                }
                else {
                    validTypes = validImageTypes.join(",");
                    ContentType = 'Image';

                    if (($.inArray(fileType, validImageTypes) <= 0)) {
                        NoticeNotification("Invalid File Type.Please select valid media types files");
                        return false;
                    }
                }


                var uploadLoader = $('#uploading__loader');
                $('#post--modal button').prop('disabled', true);
                uploadLoader.show();
                var staticPath = "/Content/gallery/";

                let paidElem = $('#public--or--paid')[0].selectedIndex;

                uploadBlob(formData).done(function (data) {
                    if (!data.success) {
                        $('#post--modal button').prop('disabled', false);
                        ResetFileInput();

                        NoticeNotification(data.returnMsg);
                        return false;
                    }
                    timestamp++;
                    var imgPath = "";
                    if ($.inArray(fileType, validImageTypes) > 0) {

                        staticPath = data.returnPath;
                    }
                    else if (isDoc(filenm)) {

                        staticPath = staticPath + "/file.png";

                    }
                    else if ($.inArray(fileType, validAudioTypes) > 0) {

                        staticPath = staticPath + "/headphones.png";
                    }
                    else if (isVideo(filenm)) {

                        staticPath = staticPath + "/playButton.png";
                    }


                    let image = `<div class="image uploading">
				<img src="${staticPath}" alt="">
				<input type="file" accept="${validTypes}" class="input__hide" name="uploadedImage" id="uploadedImage__${timestamp}" value="${data.returnPath}">
				<a href="#!" data-val="${data.returnId}" class="lock  ${paidElem ? ' ' : ' unlocked '}">
				<svg xmlns="http://www.w3.org/2000/svg" width="502.571" height="562.771" viewBox="0 0 502.571 562.771">
				<path id="Subtraction_1" data-name="Subtraction 1" d="M-9212.429,706.77H-9715V347.906h47.059a205.856,205.856,0,0,1,4.235-41.035,203.366,203.366,0,0,1,11.938-38.224,204.484,204.484,0,0,1,18.827-34.6,205.982,205.982,0,0,1,24.9-30.155,206.034,206.034,0,0,1,30.159-24.9,204.617,204.617,0,0,1,34.6-18.825,203.485,203.485,0,0,1,38.234-11.938A205.907,205.907,0,0,1-9463.993,144a205.7,205.7,0,0,1,42.227,4.485,203.391,203.391,0,0,1,39.218,12.63A204.665,204.665,0,0,1-9347.23,181a206.061,206.061,0,0,1,30.527,26.251l-31.746,27.321a161.253,161.253,0,0,0-115.845-48.8c-89.36.085-162.129,72.816-162.214,162.129l324.388.044c0-.195,0-.388,0-.582v-.023h42.033v.025c0,.177,0,.359,0,.536h47.658V706.769Zm-251.865-221.811a46.223,46.223,0,0,0-46.156,46.071,45.909,45.909,0,0,0,28.683,42.592v56.721h35.036V573.715l-.047.019-.041.017.088-.129v.093a45.931,45.931,0,0,0,28.509-42.685A46.123,46.123,0,0,0-9464.294,484.959Z" transform="translate(9715 -143.999)" fill="#fff"/>

				<path id="Subtraction_2" data-name="Subtraction 1" d="M-9212.429,706.77H-9715V347.906h47.059a205.856,205.856,0,0,1,4.235-41.035,203.366,203.366,0,0,1,11.938-38.224,204.484,204.484,0,0,1,18.827-34.6,205.982,205.982,0,0,1,24.9-30.155,206.034,206.034,0,0,1,30.159-24.9,204.617,204.617,0,0,1,34.6-18.825,203.485,203.485,0,0,1,38.234-11.938A205.907,205.907,0,0,1-9463.993,144a205.8,205.8,0,0,1,40.95,4.217,203.406,203.406,0,0,1,38.152,11.888,204.534,204.534,0,0,1,34.545,18.749,206.089,206.089,0,0,1,30.127,24.8,206.05,206.05,0,0,1,24.9,30.042,204.534,204.534,0,0,1,18.86,34.474,203.365,203.365,0,0,1,12.014,38.1,205.649,205.649,0,0,1,4.356,40.909l-42.033.031c-.482-88.885-73.231-161.3-162.171-161.43-89.36.085-162.129,72.816-162.214,162.129l324.388.044c0-.195,0-.388,0-.582v-.023h42.033v.025c0,.177,0,.359,0,.536h47.658V706.769Zm-251.865-221.811a46.223,46.223,0,0,0-46.156,46.071,45.909,45.909,0,0,0,28.683,42.592v56.721h35.036V573.715l-.047.019-.041.017.088-.129v.093a45.931,45.931,0,0,0,28.509-42.685A46.123,46.123,0,0,0-9464.294,484.959Z" transform="translate(9715 -143.999)" fill="#fff"/>


				</svg>
				</a>
				<div class="edit--wrapper">
				<label for="uploadedImage__${timestamp}">change</label>
				<a href="#" class="new--trash trash" data-val="${data.returnId}"><i class="bi bi-trash"></i></a>
				</div>
				<div class="loader__wrapper">
				<div class="loader">
				<div class="box">
				<img src="/Content/gallery/logoWhite.png" alt="fanbucket">
				</div>
				<div class="shadow"></div>
				</div>
				</div>
				</div>` ;
                    //uploadLoader.hide();
                    if ($(input).is('#post__card__link__input')) {
                        $(imageAppend).append(image);
                    } else {
                        $(input).closest('form').find('.modal--post__ph--upload').append(image);

                    }


                    $('#post--modal button').prop('disabled', false);

                    AddContentToPostCollection(data, paidElem, ContentType);

                    //apiEditModel.PostCollections.push(uploadModel);

                    setTimeout(function (e) {
                        $(imageAppend).find('.uploading').removeClass('uploading');
                    }, 3000)

                    ResetFileInput();
                    $('.image.empty--input.hide').removeClass('hide');
                    $('.image.empty--input.hide').removeClass('hide');

                    $('.modal--post__ph--upload .lock').unbind().click(LockUnlockPost);
                    $('#post--modal .new--trash.trash').unbind().click(DeleteContent);
                }).fail(function (data) {
                    ResetFileInput();
                    uploadLoader.hide(300);
                    EventNotification(data);
                });

            }
        }

    };

    var uploadPollFile = (input) => {
        debugger;
        var $this = $(input);
        if (input.files) {

            var files = input.files;


            let filesAmount = input.files.length;
            if (!ValidateMaxContents(filesAmount)) {
                return false;
            }

            for (i = 0; i < filesAmount; i++) {
                var formData = new FormData();
                formData.append("files", files[i]);
                //var fileExt = getFileExtension(files[i].name);
                var file = files[i];
                var filenm = files[i].name
                var fileType = file["type"];

                var validTypes = "";

                var val = $('#MediaType').val();
                //ContentType = val;
                debugger;
                //if (val == "Video") {
                //    validTypes = validVideoTypes.join(",");
                //    //if (!isVideo(filenm)) {
                //    //    ResetFileInput();

                //    //    NoticeNotification("Invalid File Type.Please select valid media types files.");
                //    //    return false;
                //    //}
                //}

                //else {
                //    validTypes = validImageTypes.join(",");
                //    ContentType = 'Image';

                //    if (($.inArray(fileType, validImageTypes) <= 0)) {
                //        NoticeNotification("Invalid File Type.Please select valid media types files");
                //        return false;
                //    }
                //}



                //var uploadLoader = $('#uploading__loader');
                //uploadLoader.show();
                var staticPath = "/Content/gallery/";


                uploadBlob(formData).done(function (data) {
                    if (!data.success) {
                        //$('#post--modal button').prop('disabled', false);
                        //ResetPollFileInput();

                        NoticeNotification(data.returnMsg);
                        return false;
                    }
                    else {
                        console.log($this.closest(".poll-input-wrapper"));
                        $this.closest(".poll-input-wrapper").find(".poll-file-location").val(data.returnPath);
                        NoticeNotification("Successfully Uploaded");
                        return true;
                    }
                    //                timestamp++;

                }).fail(function (data) {
                    ResetFileInput();
                    uploadLoader.hide(300);
                    EventNotification(data);
                });

            }
        }
    };
    var ResetFileInput = function () {
        $(".post__card__link__input").each(function () {
            $(this).val('');
        });
    }
    $(".post--modal__trigger").on('click', function () {
        debugger;
        ResetForm();

        $('#post--modal').removeClass("edit--post--modal");
        $('#post--modal .modal-header.title__wrapper .section__title').text("New Post");
        $('#post--modal .form-upolading.loader__wrapper .section__title').text("Creating a Post");

        $('#post--modal .card__link.card__link--top .form-group.select--input--wrapper .select-wrapper').removeClass("d-none");
        $('#post--modal .card__link .pr').removeClass("invisible");
        $('#post--modal .card__link .poll--trash').removeClass("invisible");
        $('#post--modal .card__link .shedule--link').removeClass("d-none");
        $('#post--modal .image.empty--input').removeClass("d-none");
        apiModel.Id = "00000000-0000-0000-0000-000000000000";
    });

    var UpdateModalTitleForEdit = function () {
        //$('#post--modal').removeClass("edit--post--modal");
        $('#post--modal .modal-header.title__wrapper .section__title').text("Edit Post");
        $('#post--modal .form-upolading.loader__wrapper .section__title').text("Updating a Post");
        $('#post--modal .card__link.card__link--top .form-group.select--input--wrapper .select-wrapper').addClass("d-none");
        $('#post--modal .card__link .pr').addClass("invisible");
        $('#post--modal .card__link .poll--trash').addClass("invisible");
        $('#post--modal .card__link .shedule--link').addClass("d-none");
        $('#post--modal .image.empty--input').addClass("d-none");
    };
    const ContentTypes = {
        Text: "1",
        File: "2",
        Image: "3",
        Audio: "4",
        Video: "5",

    };
    var SetFile = function (selector, item) {
        console.log(item);
        debugger;
        var staticPath = "/Content/gallery/";
        var returnId = item.ContentId;
        var paidElem = item.PaidPost === "true";
        var returnPath = item.FileLocation;
        var contentType = item.PostType;
        if (ContentTypes.Text === contentType) {
            return;
        }
        if (ContentTypes.Image === contentType) {

            staticPath = returnPath;
        }
        else if (ContentTypes.File === contentType) {

            staticPath = staticPath + "/file.png";

        }
        else if (ContentTypes.Audio === contentType) {

            staticPath = staticPath + "/headphones.png";
        }
        else if (ContentTypes.Video === contentType) {

            staticPath = staticPath + "/playButton.png";
        }
        var validTypes = "true";
        var timestamp = "true";
        debugger;
        let image = `<div class="image uploading">
				<img src="${staticPath}" alt="">
				//<input type="file" accept="${validTypes}" class="input__hide" name="uploadedImage" id="uploadedImage__${returnId}" value="${returnPath}">
				<a href="#!" data-val="${returnId}" class="lock  ${paidElem ? ' ' : ' unlocked '}">
				<svg xmlns="http://www.w3.org/2000/svg" width="502.571" height="562.771" viewBox="0 0 502.571 562.771">
				<path id="Subtraction_1" data-name="Subtraction 1" d="M-9212.429,706.77H-9715V347.906h47.059a205.856,205.856,0,0,1,4.235-41.035,203.366,203.366,0,0,1,11.938-38.224,204.484,204.484,0,0,1,18.827-34.6,205.982,205.982,0,0,1,24.9-30.155,206.034,206.034,0,0,1,30.159-24.9,204.617,204.617,0,0,1,34.6-18.825,203.485,203.485,0,0,1,38.234-11.938A205.907,205.907,0,0,1-9463.993,144a205.7,205.7,0,0,1,42.227,4.485,203.391,203.391,0,0,1,39.218,12.63A204.665,204.665,0,0,1-9347.23,181a206.061,206.061,0,0,1,30.527,26.251l-31.746,27.321a161.253,161.253,0,0,0-115.845-48.8c-89.36.085-162.129,72.816-162.214,162.129l324.388.044c0-.195,0-.388,0-.582v-.023h42.033v.025c0,.177,0,.359,0,.536h47.658V706.769Zm-251.865-221.811a46.223,46.223,0,0,0-46.156,46.071,45.909,45.909,0,0,0,28.683,42.592v56.721h35.036V573.715l-.047.019-.041.017.088-.129v.093a45.931,45.931,0,0,0,28.509-42.685A46.123,46.123,0,0,0-9464.294,484.959Z" transform="translate(9715 -143.999)" fill="#fff"/>

				<path id="Subtraction_2" data-name="Subtraction 1" d="M-9212.429,706.77H-9715V347.906h47.059a205.856,205.856,0,0,1,4.235-41.035,203.366,203.366,0,0,1,11.938-38.224,204.484,204.484,0,0,1,18.827-34.6,205.982,205.982,0,0,1,24.9-30.155,206.034,206.034,0,0,1,30.159-24.9,204.617,204.617,0,0,1,34.6-18.825,203.485,203.485,0,0,1,38.234-11.938A205.907,205.907,0,0,1-9463.993,144a205.8,205.8,0,0,1,40.95,4.217,203.406,203.406,0,0,1,38.152,11.888,204.534,204.534,0,0,1,34.545,18.749,206.089,206.089,0,0,1,30.127,24.8,206.05,206.05,0,0,1,24.9,30.042,204.534,204.534,0,0,1,18.86,34.474,203.365,203.365,0,0,1,12.014,38.1,205.649,205.649,0,0,1,4.356,40.909l-42.033.031c-.482-88.885-73.231-161.3-162.171-161.43-89.36.085-162.129,72.816-162.214,162.129l324.388.044c0-.195,0-.388,0-.582v-.023h42.033v.025c0,.177,0,.359,0,.536h47.658V706.769Zm-251.865-221.811a46.223,46.223,0,0,0-46.156,46.071,45.909,45.909,0,0,0,28.683,42.592v56.721h35.036V573.715l-.047.019-.041.017.088-.129v.093a45.931,45.931,0,0,0,28.509-42.685A46.123,46.123,0,0,0-9464.294,484.959Z" transform="translate(9715 -143.999)" fill="#fff"/>


				</svg>
				</a>

				<div class="loader__wrapper">
				<div class="loader">
				<div class="box">
				<img src="/Content/gallery/logoWhite.png" alt="fanbucket">
				</div>
				<div class="shadow"></div>
				</div>
				</div>
				</div>` ;
        //uploadLoader.hide();
        $(selector).append(image);
        setTimeout(function (e) {
            $(selector).find('.uploading').removeClass('uploading');
        }, 3000)


        $('.image.empty--input.hide').removeClass('hide');

        $('.modal--post__ph--upload .lock').unbind().click(LockUnlockPost);
        //$('#post--modal .edit--trash.trash').unbind().click(RemoveContent);//old post just delete form list
        //can be error herre
    };

    var EditLockUnlockPost = function (e) {
        e.preventDefault();
        var ContentIdf = $(this).attr('data-val');
        var index = apiEditModel.PostCollections.findIndex(x => x.ContentId == ContentIdf);

        if ($(this).hasClass('unlocked')) {
            apiEditModel.PostCollections[index].PaidPost = 'true';
        }
        else {
            apiEditModel.PostCollections[index].PaidPost = 'false';
        }
        $(this).toggleClass('unlocked');
    };

    var EditDeleteContent = function (e) {

        e.preventDefault();
        var Id = $(this).attr('data-val');
        var evt = $(this);

        DeleteBlob(Id).done(function (data) {
            if (data.success) {
                var index = apiModel.PostCollections.findIndex(x => x.ContentId == Id);
                apiModel.PostCollections.splice(index, 1);
                e.preventDefault();
                let parent = evt.parents('.image');
                let elemParent = evt.parents('.modal--post__ph--upload');
                parent.remove();
                if (elemParent.children().length <= 1) {
                    elemParent.find('.image.empty--input').addClass('hide')
                }
            }

        });

    };

    var LockUnlockPost = function (e) {
        e.preventDefault();
        var ContentIdf = $(this).attr('data-val');
        //check class of modal if it is edit or not 
        //check if it edit or post modal with modal id
        var PaidPost = $('#public--or--paid').val();
        if (PaidPost == 'false') {
            NoticeNotification("Make the post paid to lock the content.");
            return false;
        }
        if ($(this).closest('modal').hasClass('.edit--post--modal')) {
            var index = apiEditModel.PostCollections.findIndex(x => x.ContentId == ContentIdf);

            if ($(this).hasClass('unlocked')) {
                apiModel.PostCollections[index].PaidPost = 'true';
            }
            else {
                apiModel.PostCollections[index].PaidPost = 'false';
            }
        }
        else {
            var index1 = apiModel.PostCollections.findIndex(x => x.ContentId == ContentIdf);

            if ($(this).hasClass('unlocked')) {
                apiModel.PostCollections[index1].PaidPost = 'true';
            }
            else {
                apiModel.PostCollections[index1].PaidPost = 'false';
            }

        }

        $(this).toggleClass('unlocked');
    };

    var DeleteContent = function (e) {

        e.preventDefault();
        var Id = $(this).attr('data-val');
        var evt = $(this);

        DeleteBlob(Id).done(function (data) {
            if (data.success) {
                var index = apiModel.PostCollections.findIndex(x => x.ContentId == Id);
                apiModel.PostCollections.splice(index, 1);
                var Editindex = apiEditModel.PostCollections.findIndex(x => x.ContentId == Id);
                apiEditModel.PostCollections.splice(index, 1);
                e.preventDefault();
                let parent = evt.parents('.image');
                let elemParent = evt.parents('.modal--post__ph--upload');
                parent.remove();
                if (elemParent.children().length <= 1) {
                    elemParent.find('.image.empty--input').addClass('hide')
                }
            }

        });

    };


    //for edit
    var RemoveContent = function (e) {

        e.preventDefault();
        var Id = $(this).attr('data-val');
        var evt = $(this);

        if (Id === undefined) {
            return;
        }
        var index = apiModel.PostCollections.findIndex(x => x.ContentId == Id);
        if (index !== undefined) {
            apiModel.PostCollections.splice(index, 1);
            e.preventDefault();
            let parent = evt.parents('.image');
            let elemParent = evt.parents('.modal--post__ph--upload');
            parent.remove();
            if (elemParent.children().length <= 1) {
                elemParent.find('.image.empty--input').addClass('hide')
            }
        }




    };
    var ShareContent = function (e) {
        e.preventDefault();
        var ShareId = $(this).attr('data-id');

        var RootElem = $(this).parents('article').clone();
        RootElem.find('.card__action').remove();
        var modaal = $('#shareFeed--modal');
        modaal.modal('toggle');
        modaal.find('.shared-feed .card').empty();
        modaal.find('.shared-feed .card').append(RootElem);
        apiModel.Id = "00000000-0000-0000-0000-000000000000";

        modaal.find('form').find("[name='ParentPostId']").val(ShareId);

    };

    var ShareMoments = function (e) {
        e.preventDefault();

        var evt = $(this);
        var ShareId = evt.attr('data-id');

        evt.append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        var auth = $('#token___Auth').val();
        var url = apiURL + "/PostMasters/PostMoments?PostId=" + ShareId;

        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            evt.find('img').remove();
            EventNotification(result);
            loadMoments();

        }).fail(function (error) {
            evt.find('img').remove();
            result = {
                returnMsg: "Could Not Validate Your Request"
            }
            EventNotification(result);

        });

    };
    var BarAnimation = function () {
        $('.moments--modal .progress--wrapper .progress-bar').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
            function (e) {
                let parent = $(this).parent();
                parent.removeClass('active');
                if (parent.next().length > 0) {
                    parent.next().addClass('active');
                    parent.next().find('.progress-bar').addClass('paused');
                    GetMoment(e, 1);
                }
                else {
                    $('#moments--modal').modal('toggle');
                }


            });
    }
    var NextMoment = function (e) {

        let parent = $('.moments--modal .progress--wrapper .progress-bar').parent('.active');
        parent.removeClass('active');
        if (parent.next().length > 0) {
            parent.next().addClass('active');
            parent.next().find('.progress-bar').addClass('paused');
            GetMoment(e, 1);
        }
        else {
            $('#moments--modal').modal('toggle');
        }

    };
    var prevMoment = function (e) {

        let parent = $('.moments--modal .progress--wrapper .progress-bar').parent('.active');
        parent.removeClass('active');
        if (parent.prev().length > 0) {
            parent.prev().addClass('active');
            parent.prev().find('.progress-bar').addClass('paused');
            GetMoment(e, -1);
        }
        else {
            $('#moments--modal').modal('toggle');
        }

    };
    var GetMoment = function (e, incrementVal) {
        var modal = $('#moments--modal');
        if (($("#moments--modal").data('bs.modal') || {})._isShown) {
            momentsModal.CurrentLoop = momentsModal.CurrentLoop + incrementVal;
        }

        else {
            momentsModal.CurrentLoop = 0;
        }
        if (!incrementVal) {

            modal.modal('toggle');
            $('.modal-backdrop').addClass('dark');

            momentsModal.UserId = $(this).attr('id');
            momentsModal.CreatorImg = $(this).attr('data-img');
            momentsModal.CreatorName = $(this).attr('data-val');

            modal.find('.modal-body .progress--wrapper').empty();
            momentsModal.PostId = [];
            $(this).find(" :input").each(function (key) {
                var elem = '';

                momentsModal.PostId.push($(this).val());
                if (key === 0) {
                    elem = `<div class="progress active">
                            <div class="progress-bar paused" role="progressbar"></div>
                        </div>`
                }
                else {
                    elem = `<div class="progress">
                            <div class="progress-bar" role="progressbar"></div>
                        </div>`
                }

                modal.find('.modal-body .progress--wrapper').append(elem);

            });

            $('#moments--modal').find('.next').click(NextMoment);
            $('#moments--modal').find('.prev').click(prevMoment);


        }
        BarAnimation();


        momentsModal.CurrentId = momentsModal.PostId[momentsModal.CurrentLoop];
        GetMomentData(momentsModal.CurrentId, momentsModal.CreatorImg, momentsModal.CreatorName, modal);

    };

    var GetMomentData = function (PostId, CreatorImg, CreatorName, modal) {
        if (!($("#moments--modal").data('bs.modal') || {})._isShown) {
            modal.modal('toggle');
        }
        var url = appURL + "/Post/SinglePost?PostId=" + PostId;
        modal.find('.modal-body .modal-within').empty();
        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json; charset=utf-8'
        }).done(function (result) {
            modal.find('.modal-body .modal-within').append(result);
            var Id = modal.find('.modal-body .modal-within article').attr('id').replace('feedCard__', '');
            modal.find('.modal-body .modal-within').attr("data-val", "/Post/SinglePost?PostId=" + Id);
            modal.find('.modal-body .modal-within').click(GoToPost);
            modal.find('.profile--card').find('img').attr('src', CreatorImg);
            modal.find('.profile--card').find('.card__title a').text(CreatorName);
            modal.find('.progress--wrapper .active').find('.progress-bar').toggleClass('paused');
        }).fail(function (error) {
            var result = { returnMsg: 'Moments Fetch Issue' };
            EventNotification(result);
        });

    }
    var GoToPost = function () {
        $(this).css('cursor', 'pointer');
        location.href = $(this).attr('data-val');
    }
    var loadMoments = function () {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                $('.moments').find('#momentsfeed').empty();
                $('.moments').find('#momentsfeed').append(this.responseText);
                ContentAddons();
                $('.trigger--moment').click(GetMoment);
            }
        };
        xhttp.open("GET", appURL + "/Post/Moments", true);
        xhttp.send();
    };
    const setCookie = (cname, cvalue, exdays) => {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };

    // cookies light mode or dark
    const getCookie = (cname) => {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };

    var GetUserRelation = function (e) {

        e.preventDefault();

        var modal = $('#message--new');
        modal.modal('toggle');
        var UserId = document.getElementById('UserId').value;
        var Type = $(this).attr('data-val');
        var auth = $('#token___Auth').val();
        var url = apiURL + '/UserFollow/GetRelatedUser?UserId=' + UserId + '&&Type=' + Type;

        $.ajax({
            url: url,
            type: 'GET',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            modal.find('.modal-body').empty();
            var Heading = "<h2 class='section__title'>" + Type + "</h2>"
            modal.find('.modal-body').append(Heading);
            for (i = 0; i < result.length; i++) {
                var divElem = `<a href="${result[i].username}"  title="Send message to Person Name" class="card chat--card"  >
                        <div class="card__top">
                            <div class="card__img">
                                <img src="${result[i].profileImg}" alt="">
                                
                            </div>

                            <div class="card__top__body" >
                                <h2 class="card__title">${result[i].fullname}</h2>
                            </div>
                        </div>
                    </a>`;

                modal.find('.modal-body').append(divElem);
            }



        }).fail(function (error) {
            var result = { returnMsg: 'User Fetch Issue' };
            EventNotification(result);
        });
    }
    var abc = ''
    var EditPostContent = function () {
        console.log("On to edit post content");
        console.log(apiModel.PostCollections);

        //e.preventDefault();
        var $postModal = $('#post--modal');

        if (!$postModal.hasClass("edit--post--modal")) {
            $postModal.addClass("edit--post--modal");

            //change title of modal to e4dit post
        }
        UpdateModalTitleForEdit();
        let hideElem = $postModal.find('.form__wrapper');
        let showElem = $postModal.next('.loader__wrapper');
        let modalParent = $postModal;
        //hideElem.hide(400);
        console.log(apiModel);
        showElem.addClass('active');
        $postModal.find('[name="NSFW"]').val([apiModel.NSFW]);
        $postModal.find("[name='ParentPostId']").val(apiModel.ParentPostId);
        $postModal.find("[name='PostTitle']").val(apiModel.PostTitle);
        //apiModel.CreatedDate = new Date().toJSON();
        $postModal.find(".post--date--value").find(".ScheduledFor").val(apiModel.ScheduledFor);
        if (apiModel.ScheduledFor) {
            apiModel.IsScheduled = "true";
        }
        if (apiModel.PaidPost == "true") {
            $('#input__paid').closest(".input--wrapper").removeClass("hide");
        } else {
            //$('#input__paid').addClass("hide");

        }
        $postModal.find(".public--or--paid").val(apiModel.PaidPost.toString());
        apiModel.PostPrivacy = "Public";

        if (ContentType == "Polls") {
            pollCapture();
        }

        //reset the modal
        $postModal.modal('show');
        var price = $postModal.find("[name='PostPrice']").val(apiModel.PostPrice);

        $.each(apiModel.PostCollections, function (key, value) {
            console.log(value);
            console.log(key);
            SetFile('#post--modal .modal--post__ph--upload', value);
            //    alert(JSON.stringify(key));
        });
        apiModel.IsMedia = ContentType == 'Text' ? 'false' : 'true';


    };
    var SubmitEditPost = function (e) {
        e.preventDefault();
        let hideElem = $(this).find('.form__wrapper');
        let showElem = $(this).next('.loader__wrapper');
        var $postModal = $('#edit--post--modal');

        let modalParent = $postModal;
        hideElem.hide(400);
        showElem.addClass('active');
        apiEditModel.NSFW = $postModal.find('[name="NSFW"]:checked').val();
        apiEditModel.ParentPostId = $postModal.find("[name='ParentPostId']").val();
        apiEditModel.PostTitle = $postModal.find("[name='PostTitle']").val();
        apiEditModel.CreatedDate = new Date().toJSON();
        apiEditModel.ScheduledFor = $("#post--date--value").find("#ScheduledFor").val();
        if (apiEditModel.ScheduledFor) {
            apiEditModel.IsScheduled = "true";
        }
        var IsPaidPost = $postModal.find("#public--or--paid").val();
        apiEditModel.PostPrivacy = "Public";

        if (ContentType == "Polls") {
            pollCapture();
        }


        var price = parseFloat($postModal.find("[name='PostPrice']").val());
        if (IsPaidPost == "true" && isNaN(price)) {
            var result = { returnMsg: 'Post Price should be number.' };
            EventNotification(result);
            console.log("price validation failed");
            console.log("price validation failed");

            return false;
        }
        apiEditModel.PostPrice = $postModal.find("[name='PostPrice']").val();
        apiEditModel.IsMedia = ContentType == 'Text' ? 'false' : 'true';


        var url = apiURL + '/PostMasters/EditPost/';
        if (apiEditModel.PostCollections.length == 0 && IsPaidPost == "true") {
            var result = { returnMsg: 'Content is required for paid post.' };
            showElem.removeClass('active');
            console.log($(this));
            $('#post--modal').modal('hide');
            EventNotification(result);
            console.log("validation failed");
            return false;

        }
        if (apiEditModel.PostPrice <= 0 && IsPaidPost == "true") {
            var result = { returnMsg: 'Paid Post Price should be greater than 0.' };
            EventNotification(result);
            console.log("price validation failed");

            return false;

        }
        debugger;
        var param = JSON.stringify(apiEditModel);
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();
        var token_auth = "Bearer " + $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token,
                Authorization: token_auth

            },
            data: param,
        }).done(function (result) {
            console.log(result);
            modalParent.modal('hide');
            hideElem.show('slow');
            showElem.removeClass('active');
            ContentType = 'Text';
            loadContents();
            //EventNotification(result);
        }).fail(function (error) {


            var result = {
                returnMsg: 'Something Went Wrong. Please Try Again!'
            };
            EventNotification(result);
        });

        $postModal.find("[name='PostTitle']").val('');
        $(this).find('.modal--post__ph--upload>[class="image"]').remove();
        $(this).find('.modal--post__ph--upload>[class="image uploading"]').remove();
        $(this).find('.image.empty--input').addClass("hide");
        apiModel.PostCollections = [];

        $(this).find('.input__paid').closest(".input--wrapper").addClass("hide");
        $(this).find(".public--or--paid").val("false");
        //$(this).find('#input__paid').closest(".input--wrapper").addClass("hide");


        $(this).closest('form').trigger("reset");

    };
    var GetEditContentById = function (instance) {
        //show modal-post
        //set values in post modal
        //clear at last
        let elem = $(instance);
        let elemId = $(this).attr('data-id');
        ResetForm();
        var auth = $('#token___Auth').val();
        var url = apiURL + '/PostMasters/GetSinglePostByIdAjax?PostId=' + elemId;
        $.ajax({
            url: url,
            type: 'GET',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            console.log(result);
            if (true) {
                apiModel.PostCollections = [];
                console.log("a");

                console.log(apiModel.PostCollections);

                $.each(result.collection, function (key, value) {

                    console.log(value);
                    var uploadModel = {
                        ContentId: value.contentId,
                        FileLocation: value.fileLocation,
                        PaidPost: value.paidPost ? 'true' : 'false',
                        PostType: value.postType.toString()
                    };

                    apiModel.PostCollections.push(uploadModel);
                });
                console.log("b");
                console.log(apiModel.PostCollections);

                apiModel.Id = result.postId;
                apiModel.CreatedOn = result.createdOn;
                apiModel.Creator = result.creator.creator;
                apiModel.CreatorId = result.creator.creatorId;
                apiModel.CreatorImg = result.creator.creatorImg;
                apiModel.DateString = result.dateString;
                apiModel.IsMedia = result.isMedia;
                apiModel.PostTitle = result.postTitle;
                apiModel.PaidPost = result.postPrice > 0 ? "true" : "false";
                apiModel.PostPrice = result.postPrice.toString();
                apiModel.ParentPostId = result.parentPostId;
                apiModel.IsScheduled = result.isScheduled;
                apiModel.ScheduledFor = result.scheduledFor;
                apiModel.NSFW = result.nsfw;
                console.log(apiModel.PostCollections);

                console.log("Passing to edit post content");
                console.log(apiModel.PostCollections);
                EditPostContent();
                $(".modal--post :submit").removeAttr("disabled");

            }
            else {
                EventNotification(result.returnMsg);
            }

        }).fail(function (result) {
            EventNotification(result.returnMsg);
        });


    };

    var GetContentById = function (instance) {
        let elem = $(instance);
        let elemId = elem.attr('data-id');
        var auth = $('#token___Auth').val();
        var url = apiURL + '/PostMasters/GetSinglePostById?PostId=' + elemId;
        $.ajax({
            url: url,
            type: 'GET',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            if (result.success) {

                CurrentPictureModel.Collection = result.jsonObj.collection.filter(x => x.postType !== 7);//fillter polls
                CurrentPictureModel.Collection = result.jsonObj.collection.filter(x => x.fileLocation !== null);//fillter unpaid post
                console.debug(result.jsonObj);
                console.debug(CurrentPictureModel.Collection);
                CurrentPictureModel.PostId = result.jsonObj.postId;
                CurrentPictureModel.CreatedOn = result.jsonObj.createdOn;
                CurrentPictureModel.Creator = result.jsonObj.creator.creator;
                CurrentPictureModel.CreatorId = result.jsonObj.creator.creatorId;
                CurrentPictureModel.CreatorImg = result.jsonObj.creator.creatorImg;
                CurrentPictureModel.DateString = result.jsonObj.dateString;
                debugger;
                var index = CurrentPictureModel.Collection.findIndex(x => x.contentId == elemId);
                CurrentPictureModel.CurrentElem = index;
                if (CurrentPictureModel.Collection[index].postType == 3) {
                    fullImageImg = $('#fullImage--modal .aside__left .card__img img');
                    $('#fullImage--modal .aside__left .card__img video').parent().css('display', 'none');
                    $('#fullImage--modal .aside__left .card__img img').parent().css('display', 'block');
                }
                else {
                    fullImageImg = $('#fullImage--modal .aside__left .card__img video');
                    $('#fullImage--modal .aside__left .card__img img').parent().css('display', 'none');
                    $('#fullImage--modal .aside__left .card__img video').parent().css('display', 'block');
                }
                fullImageImg.attr('src', CurrentPictureModel.Collection[index].fileLocation);

                $('#fullImage--modal .right--aside .right--aside__wrapper .card__top .card__img img').attr('src', result.jsonObj.creator.creatorImg);
                $('#fullImage--modal .right--aside .card__title a').attr('href', '/User/ProfilePage?Id=' + result.jsonObj.creator.creatorId);
                $('#fullImage--modal .right--aside .card__title a').text(result.jsonObj.creator.creator);
                $('#fullImage--modal .right--aside time').text(CurrentPictureModel.DateString);//Time
                $(instance).addClass('active');

                $('#fullImage--modal .right--aside').find('.card__title').text(result.jsonObj.creator.creator);//UserName 

                setTimeout(() => {
                    imageZoom(elem)
                }, 1000);
                $('#fullImage--modal').modal('show');

                GetCommentModal(CurrentPictureModel, index);

                $('#fullImage--modal .next').unbind().click(NextPicture);
                $('#fullImage--modal .prev').unbind().click(PrevPicture);
            }
            else {
                EventNotification(result.returnMsg);
            }

        }).fail(function (result) {
            EventNotification(result.returnMsg);
        });


    };

    var NextPicture = function () {
        debugger;
        var nextIndex = CurrentPictureModel.CurrentElem + 1;
        if (nextIndex >= CurrentPictureModel.Collection.length) {
            nextIndex = 0;
        }
        CurrentPictureModel.CurrentElem = nextIndex;

        fullImageImg.attr('src', CurrentPictureModel.Collection[nextIndex].fileLocation);
        //$('#fullImage--modal .right--aside img').attr('src', CurrentPictureModel.CreatorImg);
        //$('#fullImage--modal .right--aside .card__action .love').attr('data-id',  CurrentPictureModel.Collection[nextIndex].id);
        $('#fullImage--modal .right--aside .card__title a').attr('href', '/User/ProfilePage?Id=' + CurrentPictureModel.CreatorId);
        $('#fullImage--modal .right--aside .card__title a').text(CurrentPictureModel.Creator);
        $('#fullImage--modal .right--aside time').text(CurrentPictureModel.DateString);//Time
        GetCommentModal(CurrentPictureModel, nextIndex);
    };

    var PrevPicture = function () {
        var prevIndex = CurrentPictureModel.CurrentElem - 1;
        if (prevIndex < 0) {
            prevIndex = CurrentPictureModel.Collection.length - 1;
        }
        CurrentPictureModel.CurrentElem = prevIndex;

        fullImageImg.attr('src', CurrentPictureModel.Collection[prevIndex].fileLocation);
        //$('#fullImage--modal .right--aside img').attr('src', CurrentPictureModel.CreatorImg);
        $('#fullImage--modal .right--aside .card__title a').attr('href', '/User/ProfilePage?Id=' + CurrentPictureModel.CreatorId);
        $('#fullImage--modal .right--aside .card__title a').text(CurrentPictureModel.Creator);
        $('#fullImage--modal .right--aside time').text(CurrentPictureModel.DateString);//Time
        GetCommentModal(CurrentPictureModel, prevIndex);


    };

    var UnlockNSFW = function (elem) {
        //var id = $(elem).attr('data-id');
        $(elem).parents('.card__body').addClass('NSFW_True');
        $(elem).parents('.card__body').next().removeClass('NSFW_True');
        //    $('#NSFW_' + id).removeClass('NSFW_True');
    }

    function imageZoom(elem) {
        elem.removeClass('active');
    }
    let audioControl = [];
    //let xhr = { cache: 'default', mode: 'cors', method: 'GET', credentials: 'same-origin', redirect: 'follow', referrer: 'client', headers: [ { key: 'Authorization', value: 'my-token' } ]};
    let xhr = { cache: 'default', mode: 'no-cors' };//, method: 'GET', credentials: 'same-origin', redirect: 'follow', referrer: 'client', headers: [ { key: 'Authorization', value: 'my-token' } ]};
    var ContentAddons = function () {

        $('.report--post--trigger').unbind().click(ReportPost);

        $('.unfollow--user--trigger').unbind().click(userUnfollow);

        $('.love').unbind().click(PostReact);
        $("a.comments").unbind().click(function (e) {
            $(this).parents('.card').find('.card__comments').slideDown(300);
            $(this).parents('.card').find('.card__comments input.comment__text').focus();
            var PostId = $(this).attr('data-id');
            GetComments(PostId);
        });
        $('.form__comments').unbind().submit(function (event) {
            event.preventDefault();
            PostComment(this);
        });
        $('a.bookmark').unbind().click(function (e) {
            e.preventDefault();
            PostBookmark(this);
        });

        //$(".dropdown").on("click", ".dropdown-toggle", function (e) {
        //    e.preventDefault();
        //    $(this).parent().addClass("show");
        //    $(this).attr("aria-expanded", "true");
        //    $(this).next().addClass("show");
        //});
        $('.feed>article .card__img').unbind().click(function (e) {
            e.preventDefault();
            if ($(this).hasClass('viewNSFW')) {
                UnlockNSFW(this);
            }
            else {
                GetContentById(this);
            }
        });
        $('#fullImage--modal').on('show.bs.modal', function (event) {
            let elem = $('#fullImage--modal .aside__left .card__img');
            console.log(elem)
            elem.addClass('active');
            setTimeout(() => {
                imageZoom(elem)
            }, 500);



        });
        $('.__btnUnlock').on("click", UnlockPost);

        //$(".otp .eye").click(function (e) {
        //    debugger;
        //    let elem = $(this).find("i");
        //    if (elem.hasClass("bi-eye")) {
        //        elem.removeClass("bi-eye");
        //        elem.addClass("bi-eye-slash");
        //        $(".otp> input").removeAttr("type");
        //        $(".otp> input").attr("type", "password");
        //    } else {
        //        elem.addClass("bi-eye");
        //        elem.removeClass("bi-eye-slash");
        //        $(".otp> input").removeAttr("type");
        //        $(".otp> input").attr("type", "number");
        //    }
        //});
        $(".otp input").keyup(function (event) {
            debugger;
            let key = event.keyCode || event.charCode;
            if (!(key == 8 || key == 46)) {
                if ($(this).next().length)
                    $(this).next().focus();
                else {
                    $(this).parent().next().find('button').focus();
                }
            }
            else {
                $(this).prev().focus();
            }

        });

        $('.edit--post--trigger').unbind().click(GetEditContentById);
        $('.share--post--trigger').unbind().click(ShareContent);

        $('.moment--post--trigger').unbind().click(ShareMoments);

        $('.btnDeletePost').unbind().click(DeletePost);
        $('.hide--post--trigger').unbind().click(HidePost);
        $('.unhide--post--trigger').unbind().click(UnhidePost);

        $('#btnSetpin').unbind().on("click", function (e) {
            var pSpan = $('#ott--modal').find('input[name="pinDigit"]');
            var PIN = '';
            $(pSpan).each(function () {
                PIN += $(this).val();
            });
            var Password = $('#ott--modal').find('#pinPassword').val();
            instance = this;
            $(instance).prop('disabled', true);
            $(instance).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
            Payments.SetPinCode(PIN, Password, apiURL, instance);


            $('#ott--modal').find('input[name="pinDigit"]').val('');
            $('#ott--modal').find('#pinPassword').val('')
        });

        //[...document.querySelectorAll(".wave--wrapper")].map((elem) => {
        //    let selector = elem.getAttribute("id");
        //    if ($('#' + selector).has('wave').length > 0) {
        //        // Do something here.
        //        console.log("you can log here");

        //    }
        //    //    WaveSurfer.destroy('#' + selector);
        //});
        [...document.querySelectorAll('.wave--wrapper')].filter(function (elem) {
            console.log(window)
            let selector = elem.getAttribute('id');
            const index = audioControl.indexOf(selector);
            console.log(audioControl);
            if (index > -1) {

                console.log(selector + "exists");
                return false;
            }
            console.log(selector + "doesnot exists");

            return true;
        }).map((elem) => {
            let selector = elem.getAttribute('id');

            //if ($('#' + selector).has('wave').length > 0) {
            //    // Do something here.
            //    return; 
            //    //console.log("you can log here");
            //    //window[selector].destroy();
            //    ////debugger;
            //    //    const index = audioControl.indexOf(selector);
            //    //    if (index > -1) {
            //    //      audioControl.splice(index, 1);
            //    //    }
            //}
            //audioControl = [];

            audioControl.push(selector)
            console.log(audioControl);

            let src = elem.getAttribute('src');
            window[selector] = WaveSurfer.create({
                container: document.querySelector(`#${selector}`),
                waveColor: '#facfd3',
                progressColor: '#fe4545',
                cursorColor: '#fe4545',
                barWidth: 3,
                barRadius: 3,
                cursorWidth: 3,
                height: 200,
                barGap: 3,
                backend: 'MediaElement',
                xhr: xhr
                // mediaControls:true,
            });
            if (window.innerWidth < 840) {
                window[selector].setHeight(50)
            }

            console.log("I am here"+ selector)
            window[selector].load(src);
            elem.previousElementSibling.addEventListener('click', (e) => {
                e.preventDefault();
                debugger;
                audioControl.map((elem) => {
                    if (elem != selector) {
                        window[elem].pause();
                    }
                })
                window[selector].playPause()

            })

            window[selector].on('play', function () {
                debugger;
                // active means song is playing
                elem.previousElementSibling.classList.add('active')
                elem.previousElementSibling.querySelector('i').classList.remove('bi-play-fill');
                elem.previousElementSibling.querySelector('i').classList.add('bi-pause');
            })
            window[selector].on('pause', function () {
                debugger;
                elem.previousElementSibling.classList.remove('active')
                elem.previousElementSibling.querySelector('i').classList.add('bi-play-fill');
                elem.previousElementSibling.querySelector('i').classList.remove('bi-pause');

            })


        });

        $('.poll__wrapper').find('.custom-control-input').click(VotePoll);

        $('.poll__wrapper').find('.custom-checkbox').unbind().mousedown(function () {
            if ($(this).find('.custom-control-input').is(':checked')) {
                return false;
            }
            var CurrPostId = $(this).attr('data-val');
            //Previous Element
            var prevElem = $('input[name="' + CurrPostId + '"]:checked').parent('.custom-checkbox');
            var prevCurrCount = prevElem.find('.count').text();
            var prevCount = parseInt(prevCurrCount);
            var TotalVoteCount = $('#voteCount_' + CurrPostId);
            var TotalVote = parseInt(TotalVoteCount.val());
            var NewValue = prevCount - 1;
            var Percentage = parseInt((NewValue / TotalVote) * 100);
            prevElem.find('.absolute--sorry').width(Percentage + "%");
            prevElem.find('.count').text(NewValue);

            //CurrentEleement
            var currElem = $(this);
            var currCount = parseInt(currElem.find('.count').text());
            var CurrNewValue = currCount + 1;
            var CurrPercentange = parseInt(CurrNewValue / TotalVote * 100);
            currElem.find('.absolute--sorry').width(CurrPercentange + "%");
            currElem.find('.count').text(CurrNewValue);






            $(this).find('.custom-control-input').trigger('click');
        });





    };

    var EditKYC = function () {
        var evt = $(this);
        $(this).append(`<img src="/Content/gallery/rings.svg" alt="" >`);

        var cmodel = {
            Identification: '',
            IdNO: '',
            IdIssuedFrom: '',
            Country: '',
            ContactNumber: '',
            DOB: '',
            Gender: ''
        }
        var formModal = $('.profileKYC');

        cmodel.Identification = formModal.find('select[name="Identification"]').val();
        cmodel.IdNO = formModal.find('input[name="IdNO"]').val();
        cmodel.IdIssuedFrom = formModal.find('input[name="IdIssuedFrom"]').val();
        cmodel.Country = formModal.find('input[name="Country"]').val();
        cmodel.ContactNumber = formModal.find('input[name="ContactNumber"]').val();
        cmodel.DOB = formModal.find('input[name="DOB"]').val();
        cmodel.Gender = formModal.find('select[name="Gender"]').val();

        var model = JSON.stringify(cmodel);

        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: apiURL + '/UserInterest/EditKYCDetails',
            type: 'POST',
            crossDomain: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: model
        }).done(function (data) {
            evt.find('img').remove();
            $('#kyc--modal').modal('hide');
            $d.resolve(data);
            EventNotification(data);

        }).fail(function (data) {
            evt.find('img').remove();
            $('#kyc--modal').modal('show');
            $d.reject(data);
            EventNotification(data);

        });

        return $d.promise();
    };
    var ReportPost = function (e) {
        var Elem = $(this).parents("article:first").attr('id');
        var Id = Elem.replace('feedCard__', '');
        var modal = $('#message--new');
        var url = appURL + '/Post/ReportPost/?PostId=' + Id;
        modal.modal('toggle');

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                modal.find('.modal-body').empty();
                modal.find('.modal-body').html(this.responseText);
                $('.reportListReason .reportReason').unbind().click(function (ex) {
                    ex.preventDefault();
                    var Reason = $(this).attr('data-val');
                    modal.modal('toggle');
                    var result = { returnMsg: 'Successfully Reported !' };
                    EventNotification(result);

                });
            }
        };
        xhttp.open("GET", url, true);
        xhttp.send();




    };
    var userUnfollow = function () {

        var r = confirm("Are you sure you want to unfollow ? ");
        if (r == false) {
            return false;
        }

        var UserId = $(this).attr('data-id');
        var url = apiURL + "/UserFollow?UserId=" + UserId
        var auth = $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            if (result.returnMsg == "User Followed") {

                EventNotification(result);
            }
            else {

                EventNotification(result);
            }

        }).fail(function (error) {
            console.log(error);
            console.log(error);
        });

    }
    //function validateConfirm(message?: string) {

    //    $('#modal--confirm').find('.section__title').text(message);
    //    $('#modal--confirm').modal('toggle');

    //    return true;
    //};
    var HidePost = function (e) {
        e.preventDefault();



        var id = $(this).attr('data-id');
        var Elem = $(this).parents('article');
        var url = apiURL + "/PostMasters/HidePost?PostId=" + id
        var auth = $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'GET',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            if (result.success) {

                EventNotification(result);
                Elem.remove();
            }
            else {

                EventNotification(result);
            }

        }).fail(function (error) {
            console.log(error);
        });

    }
    var UnhidePost = function (e) {
        e.preventDefault();


        var $this = $(this);
        var id = $(this).attr('data-id');
        var Elem = $(this).parents('article');
        var url = apiURL + "/PostMasters/UnHidePost?PostId=" + id
        var auth = $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'GET',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            if (result.success) {
                EventNotification(result);
                Elem.remove();
            }
            else {

                EventNotification(result);
            }

        }).fail(function (error) {
            console.log(error);
        });

    }
    var DeletePost = function (e) {
        e.preventDefault();

        var r = confirm("Are you sure you want to delete ? ");
        if (r == false) {
            return false;
        }

        var id = $(this).attr('data-id');
        var Elem = $(this).parents('article');
        var url = apiURL + "/PostMasters/DeletePost?id=" + id
        var auth = $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            if (result.success) {

                EventNotification(result);
                Elem.remove();
            }
            else {

                EventNotification(result);
            }

        }).fail(function (error) {
            console.log(error);
        });

    }

    var UnlockPost = function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        $('#modal--confirm').modal('toggle');
        $('#modal--confirm').find('.section__title').text('You are about to unlock the post. Confirm?')
        $('#modal--confirm button#__active').unbind().click(function () {
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').removeAttr('hidden');
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').find('#fbWallet').attr('data-id', dataId);
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').find('#btnPay').attr('data-id', dataId);
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').find('#fbWallet').attr('data-type', 'P');
            $('#modal--confirm').find('.modal-body').find('.paymentAction').attr('hidden', true);
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').find('#fbWallet').unbind().click(ProcessPayment);
        });

        //$('#modal--confirm button#__active').unbind().click(ProcessPayment);


    };


    var SubscribeToUser = function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-val');
        $('#modal--confirm').modal('toggle');
        $('#modal--confirm').find('.section__title').text('You are about to Subscribe the User. Confirm?')
        $('#modal--confirm button#__active').attr('data-id', dataId);
        $('#modal--confirm button#__active').unbind().click(function () {
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').removeAttr('hidden');
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').find('#fbWallet').attr('data-id', dataId);
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').find('#fbWallet').attr('data-type', 'S');
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').find('#btnPay').attr('data-id', dataId);
            $('#modal--confirm').find('.modal-body').find('.paymentAction').attr('hidden', true);
            $('#modal--confirm').find('.modal-body').find('.paymentSelector').find('#fbWallet').unbind().click(ProcessPayment);
        });
        //$('#modal--confirm button#__active').unbind().click(subsPayment);


    };
    var cancelSubscriptionPrompt = function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        $('#modal--confirm').modal('toggle');
        $('#modal--confirm').find('.section__title').text('You are sure you want  to unsubscribe the User ?')
        $('#modal--confirm button#__active').attr('data-id', dataId);
        $('#modal--confirm button#__active').unbind().click(cancelSubscription);


    };
    var cancelSubscription = function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        instance = this;
        $(instance).prop('disabled', true);
        $(instance).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        var auth = $('#token___Auth').val();
        var url = apiURL + '/Subscriptions/CancelSubscriptions?SubscriptionId=' + dataId;
        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            $('#modal--confirm').modal('toggle');
            EventNotification(result);
            setTimeout(function () {
                window.location.reload(1);
            }, 5000);

        }).fail(function (error) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            $('#modal--confirm').modal('toggle');
            var result = { returnMsg: 'We are Unable to validate your request...' };
            EventNotification(result);
        });

    };
    var subsPayment = function () {
        var datId = $(this).attr('data-id');
        instance = this;
        $(instance).prop('disabled', true);
        $(instance).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        var response = Payments.PaymentValidation(apiURL);
        if (response == 100) {
            Payments.SubscribePayment(datId, pin, apiURL, instance);


        }
        else if (response == 700) {
            var result = { returnMsg: 'You Havent Set Any PIN Code, Please Set First!' };
            EventNotification(result);
        }
        else {
            var result = { returnMsg: 'Error, Please try again.' };
            EventNotification(result);
        }
        $(instance).prop('disabled', false);
        $(instance).find("img").remove();
        $('#modal--confirm').modal('hide');

    }

    var ProcessPayment = function () {
        var datId = $(this).attr('data-id');
        var type = $(this).attr('data-type');

        instance = this;
        $(instance).prop('disabled', true);
        //$(instance).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        var response = Payments.PaymentValidation(apiURL);

        if (response == 100) {
            $(this).closest(".modal").modal('hide');

            if (type === 'P') {
                $("#enter--pin--modal").removeClass('ForSubscription');

            } else {
                $("#enter--pin--modal").addClass('ForSubscription');

            }
            //set attr value to hidden field oof modal
            $("#enter--pin--modal").find(".datId").val(datId);
            //show modal for payment pin
            $("#enter--pin--modal").modal('show');

        }
        else if (response == 700) {
            var result = { returnMsg: 'You Havent Set Any PIN Code, Please Set First!' };
            EventNotification(result);
        }
        else {
            var result = { returnMsg: 'Error, Please try again.' };
            EventNotification(result);
        }
    };
    $("#enter--pin--modal form").unbind().submit(function (e) {
        e.preventDefault();
        var $pinModal = $('#enter--pin--modal');
        var pSpan = $pinModal.find('input[name="pinDigit"]');
        var PIN = '';
        $(pSpan).each(function () {
            PIN += $(this).val();
        });
        var dataId = $pinModal.find('.datId').val();
        instance = this;
        $(instance).prop('disabled', true);
        $(instance).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        //Payments.PostPayment(dataId, PIN, apiURL, instance, appURL);
        if ($pinModal.hasClass("ForSubscription")) {
            Payments.SubscribePayment(dataId, PIN, apiURL, instance);
        } else {
            PostPayment(dataId, PIN, apiURL, instance, appURL);

        }

    });
    var loadSingleContentById = function (datId, appURL) {
        var xhttp = new XMLHttpRequest();
        var PostId = datId;
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var Feed = document.getElementById("feedCard__" + PostId);
                Feed.innerHTML = this.responseText;
                ContentAddons();
            }
        };
        xhttp.open("GET", appURL + "/Post/SingleContent?PostId=" + PostId, true);
        xhttp.send();
    };
    var GetCommentModal = function (data, index) {
        var elem = data.Collection[index];
        debugger;
        var commentElem = $('#fullImage--modal .right--aside');
        commentElem.find('.love').attr('data-id', elem.contentId);
        commentElem.find('.bookmark').attr('data-id', elem.contentId);
        commentElem.find('.love .number').text(elem.totalReacts);
        commentElem.find('.comments .number').text(elem.totalComments);
        if (elem.hasReacted) {
            commentElem.find('.love').addClass("active");
        }
        else {
            commentElem.find('.love').removeClass("active");
        }
        if (elem.hasBookMarked) {
            commentElem.find('.bookmark').addClass("active");
        } else {
            commentElem.find('.bookmark').removeClass("active");
        }

        //$('#fullImage--modal .right--aside').find('.card__title').text(data.jsonObj.creator.creator);//UserName 
        commentElem.find('.user__text').text(elem.contentTitle);//Caption
        //zcommentElem.find('.card__comments .card__img img').attr('src', $('#active_user_profileimage').val());//userimage
        commentElem.find('.card__comments .postId__comment').val(elem.contentId);//PostId
        commentElem.find('.card__comments .comments__feed').attr('id', 'viewComments__' + elem.contentId);//commentId



        GetComments(elem.contentId);

    };
    var EventNotification = function (data) {
        var Elem = $('.tost-wrapper');
        if (data.link) {
            var notification = $(`<div class="toast hide" role="alert">
              <div class="toast-header">
            <strong class="mr-auto">Alert</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    </div >
        <div class="toast-body">
            <p class="para">${data.returnMsg}<a href="${data.link}" class="link small">Click Here</a></p>
        </div>
</div>`);
            Elem.append(notification);
            //Elem.find('.toast').removeClass('hide');
            Elem.find('.toast').toast('show');
            //setTimeout(function (e) {
            //    Elem.find('.toast').addClass('hide');
            //}, 5000)
            setTimeout(function (e) {
                Elem.empty();
            }, 10000)
        }
        else {
            NoticeNotification(data.returnMsg);
        }

    };
    var NoticeNotification = function (message) {
        var Elem = $('.tost-wrapper');

        var notification = $(`<div class="toast" role="alert">
              <div class="toast-header">
            <strong class="mr-auto">Alert</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    </div >
        <div class="toast-body">
            <p class="para">${message}</p>
        </div>
</div>`);
        Elem.append(notification);
        Elem.find('.toast').addClass('show');
        setTimeout(function (e) {
            Elem.find('.toast').removeClass('show');
        }, 5000)
        setTimeout(function (e) {
            Elem.empty();
        }, 10000);
    }
    var LiveNotification = function () {
        var Elem = $('.tost-wrapper');
        var notification = $(`<div class="toast" role="alert">
              <div class="toast-header">
            <strong class="mr-auto">Alert</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    </div >
        <div class="toast-body">
            <p class="para">This is test data for alerts</p>
        </div>
</div>`);
        Elem.append(notification);
        Elem.find('.toast').addClass('show');
        setTimeout(function (e) {
            Elem.find('.toast').removeClass('show');
        }, 5000)
    };
    var PostReact = function (e) {
        e.preventDefault();
        var instance = this;
        var reactCount = parseInt($(instance).find('.number').text());
        var isActive = $(instance).hasClass('active');
        $(instance).css({
            'pointer-events': 'none'
        });
        $(instance).toggleClass('active');
        var model = {
            PostReact: '',
            PostId: '',
            record_Status: ''
        }
        model.PostReact = 'Love';
        model.PostId = $(instance).attr('data-id');
        model.record_Status = 'Active';
        if (isActive) {
            var reactCountt = reactCount - 1;
            $(instance).find('.number').text(reactCountt);
        }
        else {
            var reactCountt = reactCount + 1;
            $(instance).find('.number').text(reactCountt);
        }
        var url = appURL + '/Post/Content';
        var params = JSON.stringify(model);
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();

        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token
            },
            data: params,
        }).done(function (result) {

            $(instance).css({
                'pointer-events': ''
            });
        }).fail(function (error) {
            $(instance).toggleClass('active');

            if (isActive) {
                var reactCountt = reactCount + 1;
                $(instance).find('.number').text(reactCountt);
            }
            else {
                var reactCountt = reactCount - 1;
                $(instance).find('.number').text(reactCountt);
            }
            console.log(error);
        });




    };
    var PostPayment = function (datId, PIN, apiURL, instance, appURL) {
        var tempModel = {
            PostId: '',
            PIN: '',
            CurrId: ''

        }
        tempModel.postId = datId;
        tempModel.PIN = PIN;
        if (!tempModel.PIN) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            return false;
        }
        var auth = $('#token___Auth').val();
        var url = apiURL + '/Payment/PaymentForPost';
        var model = JSON.stringify(tempModel);
        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: model
        }).done(function (result) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            //$('#modal--confirm').modal('toggle');
            $("#enter--pin--modal").modal('hide');

            EventNotification(result);
            if (result.success) {
                loadSingleContentById(datId, appURL);
            }
            $('#enter--pin--modal').find('input[name="pinDigit"]').val('');
        }).fail(function (error) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            $('#modal--confirm').modal('toggle');
            var result = { returnMsg: 'Error! Action Failed' };
            EventNotification(result);
        });


    };
    var PostBookmark = function (instance) {

        var postId = $(instance).attr('data-id');
        $(instance).toggleClass('active');

        var url = apiURL + '/PostBookmark/BookmarkPost?PostId=' + postId;
        var auth = $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {

            EventNotification(result);

        }).fail(function (error) {
            var result = {
                returnMsg: "Internal server Error"
            };

            EventNotification(result);
        });

    };

    var PostComment = function (instance) {

        var model = {
            Comment: '',
            PostId: ''
        }
        model.Comment = $(instance).find('.comment__text').val();
        if (!model.Comment) {
            $(instance).find('.comment__text').parent().css('border-color', 'red');
            setTimeout(function () {
                // reset CSS
                $(instance).find('.comment__text').parent().css('border-color', '');
            }, 500);
            return false;
        }
        model.PostId = $(instance).find('.postId__comment').val();
        var url = appURL + '/Post/Comments';
        var params = JSON.stringify(model);
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();

        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token
            },
            data: params,
        }).done(function (result) {
            GetComments(model.PostId);
            $(instance).closest("form").find('.comment__text').val("");
            //var number = $(instance).closest("article").find(".card__action .comments .number").text();
            var noOfComments = parseInt($(instance).closest(".card__comments").prev(".card__action").find(".comments .number").text()) + 1;
            $(instance).closest(".card__comments").prev(".card__action").find(".comments .number").text(noOfComments);
            // PostComment.unbind();
        }).fail(function (error) {
            console.log(error);
        });




    };
    var GetComments = function (PostId) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var Feed = document.getElementById("viewComments__" + PostId);
                Feed.innerHTML = this.responseText;
                ContentAddons();
                $('.edit--comment--trigger').click(EditComment);
                $('.delete--comment--trigger').click(DeleteComment);
            }
        };
        xhttp.open("GET", appURL + "/Post/Comments?PostId=" + PostId, true);
        xhttp.send();
    };

    var EditComment = function () {

        var PostId = $(this).attr('data-val');

        var Id = $(this).attr('data-id');

        $("#Comment_" + Id).find('p').hide();
        //$("#Comment_" + Id).find('input').removeClass('hidden');
        $("#Comment_" + Id).find('.commentEditForm').removeClass('hidden');
        $("#Comment_" + Id).find('input[name="Comment"]').removeClass('hidden');

        $('.commentEditForm').unbind().submit(function (event) {
            event.preventDefault();
            SubmitEditComment(this);
        });



    };
    var cancelEditComment = function () {
        var PostId = $(this).attr('data-val');

        var Id = $(this).attr('data-id');

        $("#Comment_" + Id).find('input[name="Comment"]').hide();
        $("#Comment_" + Id).find('p').show();
        //$("#Comment_" + Id).find('input').removeClass('hidden');


    }
    var SubmitEditComment = function (instance) {
        var postComments = {
            Id: '',
            Comment: ''
        };

        //var PostId = $(this).find('input[name="Post"]').val();
        var Id = $(instance).find('input[name="CommentId"]').val();
        postComments.Id = Id;
        postComments.Comment = $(instance).find('input[name="Comment"]').val();

        var token = $('input:hidden[name="__RequestVerificationToken"]').val();
        var auth = $('#token___Auth').val();



        var url = apiURL + '/PostComments/EditPostComments';
        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token,
                Authorization:
                    "Bearer " + auth
            },
            data: JSON.stringify(postComments),
        }).done(function (result) {
            //GetComments(PostId);
            $("#Comment_" + Id).find('input[name="Comment"]').addClass('hidden');
            $("#Comment_" + Id).find('.commentEditForm').addClass('hidden');
            $("#Comment_" + Id).find('p').text(postComments.Comment);
            $("#Comment_" + Id).find('p').show();

            EventNotification(result);
        }).fail(function (error) {
            console.log(error);
        });


    };

    var DeleteComment = function () {
        var PostId = $(this).attr('data-val');
        var Id = $(this).attr('data-id');
        var r = confirm("Are you sure you want to delete ? ");
        if (r == false) {
            return false;
        }


        var token = $('input:hidden[name="__RequestVerificationToken"]').val();
        var auth = $('#token___Auth').val();
        var url = apiURL + '/PostComments/DeleteComment?id=' + Id;
        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token,
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            GetComments(PostId);
            EventNotification(result);
        }).fail(function (error) {
            console.log(error);
        });

    };


    var getSearchVideos = function (str) {
        var url = appURL + '/User/UserBySearch?msg=' + str;
        var $d = $.Deferred();
        $.get(url).then(function (response) {
            $d.resolve(response);
        }).fail(function (response) {
            $d.reject(response);
        });

        return $d.promise();
    };
    var SearchVideos = function () {
        var nput = $('#postSearch').val();
        var instance = this;
        if (nput) {
            getSearchVideos(nput).done(function (data) {

                if (data) {
                    $(instance).next().addClass('active');
                    $(instance).next().next().addClass('active');
                    $(instance).parents('form').find('.searchList').empty();
                    $(instance).parents('form').find('.searchList').append(data);
                    $(instance).parents('form').find('.searchList').show(300)
                }
                else {
                    $(instance).parents('form').find('.searchList').empty();
                    $(instance).next().removeClass('active');
                    $(instance).next().next().removeClass('active');
                    $(instance).parents('form').find('.searchList').hide(300);

                }
            });
        }
        else {
            $(instance).next().removeClass('active');
            $(instance).next().next().removeClass('active');
            $(instance).parents('form').find('.searchList').hide(300);
            return false;
        }

    };


    //function isElementVisible(el) {
    //    if (el == null) {
    //        return false;
    //    }
    //    var rect = el.getBoundingClientRect(),
    //        vWidth = window.innerWidth || document.documentElement.clientWidth,
    //        vHeight = window.innerHeight || document.documentElement.clientHeight,
    //        efp = function (x, y) { return document.elementFromPoint(x, y) };

    //    // Return false if it's not in the viewport
    //    if (rect.right < 0 || rect.bottom < 0
    //        || rect.left > vWidth || rect.top > vHeight)
    //        return false;

    //    // Return true if any of its four corners are visible
    //    return (
    //        el.contains(efp(rect.left, rect.top))
    //        || el.contains(efp(rect.right, rect.top))
    //        || el.contains(efp(rect.right, rect.bottom))
    //        || el.contains(efp(rect.left, rect.bottom))
    //    );
    //}


    //$(window).scroll(function () {
    //    if (scrollContentsLoad && isElementVisible(document.getElementById("End"))) {
    //        scrollContentsLoad = false;
    //        loadContents();
    //        //scrollLoad = false;
    //        //Add something at the end of the page
    //    }
    //});
    //$('#modal--post__input').keyup(function (e) {
    //    console.log(e);
    //});
    function isUrl(s) {
        if (!isUrl.rx_url) {
            // taken from https://gist.github.com/dperini/729294
            isUrl.rx_url = /^(?:(?:https?|ftp):\/\/)?(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
            // valid prefixes
            isUrl.prefixes = ['http:\/\/', 'https:\/\/', 'ftp:\/\/', 'www.'];
            // taken from https://w3techs.com/technologies/overview/top_level_domain/all
            isUrl.domains = ['com', 'ru', 'net', 'org', 'de', 'jp', 'uk', 'br', 'pl', 'in', 'it', 'fr', 'au', 'info', 'nl', 'ir', 'cn', 'es', 'cz', 'kr', 'ua', 'ca', 'eu', 'biz', 'za', 'gr', 'co', 'ro', 'se', 'tw', 'mx', 'vn', 'tr', 'ch', 'hu', 'at', 'be', 'dk', 'tv', 'me', 'ar', 'no', 'us', 'sk', 'xyz', 'fi', 'id', 'cl', 'by', 'nz', 'il', 'ie', 'pt', 'kz', 'io', 'my', 'lt', 'hk', 'cc', 'sg', 'edu', 'pk', 'su', 'bg', 'th', 'top', 'lv', 'hr', 'pe', 'club', 'rs', 'ae', 'az', 'si', 'ph', 'pro', 'ng', 'tk', 'ee', 'asia', 'mobi'];
        }

        if (!isUrl.rx_url.test(s)) return false;
        for (let i = 0; i < isUrl.prefixes.length; i++) if (s.startsWith(isUrl.prefixes[i])) return true;
        for (let i = 0; i < isUrl.domains.length; i++) if (s.endsWith('.' + isUrl.domains[i]) || s.includes('.' + isUrl.domains[i] + '\/') || s.includes('.' + isUrl.domains[i] + '?')) return true;
        return false;
    }

    function AddContentToPostCollection(data, paidElem, ContentType) {
        var uploadModel = {
            ContentId: data.returnId,
            FileLocation: data.returnPath,
            PaidPost: paidElem ? 'true' : 'false',
            PostType: ContentType
        };

        apiModel.PostCollections.push(uploadModel);
    }

    return {
        initUI: initUI,
        ContentAddons: ContentAddons,
        appURL: appURL,
        apiURL: apiURL,
        EventNotification: EventNotification,
        NoticeNotification: NoticeNotification,
        loadMoments: loadMoments,
        loadContents: loadContents
        //    isElementVisible: isElementVisible
    };
})(jQuery);
jQuery(app.CommonSite.initUI);


