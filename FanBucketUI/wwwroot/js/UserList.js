﻿// Show loading overlay when ajax request starts
$(document).ajaxStart(function () {
    $('.loading-overlay').show();
});

// Hide loading overlay when ajax request completes
$(document).ajaxStop(function () {
    $('.loading-overlay').hide();
});
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'Authorization': "Bearer " + $("#token___Auth").val()
        }
    });
    searchFilter();
});


function searchFilter(page_num, pagesize) {
    page_num = page_num ? page_num : 0;
    pagesize = 10;
    var keywords = $('#keywords').val();
    $.ajax({
        type: 'GET',
        url: '/Admin/Users',

        data: 'handler=Partial&PageNumber=' + page_num + '&PageSize=' + pagesize + '&searchText=' + keywords,
        beforeSend: function () {
            $('.loading-overlay').show();

        },
        success: function (html) {
            $('#postContent').html(html);
            $('.loading-overlay').fadeOut("slow");
        }
    });
}
