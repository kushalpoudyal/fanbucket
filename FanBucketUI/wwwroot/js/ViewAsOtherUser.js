﻿if (!window.app) {
    window.app = {};
}

app.ProfileSite = (function ($) {

    var CommonSite = app.CommonSite;
    var Payments = app.Payments;
    let Subscriptiontype = jsonString;
    var increment = 0;
    var scrollProfileContentLoad = true;
    var PageNumber = 1;
    var PageSize = 10;
    var initUI = function () {
        //loadContents();
        var Username = $('#user_username').val();
        //window.history.pushState("data", "Title", Username);
        app.Payments.GetBalance(CommonSite.apiURL, ".profile #myBalance")
        $('#FollowUser').unbind();
        //$('#FollowUser').unbind().click(userFollow);

        //$('#ppImage').click(function (e) {
        //    $('#ppImageInput').trigger('click');
        //});
        //$('#wallImage').click(function (e) {
        //    $('#wallImageInput').trigger('click');
        //});

        //$('#ppImageInput').change(function (e) {
        //    $(this).next('.circle--camera').addClass('loading');
        //    uploadProfile(this, '#ppImage');
        //});
        //$('#wallImageInput').change(function (e) {
        //    $(this).next('.circle--camera').addClass('loading');
        //    uploadCover(this, '#wallImage');
        //});
        $('#__GetScheduled').unbind().click(loadScheduled);
        $('#__GetBookMark').unbind().click(loadBookmarks);
        $('#__GetPosts').unbind().click(function () {
            $("#viewFeed .feed").empty();
            PageNumber = 1;
            loadContents();

        });
        $('#__GetImages').unbind().click(loadImages);
        $('#__GetVideos').unbind().click(loadVideos);

        $('#SetPin').unbind().on("click", SetPincode);
        //$('#btnPersonal').unbind().click(EditPersonalProfile);
        //$('#btnAddSubs').click(AddSubscription);
        //$('.deleteBtn').unbind().click(DeleteSubscription);

        //$('#btnPrivacy').click(EditPrivacy);
        //$('#btnKYC').click(EditKYC);



        //$('select[name="user.AccountType"]').change(function () {
        //    if ($(this).val() == "2") {
        //        $('.privateSection').removeClass('hide');
        //    }
        //    else {
        //        $('.privateSection').removeClass('hide');
        //    }

        //});
        //const myDatePicker = MCDatepicker.create({
        //    el: '#DOB',
        //    dateFormat: 'YYYY-MM-DD',
        //    bodyType: 'inline',
        //})


        let options = {
            root: null,
            rootMargin: '0px',
            threshold: 1.0
        }

        let callback = (entries, observer) => {
            entries.forEach(entry => {
                // Each entry describes an intersection change for one observed
                // target element:
                //   entry.boundingClientRect
                //   entry.intersectionRatio
                //   entry.intersectionRect
                //   entry.isIntersecting
                //   entry.rootBounds
                //   entry.target
                //   entry.time
                if (entry.isIntersecting && scrollProfileContentLoad) {
                    scrollProfileContentLoad = false;
                    loadContents();
                    //    scrollContentsLoad = true;
                }


                console.log(entry.isIntersecting);
            });
        };
        let observer = new IntersectionObserver(callback, options);
        let target = document.querySelector('#ProfileEnd');
        observer.observe(target);
    };

    var SetPincode = function (e) {
        e.preventDefault();
        // var UserId = $(this).attr('data-id');
        $('#ott--modal').modal('toggle');
        $('#ott--modal').find('.section__title').text('Set Your Pin');
        //var elem = `<div class="form-group otp">
        //                    <input type="password" id="pinPassword" style="width:100%" placeholder="Enter Your Password" class="form-control">
        //                </div>`;
        //$('#ott--modal').find('.forminput').append(elem);
        var container = document.getElementsByClassName("otp")[0];
        container.onkeyup = function (e) {
            var target = e.srcElement || e.target;
            var maxLength = parseInt(target.attributes["maxlength"].value, 10);
            var myLength = target.value.length;
            if (myLength >= maxLength) {
                var next = target;
                while (next = next.nextElementSibling) {
                    if (next == null)
                        break;
                    if (next.tagName.toLowerCase() === "input") {
                        next.focus();
                        break;
                    }
                }
            }
            // Move to previous field if empty (user pressed backspace)
            else if (myLength === 0) {
                var previous = target;
                while (previous = previous.previousElementSibling) {
                    if (previous == null)
                        break;
                    if (previous.tagName.toLowerCase() === "input") {
                        previous.focus();
                        break;
                    }
                }
            }
        }

    };

    var userFollow = function () {
        var evt = $(this);
        $(this).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        var UserId = document.getElementById('UserId').value;
        var url = CommonSite.apiURL + "/UserFollow?UserId=" + UserId
        var auth = $('#token___Auth').val();

        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            if (result.returnMsg == "User Followed") {
                evt.toggleClass('active');

                var messageTemplate = `<a href="/Messenger/Messenger?ChatId=${result.jsonObj}&UserId=${UserId}" class=" follow outline--common">
                                Send a message
                            </a>`;

                evt.closest(".card__top__link").prepend(messageTemplate);
                evt.text("Following");
                CommonSite.EventNotification(result);
            }
            else {
                evt.closest(".card__top__link").find("a").remove();

                evt.toggleClass('active');
                evt.text("Follow");
                CommonSite.EventNotification(result);
            }

        }).fail(function (error) {
            console.log(error);
        });

    }

    var loadContents = function () {
        debugger;
        var xhttp = new XMLHttpRequest();
        $(this).addClass('active');
        var User = document.getElementById('UserId');
        if (User) {
            var UserId = User.value;
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    debugger;

                    var Feed = document.querySelector("#viewFeed .feed");
                    console.log(Feed);
                    Feed.innerHTML += this.responseText;
                    //CommonSite.ContentAddons();
                    scrollProfileContentLoad = true;
                    PageNumber++;
                }
            };
            var Dat = new Date().toJSON();
            xhttp.open("GET", CommonSite.appURL + "/Post/ViewAsGuestUserContent?dateTime=" + Dat + "&PageNumber=" + PageNumber + "&PageSize=" + PageSize, true);
            xhttp.send();
        }
    };
    var loadScheduled = function () {
        var xhttp = new XMLHttpRequest();
        $(this).toggleClass('active');
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var Feed = document.getElementById("viewFeed");
                Feed.innerHTML = this.responseText;
                //CommonSite.ContentAddons();

            }
        };
        var Dat = new Date().toJSON();
        xhttp.open("GET", CommonSite.appURL + "/User/ScheduledPost?dateTime=" + Dat, true);
        xhttp.send();
    };
    var loadBookmarks = function () {
        var xhttp = new XMLHttpRequest();
        $(this).toggleClass('active');
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var Feed = document.querySelector("#viewFeed .feed");
                Feed.innerHTML = this.responseText;
                //CommonSite.ContentAddons();

            }
        };
        xhttp.open("GET", CommonSite.appURL + "/Post/Bookmark", true);
        xhttp.send();
    };
    var loadVideos = function () {
        var xhttp = new XMLHttpRequest();
        var UserId = document.getElementById('UserId').value;
        $(this).toggleClass('active');
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var Feed = document.querySelector("#viewFeed .feed");
                Feed.innerHTML = this.responseText;
                //CommonSite.ContentAddons();

            }
        };
        xhttp.open("GET", CommonSite.appURL + "/User/GuestUserGetPostByVideoContent", true);
        xhttp.send();
    };
    var loadImages = function () {
        var xhttp = new XMLHttpRequest();
        var UserId = document.getElementById('UserId').value;
        $(this).toggleClass('active');
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var Feed = document.querySelector("#viewFeed .feed");
                Feed.innerHTML = this.responseText;
                //CommonSite.ContentAddons();
                Fancybox.destroy("[data-fancybox]");
                Fancybox.bind("[data-fancybox]", {
                    // Your options go here
                });

            }
        };
        xhttp.open("GET", CommonSite.appURL + "/User/GuestUserPostByImageContent", true);
        xhttp.send();
    };

    var EditPersonalProfile = function () {
        var evt = $(this);
        $(this).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        var cmodel = {
            ShortBio: '',
            Profession: '',
            Username: '',
            Location: '',
            Fullname: ''
        };
        cmodel.Fullname = $('input[name="Fullname"]').val();
        cmodel.Username = $('input[name="Username"]').val();
        cmodel.Location = $('input[name="Location"]').val();
        cmodel.Profession = $('input[name="Profession"]').val();
        cmodel.ShortBio = $('textarea[name="ShortBio"]').val();

        var model = JSON.stringify(cmodel);

        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: CommonSite.apiURL + '/UserInterest/EditProfilePersonal',
            type: 'POST',
            crossDomain: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: model
        }).done(function (data) {
            evt.find('img').remove();
            $d.resolve(data);
            CommonSite.EventNotification(data);
        }).fail(function (data) {
            evt.find('img').remove();
            $d.reject(data);
            CommonSite.EventNotification(data);
        });

        return $d.promise();

    }
    var AddSubscription = function () {
        var formModal = $('#SubsInput');
        if (formModal.hasClass('hide')) {
            formModal.removeClass('hide');
            return false;
        }
        var modal = {
            id: '',
            tenure: '',
            subscriptionAmount: ''
        }
        var inc = increment + 1;
        var table = $('#SubsTbl');
        modal.tenure = formModal.find('select[name="Tenure"]').val();
        var tenure = parseInt(modal.tenure);
        //debugger;
        console.log(tenure);
        if (!isNaN(tenure) && tenure !== -1) {

        } else {
            var result = { returnMsg: 'Please select valid Tenure Value.' };

            CommonSite.EventNotification(result);
            return false;
        }
        modal.subscriptionAmount = formModal.find('input[name="SubscriptionAmount"]').val();
        modal.id = inc.toString();

        if (!modal.subscriptionAmount) {

            return false;
        }
        Subscriptiontype.push(modal);

        var rowElem = ` <tr><td>${modal.tenure}
                                    </td>
                                    <td>
${modal.subscriptionAmount}
                                    </td>
                                    <td>
<a href="#" class="deleteBtn" data-val=${inc}>
<img src="https://img.icons8.com/fluent/2x/delete-forever.png" alt="Delete Bin icon" loading="lazy" style="height: 28px; width: 28px;"></a>
                                    </td>
                                </tr>`;
        table.find('tbody').append(rowElem);
        formModal.find('input').val('');
        $('.deleteBtn').unbind().click(DeleteSubscription);


    }
    var DeleteSubscription = function (e) {
        e.preventDefault();
        var Id = $(this).attr('data-val');
        var index = Subscriptiontype.findIndex(x => x.id == Id);
        Subscriptiontype.splice(index, 1);
        $(this).parents('tr').remove();
    }
    var EditPrivacy = function () {
        var evt = $(this);
        $(this).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        var cmodel = {
            AccountType: '',
            NSFW: '',
            SubscriptionType: []
        };

        cmodel.AccountType = $('select[name="user.AccountType"]').val();
        cmodel.NSFW = $('input[name="user.NSFW"]').is(':checked').toString();
        cmodel.SubscriptionType = Subscriptiontype;

        var model = JSON.stringify(cmodel);

        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: CommonSite.apiURL + '/UserInterest/EditPrivacyProfile',
            type: 'POST',
            crossDomain: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: model
        }).done(function (data) {
            evt.find('img').remove();
            $d.resolve(data);
            $('#SubsTbl tbody').empty();
            Subscriptiontype = data.jsonObj;
            for (var i = 0; i < data.jsonObj.length; i++) {
                var modal = data.jsonObj[i];
                var rowElem = ` <tr><td>${modal.tenure}
                                    </td>
                                    <td>
${modal.subscriptionAmount}
                                    </td>
                                    <td>
<a href="#" class="deleteBtn" data-val=${modal.id}>
<img src="https://img.icons8.com/fluent/2x/delete-forever.png" alt="Delete Bin icon" loading="lazy" style="height: 28px; width: 28px;"></a>
                                    </td>
                                </tr>`;
                $('#SubsTbl tbody').append(rowElem);
            }
            $('.deleteBtn').unbind().click(DeleteSubscription);

            CommonSite.EventNotification(data);
        }).fail(function (data) {
            evt.find('img').remove();
            $d.reject(data);
            CommonSite.EventNotification(data);
        });

        return $d.promise();

    }
    var EditKYC = function () {
        var evt = $(this);
        $(this).append(`<img src="/Content/gallery/rings.svg" alt="" >`);

        var cmodel = {
            Identification: '',
            IdNO: '',
            IdIssuedFrom: '',
            Country: '',
            ContactNumber: '',
            DOB: '',
            Gender: ''
        }
        var formModal = $('.profileKYC');

        cmodel.Identification = formModal.find('select[name="Identification"]').val();
        cmodel.IdNO = formModal.find('input[name="IdNO"]').val();
        cmodel.IdIssuedFrom = formModal.find('input[name="IdIssuedFrom"]').val();
        cmodel.Country = formModal.find('input[name="Country"]').val();
        cmodel.ContactNumber = formModal.find('input[name="ContactNumber"]').val();
        cmodel.DOB = formModal.find('input[name="DOB"]').val();
        cmodel.Gender = formModal.find('select[name="Gender"]').val();

        var model = JSON.stringify(cmodel);

        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: CommonSite.apiURL + '/UserInterest/EditKYCDetails',
            type: 'POST',
            crossDomain: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: model
        }).done(function (data) {
            evt.find('img').remove();
            $d.resolve(data);
            CommonSite.EventNotification(data);
        }).fail(function (data) {
            evt.find('img').remove();
            $d.reject(data);
            CommonSite.EventNotification(data);
        });

        return $d.promise();
    };

    var uploadPropic = function (formData) {
        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: apiURL + '/Bucket/UploadUserPic/?Type=ProfilePicture',
            crossDomain: true,
            data: formData,
            type: 'POST',
            crossDomain: true,
            processData: false,
            contentType: false,
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (data) {
            //updateClaims(data.returnMsg, "Profile");
            $d.resolve(data);
        }).fail(function (data) {

            $d.reject(data);

        });

        return $d.promise();

    };
    var updateClaims = function (value, Type) {

        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                console.log('done');
            }
        };
        xhttp.open("POST", CommonSite.appURL + "/UpdateClaims", true);//?Value=" + value + "&Type=" + Type
        xhttp.send();

    }
    var uploadCoverpic = function (formData) {
        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: CommonSite.apiURL + '/Bucket/UploadUserPic/?Type=CoverPicture',
            crossDomain: true,
            data: formData,
            type: 'POST',
            crossDomain: true,
            processData: false,
            contentType: false,
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (data) {
            //updateClaims(data.returnMsg, "Cover");
            $d.resolve(data);
        }).fail(function (data) {

            $d.reject(data);

        });

        return $d.promise();

    };
    //$(window).scroll(function () {
    //    if (scrollProfileContentLoad && CommonSite.isElementVisible(document.getElementById("ProfileEnd"))) {
    //        debugger;
    //        scrollProfileContentLoad = false;
    //        loadContents();
    //        //scrollLoad = false;
    //        //Add something at the end of the page
    //    }
    //});
    const uploadProfile = function (input, imageAppend) {
        if (input.files) {
            var files = input.files;
            var formData = new FormData();
            let filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                formData.append("files", files[i]);
                uploadPropic(formData).done(function (data) {
                    $(imageAppend).attr('src', data.returnPath);
                    data.returnMsg = "Successfully Updated the Profile Picture";
                    $('#ppImageInput').next('.circle--camera').removeClass('loading');
                    CommonSite.EventNotification(data);
                }).fail(function (data) {
                    $('#ppImageInput').next('.circle--camera').removeClass('loading');
                    CommonSite.EventNotification(data)
                });

            }


        }

    };
    const uploadCover = function (input, imageAppend) {
        if (input.files) {
            var files = input.files;
            var formData = new FormData();
            let filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                formData.append("files", files[i]);
                uploadCoverpic(formData).done(function (data) {
                    $(imageAppend).attr('src', data.returnPath);
                    data.returnMsg = "Successfully Updated the Cover Picture";
                    CommonSite.EventNotification(data);
                    $('#wallImageInput').next('.circle--camera').removeClass('loading');
                }).fail(function (data) {
                    $('#wallImageInput').next('.circle--camera').removeClass('loading');
                    CommonSite.EventNotification(data)
                });

            }


        }

    };

    return {
        initUI: initUI
    };
})(jQuery);
jQuery(app.ProfileSite.initUI);