// A reference to Stripe.js initialized with a  API key.
const stripe = Stripe("pk_test_51JNrobCtlkxgnzHQ3mlHM3Gye5AlUtEIwVmlT4NFwiw1xmVyeMe0cEfUG4yq2zJtnY9mG5YTumuxlWFRL20CP16400SS9uYq8K");

// The items the customer wants to buy
const items = [{ id: "xl-tshirt" }];

let elements;

initialize();
checkStatus();

document
    .querySelector("#payment-form")
    .addEventListener("submit", handleSubmit);

// Fetches a payment intent and captures the client secret
async function initialize() {
    var modal = {
        Amount: amount
    }
    var token = document.getElementById('token___Auth').value;
    const response = await fetch(apiURL+"/Payment", {
        method: "POST",
        headers: {
            "Content-Type": "application/json", "authorization": 'Bearer ' + token
        },
        body: JSON.stringify(modal),
    });
    const { clientSecret } = await response.json();

    const appearance = {
        theme: 'none',
        variables: {
            fontWeightNormal: '500',
            borderRadius: '2px',
            colorIconTabSelected: 'white',
            spacingGridRow: '15px'
        },
        rules: {
            '.Label': {
                marginBottom: '6px'
            },
            '.Tab, .Input, .Block': {
                boxShadow: '0px 3px 10px rgba(18, 42, 66, 0.08)'
            },
            '.Tab--selected': {
                backgroundColor: '#DF1B41',
                color: 'white'
            }
        }
    };

    // Pass the appearance object to the Elements instance
    //const elements = stripe.elements({ clientSecret, appearance });

    const options = {
        clientSecret: clientSecret,
        // Fully customizable with appearance API.
        appearance: appearance,
        fields: {
            billingDetails: {
                name: 'auto',
                email: 'never',
            }
        }
    };
    //        const elements = stripe.elements(options);

    //    //const elements = stripe.elements({ appearance, clientSecret });

    //    // Customize which fields are collected by the Payment Element
    //    var paymentElement = elements.create('payment');
    //    //var paymentElement = elements.create('payment', {
    //    //    fields: {
    //    //        billingDetails: {
    //    //            name: 'auto',
    //    //            email: 'never',
    //    //        }
    //    //    }
    //    //});

    //    paymentElement.mount("#payment-element");
    //}
    elements = stripe.elements(options);

    const paymentElement = elements.create("payment");
    paymentElement.mount("#payment-element");
}

async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    //const { error } = stripe.confirmSetup({
    //    elements,
    //    confirmParams: {
    //        // Return URL where the customer should be redirected after the SetupIntent is confirmed.
    //        return_url: appURL+ '/Post/Feed',
    //    },
    //});
    const { error } = await stripe.confirmPayment({
        elements,
        confirmParams: {
            return_url: appURL + '/' + document.getElementById("username").value,
        },
    });


    // This point will only be reached if there is an immediate error when
    // confirming the payment. Otherwise, your customer will be redirected to
    // your `return_url`. For some payment methods like iDEAL, your customer will
    // be redirected to an intermediate site first to authorize the payment, then
    // redirected to the `return_url`.
    if (error.type === "card_error" || error.type === "validation_error") {
        showMessage(error.message);
    } else {
        showMessage("An unexpected error occured.");
    }

    setLoading(false);
}
//async function handleSubmit(e) {
//    e.preventDefault();
//    setLoading(true);

//    const { error } = await stripe.confirmPayment({
//        elements,
//        confirmParams: {
//            return_url: "/Post/Feed",
//        //    payment_method_data: {
//        //        billing_details: {
//        //            name: 'Jenny Rosen',
//        //            email: 'jenny.rosen@example.com',
//        //        }
//        //    },
//        },

//    });

//    // This point will only be reached if there is an immediate error when
//    // confirming the payment. Otherwise, your customer will be redirected to
//    // your `return_url`. For some payment methods like iDEAL, your customer will
//    // be redirected to an intermediate site first to authorize the payment, then
//    // redirected to the `return_url`.
//    if (error.type === "card_error" || error.type === "validation_error") {
//        showMessage(error.message);
//    } else {
//        showMessage("An unexpected error occured.");
//    }

//    setLoading(false);
//}

// Fetches the payment intent status after payment submission
async function checkStatus() {
    const clientSecret = new URLSearchParams(window.location.search).get(
        "payment_intent_client_secret"
    );

    if (!clientSecret) {
        return;
    }

    const { paymentIntent } = await stripe.retrievePaymentIntent(clientSecret);

    switch (paymentIntent.status) {
        case "succeeded":
            showMessage("Payment succeeded!");
            break;
        case "processing":
            showMessage("Your payment is processing.");
            break;
        case "requires_payment_method":
            showMessage("Your payment was not successful, please try again.");
            break;
        default:
            showMessage("Something went wrong.");
            break;
    }
}

// ------- UI helpers -------

function showMessage(messageText) {
    const messageContainer = document.querySelector("#payment-message");

    messageContainer.classList.remove("hidden");
    messageContainer.textContent = messageText;

    setTimeout(function () {
        messageContainer.classList.add("hidden");
        messageText.textContent = "";
    }, 4000);
}

// Show a spinner on payment submission
function setLoading(isLoading) {
    if (isLoading) {
        // Disable the button and show a spinner
        document.querySelector("#submit").disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#button-text").classList.add("hidden");
    } else {
        document.querySelector("#submit").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#button-text").classList.remove("hidden");
    }
}