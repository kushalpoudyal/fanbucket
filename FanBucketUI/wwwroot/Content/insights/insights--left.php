<main class="main messenger--main insights--main">
	<div class="title__wrapper search-wrapper--main">
		<h2 class="section__title">Insights</h2>


		<form class="title__link form-inline search-wrapper">
			<div class="form-group">

				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12.8 12.61" class="search--icon"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M12.8,11.89,9.86,9a5.58,5.58,0,1,0-.7.73l2.92,2.93ZM10,5.53A4.51,4.51,0,1,1,5.53,1,4.51,4.51,0,0,1,10,5.53Z"></path></g></g></svg>

				<input class="form-control"   type="search"  name="chatOrSearch">
				<i class="bi bi-x close--icon"></i>
			</div>
		</form>

		<a href="#" class="title__link search--icon ml-auto" >
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12.8 12.61"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M12.8,11.89,9.86,9a5.58,5.58,0,1,0-.7.73l2.92,2.93ZM10,5.53A4.51,4.51,0,1,1,5.53,1,4.51,4.51,0,0,1,10,5.53Z"></path></g></g></svg>
		</a>
		<a href="#" class="title__link">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12.78 11.1"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><rect class="cls-1" width="12.78" height="1.11"></rect><rect class="cls-1" y="4.99" width="9.47" height="1.11"></rect><rect class="cls-1" y="9.99" width="6.17" height="1.11"></rect></g></g></svg>
		</a>
	</div>
	<section class="feed">
		<a href="./insights-engagements.php" title="Social Engagements" class="card chat--card ">
			<div class="card__top">
				<div class="card__top__body">
					<h2 class="section__title">Social Engagements</h2>
					<p class="user__text text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, illum.
					</p>
				</div>
				<div class="card__top__link">
					<button href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-square" viewBox="0 0 16 16">
							<path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
							<path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
						</svg>
					</button>
				</div>
			</div>
		</a>

		<a href="#" title="Content Engagements" class="card chat--card ">
			<div class="card__top">
				

				<div class="card__top__body">
					<h2 class="section__title">Content Engagements</h2>
					<p class="user__text text-muted"> Lorem ipsum dolor sit amet consectetur adipisicing, elit. At, id.
					</p>
				</div>


				<div class="card__top__link">
					<button href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-square" viewBox="0 0 16 16">
							<path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
							<path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
						</svg>
					</button>
			
				</div>
			</div>
		</a>

		<a href="./insights--earning.php" title="Subscriptions & Earning" class="card chat--card active">
			<div class="card__top">
				

				<div class="card__top__body">
					<h2 class="section__title">Subscriptions & Earning</h2>
					<p class="user__text text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, veniam.
					</p>
				</div>


				<div class="card__top__link">
					<button href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-square" viewBox="0 0 16 16">
							<path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
							<path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
						</svg>
					</button>
					
				</div>
			</div>
		</a>
	</section>
</main>