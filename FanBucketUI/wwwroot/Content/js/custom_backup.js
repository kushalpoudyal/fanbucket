

$(document).ready(function () {
	var fileName = '';
	var ext = '';
	$('#imgupload').change(function (event) {

		if (!event || !event.target || !event.target.files || event.target.files.length === 0) {
			return;
		}

		const name = event.target.files[0].name;
		const lastDot = name.lastIndexOf('.');

		fileName = name.substring(0, lastDot);
		ext = name.substring(lastDot + 1);



	});

	$('#uploadPost').click(function () {
		$('#imgupload').trigger('click');
	});
	$('#btnShare').click(function () {

		var inpt = $('').val();
		$("body").css("cursor", "wait");
		setTimeout(function () {
			if (ext === "mp4") {
				$('#feedPostvideo').show();
				$("body").css("cursor", "default");
			}
			else {
				$('#feedPost').show();
				$("body").css("cursor", "default");
			}
		}, 2000);

	});

	$('.accounts--top .carousel').slick({
		infinite: false,
		lazyLoad: 'ondemand',
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		autoplay: false,
		dots: false,
		prevArrow: `<button class='slick-prev'><svg xmlns="http://www.w3.org/2000/svg" width="6.707" height="10.138" viewBox="0 0 6.707 10.138">
		<defs>
		
		</defs>
		<path id="Path_75" data-name="Path 75" d="M-6066.839-6256.145l-6.707-5.147,6.707-4.991Z" transform="translate(6073.546 6266.283)" fill="#d8d9db"/>
		</svg></button>`,
		nextArrow: `<button class='slick-next active'><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6.707" height="10.138" viewBox="0 0 6.707 10.138">
		<defs>
		<linearGradient id="linear-gradient" x1="0.564" y1="0.796" x2="-0.32" y2="0.212" gradientUnits="objectBoundingBox">
		<stop offset="0" stop-color="#d70153"/>
		<stop offset="1" stop-color="#fe4545"/>
		</linearGradient>
		</defs>
		<path id="Path_74" data-name="Path 74" d="M-6052.295-6266.283l6.707,5.147-6.707,4.991Z" transform="translate(6052.295 6266.283)" opacity="0.8" fill="url(#linear-gradient)"/>
		</svg>
		</button>`,
		responsive: [
			{
				breakpoint: 836,
				settings: {
					slidesToShow: 1.3,
				}
			},
			{
				breakpoint: 601,
				settings: {
					slidesToShow: 1.65,
					arrows: false,
				}
			},

		]
	});


	$('.accounts--top .carousel').on('afterChange', function (event, slick, currentSlide) {
		if (currentSlide > 0) {
			// $(this).find('.slick-next').removeClass('active');
			$(this).find('.slick-prev').addClass('active');
		} else {
			$(this).find('.slick-prev').removeClass('active');

		}

	});







});




// $("img").one("load", function() {
//    //do stuff
// }).each(function() {
// 	if(this.complete || /*for IE 10-*/ $(this).height() > 0)
// 		$(this).load();
// });






