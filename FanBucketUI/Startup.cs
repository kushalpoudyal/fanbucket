using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using FanBucketUI.API_Context;
using Microsoft.AspNetCore.Routing;
using FanBucketUI.Routing;
using Templates;
using Stripe;

namespace FanBucketUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages().AddRazorRuntimeCompilation();
            services.Configure<StripeSettings>(Configuration.GetSection("Stripe"));
            services.Configure<RouteOptions>(routeOptions =>
            {
                routeOptions.ConstraintMap.Add("UNC",
                typeof(UsernameConstraint));
            });
            //services.AddSignalR(o =>
            //{
               
            //    o.EnableDetailedErrors = true;
            //});
                services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(cookieOptions =>
            {
                cookieOptions.LoginPath = "/Login";
            });
            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);

            //services.AddSingleton<ChatHubService>();
            services.AddScoped<SessionHelper>();
            services.AddScoped<apiRequest>();
            services.AddScoped<SignalR>();
            services.AddScoped<HelperModel>();
            services.AddTransient<IRazorViewToStringRenderer, RazorViewToStringRenderer>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            StripeConfiguration.ApiKey = Configuration.GetSection("Stripe")["SecretKey"];
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                //endpoints.MapHub<ChatHub>("/chatHub");
            });
        }
    }
}
