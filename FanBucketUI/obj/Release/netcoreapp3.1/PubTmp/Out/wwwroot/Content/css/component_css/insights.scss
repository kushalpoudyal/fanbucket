@import "../custom";
@import "./component";

$primary-color: #fe4545;

.text--blue {
  color: #1287cd;
  fill: #1287cd;
}
.text--sucess {
  color: #12cd1e;
  fill: #12cd1e;
}

.text--danger {
  color: #cd3e12;
  fill: #cd3e12;
}

.text--prup {
  color: #8112cd;
  fill: #8112cd;
}

// background

.bg--blue {
  background-color: #1287cd;
}

.bg--sucess {
  background-color: #12b3cd;
}

.bg--danger {
  background-color: #cd3e12;
}

.bg--prup {
  background-color: #8112cd;
}

.para {
  color: rgba(35, 35, 35, 0.55);
}
.section__title__lg {
  font-size: 24px;
}
.card__margin {
  margin-bottom: 30px;
}

.insights--main {
  overflow-y: hidden;
  @include responsivemd {
    height: 100%;
    width: 100%;
    padding: 0 15px;
  }
  .feed {
    .chat--card {
      background-color: #fff !important;
      @include responsivexlg {
        font-size: 16px;
      }
      .section__title {
        margin-bottom: 0;
        @include responsivexlg {
          font-size: 20px;
        }
      }
      .card__top {
        .card__top__body {
          @include responsivexlg {
            padding: 0 5px;
          }
        }
        .card__top__link {
          svg {
            height: 25px;
            width: auto;
            fill: #b4b4b4;
          }
        }
      }
      &.active {
        background-color: #ffffff87 !important;
      }
    }
    .section__title {
      @include responsivemmd {
        display: block;
      }
    }
  }
}

.insights--aside {
  @include responsivemd {
    z-index: 1;
    height: 100%;
  }
  .chat--wrapper--top {
    .search-wrapper--main {
      @include responsivemd {
        padding: 15px;
        flex-wrap: wrap;
        justify-content: flex-start;
      }
    }
    .title__link {
      padding: 6px 18px;
      color: #565656;
      font-size: 15px;
      border-radius: 12px;
      @include responsivemd {
        font-size: 14px;
        padding: 6px;
      }
    }
    a.title__link {
      background-color: #f6f7f9;
      margin-right: 12px;
      b {
        font-weight: bold;
      }
    }
    div.title__link {
      // @include br;
      border: 1px solid #565656;
      display: flex;
      padding: 0px 10px;
      align-items: center;
      justify-content: center;
      select {
        border: 0;
        padding: 6px 8px;
        box-shadow: none !important;
        outline: none !important;
        font-weight: bold;
      }
      svg {
        margin-left: 3px;
      }
    }
  }

  .section__title {
    font-size: 20px;
  }

  .chart--card {
    .section__title {
      margin-bottom: 0;
    }
    .card__top {
      display: flex;
      border-bottom: 2px solid #eee;
      padding-bottom: 12px;
      button {
        background: none !important;
        color: $primary-color !important;
        opacity: 0.6;
        font-weight: bold;
        padding: 4px 0;
        font-size: 16px;
        svg {
          height: 18px !important;
          margin-left: 8px;
          fill: #bbb;
        }
      }
    }
    .chart__wrapper {
      .loader__wrapper {
        height: 170px;
        justify-content: center;
        opacity: 0.5;
      }
    }
    .para {
      color: rgba(35, 35, 35, 0.55) !important;
    }
    .card__body {
      display: flex;
      align-items: flex-end;
      @include responsivemd {
        flex-direction: column;
      }

      .card__body__left {
        flex-grow: 2;
        @include responsivemd {
          width: 100%;
        }
      }
      .card__body__right {
        width: 340px;
        min-width: 325px;
        padding-left: 25px;
        display: flex;
        flex-direction: column;
        justify-content: flex-end;
        height: 170px;
        @include responsivexlg {
          width: 240px;
          min-width: 240px;
          padding-left: 15px;
        }
        @include responsivemd {
          width: 100%;
          padding: 10px;
        }
        .point {
          float: right;
          color: #333;
        }
        ul {
          display: flex;
          flex-direction: column;
          justify-content: space-evenly;
          height: 100%;
        }
      }
    }

    svg {
      font-weight: 600;
      color: rgba(218, 218, 218, 0.3);
      overflow: visible;
      text {
        color: rgba(35, 35, 35, 0.3);
      }
    }

    rect.bar {
      transform: translateY(40px);
      transition: 0.3s ease;
    }

    .vertical-y text {
      transform: translateY(25px);
    }

    .line--hr,
    .line--vl {
      .domain,
      text {
        display: none;
      }
      // .tick {
      //   line {
      //     // color: rgba(218, 218, 218, 0.55);
      //   }
      // }
    }

    .line {
      fill: transparent !important;
      transition: 0.3s ease;
      &.text--blue {
        stroke: #1287cd;
      }
      &.text--sucess {
        stroke: #12cd1e;
      }
      &.text--danger {
        stroke: #cd3e12;
      }
      &.text--prup {
        stroke: #8112cd;
      }
    }
    .dot {
      opacity: 1;
    }
    .dotWrapper {
      circle:not(:last-child) {
        opacity: 0;
        &:hover {
          opacity: 1;
        }
      }
    }
    .bot {
      opacity: 0.3;
    }
  }
  .chat--wrapper__body {
    justify-content: flex-start;
    @include responsivemd {
      padding: 0;
    }
  }
  .chat--wrapper .chat--wrapper--top {
    padding: 20px 30px;
  }
}
