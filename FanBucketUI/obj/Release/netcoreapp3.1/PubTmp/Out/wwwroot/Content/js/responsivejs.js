$(document).ready(function(){
	if($(window).width() < 840){
		
		$('.image--multi__left').not('.card__img').contents().unwrap();
		$('.image--multi__left').removeClass('image--multi__left');
		

		$('.main .feed .image--multi').parent().append('<li class="image--multi__dots dark"></li>');
		

		$('.main .feed .image--multi').each((index, elem)=>{
			$(elem).siblings('.image--multi__dots').text("/ " + $(elem).children('.card__img').length)
			$(elem).children('.card__img').each(function (index, abc) {
				$(abc).append(`<p class="image--multi__dot dark">${index+1}</p>`)
			})
		})

		$(window).on('locationchange', function (event) {
			console.log('hi')
			if(window.location.hash != "#new--post") {
				// $('.modal--post').modal('hide');
			}
		});
		


		// cleaning shared image

		$('.feed .card.shared .card__img:not(:first-child)').remove();
		$('.feed .card.shared .image--multi').contents().unwrap();
		$('.feed .card.shared .image--multi').removeClass('image--multi__left');
		$('.feed .card.shared').each((index, elem)=>{
			let count = $(elem).find('.image--multi__dots').text().replace('/','+');
			$(elem).find('.image--multi__dots').remove();
			$(elem).find('.image--multi__dot').text(count);

		});


		const menuSlide = ()=>{
			$('.profile--aside--menu').toggleClass('active');
			$('body').toggleClass('overflow');
		}

		$('.hamburger-menu').click(function(e){
			e.stopPropagation()
			e.preventDefault();
			menuSlide();
		});

		$('.profile--aside--menu').click(function(e){
			menuSlide();
		})


		$('.profile--aside--menu.profile--right--aside .other--nav .list-group .list-group-item.level-1').click(function(e){
			e.stopPropagation();
			e.preventDefault();
			$('.list-group.level-2').removeClass('show"')
			$(this).next().toggleClass('show')
		});

		// darkmode and light
		const setCookie = (cname, cvalue, exdays) => {
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+ d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		// cookies light mode or dark
		const getCookie = (cname) => {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');
			for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		}

		let darkMode = getCookie('darkmode');
		const darModeElem = $('#darkmode-mob');
		if(darkMode){
			$('body').addClass('darkmode')
			darModeElem.addClass('active')
			darModeElem.find('i').addClass('bi-brightness-high')			
			text = darModeElem.html().replace('Dark', 'Light');
			darModeElem.html(text)
		}
		else{
			$('body').removeClass('darkmode')
		}




		darModeElem.click(function(e){
			e.preventDefault();
			let text;
			if($(this).hasClass('active')){
				$('body').removeClass('darkmode')
				$(this).removeClass('active')
				$(this).find('i').removeClass('bi-brightness-high');
				text = $(this).html().replace('Light','Dark' );
				setCookie('darkmode', false, 0)
			}else{
				$('body').addClass('darkmode')
				$(this).addClass('active')
				$(this).find('i').addClass('bi-brightness-high')			
				text = $(this).html().replace('Dark', 'Light');
				setCookie('darkmode', true, 30)
			}

			$(this).html(text)
		})

		$('#message--new').on('show.bs.modal', function () {
			$('form #searchFriend').first().focus();
		});
		$('form #searchFriend').keyup(function(e){
			$(this).val() ? $(this).parents('.modal-content').find('.modal-body h2.section__title').text('Result') : $(this).parents('.modal-content').find('.modal-body h2.section__title').text('Online')

		})

		$('#ott--modal').on('show.bs.modal', function (event) {
			$('#ott--modal form input:first-child').focus();
		});

		$(".otp input").keyup(function (event) {
			debugger;
			let key = event.keyCode || event.charCode;
			if(!( key == 8 || key == 46) ){
				$(this).next().focus();
			}
			else{
				$(this).prev().focus();
			}

		});

		$('.notification--main .image--multi__dots').text().replace('/','+')
	}
})