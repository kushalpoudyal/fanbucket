$(document).ready(function () {
    // cookies light mode or dark
    const getCookie = (cname) => {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " ") {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };

    let darkMode = getCookie("darkmode");
    // console.log('darkmode',darkmode)
    if (darkMode) {
        $("body").addClass("darkmode");
    } else {
        $("body").removeClass("darkmode");
    }

    //$("header .dropdown").hover(
    //    function () {
    //        $(this).find(".dropdown-menu").addClass("show");
    //    },
    //    function () {
    //        $(this).find(".dropdown-menu").removeClass("show");
    //    }
    //);

    // function isTouchDevice(){
    // 	return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
    // }

     //if(isTouchDevice()===false && (window.innerWidth > 840 )) {
     if( window.innerWidth > 840 ) {
     	$('[data-toggle="tooltip"]').tooltip();
     }
    const currentLocation = location.href;
    const menuItem = $("header .navbar .nav__item");

    if (
        currentLocation === "http://localhost/fanbucket/" ||
        currentLocation === "http://sunbi.live/projects/ap/fanbucket/"
    ) {
        menuItem[0].children[0].classList.add("active");
    }
    const menuLength = menuItem.length;

    for (let i = 0; i < menuLength; i++) {
        if (menuItem[i].children[0].href === currentLocation) {
            menuItem[i].children[0].classList.add("active");
        }
    }

    $(".love").click(function (e) {
        e.preventDefault();
        $(this).toggleClass("active");
    });

    $(".bookmark").click(function (e) {
        e.preventDefault();
        $(this).toggleClass("active");
        if ($(this).hasClass("active")) {
            //do this
        } else {
            // do this one
        }
    });

    //

    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight, navbar;
    if ($(window).width() > 601) {
        navbar = $(".post");
    } else {
        navbar = $(".mob--header");
        $('input:not([type="checkbox"]):not([type="radio"])')
            .focusin(function () {
                $(".header--primary").addClass("relative");
            })
            .focusout(function () {
                $(".header--primary").removeClass("relative");
            });
    }

    navbarHeight = navbar.outerHeight() + 50;

    $(window).scroll(function (event) {
        didScroll = true;
    });

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta) return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.

        if ((st > lastScrollTop && st > navbarHeight) || st < navbarHeight) {
            // Scroll Down
            navbar.removeClass("active");
        } else {
            // Scroll Up
            if (st + $(window).height() < $(document).height()) {
                navbar.addClass("active");
            }
        }

        lastScrollTop = st;
    }

    $("a.comments").click(function (e) {
        $(this).parents(".card").find(".card__comments").slideDown(300);
        $(this)
            .parents(".card")
            .find(".card__comments input.comment__text")
            .focus();
        // $(this).parents('.card').find('.card__comments').show()
    });

    $(".moment .carousel").slick({
        infinite: false,
        lazyLoad: "ondemand",
        slidesToShow: 2.7,
        slidesToScroll: 1,
        arrows: true,
        autoplay: false,
        swipeToSlide: true,
        dots: false,
        prevArrow: `<button class='slick-prev'><svg xmlns="http://www.w3.org/2000/svg" width="6.707" height="10.138" viewBox="0 0 6.707 10.138">
	<path id="Path_75" data-name="Path 75" d="M-6066.839-6256.145l-6.707-5.147,6.707-4.991Z" transform="translate(6073.546 6266.283)" fill="#d8d9db"/>
	</svg></button>`,
        nextArrow: `<button class='slick-next active'><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6.707" height="10.138" viewBox="0 0 6.707 10.138">
	<defs>
	<linearGradient id="linear-gradient" x1="0.564" y1="0.796" x2="-0.32" y2="0.212" gradientUnits="objectBoundingBox">
	<stop offset="0" stop-color="#d70153"/>
	<stop offset="1" stop-color="#fe4545"/>
	</linearGradient>
	</defs>
	<path id="Path_74" data-name="Path 74" d="M-6052.295-6266.283l6.707,5.147-6.707,4.991Z" transform="translate(6052.295 6266.283)"  fill="#d8d9db"/>
	</svg>
	</button>`,
        responsive: [
            {
                breakpoint: 836,
                settings: {
                    slidesToShow: 1.3,
                },
            },
            {
                breakpoint: 601,
                settings: {
                    swipeToSlide: true,
                    slidesToShow: 1.75,
                    arrows: false,
                    swipeToSlide: true,
                    touchThreshold: 10,
                },
            },
        ],
    });

    // search
    $(".form input[type=search]").keyup(function (e) {
        if ($(this).val()) {
            $(this).next().addClass("active");
            $(this).next().next().addClass("active");
            $(this).parents("form").find(".searchList").show(300);
        } else {
            $(this).next().removeClass("active");
            $(this).next().next().removeClass("active");
            $(this).parents("form").find(".searchList").hide(300);
        }
    });

    $("form .cross").click(function (e) {
        let parent = $(this).parent().find('input[type="search"]');
        $(this).removeClass("active");
        $(this).prev().removeClass("active");
        $(this).parents("form").find(".searchList").hide(300);

        parent.val(null);
        parent.focus();
    });

    $("form .searchList .copy--text").click(function (e) {
        e.preventDefault();
        let elemVal = $(this).text();
        $(this).parents("form").find('input[type="search"]').val(elemVal);
        $(this).parents(".searchList").hide("300");
    });

    $(".post input").focus(function () {
        $(this).parents(".profile--card").addClass("active");
    });

    $(".post input").blur(function () {
        $(this).parents(".profile--card").removeClass("active");
    });

    // logout modal
    $("#modal--logout").on("shown.bs.modal", function () {
        $(this).find("button:last-child").trigger("focus");
    });

    // post modal
    // modal post things

    $(".modal--post").on("shown.bs.modal", function () {
        $(this).find("form textarea").trigger("focus");
        window.location.hash = "#new--post";
    });

    //$(".modal--post form").submit(function (e) {
    //    e.preventDefault();
    //    let hideElem = $(this).find(".form__wrapper");
    //    let showElem = $(this).next(".loader__wrapper");
    //    let modalParent = $(this).parents(".modal");
    //    hideElem.hide(400);
    //    showElem.addClass("active");

    //    setTimeout(function (elem) {
    //        modalParent.modal("hide");
    //        hideElem.show("slow");
    //        showElem.removeClass("active");
    //    }, 5000);
    //});


    // date fail
    let today = new Date();
    let nextMonth;
    let dd = String(today.getDate()).padStart(2, "0");
    let mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    let yyyy = today.getFullYear();
    let totalDay = new Date(yyyy, mm, 0).getDate();

    today =
        dd == totalDay
            ? yyyy + "-" + (parseInt(mm) + 1) + "-" + dd
            : yyyy + "-" + mm + "-" + dd;

    if (nextMonth == 12) {
        nextMonth = parseInt(yyyy) + 1 + "-" + mm + "-" + dd;
    } else {
        nextMonth =
            yyyy + "-" + String(parseInt(mm) + 1).padStart(2, "0") + "-" + dd;
    }

    tail.DateTime(".tail-datetime-field", {
        animate: true,
        static: null,
        position: "top",
        classNames: "",
        dateFormat: "YYYY-mm-dd",
        timeFormat: "HH:ii",
        dateStart: today,
        dateEnd: nextMonth,
        weekStart: "MON",
        startOpen: false,
        stayOpen: false,
        today: false,
        timeSeconds: false,
        time12h: true,
        // timeIncrement: true,
        timeIncrement: false,
    });

    $(".timepicker-field.timepicker-seconds").remove();

    $("picker-step.step-up").off("mousedown");

    const calenderFull = $(".datetime-field.tail-datetime-field");
    const calenderFullShow = $("#post--date--value");
    const sheduleLink = $(".shedule--link");
    calenderFullShow.val(null);

    $("#reset").click(function (e) {
        $(this).prev().remove();
        $(this).addClass("hide");
        sheduleLink.removeClass("active");
        $('#post--modal form button[type="submit"]').text("post");
    });

    const calenderFullChange = () => {
        let elemVal = new Date(calenderFull.val());
        let JsonVal = new Date(calenderFull.val()).toJSON();
        let elemYYYY, elemMM, elemDD, elemHH, elemMm, elemDate;

        if (elemVal) {
            elemYYYY = elemVal.getFullYear();
            elemMM = elemVal.toLocaleString("default", { month: "short" });
            elemDD = elemVal.getDate();
            elemHH = elemVal.getHours();
            elemMm = elemVal.getMinutes();

            elemDate =
                elemMM + " " + elemDD + " " + elemYYYY + " | " + elemHH + ":" + elemMm;
            debugger;
            calenderFullShow.find("time").remove();
            calenderFullShow.find("#ScheduledFor").remove();
            calenderFullShow.prepend(`<time> ${elemDate} </time>`);
            calenderFullShow.prepend(`<input type="hidden" id="ScheduledFor" value=" ${JsonVal}"/>`);
            $("#reset").removeClass("hide");
            sheduleLink.addClass("active");
            $('#post--modal form button[type="submit"]').text("schedule");



            //calenderFullShow.find("time").remove();
            //calenderFullShow.prepend(`<time> ${elemDate} </time>`);
            //$("#reset").removeClass("hide");
            //sheduleLink.addClass("active");
            //$('#post--modal form button[type="submit"]').text("schedule");
        }
    };

    calenderFull.change(function () {
        calenderFullChange();
    });


    const progress = $("#post--modal .progress-bar");
    const progressInput = $('#post--modal textarea[name="PostTitle"]');
    const maxLength = progressInput.attr("maxlength");

    const progressCalc = (e) => {
        let length = progressInput.val().length;
        let width = Math.floor((length * 100) / maxLength);
        progress.css("width", width + "%");
        $("#my-post--value").val(progressInput.val().replace(/\s/gm, " "));
    };

    // progressInput.keyup(function(e){
    // 	progressCalc();
    // });
    progressInput.bind("keyup keypress blur change focus", function (e) {
        progressCalc();
    });

    //$(document).on("click", ".modal--post .lock", function (e) {
    //    $(this).toggleClass("unlocked");
    //});

    $(document).on("click", "#post--modal .trash", function (e) {
        e.preventDefault();
        let parent = $(this).parents(".image");
        let elemParent = $(this).parents(".modal--post__ph--upload");
        parent.remove();
        if (elemParent.children().length <= 1) {
            elemParent.find(".image.empty--input").addClass("hide");
        }
    });

    $(document).on(
        "change",
        "#post--modal input[name=uploadedImage]",
        function (e) {
            let input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).prev().attr("src", e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    );

    $(".post__card__link__input").change(function (e) {
        let timestamp = e.timeStamp;
        uploadImage(this, "#post--modal .modal--post__ph--upload", timestamp);
    });

    const uploadImage = function (input, imageAppend, timestamp) {
        debugger;
        if (input.files) {
            let filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                timestamp++;

                var reader = new FileReader();
                reader.onload = (function (input) {
                    return function (e) {
                        result = e.target.result;
                        if (input.type === "video/webm" || input.type === "video/mp4") {
                            // result = capture(result);
                            result = "./gallery";
                        }

                        let paidElem = $("#public--or--paid")[0].selectedIndex;

                        let image = `<div class="image uploading">
					<img src="${result}" alt="">
					<input type="file" accept="image/*,image/heif,image/heic,video/*,video/mp4,video/x-m4v,video/x-matroska,.mkv" class="input__hide" name="uploadedImage" id="uploadedImage__${timestamp}" value="${result}">
					<a href="#!" class="lock  ${paidElem ? " " : " unlocked "}">
					<svg xmlns="http://www.w3.org/2000/svg" width="502.571" height="562.771" viewBox="0 0 502.571 562.771">
					<path id="Subtraction_1" data-name="Subtraction 1" d="M-9212.429,706.77H-9715V347.906h47.059a205.856,205.856,0,0,1,4.235-41.035,203.366,203.366,0,0,1,11.938-38.224,204.484,204.484,0,0,1,18.827-34.6,205.982,205.982,0,0,1,24.9-30.155,206.034,206.034,0,0,1,30.159-24.9,204.617,204.617,0,0,1,34.6-18.825,203.485,203.485,0,0,1,38.234-11.938A205.907,205.907,0,0,1-9463.993,144a205.7,205.7,0,0,1,42.227,4.485,203.391,203.391,0,0,1,39.218,12.63A204.665,204.665,0,0,1-9347.23,181a206.061,206.061,0,0,1,30.527,26.251l-31.746,27.321a161.253,161.253,0,0,0-115.845-48.8c-89.36.085-162.129,72.816-162.214,162.129l324.388.044c0-.195,0-.388,0-.582v-.023h42.033v.025c0,.177,0,.359,0,.536h47.658V706.769Zm-251.865-221.811a46.223,46.223,0,0,0-46.156,46.071,45.909,45.909,0,0,0,28.683,42.592v56.721h35.036V573.715l-.047.019-.041.017.088-.129v.093a45.931,45.931,0,0,0,28.509-42.685A46.123,46.123,0,0,0-9464.294,484.959Z" transform="translate(9715 -143.999)" fill="#fff"/>

					<path id="Subtraction_2" data-name="Subtraction 1" d="M-9212.429,706.77H-9715V347.906h47.059a205.856,205.856,0,0,1,4.235-41.035,203.366,203.366,0,0,1,11.938-38.224,204.484,204.484,0,0,1,18.827-34.6,205.982,205.982,0,0,1,24.9-30.155,206.034,206.034,0,0,1,30.159-24.9,204.617,204.617,0,0,1,34.6-18.825,203.485,203.485,0,0,1,38.234-11.938A205.907,205.907,0,0,1-9463.993,144a205.8,205.8,0,0,1,40.95,4.217,203.406,203.406,0,0,1,38.152,11.888,204.534,204.534,0,0,1,34.545,18.749,206.089,206.089,0,0,1,30.127,24.8,206.05,206.05,0,0,1,24.9,30.042,204.534,204.534,0,0,1,18.86,34.474,203.365,203.365,0,0,1,12.014,38.1,205.649,205.649,0,0,1,4.356,40.909l-42.033.031c-.482-88.885-73.231-161.3-162.171-161.43-89.36.085-162.129,72.816-162.214,162.129l324.388.044c0-.195,0-.388,0-.582v-.023h42.033v.025c0,.177,0,.359,0,.536h47.658V706.769Zm-251.865-221.811a46.223,46.223,0,0,0-46.156,46.071,45.909,45.909,0,0,0,28.683,42.592v56.721h35.036V573.715l-.047.019-.041.017.088-.129v.093a45.931,45.931,0,0,0,28.509-42.685A46.123,46.123,0,0,0-9464.294,484.959Z" transform="translate(9715 -143.999)" fill="#fff"/>


					</svg>
					</a>
					<div class="edit--wrapper">
					<label for="uploadedImage__${timestamp}">change</label>
					<a href="#" class="trash"><i class="bi bi-trash"></i></a>
					</div>
					<div class="loader__wrapper">
					<div class="loader">
					<div class="box">
					<img src="./gallery/logoWhite.png" alt="fanbucket">
					</div>
					<div class="shadow"></div>
					</div>
					</div>
					</div>`;

                        $(imageAppend).append(image);

                        setTimeout(function (e) {
                            $(imageAppend).find(".uploading").removeClass("uploading");
                        }, 3000);
                    };
                })(input.files[i]);

                $(".image.empty--input.hide").removeClass("hide");
                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    //$(".feed .dropdown").on("click", ".dropdown-toggle", function (e) {
    //    e.preventDefault();
    //    // $(this).parent().addClass("show");
    //    // $(this).attr("aria-expanded", "true");
    //    // $(this).next().addClass("show");
    //});

    $("button.follow").click(function (e) {
        debugger;
        e.preventDefault();
        $(this).append(`<img src="gallery/rings.svg" alt="" >`);
        $(this).toggleClass("active");

        let elem = $(this);
        $(".toast").toast("show");

        if ($(this).hasClass("active")) {
            $(this).text("follow");
        } else {
            setTimeout(function (e) {
                // console.log($(this).find('img'))
                // $(this).find('img').remove();
                // console.log(elem)
                elem.text("following");
            }, 1500);
        }
    });

    //$(".toast").toast({
    //    delay: 5000,
    //    // hide:'false'
    //});

    // modal fullImage
    const fullImageImg = $("#fullImage--modal .aside__left .card__img img");

    function imageZoom(elem) {
        elem.removeClass("active");
    }

    $(".feed article .card__img:not(.locked)").click(function (e) {
        debugger;
        e.preventDefault();
        let elem = $(this);
        let elemImage = elem.find("img").attr("src");
        fullImageImg.attr("src", elemImage);
        $(this).addClass("active");
        setTimeout(() => {
            imageZoom(elem);
        }, 300);

        $("#fullImage--modal").modal("show");
    });

    $("#fullImage--modal").on("show.bs.modal", function (event) {
        let elem = $("#fullImage--modal .aside__left .card__img");
        // console.log(elem)
        elem.addClass("active");
        setTimeout(() => {
            imageZoom(elem);
        }, 500);
    });

    let square = $("main.main .feed .image--multi");

    if (square) {
        square.each(function (i, elem) {
            $(elem).css("height", elem.clientWidth + "px");
        });
    }

    // poll added
    //const pollWrapper = $(".modal--post  .poll__wrapper .poll--inside");
    //let elem = $("#poll--inside--count");
    //elem.val(pollWrapper[0].childElementCount - 1);

    //// function remove cross if there is only two list item
    //const crossCheck = () => {
    //  // console.log(pollWrapper[0].childElementCount)
    //  if (pollWrapper[0].childElementCount > 2) {
    //    pollWrapper.removeClass("removeCross");
    //  } else {
    //    pollWrapper.addClass("removeCross");
    //  }
    //};
    //crossCheck();
    //$(".modal--post .poll__wrapper .poll--buttom button.poll--add").click(
    //  function (e) {
    //    debugger;
    //    e.preventDefault();

    //    // init
    //    let pollMax = elem[0].max;

    //    // universal input value
    //    let pollCount = parseInt(elem.val());

    //    // for placeholder option 1234 ..
    //    let placeholderCount = pollCount + 1;
    //    pollWrapper.removeClass("removeCross");

    //    if (pollCount < pollMax) {
    //      // clone template of poll and its child
    //      let pollTemplate = pollWrapper
    //        .find(".custom-control:last-child")
    //        .clone();
    //      let pollTemplateInput = pollTemplate.find("input");
    //      pollTemplateInput.val("");

    //      // grab recent id
    //      let elemId = pollTemplateInput.attr("id");

    //      // set new id for template using replace
    //      elemId = elemId.replace(`_${pollCount}`, `_${++pollCount}`);

    //      // grab recent placeholder and replace bew one
    //      let newPlaceholder = pollTemplateInput
    //        .attr("placeholder")
    //        .replace(
    //          `Option ${placeholderCount}`,
    //          `Option ${++placeholderCount}`
    //        );

    //      // add new placeholder and id from above
    //      pollTemplateInput.attr("placeholder", newPlaceholder);
    //      pollTemplateInput.attr("id", elemId);

    //      // set elem value
    //      elem.val(pollCount);

    //      // after all these finally appen the poll template phew
    //      pollTemplate.appendTo(pollWrapper);
    //      pollTemplateInput.focus();
    //      if (pollCount >= pollMax) {
    //        $(this).hide();
    //      }
    //    } else {
    //      $(this).hide();
    //    }
    //  }
    //);

    //$(".modal--post .poll__wrapper .poll--inside").on(
    //  "click",
    //  "a.count",
    //  function (e) {
    //    debugger;

    //    let elemC = $(this);

    //    let elemParents = elemC.parents(".custom-control");
    //    let elemIndex = elemParents.index();
    //    // let placeholderCount = elemIndex + 2;
    //    let elemVal = parseInt(elem.val());

    //    elem.val(--elemVal);

    //    elemParents.nextAll().each((index, elem) => {
    //      let elemCC = $(elem).find("input");
    //      // console.log(elemCC)

    //      // grab recent id
    //      let elemId = elemCC.attr("id");

    //      // set new id for template using replace
    //      elemId = elemId.replace(
    //        `_${elemIndex + index + 1}`,
    //        `_${elemIndex + index}`
    //      );

    //      // grab recent placeholder and replace bew one
    //      let newPlaceholder = elemCC
    //        .attr("placeholder")
    //        .replace(
    //          `Option ${elemIndex + index + 2}`,
    //          `Option ${elemIndex + index + 1}`
    //        );

    //      // add new placeholder and id from above
    //      elemCC.attr("placeholder", newPlaceholder);
    //      elemCC.attr("id", elemId);
    //    });
    //    elemParents.hide(300);
    //    elemParents.remove();
    //    $(".modal--post .poll__wrapper .poll--buttom button.poll--add").show();
    //    crossCheck();
    //  }
    //);

    //$(".modal--post .poll--trash").click(function (e) {
    //  debugger;
    //  $(".modal--post .poll__wrapper").toggle(300);
    //});

    // https://emoji-api.com/emojis?access_key=931e6b774777a7f37480afd20a0b8ad217bebcf7

    $.ajax({
        type: "GET",
        url: "/Content/html/emoji.html",
        async: true,
    }).then(function (data) {
        $("#emoji__wrapper .loader").hide();
        $("#emoji__wrapper ul").html(data);

    });

    $("#emoji__wrapper").on("click", "li", function (e) {
        e.stopPropagation();
        debugger;
        progressInput.val(progressInput.val() + $(this).text());
        progressInput.focus();
    });

    // modal -opt .js
    $("#ott--modal form").submit(function (e) {
        debugger;
        e.preventDefault();
        let parent = $(this).parents(".modal-body");
        parent.addClass("active");
        parent.find(".sucess--part").addClass("active");
    });

    $(".close-btn").click(function (e) {
        debugger;
        let parent = $(this).parents(".modal-body");
        setTimeout(() => {
            parent.removeClass("active");
            parent.find(".sucess--part").removeClass("active");
        }, 1000);
    });

    // moments modal
    $("#moments--modal").on("shown.bs.modal", function () {
        $(".modal-backdrop").addClass("dark");
    });

    $("#moments--modal").on("hidden.bs.modal", function (event) {
        $(".modal-backdrop").removeClass("dark");
    });

    $(".moments--modal .play").click(function (e) {
        $(this).find("i").toggleClass("bi-play-fill");
        $(this)
            .parents(".modal-content")
            .find(".progress-bar")
            .toggleClass("paused");
    });

    $(".moments--modal .progress--wrapper .progress-bar").one(
        "webkitAnimationEnd oanimationend msAnimationEnd animationend",
        function (e) {
            debugger;
            let parent = $(this).parent();
            parent.removeClass("active");
            parent.next().addClass("active");
        }
    );
    //});


});

/// Password
// The possible validation methods are:
//     minLength <int> - If the password is below this value
//     maxLength <int> - If the password is above this value
//     regex <RegExp>  - If the password does not match this regular expression

// All validation methods support an ErrorMessage parameter.

var passwordValidDefinition =
    [
        {
            minLength: 6,
            ErrorMessage: "Your password must be at least six characters long.",
        },
        {
            maxLength: 50,
            ErrorMessage: "Your password cannot be longer than 50 characters.",
        },
        {
            regex: /.*\d/,
            ErrorMessage: "Your password must contain at least one digit.",
        },
        {
            regex: /.*[a-z]/,
            ErrorMessage: "Your password must contain at least one lowercase letter.",
        },
        {
            regex: /.*[A-Z]/,
            ErrorMessage: "Your password must contain at least one uppercase letter.",
        },
        {
            regex: /.*[!@#$%^&*() =+_-]/,
            ErrorMessage: "Your password must contain at least one symbol in this list !@#$%^&*()=+_- or a space.",
        },
    ];

/**
* Validate the passed password (and confirm Password for comparison) using the passed validator list.
* The function will add any error messages it generates to the errorList parameter.
*
* @param {string} password - The password to validate.
* @param {string} confirmPassword - The confirmation password (which must match the password, or an error is added to errorList).
* @param {Object[]} passwordValidators - An array of validators. Each validator object has a test type and an error message when the test fails.
* @param {string[]} errorList - An array to which we will append any validation failure text (provided on each validator).
*
* @return {bool} true if the password passed all of the validator tests.
*/
function ValidatePassword(password, confirmPassword, passwordValidators, errorList) {
    var errors = [];

    if (password !== confirmPassword) {
        errors.push("The confirmation password does not match.")
        return false;
    }

    for (var i = 0; i < passwordValidators.length; i++) {
        var validator = passwordValidators[i];
        var valid = true;

        if (validator.hasOwnProperty("regex")) {
            if (password.search(validator.regex) < 0) valid = false;

        }

        if (validator.hasOwnProperty("minLength")) {
            if (password.length < validator.minLength) valid = false;
        }

        if (validator.hasOwnProperty("maxLength")) {
            if (password.length > validator.maxLength) valid = false;
        }

        if (!valid) {
            errors.push(validator.ErrorMessage);
            break;
        }
    }

    if (errors.length > 0) {
        errorList.push(...errors);

        // Use this instead if you need to support Internet Explorer or Opera
        //Array.prototype.push.apply(errorList, errors);

        return false;
    }

    return true;
}


function IsValidatePassword(password, confirmPassword, displayErrorSelector) {
    if (typeof confirmPassword === "undefined") confirmPassword = password;

    var errorList = [];

    //var textNode = document.createTextNode('Test using "' + password + '", "' + confirmPassword + '"');

    var valid = ValidatePassword(password, confirmPassword, passwordValidDefinition, errorList);
    jQuery(displayErrorSelector).empty();

    if (valid) {

        //    jQuery("#pass_error").append('<hr/>').append(textNode).append(' - VALID');
    }
    else {
        var $unorderedList = $('<ul />');
        $.each(errorList, i => $('<li/>').text(errorList[i]).appendTo($unorderedList));
        jQuery(displayErrorSelector).append($unorderedList);
    }

    return valid;
}

///


// common vanilla js
window.addEventListener("DOMContentLoaded", (event) => {
    // audio element
    //let audioControl = [];
    //let xhr = { cache: "default", mode: "no-cors" };
    //[...document.querySelectorAll(".wave--wrapper")].map((elem) => {
    //    let selector = elem.getAttribute("id");

    //    window[selector].destroy();
    //});
    //[...document.querySelectorAll(".wave--wrapper")].map((elem) => {
    //    let selector = elem.getAttribute("id");
    //    audioControl.push(selector);
    //    let src = elem.getAttribute("src");
    //    window[selector] = WaveSurfer.create({
    //        container: document.querySelector(`#${selector}`),
    //        waveColor: "#facfd3",
    //        progressColor: "#fe4545",
    //        cursorColor: "#fe4545",
    //        barWidth: 3,
    //        barRadius: 3,
    //        cursorWidth: 3,
    //        height: 200,
    //        barGap: 3,
    //        backend: "MediaElement",
    //        xhr: xhr,
    //        // mediaControls:true,
    //    });

    //    if (window.innerWidth < 840) {
    //        window[selector].setHeight(50);
    //    }
    //    window[selector].load(src);
    //    elem.previousElementSibling.addEventListener("click", (e) => {
    //        e.preventDefault();
    //        debugger;
    //        audioControl.map((elem) => {
    //            if (elem != selector) {
    //                window[elem].pause();
    //            }
    //        });
    //        window[selector].playPause();
    //    });

    //    window[selector].on("play", function () {
    //        debugger;
    //        // active means song is playing
    //        elem.previousElementSibling.classList.add("active");
    //        elem.previousElementSibling
    //            .querySelector("i")
    //            .classList.remove("bi-play-fill");
    //        elem.previousElementSibling.querySelector("i").classList.add("bi-pause");
    //    });
    //    window[selector].on("pause", function () {
    //        debugger;
    //        elem.previousElementSibling.classList.remove("active");
    //        elem.previousElementSibling
    //            .querySelector("i")
    //            .classList.add("bi-play-fill");
    //        elem.previousElementSibling
    //            .querySelector("i")
    //            .classList.remove("bi-pause");
    //    });
    //});
    //var medias = Array.prototype.slice.apply(document.querySelectorAll('audio,video'));
    //medias.forEach(function (media) {
    //    media.addEventListener('play', function (event) {
    //        medias.forEach(function (media) {
    //            if (event.target != media) media.pause();
    //        });
    //    });
    //});
    //tenor gif api key Q8XZ0A0EMFE7
    const apikey = "LIVDSRZULELA";
    const lmt = 8;
    const gifWrapper = document.getElementById("emoji__wrapper");

    // url Async requesting function
    function httpGetAsync(theUrl, callback) {
        // create the request object
        var xmlHttp = new XMLHttpRequest();

        // set the state change callback to capture when the response comes in
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                callback(xmlHttp.responseText);
            }
        };

        // open as a GET call, pass in the url and set async = True
        xmlHttp.open("GET", theUrl, true);

        // call send with no params as they were passed in on the url string
        xmlHttp.send(null);

        return;
    }

    // callback for the top 8 GIFs of search
    function tenorCallback_search(responsetext) {
        // parse the json response

        var response_objects = JSON.parse(responsetext);
        top_10_gifs = response_objects["results"];
        if (top_10_gifs.length) {
            gifWrapper.querySelector(".grid").innerHTML = "";
        }
        top_10_gifs.forEach((elem) => {
            let div = document.createElement("div");
            let img = document.createElement("img");
            img.setAttribute("src", elem?.media[0]?.gif.url);
            //img.setAttribute("src", elem?.url);
            img.setAttribute("title", elem?.content_description || "title");

            div.appendChild(img);
            gifWrapper.querySelector(".grid").appendChild(div);
        });

        return;
    }

    // function to call the trending and category endpoints
    function grab_data(search_term) {
        // set the apikey and limit

        // test search term
        // using default locale of en_US

        var search_url =
            "https://g.tenor.com/v1/search?q=" +
            search_term +
            "&key=" +
            apikey +
            "&limit=" +
            lmt;

        httpGetAsync(search_url, tenorCallback_search);

        // data will be loaded by each call's callback
        return;
    }

    // SUPPORT FUNCTIONS ABOVE
    // MAIN BELOW
    grab_data("hello");
    // start the flow
    document.getElementById("searchGif").addEventListener("keyup", (e) => {
        gifWrapper.querySelector(".grid").innerHTML = "";
        if (e.target.value) {
            for (let i = 0; i <= 3; i++) {
                let div = document.createElement("div");
                div.setAttribute("class", "loader");
                gifWrapper.querySelector(".grid").appendChild(div);
            }
            grab_data(e.target.value);
        }
    });

    // on clicking on gif, add its url to input value in jquery at line 720
});