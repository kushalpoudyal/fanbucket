	window.addEventListener('DOMContentLoaded', (event) => {
		let data;
		let margin = {top: 20, right: 20, bottom: 30, left: 40},
		height = 170 - margin.top - margin.bottom;
		
		async function postData(url = '', ) {

			const response = await fetch(url, {
				method: 'GET',
				mode: 'cors', 
				cache: 'no-cache', 
				credentials: 'same-origin', 
				headers: {
					'Content-Type': 'application/json'

				},
				redirect: 'follow', 
				referrerPolicy: 'no-referrer', 
				body: JSON.stringify(data) 
			});
			return response.json(); 
		}

		postData('/Content/json/insights-subcriber.json')
		.then(elemData => {
			let data = elemData;
			let elem = "#subcribe--svg";
			document.querySelector(elem + " .loading").remove();
			drawSubcribe(data, elem);
		});

		postData('/Content/json/insights-locked-post.json')
		.then(elemData => {
			data = elemData;
			let elem = "#en--contacta--svg";
			color = 'text--sucess'
			document.querySelector(elem + " .loading").remove();
			drawLockedPost(data, elem, color);
		});

		postData('/Content/json/insights-locked-post.json')
		.then(elemData => {
			data = elemData;
			let elem = "#en--profile--view--svg";
			color = 'text--blue'
			document.querySelector(elem + " .loading").remove();
			drawLockedPost(data, elem ,color);
		});

		postData('/Content/json/insights-locked-post.json')
		.then(elemData => {
			data = elemData;
			let elem = "#en--likes--svg";
			color = 'text--prup'
			document.querySelector(elem + " .loading").remove();
			drawLockedPost(data, elem ,color);
		});




		

		
		const drawSubcribe = (data, elem)=>{
			// set the dimensions and margins of the graph


			let width = document.querySelector(elem).getBoundingClientRect().width - margin.left - margin.right;
			// set the ranges
			var y = d3.scaleBand()
			.range([height, 0])
			.padding(0.1);
			var x = d3.scaleLinear()
			.range([0, width]);

			// append the svg object to the body of the page
			// append a 'group' element to 'svg'
			// moves the 'group' element to the top left margin
			var svg = d3.select(elem).append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", 
				"translate(" + margin.left + "," + margin.top + ")");

		  // format the data
		  data.forEach(function(d) {
		  	d.sales = +d.sales;
		  });

		  // Scale the range of the data in the domains
		  x.domain([0, d3.max(data, function(d){ return d.value; }) * 1.5 + 30])
		  y.domain(data.map(function(d) { return d.title; }));
		  //y.domain([0, d3.max(data, function(d) { return d.sales; })]);

		  svg.append("g")
		  .attr("transform", "translate(0,-10)")
		  .attr('height',height )
		  .attr('class','line--hr')
		  .call(d3.axisTop(x));

		  svg.append("g")
		  .attr("transform", "translate(0,-10)")
		  .attr('width',width)
		  .attr('class','line--vl')
		  .call(d3.axisLeft(y));

		  // append the rectangles for the bar chart
		  svg.selectAll(".bar")
		  .data(data)
		  .enter().append("rect")
		  .attr("class", "bar text--blue") //color change class here
 	      //.attr("x", function(d) { return x(d.sales); })
 	      .attr("y", function(d) { return y(d.title); })
 	      .attr("height", 6)
 	      .transition()
 	      .duration(300)
 	      .ease(d3.easeLinear)
 	      .attr("width", function(d) {return x(d.value); } );
 	      

		  // add the x Axis
		  svg.append("g")
		  .attr("transform", "translate(0," + (height + 8) + ")")
		  .attr("class", "horizontal-x")
		  .call(d3.axisBottom(x));

		  // add the y Axis
		  svg.append("g")
		  .attr("class", "vertical-y")
		  .call(d3.axisLeft(y));


		  

		  document.querySelectorAll('.chart--card .line--hr .tick line').forEach((elem, index)=>{
		  	elem.setAttribute('y2', height + 10);
		  })

		  document.querySelectorAll('.chart--card .line--vl .tick line').forEach((elem, index)=>{
		  	elem.setAttribute('x2', width + 10);
		  })
		}



		// draw locked
		
		const drawLockedPost = (data, elem, color) =>{
			
			const	width = document.querySelector(elem).getBoundingClientRect().width - margin.left - margin.right;

			// The number of datapoints
			var n = 21;

			// 5. X scale will use the index of our data
			var yScale = d3.scaleLinear()
			.domain([0, d3.max(data, function(d){ return d.value; }) * 2])
			.range([height, 0]);


			// 6. Y scale will use the randomly generate number 
			let xScale = d3.scaleLinear()
    			.domain([0, data.length-1]) // input 
    			.range([0, width]);  			

			// 7. d3's line generator
			var line = d3.line()
		    .x(function(d, i) { return xScale(i); }) // set the x values for the line generator
		    .y(function(d) { return yScale(d.y); }) // set the y values for the line generator 
		    .curve(d3.curveMonotoneX) // apply smoothing to the line

			// 8. An array of objects of length N. Each object has key -> value pair, the key being "y" and the value is a random number
			// var dataset = d3.range(n).map(function(d) { return {"y": d3.randomUniform(1)() } })
			var dataset = data.map(function(d) { return {"y": d.value } })


			// 1. Add the SVG to the page and employ #2
			var svg = d3.select(elem).append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			// 3. Call the x axis in a group tag
			svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + height + ")")
			.call(d3.axisBottom(xScale))

    		 // Create an axis component with d3.axisBottom

			// 4. Call the y axis in a group tag
			svg.append("g")
			.attr("class", "y axis")
    		.call(d3.axisLeft(yScale)); // Create an axis component with d3.axisLeft

			// 9. Append the path, bind the data, and call the line generator 
			svg.append("path")
    		.datum(dataset) // 10. Binds data to the line 
    		.attr("class", "line " + color ) // Assign a class for styling 
    		.attr("d", line); // 11. Calls the line generator 

			// 12. Appends a circle for each datapoint 
			let dotWrapper = svg.append("g").attr('class', 'dotWrapper' )
			dotWrapper.selectAll(".dot")
			.data(dataset)
			.enter().append("circle") 
			.attr("class", "dot " + color + " tooltip")
			.attr("cx", function(d, i) { return xScale(i) })
			.attr("cy", function(d) { return yScale(d.y) })
			.attr("r", 4)
			.attr('title',function(d) { return (d.y + '$')});

			let botWrapper = svg.append("g").attr('class', 'dotWrapper' )
			
			botWrapper.selectAll(".bot")
			.data(dataset)
			.enter().append("circle") 
			.attr("class", "bot " + color + " dot--shadow tooltip")
			.attr("cx", function(d, i) { return xScale(i) })
			.attr("cy", function(d) { return yScale(d.y) })
			.attr("r", 7)
			.attr('title',function(d) { return (d.y + ' $')})

			

   			// d3 just wasting my time
   			let textList = document.querySelectorAll(elem + ' .x.axis .tick text')
   			textList = [...textList]
   			textList.map((elem,index)=>{
   				elem.innerHTML= parseInt(elem.innerHTML);
   			});
   			textList.map((elem,index)=>{
   				elem.innerHTML= parseInt(elem.innerHTML);

   				if(textList[index + 1]){
   					if(elem.innerHTML == textList[index+1].innerHTML ){
   						textList[index+1].parentNode.remove();
   					}
   				}
   			})

   			textList = document.querySelectorAll(elem + ' .x.axis .tick text')
   			textList = [...textList]
   			textList.map((elem,index)=>{
   				elem.innerHTML =  data[index].title;
   			})


   			// dollor text y-axis
   			let dollorText = document.querySelectorAll(elem + ' .y.axis .tick text')
   			dollorText = [...dollorText]
   			let dollorList = dollorText.length;
   			dollorText.map((elem,index)=>{
   				if(index===0 || index === Math.floor(dollorList/2) || index ===dollorList-1){
   				}
   				else{
   					elem.parentNode.remove();
   				}
   				elem.innerHTML +=  ' $';
   			})
   			
   			// time to create checkbox

   			document.querySelectorAll(elem + ' .y.axis .tick line').forEach((elem, index)=>{
   				elem.setAttribute('x2', width + 10);
   			})
   			document.querySelectorAll(elem + ' .x.axis .tick line').forEach((elem, index)=>{
   				elem.setAttribute('y2', height + 10);
   				const h = height + 10;
   				elem.style.transform= `translateY(-${h}px)`;
   			})

   			

   			

   		}
   	});

    		$(document).ready(function(e){
    			$('body').tooltip({
    				selector: '.tooltip'
    			});
    		})