$(document).ready(function(){
	$('.form input[type=search]').keyup( function(e) {
		if($(this).val()){
			$(this).next().addClass('active');
			$(this).next().next().addClass('active');
			$(this).parents('form').find('.searchList').show(300)
		}
		else{
			$(this).next().removeClass('active');
			$(this).next().next().removeClass('active');
			$(this).parents('form').find('.searchList').hide(300);
		}

	});
	if($('.form input[type=search]').val()){
		$(this).next().addClass('active');
		$(this).next().next().addClass('active');
	}

	$.ajax({
		url: "https://restcountries.eu/rest/v2/all"
	}).then(function(data) {


		$.each( data, function( key, elem ) {
			$("#countryName").append($('<option>', {
				value: elem.alpha2Code,
				text: elem.name
			}));
		});


	})

	$('.feed button.seeAll').click(function(e){
		debugger;
		$('.nav-link[href="#tab2"]').click();
	})

	// responsive 
	if($(window).width() < 840){
		$('.accounts--top .carousel').slick({
			infinite: false,
			lazyLoad: 'ondemand',
			slidesToScroll: 1,
			autoplay:false,	
			dots:false,
			swipeToSlide:true,
			slidesToShow: 1.65,
			arrows:false,
			swipeToSlide:true,
			touchThreshold: 10,
		});

		const menuSlide = ()=>{
			$('.aside--search').toggleClass('active');
			$('body').toggleClass('overflow');
			$('.filter--trigger i').toggleClass('d-none');
		}

		$('.filter--trigger').click(function(e){
			e.stopPropagation();
			e.preventDefault();
			menuSlide();
		})
		
		$('.main.mob--search').click(function(e){
			if($('.aside--search').hasClass('active')){
				menuSlide();
			}
		})

		
	}

})

document.addEventListener("DOMContentLoaded", function(){
	let today = new Date();
	tail.DateTime(".tail-datetime-field--search", { 
		animate: true,
		static: null,
		position: "top",
		classNames: "",
		dateFormat: "dd-mm-YYYY",
		timeFormat: false,
		dateEnd:today,
		weekStart: "MON",
		startOpen: false,
		stayOpen: false,
		today: true,
		
	});
});