$(document).ready(function () {
    $('form #searchFriend').val(null);
    var CommonSite = app.CommonSite;
    function ResetChangePinFields() {
        $('input[name="CurrentPin"]').val('');
        $('input[name="NewPin"]').val('');
        $('input[name="CurrentPassword"]').val('');
    }
    function ResetChangePasswordFields() {
        $('input[name="OldPassword"]').val('');
        $('input[name="NewPassword"]').val('');
        $('input[name="ConfirmPassword"]').val('');
    }
    var ChangePassword = function (e) {
        e.preventDefault();
        var evt = $(this);
        evt.attr('disabled', true);
        $(this).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        var cmodel = {
            OldPassword: '',
            NewPassword: '',
            ConfirmPassword: ''
        };
        cmodel.OldPassword = $('input[name="OldPassword"]').val();
        cmodel.NewPassword = $('input[name="NewPassword"]').val();
        cmodel.ConfirmPassword = $('input[name="ConfirmPassword"]').val();
        var valid = IsValidatePassword(cmodel.NewPassword, cmodel.CommonSite, "#pass_error");
        if (!valid) {
            evt.find('img').remove();
            evt.removeAttr('disabled');

            return false;
        }
        var model = JSON.stringify(cmodel);

        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: CommonSite.apiURL + '/Authentication/ChangePassword',
            type: 'POST',
            crossDomain: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: model
        }).done(function (data) {
            $d.resolve(data);

            CommonSite.EventNotification(data);
            ResetChangePasswordFields();
        }).fail(function (data) {
            $d.reject(data);
            CommonSite.EventNotification(data);
            ResetChangePasswordFields();
        });

        evt.find('img').remove();
        evt.removeAttr('disabled');

        return $d.promise();

    }

    var ChangePin = function (e) {
        e.preventDefault();
        var evt = $(this);
        evt.attr('disabled', true);

        $(this).append(`<img src="/Content/gallery/rings.svg" alt="" >`);
        var cmodel = {
            CurrentPin: '',
            NewPin: '',
            CurrentPassword: ''
        };
        cmodel.CurrentPin = parseInt($('input[name="CurrentPin"]').val());
        cmodel.NewPin = parseInt($('input[name="NewPin"]').val());
        cmodel.CurrentPassword = $('input[name="CurrentPassword"]').val();

        debugger;
        var model = JSON.stringify(cmodel);

        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: CommonSite.apiURL + '/Payment/ChangeWalletPin',
            type: 'POST',
            crossDomain: true,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: model
        }).done(function (data) {
            $d.resolve(data);

            CommonSite.EventNotification(data);
            ResetChangePinFields();
        }).fail(function (data) {
            $d.reject(data);
            CommonSite.EventNotification(data);
            ResetChangePinFields();
        });

        evt.find('img').remove();
        evt.removeAttr('disabled');
        return $d.promise();

    }

    $("#ChangePassword").click(ChangePassword);
    $("#ChangePin").click(ChangePin);

    //function checkPasswordMatch() {
    //    var password = $('input[name="NewPassword"]').val();
    //    var confirmPassword = $('input[name="ConfirmPassword"]').val();
    //    if (password != confirmPassword)
    //        $("#ConfirmPassword").closest(".form-group").find(".error").html("Passwords does not match!");
    //    else
    //        $("#ConfirmPassword").closest(".form-group").find(".error").html("");
    //}
    //$('#NewPassword,#ConfirmPassword').keyup(checkPasswordMatch);
    //    if ($(window).width() > 840) {
    //        $('#message--new').on('show.bs.modal', function () {
    //            $('form #searchFriend').first().focus();
    //        });
    //        $('form #searchFriend').keyup(function (e) {
    //            $(this).val() ? $(this).parents('.modal-content').find('.modal-body h2.section__title').text('Result') : $(this).parents('.modal-content').find('.modal-body h2.section__title').text('Online')
    //        })

    //        $('#ott--modal').on('show.bs.modal', function (event) {
    //            $('#ott--modal form input:first-child').focus();
    //        });

    //        $(".otp input").keyup(function (event) {
    //            debugger;
    //            let key = event.keyCode || event.charCode;
    //            if (!(key == 8 || key == 46)) {
    //                if ($(this).next().length)
    //                    $(this).next().focus();
    //                else {
    //                    $(this).parent().next().find('button').focus();
    //                }
    //            }
    //            else {
    //                $(this).prev().focus();
    //            }

    //        });

    //        const setCookie = (cname, cvalue, exdays) => {
    //            var d = new Date();
    //            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    //            var expires = "expires=" + d.toUTCString();
    //            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    //        }

    //        // cookies light mode or dark
    //        const getCookie = (cname) => {
    //            var name = cname + "=";
    //            var decodedCookie = decodeURIComponent(document.cookie);
    //            var ca = decodedCookie.split(';');
    //            for (var i = 0; i < ca.length; i++) {
    //                var c = ca[i];
    //                while (c.charAt(0) == ' ') {
    //                    c = c.substring(1);
    //                }
    //                if (c.indexOf(name) == 0) {
    //                    return c.substring(name.length, c.length);
    //                }
    //            }
    //            return "";
    //        }

    //        let darkMode = getCookie('darkmode');
    //        const darModeElem = $('#darkmode');
    //        if (darkMode) {
    //            $('body').addClass('darkmode')
    //            darModeElem.addClass('active')
    //            darModeElem.find('i').addClass('bi-brightness-high')
    //            text = darModeElem.html().replace('Dark', 'Light');
    //            darModeElem.html(text)
    //        }
    //        else {
    //            $('body').removeClass('darkmode')
    //        }




    //        darModeElem.click(function (e) {
    //            e.preventDefault();
    //            let text;
    //            if ($(this).hasClass('active')) {
    //                $('body').removeClass('darkmode')
    //                $(this).removeClass('active')
    //                $(this).find('i').removeClass('bi-brightness-high');
    //                text = $(this).html().replace('Light', 'Dark');
    //                setCookie('darkmode', false, 0)
    //            } else {
    //                $('body').addClass('darkmode')
    //                $(this).addClass('active')
    //                $(this).find('i').addClass('bi-brightness-high')
    //                text = $(this).html().replace('Dark', 'Light');
    //                setCookie('darkmode', true, 30)
    //            }

    //            $(this).html(text)
    //        })

    //        const ppBadge = $('.pp-badge');

    //        $('.badge  .yourbadge .image').click(function (e) {
    //            let img = $(this).find('img').attr('src');
    //            ppBadge.attr('src', img);


    //        })
    //    }
})

