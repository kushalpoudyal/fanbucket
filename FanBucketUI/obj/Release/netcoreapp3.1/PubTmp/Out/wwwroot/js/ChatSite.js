﻿if (!window.app) {
    window.app = {};
}

app.ChatSite = (function ($) {

    var responseModel = {
        userId: '',
        profileImgLocation: '',
        chatWithName: '',
        text: '',
        chatId: '',
        time: '',
        fileLocation: '',
        tipAmount: '',
        attachments: []
    }

    var textModel = {
        Message: '',
        FileLocation: '',
        ChatId: '',
        targetId: '',
        TipAmount: '',
        PIN: '',
        Attachments: []

    }
    var TipAmount = '';
    var PINCode = '';
    var acessTokenVal = $('#token___Auth').val().toString();
    acessTokenVal = encodeURIComponent(acessTokenVal);
    const apiURLC = apiURL.slice(0, -4) + "/chatHubserver";

    function acessToken() {
        return `${acessTokenVal}`;
    }
    debugger;
    //const connection = new signalR.HubConnectionBuilder().withUrl("https://localhost:44320/chatHubserver").build();
    //const connection = new signalR.HubConnectionBuilder().withUrl("https://localhost:44320/chatHubserver", { accessTokenFactory: () => "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiY2xpZW50IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZWlkZW50aWZpZXIiOiIyNzcyN2IzZC0yNzdjLTQwMWQtYjhjMy05Yjk3ZTc2NjY3NzgiLCJuYmYiOiIxNjM2NTM1NDA5IiwiZXhwIjoiMTYzNjYyMTgwOSIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IkNsaWVudCJ9.DltXhi8Dk-q7oyiBLS56FYnrH-ALG8bxhyfJ7zfxQdo" }).build();
    //const connection = new signalR.HubConnectionBuilder().withUrl(apiURLC, { accessTokenFactory: () => acessToken }).build();
    const connection = new signalR.HubConnectionBuilder()
        .withUrl(apiURLC, { accessTokenFactory: () => { return acessTokenVal } })
        .withAutomaticReconnect()
        //.withAutomaticReconnect([0, 1000, 5000, null])
        .configureLogging(signalR.LogLevel.Trace)
        .build();


    var ConnectionByUser = function () {


    };
    var UpdateOnlineUsers = function () {
        //connection.invoke('OnConnected').then(function (response) {
        //    alert(response);
        //}).catch(function (err) {
        //    return console.error(err.toString());
        //});
    };


    var CommonSite = app.CommonSite;
    var Payments = app.Payments;

    const messageElement = $("#send-message [name='Message']");

    var initUI = function () {
        $('.btn_getOnlineUser').click(getOnlineUsers);
        $('.messenger--main #chatUserFeed .card__top__body ').unbind().click(getUserMessage);
        console.log("starting connection...");
        connection.start().then(function () {
            //document.getElementById("sendButton").disabled = false;
            messageElement.attr("readonly", "readonly");
            ConnectionByUser();
            UpdateOnlineUsers();
            getUserMessageInit();

            connection.on("MessageNotification", (response) => {
                getChatUser();
            });

            connection.on("ReceiveMessage", (response) => {
                console.log("Recieved message" + response);
                console.log(response);
                var chatBot = $('.chat--wrapper__body');
                var chatBottom = $('#chat_scroll_bottom');
                var chatId = $('#CurrChatId').val();
                var CurrUser = $('#CurrUserId').val();
                var chatClass = '';
                var TipMessage = '';
                var ChatWithName = $('#ChatWithName').val();
                if (response.success) {

                    responseModel.userId = response.user;
                    responseModel.profileImgLocation = response.profileImgLocation;
                    responseModel.chatWithName = response.chatWithName;
                    responseModel.text = response.text;
                    responseModel.chatId = response.chatId;
                    responseModel.time = response.time;
                    responseModel.fileLocation = response.fileLocation;
                    responseModel.tipAmount = response.tipAmount;

                    if (CurrUser != response.user) {
                        UserMsgTab(responseModel);
                    }

                    if (chatId == response.chatId) {
                        if (response.user === CurrUser) {
                            chatClass = 'active';
                            TipMessage = `You sent $ ${responseModel.tipAmount} to  ${ChatWithName}`;
                        }
                        else {
                            TipMessage = `You received $ ${responseModel.tipAmount} from  ${ChatWithName}`;
                        }
                        if (responseModel.text) {
                            var Elem = `
                    <article class="chat--box ${chatClass}">
                        <div class="chat--box__body">
                            <div class="user--text">
                                <p class="para">${response.text}</p>
                            </div>
                            <time class="date small text-muted">${response.time} <i class="bi bi-check2-all"></i></time>
                        </div>
                    </article>`;
                            chatBot.append(Elem);

                        }

                        if (responseModel.fileLocation) {
                            var Elem = `
                       <article class="chat--box ${chatClass}">
                            <div class="chat--box__body">
                                <div class="image--wrapper">
                                    <a class="card__img" href="${responseModel.fileLocation}" title="View Full Size" data-fancybox>
                                        <img src="${responseModel.fileLocation}" alt="">
                                    </a>
                                </div>
                            </div>
                        </article>`;

                            chatBot.append(Elem);
                        }
                        //console.log(responseModel);
                        //console.log("responseModel");

                        //console.log(responseModel.attachments);
                        //console.log(responseModel.attachments);
                        if (response.attachments.length > 0) {

                            var imgCards = '';
                            for (var i = 0; i < response.attachments.length; i++) {
                                console.log(response.attachments[i]);
                                console.log("response.attachments[i]");
                                imgCards += `<a class="card__img" href="${response.attachments[i]}" title="View Full Size" data-fancybox>
                                    <img src="${response.attachments[i]}" alt="">
                                    </a>`;
                            }
                            var Elem = `
                       <article class="chat--box ${chatClass}">
                            <div class="chat--box__body">
                                <div class="image--wrapper">
                                    ${imgCards}
                                   
                                </div>
                            </div >
                        </article > `;
                            console.log(imgCards);
                            console.log(Elem);
                            chatBot.append(Elem);
                        }
                        //debugger;


                        if (!isNaN(responseModel.tipAmount) && parseFloat(responseModel.tipAmount) > 0) {
                            var Elem = `<article class="chat--box ${chatClass}" >

                        <div class="chat--box__body">
                            <div class="shared-feed">
                                <div class="feed ">
                                    <a href="/User/PaymentHistory" class="card">

                                        <div class="card__top">
                                            <div class="card__img">
                                                <img src="${responseModel.profileImgLocation}" alt="">
                                                </div>

                                                <div class="card__top__body">
                                                    <h2 class="card__title">${responseModel.chatWithName}</h2>
                                                </div>
                                            </div>
                                            <button type="submit">
                                                ${TipMessage}
                                            </button>
                                        </a>
                                    </div>
                                    <time class="date small text-muted">${responseModel.time}</time>
                                </div>
                            </div>
                        </article>`;
                            chatBot.append(Elem);
                        }
                        Fancybox.destroy("[data-fancybox]");
                        Fancybox.bind("[data-fancybox]", {
                            // Your options go here
                        });
                    }
                    debugger;
                    //$('html, body').animate({ scrollTop: chatBot.height() }, 100);
                    //console.log("saasjj");
                    //chatBottom.scrollIntoView();
                    $('#chat_scroll_bottom')[0].scrollIntoView();
                    getChatUser();


                }
            });
            connection.on("SendAction", (userId, status) => {
                var $profileChatElement = $(".online-status-chat[data-id='" + userId + "']");
                if (status === "joined") {
                    $profileChatElement.addClass("online");
                    $profileChatElement.removeClass("offline");
                } else {
                    $profileChatElement.removeClass("online");

                    $profileChatElement.addClass("offline");
                }
            });

            connection.on("RecieveMessageUnreadCount", (count) => {
                if (count == 0) {
                    $('.notification--badge').addClass("d-none");
                    $('.notification--badge').text(count);
                } else {
                    $('.notification--badge').removeClass("d-none");
                    $('.notification--badge').text(count);
                }
            });
        }).catch(function (err) {
            return console.error(err.toString());
        });

        $("#report--user form").unbind().submit(ReportConversation);


        $(".message--read--trigger").unbind().click(updateChatStatus);
        $(".message--unread--trigger").unbind().click(markAsUnread);
        $(".delete--conversation--trigger").unbind().click(deleteConversation);
        $(".hide--conversation--trigger").unbind().click(hideConversation);
        $(".conversation--report--trigger").unbind().click(ShowReportModel);
        $(".conversation--block--trigger").unbind().click(BlockConversation);
        $(".conversation--unblock--trigger").unbind().click(UnblockConversation);






    };
    var RemoveAttachment = function (e) {
        e.preventDefault();
        var Id = $(this).closest(".image").data('id');

        if (Id === undefined) {
            return;
        }
        var index = textModel.Attachments.findIndex(x => x.ContentId == Id);
        if (index !== undefined) {
            textModel.Attachments.splice(index, 1);
            let parent = $(this).parents('.image');

            let elemParent = $(this).parents('.chat__ph--upload');
            parent.remove();
            if (elemParent.children().length <= 1) {
                elemParent.addClass('hide')
            }
        }

    }
    var UserMsgTab = function (response) {
        var userTabs = $('.tab-content');
        if (!response.text) {
            response.text = "Image";
        }
        if (userTabs.find('#tabUser_' + response.userId).length === 0) {
            var userElem = `<a href = "#" title = "Send message to Person Name" id = "tabUser_${response.userId}" class="card chat--card readed btnOnlineUsers" data-val="${response.userId}" data-id="${response.chatId}" >
                        <div class="card__top">
                            <div class="card__img">
                                <img src="${response.profileImgLocation}" alt="">
                                    <div class="online">

                                    </div>
                        </div>

                                <div class="card__top__body">
                                    <h2 class="card__title">${response.chatWithName}</h2>
                                    <p class="user__text">
                                        ${response.text}
                                    </p>
                                </div>


                                <div class="card__top__link">
                                    <button href="#">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="11.784" height="2.703" viewBox="0 0 11.784 2.703">
                                            <g id="Group_144" data-name="Group 144" transform="translate(6425.085 6208.133)" opacity="0.3">
                                                <path id="Path_95" data-name="Path 95" d="M-6416-6206.782a1.351,1.351,0,0,0,1.351,1.352,1.351,1.351,0,0,0,1.351-1.352,1.351,1.351,0,0,0-1.351-1.351A1.351,1.351,0,0,0-6416-6206.782Z" />
                                                <path id="Path_96" data-name="Path 96" d="M-6417.842-6206.782a1.35,1.35,0,0,0-1.351-1.351,1.351,1.351,0,0,0-1.351,1.351,1.351,1.351,0,0,0,1.351,1.352A1.351,1.351,0,0,0-6417.842-6206.782Z" />
                                                <path id="Path_97" data-name="Path 97" d="M-6422.383-6206.782a1.35,1.35,0,0,0-1.351-1.351,1.351,1.351,0,0,0-1.351,1.351,1.351,1.351,0,0,0,1.351,1.352A1.351,1.351,0,0,0-6422.383-6206.782Z" />
                                            </g>
                                        </svg>
                                    </button>
                                    <time><i class="bi bi-circle-fill"></i> ${response.time}</time>
                                </div>
                            </div>
                </a>`;
            userTabs.prepend(userElem);
        }
        else {
            userTabs.find('#tabUser_' + response.userId).remove();

            var userElem = `<a href = "#" title = "Send message to Person Name" id = "tabUser_${response.userId}" class="card chat--card readed btnOnlineUsers" data-val="${response.userId}" data-id="${response.chatId}" >
                        <div class="card__top">
                            <div class="card__img">
                                <img src="${response.profileImgLocation}" alt="">
                                    <div class="online">

                                    </div>
                        </div>

                                <div class="card__top__body" >
                                    <h2 class="card__title">${response.chatWithName}</h2>
                                    <p class="user__text">
                                        ${response.text}
                                    </p>
                                </div>


                                <div class="card__top__link">
                                    <button href="#">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="11.784" height="2.703" viewBox="0 0 11.784 2.703">
                                            <g id="Group_144" data-name="Group 144" transform="translate(6425.085 6208.133)" opacity="0.3">
                                                <path id="Path_95" data-name="Path 95" d="M-6416-6206.782a1.351,1.351,0,0,0,1.351,1.352,1.351,1.351,0,0,0,1.351-1.352,1.351,1.351,0,0,0-1.351-1.351A1.351,1.351,0,0,0-6416-6206.782Z" />
                                                <path id="Path_96" data-name="Path 96" d="M-6417.842-6206.782a1.35,1.35,0,0,0-1.351-1.351,1.351,1.351,0,0,0-1.351,1.351,1.351,1.351,0,0,0,1.351,1.352A1.351,1.351,0,0,0-6417.842-6206.782Z" />
                                                <path id="Path_97" data-name="Path 97" d="M-6422.383-6206.782a1.35,1.35,0,0,0-1.351-1.351,1.351,1.351,0,0,0-1.351,1.351,1.351,1.351,0,0,0,1.351,1.352A1.351,1.351,0,0,0-6422.383-6206.782Z" />
                                            </g>
                                        </svg>
                                    </button>
                                    <time><i class="bi bi-circle-fill"></i> ${response.time}</time>
                                </div>
                            </div>
                </a>`;
            userTabs.prepend(userElem);

        }
    };

    var getChatUser = function () {
        var url = CommonSite.appURL + '/Messenger/Messenger';
        console.log("get chat user calling");

        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json; charset=utf-8'
        }).done(function (result) {
            console.log("get chat user callled");

            $('.messenger--main #chatUserFeed').empty();
            $('.messenger--main #chatUserFeed').html(result);
            $('.messenger--main #chatUserFeed .card__top__body ').unbind().click(getUserMessage);

            //$('.messenger--main #chatUserFeed .card__top__body ').unbind().click(getUserMessage);
            $(".message--read--trigger").unbind().click(updateChatStatus);
            $(".message--unread--trigger").unbind().click(markAsUnread);
            $(".delete--conversation--trigger").unbind().click(deleteConversation);
            $(".hide--conversation--trigger").unbind().click(hideConversation);
            $(".conversation--report--trigger").unbind().click(ShowReportModel);
            $(".conversation--block--trigger").unbind().click(BlockConversation);
            $(".conversation--unblock--trigger").unbind().click(UnblockConversation);

        });

    };


    var getOnlineUsers = function () {

        var modal = $('#message--new');
        modal.modal('toggle');

        var auth = $('#token___Auth').val();
        var url = CommonSite.apiURL + '/Message/GetOnlineUser';
        $.ajax({
            url: url,
            type: 'GET',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            modal.find('.modal-body').empty();
            for (i = 0; i < result.length; i++) {
                var divElem = `<a href = "#"  title = "Send message to Person Name" class="card chat--card btnOnlineUsers" data-val="${result[i].userId}" data-id="${result[i].chatId}" data-ajax="True" >
                        <div class="card__top">
                            <div class="card__img">
                                <img src="${result[i].profileImg == null ? "/Content/gallery/userimg.png" : result[i].profileImg}" alt="">
                                    <div class="online">

                                    </div>
                            </div>

                                <div class="card__top__body" >
                                    <h2 class="card__title">${result[i].fullname}</h2>
                                </div>
                            </div>
                    </a>`;

                modal.find('.modal-body').append(divElem);
            }

            $('.messenger--main #chatUserFeed .card__top__body ').unbind().click(getUserMessage);


        }).fail(function (error) {
            var result = { returnMsg: 'User Fetch Issue' };
            EventNotification(result);
        });


    };
    var StartConnection = function () {
        console.log('start connection called');

        var ConnectionId = $('#CurrChatId').val();
        console.log("CurrChatId " + ConnectionId);
        
        connection.invoke('joinRoom', ConnectionId).then(function () {
            console.log('successfully connected');
            $('#modal--tips form').unbind().submit(function (e) {
                debugger;
                e.preventDefault();
                TipAmount = $('#amount--number').val();
                $('#amount--number').val('');
                $('.chat__tip--upload').find('label').text('Send Tip of: $' + TipAmount);
                $('.chat__tip--upload.hide').removeClass('hide');
                $('#modal--tips').modal('toggle');

                $(".tip--modal").modal('show');
            });

            $('form#send-message').unbind().submit(function (event) {
                debugger;
                console.log("sending");
                console.log("Tip Amount is " + TipAmount);
                console.log("Pin Code is " + PINCode);
                event.preventDefault();
                sendMessage(this);
            });

            messageElement.removeAttr("readonly");

            //$('form').find('input[name="Message"]').click(MessageStatus);
        }).catch(function (err) {
            return console.error(err.toString());
        });




        window.addEventListener('onunload', function () {
            connection.invoke('leaveRoom', ConnectionId);
        });


        //connection.start().then(function () {
        //    //document.getElementById("sendButton").disabled = false;
        //    console.log('successfully connected');
        //}).catch(function (err) {
        //    return console.error(err.toString());
        //});
    }
    $(".tip--modal").unbind().submit(function (e) {
        e.preventDefault();
        var pSpan = $('#enter--pin--modal1').find('input[name="pinDigit"]');
        PINCode = '';
        $(pSpan).each(function () {
            PINCode += $(this).val();
        });
        $('#enter--pin--modal1').find('input[name="pinDigit"]').val('');//
        $(".tip--modal").modal('hide');
    });

    var sendMessage = function (event) {
        debugger;
        if (parseFloat(TipAmount) > 0) {
            //textModel.Message = "Tip Message";
            textModel.TipAmount = TipAmount;
            textModel.PIN = PINCode;
        } else {
            textModel.TipAmount = TipAmount;
            textModel.PIN = PINCode;
        }
        debugger;
        TipAmount = '';
        PINCode = '';
        textModel.FileLocation = $(event).find('input[name="FileLocation"]').val();
        textModel.Message = $(event).find('input[name="Message"]').val();

        if (textModel.FileLocation && !textModel.Message) {
            //textModel.Message = "Image";

        }
        if (!textModel.Message && textModel.Attachments.length == 0) {
            $(event).find('input[name="Message"]').parent().css('border-color', 'red');
            setTimeout(function () {
                // reset CSS
                $(event).find('input[name="Message"]').parent().css('border-color', '');
            }, 500);
            return false;
        }
        textModel.ChatId = $(event).find('input[name="ChatId"]').val();
        textModel.targetId = $("#ChatUserId").val();
        //var data = new FormData(form);
        //document.getElementById('message-input').value = '';

        var model = JSON.stringify(textModel);
        console.log(model);
        textModel.Attachments = [];
        var url = CommonSite.appURL + '/Messenger/ChatContent';
        var token = $('input:hidden[name="__RequestVerificationToken"]').val();

        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                RequestVerificationToken:
                    token
            },
            data: model,
        }).done(function (msg) {
            if (!msg.success) {
                CommonSite.NoticeNotification(msg.returnMsg);
            } else {
                textModel.Attachments = [];
            }
            console.log(msg);
        }).fail(function (jqXHR, textStatus) {
            console.log(textStatus);
            console.log(jqXHR);

            //    alert("Request failed: " + textStatus);
        });
        $('.chat__ph--upload').addClass('hide');
        $('.chat__tip--upload').addClass('hide');
        $('.chat__ph--upload').find('.image').not('.empty--input').remove();
        $(event).find('input[name="Message"]').val('');
        $(event).find('input[name="FileLocation"]').val('');
        ResetTips();

        //axios.post('/Messenger/ChatContent', model)
        //    .then(res => {
        //        console.log("Message Sent!")
        //    })
        //    .catch(err => {
        //        console.log("Failed to send message!")
        //    })
    }
    var ResetTips = function () {
        TipAmount = '';
        $('#amount--number').val('');
        $('.chat__tip--upload').find('label').text('');
        //$('.chat__tip--upload.hide').removeClass('hide');
        //$('#modal--tips').modal('toggle');

        $(".tip--modal").modal('hide');
    }
    //
    //ChatId
    var updateChatStatus = function (e) {
        var ChatId = $(this).data('id');
        var auth = $('#token___Auth').val();
        var url = app.CommonSite.apiURL + '/Message/UpdateMessageStatus?ChatId=' + ChatId;

        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            //    data: model,
        });
        getChatUser();
    };

    var markAsUnread = function (e) {
        var ChatId = $(this).data('id');
        var auth = $('#token___Auth').val();
        var url = app.CommonSite.apiURL + '/Message/MarkAsUnread?ChatId=' + ChatId;

        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            //    data: model,
        });

        getChatUser();
    };
    var ShowReportModel = function (e) {
        var ChatId = $(this).data('id');
        $('#ChatId').val(ChatId);
    };
    var ReportConversation = function (e) {
        var MessageId = $('#ChatId').val();
        var auth = $('#token___Auth').val();
        var url = app.CommonSite.apiURL + '/Message/ReportConversation';
        var model = {
            ReportedFor: '',
            ChatId: ''
        };

        model.ChatId = MessageId;
        var selectedVal = "";
        var selected = $("input[type='radio'][name='report-input']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val();
        }
        model.ReportedFor = selectedVal;
        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: JSON.stringify(model),
        }).done(function (res) {
            CommonSite.NoticeNotification(res.returnMsg);
        });



    };

    var deleteMessage = function (e) {
        var MessageId = $(this).data('id');
        var auth = $('#token___Auth').val();
        var url = app.CommonSite.apiURL + '/Message/Delete?MessageId=' + MessageId;

        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            //    data: model,
        });

        var ChatId = $("#CurrChatId").val();
        var UserId = $("#ChatUserId").val();
        var IsAjax = "True";
        IsAjax = "False";
        newFunction(IsAjax, ChatId, UserId);

    };
    var BlockConversation = function (e) {
        var $this = $(this);
        var ChatId = $(this).data('id');
        var userId = $(this).data('userid');


        var model = {
            ChatId: '',
            UserId: ''
        };
        model.ChatId = ChatId;
        model.UserId = userId;
        var auth = $('#token___Auth').val();
        var url = app.CommonSite.apiURL + '/Message/BlockConversation';

        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: JSON.stringify(model),
        }).done(function (success) {
            if (success) {
                $this.closest("ul").find(".conversation--block--trigger").removeClass("conversation--block--trigger").addClass("conversation--unblock--trigger").text("Unblock Conversation");
                $(".conversation--unblock--trigger").unbind().click(UnblockConversation);

                CommonSite.NoticeNotification("Successfully Blocked");
            } else {
                CommonSite.NoticeNotification("Blocked Failed");

            }


        });

    };
    var UnblockConversation = function (e) {
        var $this = $(this);

        var ChatId = $(this).data('id');
        var userId = $(this).data('userid');


        var model = {
            ChatId: '',
            UserId: ''
        };
        model.ChatId = ChatId;
        model.UserId = userId;
        var auth = $('#token___Auth').val();
        var url = app.CommonSite.apiURL + '/Message/UnblockConversation';

        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: JSON.stringify(model),
        }).done(function (success) {
            if (success) {
                $this.closest("ul").find(".conversation--block--trigger").removeClass("conversation--unblock--trigger").addClass("conversation--block--trigger").text("Block Conversation");
                $(".conversation--block--trigger").unbind().click(BlockConversation);
                CommonSite.NoticeNotification("Successfully Unblocked");
            } else {
                CommonSite.NoticeNotification("Unblocked Failed");

            }


        });

    };
    var deleteConversation = function (e) {
        var ChatId = $(this).data('id');
        var userId = $(this).data('userid');
        var auth = $('#token___Auth').val();
        var url = app.CommonSite.apiURL + '/Message/DeleteConversation?ChatId=' + ChatId;

        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            //    data: model,
        }).done(function () {
            var currentChatTabopened = $('#tabUser_' + userId).closest(".tab-pane").hasClass("active");
            getChatUser();
            if (currentChatTabopened) {
                $(".chat--wrapper__body").empty();
            }
        });

    };

    var hideConversation = function (e) {
        var ChatId = $(this).data('id');
        var userId = $(this).data('userid');
        var auth = $('#token___Auth').val();
        var url = app.CommonSite.apiURL + '/Message/HideConversation?ChatId=' + ChatId;

        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            //    data: model,
        }).done(function () {
            console.log("ajax called");
            getChatUser();

        });

    };
    var getUserMessage = function (e) {
        e.preventDefault();
        var ChatId = $(this).closest(".chat--card").attr('data-id');
        var UserId = $(this).closest(".chat--card").attr('data-val');
        //responseModel.userId = UserId;
        //responseModel.profileImgLocation = $(this).find('img').attr('src');
        //responseModel.chatWithName = $(this).find('.card__title').text();
        //responseModel.text = "";
        //responseModel.chatId = ChatId;
        //responseModel.time = "";
        var IsAjax = $(this).attr('data-ajax');

        newFunction(IsAjax, ChatId, UserId);

        $(".message--delete--trigger").unbind().click(deleteMessage);

    }
    var uploadBlob = function (formData) {
        var $d = $.Deferred();
        var auth = $('#token___Auth').val();
        $.ajax({

            url: apiURL + '/Bucket?contentType=Image',
            crossDomain: true,
            data: formData,
            type: 'POST',
            processData: false,
            contentType: false,
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (data) {

            $d.resolve(data);
        }).fail(function (data) {

            $d.reject(data);

        });

        return $d.promise();


    };
    const uploadImage = function (input, imageAppend) {
        debugger;

        // console.log(imageAppend);
        if (input.files) {
            let filesAmount = input.files.length;
            var files = input.files;
            for (let i = 0; i < filesAmount; i++) {
                var formData = new FormData();
                formData.append("files", files[i]);

                uploadBlob(formData).done(function (data) {
                    if (!data.success) {
                        //$('#post--modal button').prop('disabled', false);
                        ResetFileInput();

                        NoticeNotification(data.returnMsg);
                        return false;
                    }

                    let image = `<div class="image uploading" data-id="${data.returnId}">
                            <img src="${data.returnPath}" alt="">
                            <input type="file" accept="image/*,image/heif,image/heic,video/*,video/mp4,video/x-m4v,video/x-matroska,.mkv"
                                class="input__hide" name="chatUploadedImage" value="${data.returnPath}">
                                <div class="edit--wrapper hide">
                                    <a href="#" class="trash "><i class="bi bi-x"></i></a>
                                </div>
                                <div class="loader__wrapper">
                                    <div class="loader">
                                        <div class="box">
                                            <img src="~/Content/gallery/logoWhite.png" alt="fanbucket">
					                    </div>
                                            <div class="shadow"></div>
                                        </div>
                                    </div>
                                </div>` ;

                    $(imageAppend).append(image);
                    var uploadModel = {
                        ContentId: data.returnId,
                        FileLocation: data.returnPath,
                        //    ContentType: ContentType
                    };
                    textModel.Attachments.push(uploadModel);
                    //$('form').find('input[name="FileLocation"]').val(data.returnPath);

                    setTimeout(function (e) {
                        $(imageAppend).find('.uploading').removeClass('uploading');
                    }, 3000)
                    //}, 100)
                    setTimeout(function (e) {
                        $(imageAppend).find('.edit--wrapper.hide').removeClass('hide');
                    }, 3000 - 400)
                    //    }, 100)
                });
                $('.chat__ph--upload.hide').removeClass('hide');

            }

            $('#chat__link__input').val('');
        }

    };
    var newFunction = function (IsAjax, ChatId, UserId) {
        var xhttp = new XMLHttpRequest();
        //if (IsAjax === "True") {
        var modal = $('#message--new');
        modal.modal('hide');
        /*}*/
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                //UserMsgTab(responseModel);
                $('.messenger--aside:not(#chatFeed)').remove();
                $('#chatFeed').empty();
                $('#chatFeed').append(this.responseText);
                console.log("1");
                console.log(messageElement);
                messageElement.attr("readonly", "readonly");
                $('#chatFeed').removeAttr("hidden");
                //$('html, body').animate({
                //    scrollTop: $('.chat--wrapper__body').height()
                //}, 100);
                $('#chat_scroll_bottom')[0].scrollIntoView();

                $('#chat__link__input').unbind().change(function (e) {
                    uploadImage(this, '.chat--wrapper__bottom .chat__ph--upload');
                });
                StartConnection();
                getChatUser();
                $('.messenger--main #chatUserFeed .card__top__body ').unbind().click(getUserMessage);
                $(".message--delete--trigger").unbind().click(deleteMessage);
                // chat form
                $(document).on('click', '.chat--wrapper__bottom .trash', RemoveAttachment);
                window.history.pushState("data", "Title", "/Messenger/Messenger?ChatId=" + ChatId + "&UserId=" + UserId);
            }
        };
        xhttp.open("GET", CommonSite.appURL + "/Messenger/ChatContent?ChatId=" + ChatId + "&&UserId=" + UserId, true);
        xhttp.send();
        $(".message--delete--trigger").unbind().click(deleteMessage);

    }
    var getUserMessageInit = function (e) {

        if (!$('#extChatId').val()) {
            return false;
        }
        if (!$('#extUserId').val()) {
            return false;
        }

        var ChatId = $('#extChatId').val()
        var UserId = $('#extUserId').val()
        responseModel.userId = UserId;
        responseModel.profileImgLocation = $('#extUserImg').val()
        responseModel.chatWithName = $('#extName').val()
        responseModel.text = "";
        responseModel.chatId = ChatId;
        responseModel.time = "";
        var xhttp = new XMLHttpRequest();
        //var IsAjax = $(this).attr('data-ajax');
        //if (IsAjax === "True") {
        //    var modal = $('#message--new');
        //    modal.modal('toggle');
        //}

        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                console.log("getUserMessageInit called");
                //UserMsgTab(responseModel);

                $('.messenger--aside:not(#chatFeed)').remove();
                $('#chatFeed').empty();
                $('#chatFeed').append(this.responseText);
                console.log(messageElement);
                messageElement.attr("readonly", "readonly");

                $('#chatFeed').removeAttr("hidden");
                //$('html, body').animate({
                //    scrollTop: $('.chat--wrapper__body').height()
                //}, 100);
                $('#chat_scroll_bottom')[0].scrollIntoView();

                StartConnection();
                getChatUser();
                console.log("caliing message bind");

                $('.messenger--main #chatUserFeed .card__top__body ').unbind().click(getUserMessage);
                $(".message--delete--trigger").unbind().click(deleteMessage);
                // chat form
                $(document).on('click', '.chat--wrapper__bottom .trash', RemoveAttachment);

                $('#chat__link__input').unbind().change(function (e) {
                    uploadImage(this, '.chat--wrapper__bottom .chat__ph--upload');
                });
                //messageElement.removeAttr("readonly");

            }
        };
        xhttp.open("GET", CommonSite.appURL + "/Messenger/ChatContent?ChatId=" + ChatId + "&UserId=" + UserId, true);
        xhttp.send();


    }

    return {
        initUI: initUI,
        updateChatStatus: updateChatStatus
    };
})(jQuery);
jQuery(app.ChatSite.initUI);



