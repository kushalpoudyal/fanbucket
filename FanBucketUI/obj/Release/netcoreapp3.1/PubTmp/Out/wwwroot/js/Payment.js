﻿if (!window.app) {
    window.app = {};
}

app.Payments = (function ($) {
    //    var CommonSite = app.CommonSite;

    //var loadContents = function (datId, appURL) {
    //    var xhttp = new XMLHttpRequest();
    //    var PostId = datId;
    //    xhttp.onreadystatechange = function () {
    //        if (this.readyState == 4 && this.status == 200) {
    //            var Feed = document.getElementById("feedCard__" + PostId);
    //            Feed.innerHTML = this.responseText;
    //            CommonSite.ContentAddons();
    //        }
    //    };
    //    xhttp.open("GET", appURL + "/Post/SingleContent?PostId=" + PostId, true);
    //    xhttp.send();
    //};

    var SubscribePayment = function (datId, pin, apiURL, instance) {
        var tempModel = {
            SubscriptionId: '',
            PIN: '',

        }
        tempModel.SubscriptionId = datId;
        //tempModel.PIN = prompt("Enter Your PIN:", "");
        tempModel.PIN = pin;

        if (!tempModel.PIN) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            return false;
        }
        var auth = $('#token___Auth').val();
        var url = apiURL + '/Subscriptions/PostSubscriptions?SubscriptionId=' + tempModel.SubscriptionId + '&PIN=' + tempModel.PIN;
        var model = JSON.stringify(tempModel);
        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            $('#modal--confirm').modal('hide');
            EventNotification(result);
            if (result.success) {
                setTimeout(function () {
                    window.location.reload(1);
                }, 5000);
            } else {
                $('#enter--pin--modal').modal('hide')
            }


        }).fail(function (error) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            $('#modal--confirm').modal('hide');
            var result = { returnMsg: 'We are Unable to validate your request...' };
            EventNotification(result);
        });

    }

    var PostPayment = function (datId, PIN, apiURL, instance, appURL) {
        var tempModel = {
            PostId: '',
            PIN: '',
            CurrId: ''

        }
        tempModel.postId = datId;
        tempModel.PIN = PIN;
        if (!tempModel.PIN) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            return false;
        }
        var auth = $('#token___Auth').val();
        var url = apiURL + '/Payment/PaymentForPost';
        var model = JSON.stringify(tempModel);
        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            },
            data: model
        }).done(function (result) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            //$('#modal--confirm').modal('toggle');
            $("#enter--pin--modal").modal('hide');

            EventNotification(result);
            loadContents(datId, appURL);

        }).fail(function (error) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            $('#modal--confirm').modal('toggle');
            var result = { returnMsg: 'Error! Action Failed' };
            EventNotification(result);
        });


    };


    var PaymentValidation = function (apiURL) {
        function getVal() {
            var auth = $('#token___Auth').val();
            var url = apiURL + '/Payment/PaymentValidation';
            var x = 0;
            $.ajax({
                url: url,
                type: 'POST',
                crossDomain: true,
                async: false,
                contentType: 'application/json; charset=utf-8',
                headers: {
                    Authorization:
                        "Bearer " + auth
                }
            }).done(function (result) {
                if (result.success) {
                    x = result.responseCode;
                }
                else {
                    if (result.responseCode == 700) {
                        x = result.responseCode;
                    }
                    else {
                        EventNotification(result);
                    }


                }

            }).fail(function (error) {
                var result = { returnMsg: 'Error! Action Failed' };
                EventNotification(result);

            });
            return x;
        }
        return parseInt(getVal());
    };
    var SetPinCode = function (PIN, Password, apiURL, instance) {
        var auth = $('#token___Auth').val();
        var url = apiURL + '/Payment/SetPinCode?PIN=' + PIN + '&Password=' + Password;
        $.ajax({
            url: url,
            type: 'POST',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            $('#ott--modal').modal('toggle');

            EventNotification(result);


        }).fail(function (error) {
            $(instance).prop('disabled', false);
            $(instance).find("img").remove();
            var result = { returnMsg: 'Error! Action Failed' };
            EventNotification(result);
        });
    };
    var GetBalance = function (apiURL, input) {
        var auth = $('#token___Auth').val();
        var url = apiURL + '/Payment/GetBalance';
        $.ajax({
            url: url,
            type: 'GET',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            headers: {
                Authorization:
                    "Bearer " + auth
            }
        }).done(function (result) {
            var amount = result.returnMsg * 1;
            $(input).text(' $' + amount.toFixed(2));


        }).fail(function (error) {
            var result = { returnMsg: 'Could Not Load Balance' };
            EventNotification(result);
            $(input).text('$0.00');
        });
    };
    var EventNotification = function (data) {
        var Elem = $('.tost-wrapper');
        var notification = $(`<div class="toast" role="alert">
              <div class="toast-header">
            <strong class="mr-auto">Alert</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    </div >
        <div class="toast-body">
            <p class="para">${data.returnMsg}</p>
        </div>
</div>`);

        Elem.append(notification);
        Elem.find('.toast').addClass('show');
        setTimeout(function (e) {
            Elem.find('.toast').removeClass('show');
        }, 5000)
        setTimeout(function (e) {
            Elem.empty();
        }, 10000)

    };
    return {
        PaymentValidation: PaymentValidation,
        PostPayment: PostPayment,
        SetPinCode: SetPinCode,
        GetBalance: GetBalance,
        SubscribePayment: SubscribePayment


    };
})(jQuery);