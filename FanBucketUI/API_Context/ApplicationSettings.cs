﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketUI.API_Context
{
    public class ApplicationSettings
    {
        public string APIEndPoint { get; set; }

        public string DevAPIEndPoint { get; set; }
    }
}
