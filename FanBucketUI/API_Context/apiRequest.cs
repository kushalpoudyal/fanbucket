﻿using FanBucketAPI.APIModels;
using FanBucketAPI.Models;
using FanBucketAPI.ViewModel;
using FanBucketEntity.Model.PostDetails;
using FanBucketEntity.Procedure;
using FanBucketEntity.ViewModel;
using FanBucketEntity.Wrappers;
using FanBucketRepo.ViewModel;
using FanBucketUI.RequestModel;
using FanBucketUI.ResponseModel;
using FanBucketUI.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Session;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FanBucketUI.API_Context
{
    public class apiRequest
    {

        private string URL { get; }
        private readonly ApplicationSettings _appSettings;
        private readonly IWebHostEnvironment hostEnvironment;

        public async Task<bool> UserCommissionReview(CommissionReviewViewModel commissionReviewViewModel, string token)
        {
            var request = new HttpClient();
            var Uri = URL + "/Admin/Commision/Review";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var body = JsonConvert.SerializeObject(commissionReviewViewModel);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //var result = await response.Content.ReadAsStringAsync();
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> PostReview(PostReviewViewModel postReviewViewModel, string token)
        {
            var request = new HttpClient();
            var Uri = URL + "/Admin/PostModeration";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var body = JsonConvert.SerializeObject(postReviewViewModel);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //var result = await response.Content.ReadAsStringAsync();
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> UserReview(ReportActionViewModel reportActionViewModel, string token)
        {
            var request = new HttpClient();
            var Uri = URL + "/Admin/ModerateUser";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var body = JsonConvert.SerializeObject(reportActionViewModel);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //var result = await response.Content.ReadAsStringAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        public apiRequest(IOptions<ApplicationSettings> appSettings, IWebHostEnvironment _hostEnvironment)
        {


            _appSettings = appSettings.Value;
            //if (_hostEnvironment.IsDevelopment())
            //{
            //    URL = _appSettings.DevAPIEndPoint;
            //}
            //else
            //{
            //    URL = _appSettings.APIEndPoint;
            //}
            URL = _appSettings.APIEndPoint;
        }



        public async Task<TokenModel> GenerateToken(UserCred userCred)
        {

            var request = new HttpClient();
            var Uri = URL + "/Authentication/token";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var body = JsonConvert.SerializeObject(userCred);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TokenModel>(result);
            }
            else
            {
                return null;
            }
        }

        public async Task<List<PaymentHistory>> GetPaymentHistory(Guid NotifyId, string token)
        {

            var request = new HttpClient();
            var Uri = URL + "/Payment/History";
            if (NotifyId != default)
            {
                Uri = Uri + "?NotifyId=" + NotifyId;
            }
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PaymentHistory>>(result);
            }
            else
            {
                return null;
            }
        }


        public async Task<List<PostMomentsViewModel>> GetUserMoments(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetMoments";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMomentsViewModel>>(result);
            }
            else
            {
                return null;
            }

        }
        public async Task<int> GetNotificationCount(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Notification/GetNotificationCount";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(result);
            }
            else
            {
                return 0;
            }

        }
        public async Task<int> GetMessageCount(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Notification/GetMessageCount";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(result);
            }
            else
            {
                return 0;
            }

        }
        public async Task<UserInteractionViewModel> GetUserData(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/UserInteraction/GetUserStatistics";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserInteractionViewModel>(result);
            }
            else
            {
                return null;
            }

        }

        //GetTopAccounts
        public async Task<List<TopAccountsViewModel>> GetTopAccounts(string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetTopAccounts";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<TopAccountsViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        //GetTopAccounts
        public async Task<List<NotificationViewModel>> GetUserNotifications(string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/Notification/";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<NotificationViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<JsonResponse> RegisterClient(RegisterClient userCred)
        {
            var responseF = new RegistryModel();
            var request = new HttpClient();
            var Uri = URL + "/Authentication/RegisterClient";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var body = JsonConvert.SerializeObject(userCred);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await request.PostAsync(Uri, BodyContent);
            var result = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<JsonResponse>(result);
            }
            else
            {
                return JsonConvert.DeserializeObject<JsonResponse>(result);
            }

            //return responseF;

        }
        public async Task<RegistryModel> RegisterPost(PostMasterAPIModel model, string token)
        {
            var responseF = new RegistryModel();
            var request = new HttpClient();
            var Uri = URL + "/PostMasters/CreatePost";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var body = JsonConvert.SerializeObject(model);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                responseF.Registered = true;
            }
            else
            {
                responseF.Registered = false;
            }

            return responseF;

        }
        public async Task<List<PostMasterCollectionViewModel>> GetContents(string token, int PageNumber = 1, int PageSize = 10)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetPost?PageNumber=" + PageNumber + "&PageSize=" + PageSize;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<List<PostMasterCollectionViewModel>> SearchPost(string token, string hashtag)
        {

            var request = new HttpClient();
            var Uri = URL + "/PostMasters/Search?hashtag=" + hashtag;

            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }
        }


        public async Task<List<PostMasterCollectionViewModel>> GetContents(string token, Guid Id, int PageNumber = 1, int PageSize = 10)
        {
            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetPostById?UserId=" + Id;
            Uri = Uri + "&PageNumber=" + PageNumber + "&PageSize=" + PageSize;

            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }
        }


        public async Task<List<PostMasterCollectionViewModel>> GetContentsAsGuestUser(string token, int PageNumber = 1, int PageSize = 10)
        {
            var request = new HttpClient();

            var Uri = URL + "/PostMasters/ViewAsGuestUser";
            Uri = Uri + "?PageNumber=" + PageNumber + "&PageSize=" + PageSize;

            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }
        }
        public async Task<List<SearchUserViewModel>> GetUsersBySearch(string Msg, string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/Authentication/GetUserBySearch?StringText=" + Msg;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<SearchUserViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<PostMasterCollectionViewModel> GetSinglePostById(Guid PostId, string token, string NotifyId = null)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetSinglePostByIdAjax?PostId=" + PostId;
            if (!string.IsNullOrEmpty(NotifyId))
            {
                Uri = Uri + "&NotifyId=" + NotifyId;
            }
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<PostMasterCollectionViewModel>(result);
            }
            else
            {
                return null;
            }


        }


        public async Task<UserDetailsViewModel> GetUsersById(string Username)
        {

            var request = new HttpClient();

            var Uri = URL + "/Authentication/GetUserById?Username=" + Username;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //request.DefaultRequestHeaders.Add("Authorization", "Bearer "+token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserDetailsViewModel>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<UserDetailsViewModel> GetUsersById(string Username, string Token, string NotifyId = null)
        {

            var request = new HttpClient();

            var Uri = URL + "/Authentication/GetUserById?Username=" + Username;
            if (!string.IsNullOrEmpty(NotifyId))
            {
                Uri = Uri + "&NotifyId=" + NotifyId;
            }
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserDetailsViewModel>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<UserDetailsViewModel> GetUserDetailsById(Guid Id, string Token)
        {

            var request = new HttpClient();

            var Uri = URL + "/Admin/GetUserDetails?Id=" + Id;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserDetailsViewModel>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<UserDetailsViewModel> GetUserDetailsByUsername(string username, string Token)
        {

            var request = new HttpClient();

            var Uri = URL + "/Admin/GetUserDetailsByUsername?username=" + username;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserDetailsViewModel>(result);
            }
            else
            {
                return null;
            }

        }
        public async Task<List<GetUserMediaViewModel>> GetMediaByUser(string Token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetMedia";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<GetUserMediaViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<PostCommentViewModel>> GetComments(string token, Guid PostId)
        {
            var request = new HttpClient();

            var Uri = URL + "/PostComments/GetCommentsByPost?PostId=" + PostId;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostCommentViewModel>>(result);
            }
            else
            {
                return null;
            }
        }
        public async Task<RegistryModel> CreateComment(CommentViewModel model, string token)
        {
            var responseF = new RegistryModel();
            var request = new HttpClient();
            var Uri = URL + "/PostComments/PostPostComments";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var body = JsonConvert.SerializeObject(model);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                responseF.Registered = true;
            }
            else
            {
                responseF.Registered = false;
            }

            return responseF;

        }
        public async Task<RegistryModel> ReactPost(ReactViewModel model, string token)
        {
            var responseF = new RegistryModel();
            var request = new HttpClient();
            var Uri = URL + "/PostReacts";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var body = JsonConvert.SerializeObject(model);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                responseF.Registered = true;
            }
            else
            {
                responseF.Registered = false;
            }

            return responseF;

        }

        public async Task<List<PostMasterCollectionViewModel>> GetBookmark(string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostBookmark/GetBookmarkByUser";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<PostMasterCollectionViewModel>> GetHiddenPost(string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetHiddenPost";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<PostMasterCollectionViewModel>> GetScheduled(string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetScheduledPost";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<PostMasterCollectionViewModel>> GetImages(Guid Id, string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetPostByImages?UserId=" + Id;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<GetUserMediaViewModel>> GetUserImageContents(Guid Id, string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetUserImageContents?UserId=" + Id;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<GetUserMediaViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<PostMasterCollectionViewModel>> GetVideos(Guid user, string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetPostByVideo?UserId=" + user;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<DashboardViewModel> GetDashboardInfo(string token, int Days = 7)
        {

            var request = new HttpClient();

            var Uri = URL + "/Admin/GetDashboardInfo?Days=" + Days;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<DashboardViewModel>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<PagedResponse<List<UserDetailsViewModel>>> GetUsers(string token, int PageNumber, int PageSize, string filterString)
        {
            var request = new HttpClient();

            var Uri = URL + "/Admin/GetAll?PageNumber=" + PageNumber + "&PageSize=" + PageSize;
            if (!string.IsNullOrEmpty(filterString))
            {
                Uri = Uri + "&filterString=" + filterString;
            }

            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<PagedResponse<List<UserDetailsViewModel>>>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<List<UserDetailsViewModel>> GetNewUsers(string token, int Days = 7)
        {
            var request = new HttpClient();

            var Uri = URL + "/Admin/GetNewUsers?Days=" + Days;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<UserDetailsViewModel>>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<List<PostReportedViewModel>> GetReportedPosts(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Admin/GetReportedPosts";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostReportedViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<PostReportSummaryViewModel>> ReportedPosts(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Admin/ReportedPosts";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostReportSummaryViewModel>>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<PostReportSummaryViewModel> ReportedPostsDetails(Guid Id, string token)
        {
            var request = new HttpClient();
            var Uri = URL + "/Admin/ReportedPostDetail?Id=" + Id;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<PostReportSummaryViewModel>(result);
            }
            else
            {
                return null;
            }


        }



        public async Task<List<UserReportSummaryViewModel>> ReportedUsers(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Admin/ReportedUsers";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<UserReportSummaryViewModel>>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<UserReportSummaryViewModel> ReportedUserDetails(Guid Id, string token)
        {
            var request = new HttpClient();
            var Uri = URL + "/Admin/ReportedUserDetail?Id=" + Id;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserReportSummaryViewModel>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<BadgeViewModel> CreateBadge(BadgeViewModel model, string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Badge";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var body = JsonConvert.SerializeObject(model);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<BadgeViewModel>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<List<BadgeViewModel>> GetBadges(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Badge";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<BadgeViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<BadgeViewModel> GetBadgeById(string Id, string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Badge/" + Id;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<BadgeViewModel>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<InterestViewModel> CreateInterest(InterestViewModel model, string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Interest";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var body = JsonConvert.SerializeObject(model);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<InterestViewModel>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<InterestViewModel>> GetInterests(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Interest";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<InterestViewModel>>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<InterestViewModel> GetInterestById(string Id, string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Interest/" + Id;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<InterestViewModel>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<UserDetailsViewModel> GetUserDetail(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Authentication/GetUserDetail";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserDetailsViewModel>(result);
            }
            else
            {
                return null;
            }

        }
        public async Task<bool> VerifyUserById(string UserId, string token)
        {
            var request = new HttpClient();
            var Uri = URL + "/Admin/VerifyUserById";

            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var body = JsonConvert.SerializeObject(UserId);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<bool>(result);
            }
            else
            {
                return false;
            }

        }
        public async Task<bool> UpdateFeaturedBadge(string NewFeaturedBadgeCode, string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Authentication/UpdateFeatureBadge";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var body = JsonConvert.SerializeObject(
            NewFeaturedBadgeCode
            );
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");

            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<bool>(result);
            }
            else
            {
                return false;
            }


        }


        public async Task<List<MessageReportTypeViewModel>> GetConversationReportTypes(string token)
        {
            var request = new HttpClient();

            var Uri = URL + "/Message/ReportTypes";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                var a = JsonConvert.DeserializeObject<JsonResponse<List<MessageReportTypeViewModel>>>(result);
                return a.JsonObj;
            }
            else
            {
                return new List<MessageReportTypeViewModel>();
            }


        }

        #region ViewAsOther
        public async Task<UserDetailsViewModel> GetViewAsOtherUsersById(string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/Authentication/ViewAsOther";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserDetailsViewModel>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<PostMasterCollectionViewModel>> GetUserPostByVideoAsGuestUser(string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetUserPostByVideoAsGuestUser";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PostMasterCollectionViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<List<GetUserMediaViewModel>> GetPostByImageAsGuestUser( string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/PostMasters/GetPostByImageAsGuestUser";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<GetUserMediaViewModel>>(result);
            }
            else
            {
                return null;
            }


        }
        #endregion


        #region Settings

        public async Task<SettingsViewModel> GetSettings(string token)
        {

            var request = new HttpClient();

            var Uri = URL + "/User/Settings";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<SettingsViewModel>(result);
            }
            else
            {
                return null;
            }


        }
        #endregion
    }
}
