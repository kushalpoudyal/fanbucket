﻿using FanBucketEntity.ViewModel;
using FanBucketUI.ResponseModel;
using FanBucketUI.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketUI.API_Context
{
    public class SignalR
    {

        private string URL { get; }
        private readonly ApplicationSettings _appSettings;
        private readonly IWebHostEnvironment _hostEnvironment;
        public SignalR(IOptions<ApplicationSettings> appSettings, IWebHostEnvironment _hostEnvironment)
        {


            _appSettings = appSettings.Value;
            //if (_hostEnvironment.IsDevelopment())
            //{
            //    URL = _appSettings.DevAPIEndPoint;
            //}
            //else
            //{
            //    URL = _appSettings.APIEndPoint;
            //}
            URL = _appSettings.APIEndPoint;
        }
        public async Task<List<ChatUsersViewModel>> GetChatUsers(string token)
        {

            var request = new HttpClient();
            var Uri = URL + "/Message/GetChatUsers";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<ChatUsersViewModel>>(result);
            }
            else
            {
                return null;
            }


        }

        public async Task<int> GetMessageUnreadCount(string token)
        {

            var request = new HttpClient();
            var Uri = URL + "/Notification/GetMessageCount";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(result);
            }
            else
            {
                return -1;
            }
        }

            public async Task<ChatViewModel> GetMessageByUser(string ChatId, string UserId, string token)
        {

            var request = new HttpClient();
            var Uri = URL + "/Message/GetMessageByUser?ChatId=" + ChatId + "&&UserId=" + UserId;
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await request.GetAsync(Uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ChatViewModel>(result);
            }
            else
            {
                return null;
            }


        }
        public async Task<JsonResponse> PostMessage(PostChatViewModel model, string token)
        {

            var request = new HttpClient();
            var Uri = URL + "/Message/PostMessages";
            request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var body = JsonConvert.SerializeObject(model);
            var BodyContent = new StringContent(body, Encoding.UTF8, "application/json");
            var response = await request.PostAsync(Uri, BodyContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<JsonResponse>(result);
            }
            else
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<JsonResponse>(result);
                
            }


        }
    }
}
