﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace FanBucketUI.API_Context
{
    public class SessionHelper
    {

        public string GetName(ClaimsPrincipal UserC)
        {
            var Name = UserC.Identity.GetName();
            return Name;
        }

        public string GetProfileImg(ClaimsPrincipal UserC)
        {
            var Name = UserC.Identity.GetProfileImg();
            return Name;
        }
        public string GetCoverImg(ClaimsPrincipal UserC)
        {
            var Name = UserC.Identity.GetCoverImg();
            return Name;
        }

        public string GetUsername(ClaimsPrincipal UserC)
        {
            var Name = UserC.Identity.GetUsername();
            return Name;
        }
        public string GetId(ClaimsPrincipal UserC)
        {
            var Name = UserC.Identity.GetUserId();
            return Name;
        }
        public string GetBearer(ClaimsPrincipal UserC)
        {
            var Name = UserC.Identity.GetJWT();
            return Name;
        }


    }
}
