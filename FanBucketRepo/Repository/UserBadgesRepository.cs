﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Model;
using FanBucketEntity.Model.Rendering;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Repository
{
    public class UserBadgesRepository : Repository<UserBadges>, IUserBadgesRepository
    {

        public UserBadgesRepository(FbDbContext context) : base(context)
        {
        }

        public List<UserBadgesViewModel> GetBagesById(Guid userId)
        {
            var badges = context.UserBadges.Where(x => x.UserId == userId).Include(c => c.BadgeInfo).Select(x => new UserBadgesViewModel()
            {
                Name = x.BadgeInfo.Name,
                ImageUrl = x.BadgeInfo.ImageUrl
            }).ToList();
            return badges;
        }
    }
}
