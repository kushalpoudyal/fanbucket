﻿using FanBucketEntity.CustomExceptions;
using FanBucketEntity.DBContext;
using FanBucketEntity.Extension;
using FanBucketEntity.Model.Chat;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketRepo.Repository
{
    public class ChatRepository : Repository<ChatUser>, IChatRepository
    {
        private readonly IMessageRepository messageRepository;

        public ChatRepository(FbDbContext context, IMessageRepository messageRepository) : base(context)
        {
            this.messageRepository = messageRepository;
        }

        public async Task<ChatViewModel> GetMessagesOfChat(Guid ChatId, Guid CurrId, Guid UserId)
        {
            var model = new ChatViewModel();

            var DeltedMessageIdsByCurrentUsers = await context.MessagesDeletedByUser.Where(x => x.UserId == CurrId).Select(x => x.MessageId).ToArrayAsync();
            var data = await context.Message.Where(x => x.ChatId == ChatId && x.DeletedStatus == false && !DeltedMessageIdsByCurrentUsers.Contains(x.Id)).ToListAsync();

            var userINfo = await context.Users.Where(x => x.Id == UserId).FirstOrDefaultAsync();

            model.ChatId = ChatId;
            model.UserId = UserId;
            model.ChatWithName = userINfo.Fullname;
            model.ChatWithUserName = userINfo.UserName;
            model.ProfileImgLocation = userINfo.ProfileImgLocation;

            //var IsConversationBlocked = context.ChatUserBlockedByUser.Any(x => x.ChatId == model.ChatId && (x.BlockedByUserId == CurrId || x.UserId == UserId));
            var IsConversationBlocked = context.ChatUserBlockedByUser.Any(x => x.ChatId == model.ChatId && (x.BlockedByUserId == CurrId || x.UserId == CurrId));
            var IsBlockedByCurrentUser = context.ChatUserBlockedByUser.Any(x => x.ChatId == model.ChatId && x.BlockedByUserId == CurrId);

            model.IsBlocked = IsConversationBlocked;
            model.Blocking = IsBlockedByCurrentUser;
            model.ChatMessages = data.Select(x => new ChatMessageViewModel()
            {
                Id = x.Id,
                UserId = x.UserId,

                Message = x.Message,
                When = x.When,
                DateString = x.When.Stringify(),
                Username = context.Users.Where(y => y.Id == x.UserId).FirstOrDefault().UserName,

                FileLocation = x.FileLocation,
                TipAmount = x.TipAmount,
                HasRead = x.HasRead,
                Attachments = context.MessagesAttachments.Where(z => z.MessageId == x.Id).OrderBy(x => x.When).Select(attach => new MessagesAttachmentViewModel()
                {
                    FileLocation = attach.FileLocation
                }).ToList()

            }).ToList();
            await UpdateChatStatus(CurrId, ChatId);
            return model;
        }

        public async Task<List<ChatUsersViewModel>> GetUserChats(Guid UserId)
        {
            var chatIds = await context.ChatUsers.Where(z => z.UserId == UserId && !z.Hidden).Select(z => z.ChatId).Distinct().ToListAsync();
            var chatId = await context.ChatUsers.Where(x => chatIds.Contains(x.ChatId)).ToListAsync();
            var ChatIds = chatId.Select(x => x.ChatId).Distinct().ToList();
            //deleted messageIDs by current user

            var DeltedMessageIdsByCurrentUsers = await context.MessagesDeletedByUser.Where(x => x.UserId == UserId).Select(x => x.MessageId).ToArrayAsync();
            var data = await context.Message.Where(x => ChatIds.Contains(x.ChatId) && x.DeletedStatus == false && !DeltedMessageIdsByCurrentUsers.Contains(x.Id)).ToListAsync();

            var IDs = chatId.Select(x => x.UserId).ToList();
            var UserD = await context.Users.Where(x => IDs.Contains(x.Id) && x.Id != UserId).ToListAsync();

            var info = data.GroupBy(x => x.ChatId).Select(x =>
            {
                var LastMessage = x.Where(z => z.ChatId == x.Key).OrderByDescending(x => x.When).Select(x => new { x.Message, x.Id }).FirstOrDefault();

                ChatUsersViewModel chatUsersViewModel = new ChatUsersViewModel()
                {
                    ChatId = x.Key,
                    LastMessage = LastMessage.Message,
                    //HasFile = !string.IsNullOrEmpty(x.Where(z => z.ChatId == x.Key).OrderByDescending(x => x.When).Select(x => x.FileLocation).FirstOrDefault()),

                    recentTime = x.Where(z => z.ChatId == x.Key).Max(z => z.When),
                    ChatWithUserName = UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.UserName).FirstOrDefault() == null ? UserD.Where(y => y.Id == (chatId.Where(z => z.ChatId == x.Key && z.UserId != UserId).Select(z => z.UserId).FirstOrDefault())).Select(y => y.UserName).FirstOrDefault() : UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.UserName).FirstOrDefault(),

                    ChatWithName = UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.Fullname).FirstOrDefault() == null ? UserD.Where(y => y.Id == (chatId.Where(z => z.ChatId == x.Key && z.UserId != UserId).Select(z => z.UserId).FirstOrDefault())).Select(y => y.Fullname).FirstOrDefault() : UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.Fullname).FirstOrDefault(),
                    ProfileImgLocation = UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.ProfileImgLocation).FirstOrDefault() == null ? UserD.Where(y => y.Id == (chatId.Where(z => z.ChatId == x.Key && z.UserId != UserId).Select(z => z.UserId).FirstOrDefault())).Select(y => y.ProfileImgLocation).FirstOrDefault() : UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.ProfileImgLocation).FirstOrDefault(),
                    //ProfileImgLocation = UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.ProfileImgLocation).FirstOrDefault() == null ? UserD.Where(y => y.Id == (chatId.Where(z => z.ChatId == x.Key && z.UserId != UserId).Select(z => z.UserId).FirstOrDefault())).Select(y => y.ProfileImgLocation).FirstOrDefault() : UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.ProfileImgLocation).FirstOrDefault(),
                    UserId = UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.Id).FirstOrDefault() == Guid.Empty ? UserD.Where(y => y.Id == (chatId.Where(z => z.ChatId == x.Key && z.UserId != UserId).Select(z => z.UserId).FirstOrDefault())).Select(y => y.Id).FirstOrDefault() : UserD.Where(y => y.Id == (x.Where(z => z.UserId != UserId).Select(x => x.UserId).FirstOrDefault())).Select(y => y.Id).FirstOrDefault(),
                    DateString = (x.Where(z => z.ChatId == x.Key).Max(z => z.When)).Stringify(),
                    HasRead = x.Any(x => x.HasRead == false && x.UserId != UserId) ? false : true,
                };
                chatUsersViewModel.HasFile = context.MessagesAttachments.Any(z => z.MessageId == LastMessage.Id);
                chatUsersViewModel.Blocking = context.ChatUserBlockedByUser.Any(z => z.ChatId == chatUsersViewModel.ChatId && z.BlockedByUserId == UserId);
                chatUsersViewModel.IsBlocked = context.ChatUserBlockedByUser.Any(z => z.ChatId == chatUsersViewModel.ChatId && z.UserId == UserId);

                return chatUsersViewModel;
            }).ToList();

            return info;
        }

        public async Task<int> GetUnreadMessagesCount(Guid CurrId)
        {
            var chatIds = await context.ChatUsers.Where(z => z.UserId == CurrId).Select(z => z.ChatId).Distinct().ToListAsync();
            var chatId = await context.ChatUsers.Where(x => chatIds.Contains(x.ChatId)).ToListAsync();
            var ChatIds = chatId.Select(x => x.ChatId).Distinct().ToList();
            var data = await context.Message.Where(x => ChatIds.Contains(x.ChatId) && x.DeletedStatus == false).ToListAsync();

            var info = data.Where(x => x.HasRead == false && x.UserId != CurrId).GroupBy(x => x.ChatId).Count();

            return info;
        }

        public async Task<bool> UpdateChatStatus(Guid UserId, Guid ChatId)
        {
            try
            {
                var data = messageRepository.GetAll();
                var Chat = data.Where(x => x.ChatId == ChatId && x.UserId != UserId && x.HasRead == false).ToList();
                foreach (var item in Chat)
                {
                    item.HasRead = true;
                    item.ReadDate = DateTime.UtcNow;
                }

                context.Message.UpdateRange(Chat);

                //change hidden to shown
                var chatuser = await context.ChatUsers.FirstOrDefaultAsync(x => x.UserId == UserId && x.ChatId == ChatId);
                if (chatuser != null)
                {
                    chatuser.Hidden = false;
                }

                await context.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> MarkAsUnread(Guid UserId, Guid ChatId)
        {
            try
            {
                var data = messageRepository.GetAll();
                var lastMessage = data.Where(x => x.ChatId == ChatId && x.UserId != UserId).OrderByDescending(x => x.When).FirstOrDefault();
                if (lastMessage != null)
                {
                    lastMessage.HasRead = false;
                    lastMessage.ReadDate = null;

                    context.Message.Update(lastMessage);

                    await context.SaveChangesAsync();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }


        public async Task<bool> HideConversation(Guid UserId, Guid ChatId)
        {
            try
            {
                var UserChatExists = await context.ChatUsers.AnyAsync(x => x.ChatId == ChatId && x.UserId == UserId);
                if (!UserChatExists)
                {
                    return false;
                }
                var chatuser = await context.ChatUsers.FirstOrDefaultAsync(x => x.UserId == UserId && x.ChatId == ChatId);
                if (chatuser != null)
                {
                    chatuser.Hidden = true;
                    await context.SaveChangesAsync();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public async Task<bool> BlockMessage(Guid ChatId, Guid CurrentId, Guid UserId)
        {
            try
            {
                var HasTargetUserIDInConversation = await context.ChatUsers.AnyAsync(x => x.ChatId == ChatId && x.UserId == UserId);
                if (!HasTargetUserIDInConversation)
                {
                    return false;
                }
                var HasCurrenUserIDInConversation = await context.ChatUsers.AnyAsync(x => x.ChatId == ChatId && x.UserId == CurrentId);
                if (!HasCurrenUserIDInConversation)
                {
                    return false;
                }
                var HasPendingBlock = await context.ChatUserBlockedByUser.AnyAsync(x => x.ChatId == ChatId && x.BlockedByUserId == CurrentId);

                if (HasPendingBlock)
                {
                    return true;

                }

                var ChatUserBlockedByUser = new ChatUserBlockedByUser()
                {
                    ChatId = ChatId,
                    UserId = UserId,
                    BlockedByUserId = CurrentId,
                    Id = Guid.NewGuid(),
                    BlockedDate = DateTime.UtcNow,

                };
                await context.ChatUserBlockedByUser.AddAsync(ChatUserBlockedByUser);
                await context.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UnblockMessage(Guid ChatId, Guid CurrentId, Guid UserId)
        {
            try
            {
                var HasTargetUserIDInConversation = await context.ChatUsers.AnyAsync(x => x.ChatId == ChatId && x.UserId == UserId);
                if (!HasTargetUserIDInConversation)
                {
                    return false;
                }
                var HasCurrenUserIDInConversation = await context.ChatUsers.AnyAsync(x => x.ChatId == ChatId && x.UserId == CurrentId);
                if (!HasCurrenUserIDInConversation)
                {
                    return false;
                }
                var ChatUserBlockedByUser = await context.ChatUserBlockedByUser.FirstOrDefaultAsync(x => x.ChatId == ChatId && x.BlockedByUserId == CurrentId && x.UserId == UserId);

                if (ChatUserBlockedByUser != null)
                {
                    context.ChatUserBlockedByUser.Remove(ChatUserBlockedByUser);
                    await context.SaveChangesAsync();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DeleteConversation(Guid CurrentId, Guid ChatId)
        {
            try
            {
                var data = messageRepository.GetAll();

                var alreadyDeleteMessageIds = await context.MessagesDeletedByUser.Where(x => x.UserId == CurrentId).Select(x => x.MessageId).ToArrayAsync();

                //get all non deleted conversation message ids
                var lastMessage = await data.Where(x => x.ChatId == ChatId && !alreadyDeleteMessageIds.Contains(x.Id)).Select(x => x.Id).ToArrayAsync();
                var deletedMessageList = new List<MessagesDeletedByUser>();

                foreach (var MessageId in lastMessage)
                {
                    var messageDelete = new MessagesDeletedByUser()
                    {
                        Id = Guid.NewGuid(),
                        CreatedDate = DateTime.UtcNow,
                        Deleted = true,
                        MessageId = MessageId,
                        UserId = CurrentId
                    };

                    deletedMessageList.Add(messageDelete);
                }
                if (lastMessage.Length > 0)
                {
                    await context.MessagesDeletedByUser.AddRangeAsync(deletedMessageList);
                    await context.SaveChangesAsync();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> Delete(Guid CurrentId, Guid MessageId)
        {
            try
            {
                var data = await messageRepository.GetAll().FirstOrDefaultAsync(x => x.Id == MessageId);
                if (data == null)
                {
                    return false;
                }

                if (data != null)
                {
                    var messageDelete = new MessagesDeletedByUser()
                    {
                        Id = Guid.NewGuid(),
                        CreatedDate = DateTime.UtcNow,
                        Deleted = true,
                        MessageId = MessageId,
                        UserId = CurrentId
                    };
                    await context.MessagesDeletedByUser.AddAsync(messageDelete);
                    await context.SaveChangesAsync();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        //public async Task<bool> DeleteChatId(Guid ChatId)
        //{
        //    try
        //    {
        //        var Chat = await context.Message.Where(x => x.ChatId == ChatId && x.UserId != ).ToListAsync();
        //        Chat.Where(x => x.HasRead == false).Select(x => { x.HasRead = true; return x; }).FirstOrDefault();
        //        Chat.Where(x => x.HasRead == false).Select(x => { x.ReadDate = DateTime.UtcNow; return x; }).FirstOrDefault();
        //        context.Message.UpdateRange(Chat);

        //        await context.SaveChangesAsync();

        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }

        //}
        public async Task<List<OnlineUserViewMOdel>> GetOnlineUserById(Guid UserId)
        {
            var Chats = context.ChatUsers.Where(x => x.UserId == UserId).ToList();
            var ChatId = Chats.Select(x => x.ChatId);
            var UserChat = context.ChatUsers.Where(x => ChatId.Contains(x.ChatId) && x.UserId != UserId).Select(x => new
            {
                UserId = x.UserId,
                ChatId = x.ChatId
            }).ToList();
            if (UserChat.Count() > 0)
            {
                var Users = await (from follow in context.ApplicationUserFollowlist
                                   join user in context.Users on follow.UserId equals user.Id
                                   where follow.FollowedBy == UserId
                                   && follow.Record_Status == Record_Status.Active
                                   select new OnlineUserViewMOdel()
                                   {
                                       ProfileImg = user.ProfileImgLocation,
                                       UserId = user.Id,
                                       Fullname = user.Fullname,
                                       ChatId = Guid.NewGuid()
                                   }).ToListAsync();
                //
                return Users.Select(x => new OnlineUserViewMOdel()
                {
                    ProfileImg = x.ProfileImg,
                    UserId = x.UserId,
                    Fullname = x.Fullname,
                    ChatId = UserChat.Where(z => z.UserId == x.UserId).Count() > 0 ? UserChat.Where(z => z.UserId == x.UserId).Select(z => z.ChatId).FirstOrDefault() : x.ChatId
                }).ToList();
            }
            else
            {
                var Users = await (from follow in context.ApplicationUserFollowlist
                                   join user in context.Users on follow.UserId equals user.Id
                                   where follow.FollowedBy == UserId
                                   && follow.Record_Status == Record_Status.Active
                                   select new OnlineUserViewMOdel()
                                   {
                                       ProfileImg = user.ProfileImgLocation,
                                       UserId = user.Id,
                                       Fullname = user.Fullname,
                                       ChatId = Guid.NewGuid()
                                   }).ToListAsync();
                return Users;
            }
        }

        public async Task<Messages> CreateMessage(Guid chatId, string message, string FileLocation, decimal? TipAmount, Guid userId, Guid targetId, Guid MessageId, List<PostMessagesAttachmentViewModel> messagesAttachments)
        {
            if (!ChatExists(chatId))
            {
                await CreatePrivateRoom(userId, targetId, chatId);
            }
            var IsBlockedByCurrentUser = context.ChatUserBlockedByUser.Any(x => x.ChatId == chatId && x.BlockedByUserId == userId);
            if (IsBlockedByCurrentUser)
            {
                throw new CommonException("Conversation has been blocked.");
            }
            //check if blocked current user orr other user
            var IsConversationBlocked = context.ChatUserBlockedByUser.Any(x => x.ChatId == chatId && (x.BlockedByUserId == userId || x.UserId == userId));
            //var IsConversationBlocked = context.ChatUserBlockedByUser.Any(x => x.ChatId ==chatId && (x.BlockedByUserId == userId || x.UserId == targetId));
            if (IsConversationBlocked)
            {
                throw new CommonException("Conversation is blocked");
            }


            var Message = new Messages
            {
                ChatId = chatId,
                Message = message,
                UserId = userId,
                When = DateTime.Now,
                FileLocation = FileLocation,
                TipAmount = TipAmount,
                HasRead = false,
                ReadDate = null,
                Id = MessageId
            };

            context.Message.Add(Message);
            //add message attachments
            foreach (var item in messagesAttachments)
            {
                var MessagesAttachment = new MessagesAttachment()
                {
                    FileLocation = item.FileLocation,
                    Id = Guid.NewGuid(),
                    MessageId = MessageId,
                    When = DateTime.UtcNow,
                    UserId = userId,
                    ContentId = item.ContentId

                };

                context.MessagesAttachments.Add(MessagesAttachment);
            }
            //change hidden to shown
            var chatuser = await context.ChatUsers.Where(x => x.ChatId == chatId).ToListAsync();

            if (chatuser.Any())
            {
                foreach (var item in chatuser)
                {
                    item.Hidden = false;
                }
            }
            await context.SaveChangesAsync();

            
            return Message;
        }

        public bool ChatExists(Guid ChatId)
        {
            return context.Chats.Any(x => x.Id == ChatId);
        }

        public async Task<Guid> CreatePrivateRoom(Guid rootId, Guid targetId, Guid chatId)
        {
            var chat = new Chat
            {
                Id = chatId,
                Type = ChatType.Private,
                CreatedOn = DateTime.Now,
                CreatedById = rootId
            };

            chat.Users.Add(new ChatUser
            {
                UserId = targetId
            });

            chat.Users.Add(new ChatUser
            {
                UserId = rootId
            });

            context.Chats.Add(chat);
            await context.SaveChangesAsync();

            return chat.Id;
        }

        public async Task CreateRoom(string name, Guid userId)
        {
            var chat = new Chat
            {
                Id = Guid.NewGuid(),
                Name = name,
                Type = ChatType.Room,
                CreatedOn = DateTime.Now,
                CreatedById = userId,
            };

            chat.Users.Add(new ChatUser
            {
                UserId = userId,
                Role = ChatUserRole.Admin
            });

            context.Chats.Add(chat);

            await context.SaveChangesAsync();
        }

        public Chat GetChat(Guid id)
        {
            return context.Chats
                .Include(x => x.Messages)
                .FirstOrDefault(x => x.Id == id);
        }

        public List<ChatUser> GetChatUsers(Guid ChatId)
        {
            return context.ChatUsers.Where(x => x.ChatId == ChatId)
                .ToList();
        }

        public async Task<List<ChatUsersViewModel>> GetChats(Guid userId)
        {
            //var data = context.Chats.Include(x => x.Users).Where(x => x.CreatedById == userId).ToList();
            var data = await (from cts in context.ChatUsers
                              join room in context.Chats on cts.ChatId equals room.Id
                              where room.CreatedById == userId && cts.UserId != userId
                              select new ChatUsersViewModel()
                              {
                                  ChatId = cts.ChatId,
                                  LastMessage = context.Message.Where(x => x.ChatId == cts.ChatId).OrderByDescending(x => x.When).Select(x => x.Message).FirstOrDefault(),
                                  ChatWithName = context.Users.Where(x => x.Id == cts.UserId).Select(x => x.Fullname).FirstOrDefault(),
                                  recentTime = context.Message.Where(x => x.ChatId == cts.ChatId).OrderByDescending(x => x.When).Select(x => x.When).FirstOrDefault(),
                                  DateString = context.Message.Where(x => x.ChatId == cts.ChatId).OrderByDescending(x => x.When).Select(x => x.When).FirstOrDefault().Stringify(),
                              }).ToListAsync();

            return data;
        }

        public IEnumerable<Chat> GetPrivateChats(Guid userId)
        {
            return context.Chats
                   .Include(x => x.Users)
                       .ThenInclude(x => x.User)
                   .Where(x => x.Type == ChatType.Private
                       && x.Users
                           .Any(y => y.UserId == userId))
                   .ToList();
        }

        public async Task JoinRoom(Guid chatId, Guid userId)
        {
            var chatUser = new ChatUser
            {
                ChatId = chatId,
                UserId = userId,
                Role = ChatUserRole.Member
            };

            context.ChatUsers.Add(chatUser);

            await context.SaveChangesAsync();
        }

        #region ReportType
        public async Task<List<MessageReportTypeViewModel>> GetReportTypesAsync()
        {
            return await context.MessageReportType.Where(x => x.IsActive).OrderBy(x => x.Order).Select(x => new MessageReportTypeViewModel
            {

                Description = x.Description,
                Value = x.DisplayValue,
                Title = x.Title
            })
           .ToListAsync();
        }


        public async Task<bool> ReportConversationAsync(Guid chatId, Guid userId, string reportType)
        {
            var ReporrtType = await context.MessageReportType.FirstOrDefaultAsync(x => x.DisplayValue == reportType);
            if (ReporrtType != null)
            {
                var newConversationReport = new ChatReportedByUser()
                {
                    ChatId = chatId,
                    CreatedDate = DateTime.UtcNow,
                    IsAcive = true,
                    DeletedStatus = false,
                    MessageReportTypeId = ReporrtType.Id,
                    ReportedByUserId = userId,
                    Id = Guid.NewGuid()
                };

                context.ChatReportedByUser.Add(newConversationReport);
                await context.SaveChangesAsync();
            }


            return true;
        }
        #endregion
    }
}