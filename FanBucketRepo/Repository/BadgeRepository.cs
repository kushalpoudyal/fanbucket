﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Model;
using FanBucketEntity.Model.Rendering;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Repository
{
    public class BadgeRepository : Repository<BadgeInfo>, IBadgeRepository
    {
        private readonly IUserRepository _userRepository;

        public BadgeRepository(FbDbContext context, IUserRepository userRepository) : base(context)
        {
            this._userRepository = userRepository;
        }

        public async Task<BadgeInfo> GetByCode(string BadgeCode)
        {
            return await context.BadgeInfo.FirstOrDefaultAsync(x => x.Code == BadgeCode);
        }

        public async Task<UserBadges> AddUserBadgeAsync(UserBadges entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                await context.AddAsync(entity);
                await context.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be saved: {ex.Message}");
            }
        }

    }
}

