﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Model;
using FanBucketEntity.Model.Rendering;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Repository
{
    public class InterestRepository : Repository<Interest>, IInterestRepository
    {
        private readonly IUserRepository _userRepository;

        public InterestRepository(FbDbContext context, IUserRepository userRepository) : base(context)
        {
            this._userRepository = userRepository;
        }


    }
}

