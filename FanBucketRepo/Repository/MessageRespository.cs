﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Model.Chat;
using FanBucketRepo.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketRepo.Repository
{
    public class MessageRespository : Repository<Messages>, IMessageRepository
    {
        public MessageRespository(FbDbContext context) : base(context)
        {
        }
    }
}
