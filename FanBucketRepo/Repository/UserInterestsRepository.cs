﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Model;
using FanBucketEntity.Model.Rendering;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Repository
{
    public class UserInterestsRepository : Repository<UserInterests>, IUserInterestsRepository
    {
        private readonly IUserRepository _userRepository;

        public UserInterestsRepository(FbDbContext context, IUserRepository userRepository) : base(context)
        {
            this._userRepository = userRepository;
        }

        public async Task<bool> AddInterests(IList<UserInterests> userInterests)
        {

            try
            {
                await context.AddRangeAsync(userInterests);
                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"User interest could not be saved: {ex.Message}");
            }
        }
    }
}
