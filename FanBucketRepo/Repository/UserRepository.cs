﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Repository
{
    public class UserRepository : Repository<ApplicationUser>, IUserRepository
    {
        private readonly IUserBadgesRepository userBadgesRepository;
        private readonly IChatRepository chatRepository;

        public UserRepository(FbDbContext context, IUserBadgesRepository userBadgesRepository, IChatRepository chatRepository) : base(context)
        {
            this.userBadgesRepository = userBadgesRepository;
            this.chatRepository = chatRepository;
        }

        public async Task<UserDetailsViewModel> GetUserById(Guid Id, Guid CurrId)
        {
            var Query = chatRepository.GetAll();
            var Chats = Query.Where(x => x.UserId == CurrId).Select(x => x.ChatId);
            var ChatId =await context.ChatUsers.Where(x => Chats.Contains(x.ChatId) && x.UserId == Id).Select(x => x.ChatId).FirstOrDefaultAsync();
            var userBadgesId = await context.UserBadges.Where(x => x.UserId == Id).Select(x => x.BadgeId).ToArrayAsync();
            var userBadges = await context.BadgeInfo.Where(x => userBadgesId.Contains(x.Id))
                                .Select(x => new UserBadgesViewModel()
                                {
                                    Name = x.Name,
                                    ImageUrl = x.ImageUrl
                                }).ToListAsync();

            var allBadges = await context.BadgeInfo.Where(x => !x.Deleted_Status && !userBadgesId.Contains(x.Id))
                .Select(x => new UserBadgesViewModel()
                {
                    Name = x.Name,
                    ImageUrl = x.ImageUrl
                }).ToListAsync();

            var SubscribedTill = await context.UserSubscribedStatus
                .Where(x => x.CreatedById == CurrId && x.UserId == Id && x.Status == FanBucketEntity.Model.ENUM.Record_Status.Active)
                .Select(x => x.SubscribedTill)
                .FirstOrDefaultAsync();
            var RemainingDays = 0;
            if (SubscribedTill != null && SubscribedTill != DateTime.MinValue)
            {
                RemainingDays = (SubscribedTill - DateTime.Now).Days;
            }

            var ud = await context.Users.Where(x => x.Id == Id).Select(x => new UserDetailsViewModel()
            {
                Id = x.Id,
                Fullname = x.Fullname,
                Email = x.Email,
                ProfileImgLocation = string.IsNullOrEmpty(x.ProfileImgLocation) ? "/Content/gallery/userimg.png" : x.ProfileImgLocation,
                Username = x.UserName,
                UserFollowed = context.ApplicationUserFollowlist.Any(z => z.FollowedBy == CurrId && z.UserId == Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                TotalFollowers = context.ApplicationUserFollowlist.Where(z => z.UserId == Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                TotalFollowing = context.ApplicationUserFollowlist.Where(z => z.FollowedBy == Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                TotalSubscribers = context.SubscriptionByUser.Where(z => z.PaidForUserId == Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                Badges = userBadges,
                AllBadges = allBadges,
                //userBadgesRepository.GetBagesById(x.Id),
                UserInterestCount = context.UserInterests.Where(z => z.UserId == Id).Count(),
                HideNewUserBadge = x.CreatedOn.AddDays(7) >= DateTime.Now,
                Interests = context.UserInterests.Where(z => z.UserId == Id).Select(z => new SelectListItem
                {
                    Text = context.Interest.Where(y => y.Id == z.InterestId).Select(y => y.Name).FirstOrDefault(),
                    Value = z.InterestId.ToString()
                }).ToList(),
                AccountType = x.AccountType,
                ChatId = ChatId == Guid.Empty ? Guid.NewGuid() : ChatId,
                Profession = context.ApplicationUserDetails.Where(y => y.UserId == x.Id).Select(x => x.Profession).FirstOrDefault(),
                ShortBio = context.ApplicationUserDetails.Where(y => y.UserId == x.Id).Select(x => x.ShortBio).FirstOrDefault(),
                Location = context.ApplicationUserDetails.Where(y => y.UserId == x.Id).Select(x => x.Location).FirstOrDefault(),
                NSFW = context.PostMaster.Any(z => z.NSFW == true && z.CreatedById == x.Id && z.Deleted_Status == false),
                CoverImgLocation = x.CoverImgLocation,
                KYC_Details = null,
                
                SubsInfo = context.ApplicationUserSubscriptionInfo.Where(z => z.UserId == x.Id && z.Record_Status != FanBucketEntity.Model.ENUM.Record_Status.Inactive).Select(z => new ApplicationUserSubscriptionINfo
                {
                    Id = z.Id,
                    UserId = z.UserId,
                    CreatedOn = z.CreatedOn,
                    Record_Status = z.Record_Status,
                    SubscriptionAmount = z.SubscriptionAmount,
                    Tenure = z.Tenure
                }).ToList(),
                FeaturedBadgeCode = x.FeaturedBadgeId == null ? null : context.BadgeInfo.FirstOrDefault(y => y.Id == x.FeaturedBadgeId) != null ? context.BadgeInfo.FirstOrDefault(y => y.Id == x.FeaturedBadgeId).Code : null,
            }).FirstOrDefaultAsync();
            var SubIds = ud.SubsInfo.Select(x => x.Id);

            ud.HasSubscribed = context.SubscriptionByUser.Any(x => SubIds.Contains(x.SubscriptionId) && x.PaidByUserId == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active);
            //if (ud.HasSubscribed)
            //{
                ud.SubscriptionId = context.SubscriptionByUser
                    .Where(x => x.PaidByUserId == CurrId && SubIds.Contains(x.SubscriptionId) && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active)
                    .Select(x => x.SubscriptionId)
                    .FirstOrDefault();
                ud.RemainingSubscriptionDays = RemainingDays;
            //}

            return ud;
        }
        public async Task<UserDetailsViewModel> GetUserById(Guid Id)
        {
            var ud = await context.Users.Where(x => x.Id == Id).Select(x => new UserDetailsViewModel()
            {
                Id = x.Id,
                Fullname = x.Fullname,
                Email = x.Email,
                ProfileImgLocation = string.IsNullOrEmpty(x.ProfileImgLocation) ? "/Content/gallery/userimg.png" : x.ProfileImgLocation,
                Username = x.UserName,
                Profession = context.ApplicationUserDetails.Where(y => y.UserId == x.Id).Select(x => x.Profession).FirstOrDefault(),
                ShortBio = context.ApplicationUserDetails.Where(y => y.UserId == x.Id).Select(x => x.ShortBio).FirstOrDefault(),
                Location = context.ApplicationUserDetails.Where(y => y.UserId == x.Id).Select(x => x.Location).FirstOrDefault(),
                NSFW = context.PostMaster.Any(z => z.NSFW == true && z.CreatedById == x.Id && z.Deleted_Status == false),
                //UserFollowed = context.ApplicationUserFollowlist.Any(z => z.FollowedBy == CurrId && z.UserId == Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                TotalFollowers = context.ApplicationUserFollowlist.Where(z => z.UserId == Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                TotalFollowing = context.ApplicationUserFollowlist.Where(z => z.FollowedBy == Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                TotalSubscribers = context.ApplicationUserFollowlist.Where(z => z.UserId == Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                Badges = context.UserBadges.Where(x => x.UserId == Id).Include(c => c.BadgeInfo).Select(x => new UserBadgesViewModel()
                {
                    Name = x.BadgeInfo.Name,
                    ImageUrl = x.BadgeInfo.ImageUrl
                }).ToList(),//userBadgesRepository.GetBagesById(x.Id),                UserInterestCount = context.UserInterests.Where(z => z.UserId == Id).Count(),
                HideNewUserBadge = x.CreatedOn.AddDays(7) >= DateTime.Now,
                Interests = context.UserInterests.Where(z => z.UserId == Id).Select(z => new SelectListItem
                {
                    Text = context.Interest.Where(y => y.Id == z.InterestId).Select(y => y.Name).FirstOrDefault(),
                    Value = z.InterestId.ToString()
                }).ToList(),
                AccountType = x.AccountType,
                CoverImgLocation = x.CoverImgLocation,
                KYC_Details = null,
                FeaturedImageLeft = x.FeaturedImageLeft,
                FeaturedImageRight = x.FeaturedImageRight,
                SocialMediaLinks = (from SocialMediaType in context.SocialMediaType.OrderBy(x => x.SortOrder)
                                    join UserSocialMediaLink in context.UserSocialMediaLinks.Where(x => x.UserId == Id && x.IsPublic == true)
                                    on SocialMediaType.Id equals UserSocialMediaLink.SocialMediaTypeId into SM
                                    from UserSocialMediaLink in SM.DefaultIfEmpty()
                                    select new SocialMediaLinksViewModel()
                                    {
                                        BaseUrl = SocialMediaType.BaseUrl,
                                        IsPublic = UserSocialMediaLink.IsPublic,
                                        Name = SocialMediaType.DisplayText,
                                        UserName = UserSocialMediaLink.UserName,
                                        LogoUrl = SocialMediaType.LogoUrl
                                    }).ToList(),
                SubsInfo = context.ApplicationUserSubscriptionInfo.Where(z => z.UserId == x.Id && z.Record_Status != FanBucketEntity.Model.ENUM.Record_Status.Active).Select(z => new ApplicationUserSubscriptionINfo
                {
                    Id = z.Id,
                    UserId = z.UserId,
                    CreatedOn = z.CreatedOn,
                    Record_Status = z.Record_Status,
                    SubscriptionAmount = z.SubscriptionAmount,
                    Tenure = z.Tenure
                }).ToList(),
                FeaturedBadgeCode = x.FeaturedBadgeId == null ? null : context.BadgeInfo.FirstOrDefault(y => y.Id == x.FeaturedBadgeId) != null ? context.BadgeInfo.FirstOrDefault(y => y.Id == x.FeaturedBadgeId).Code : null,
            }).FirstOrDefaultAsync();
            var SubIds = ud.SubsInfo.Select(x => x.Id);

            ud.HasSubscribed = false;
            return ud;
        }
        public async Task<List<OnlineUserViewMOdel>> GetUserRelatedFollowing(Guid CurrId)
        {
            var Query = chatRepository.GetAll();
            var Chats = Query.Where(x => x.UserId == CurrId).Select(x => x.ChatId);

            var data = await (from follow in context.ApplicationUserFollowlist
                              where follow.FollowedBy == CurrId && follow.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active
                              select new OnlineUserViewMOdel
                              {

                                  ChatId = context.ChatUsers.Where(x => Chats.Contains(x.ChatId) && x.UserId == follow.UserId).Select(x => x.ChatId).FirstOrDefault(),
                                  Username = context.Users.Where(x => x.Id == follow.UserId).Select(x => x.UserName).FirstOrDefault(),
                                  Fullname = context.Users.Where(x => x.Id == follow.UserId).Select(x => x.Fullname).FirstOrDefault(),
                                  UserId = follow.UserId,
                                  ProfileImg = string.IsNullOrEmpty(context.Users.Where(x => x.Id == follow.UserId).Select(x => x.ProfileImgLocation).FirstOrDefault()) ? "/Content/gallery/userimg.png" : context.Users.Where(x => x.Id == follow.UserId).Select(x => x.ProfileImgLocation).FirstOrDefault(),
                              }).ToListAsync();

            return data.Select(x => new OnlineUserViewMOdel
            {

                ChatId = x.ChatId == Guid.Empty ? Guid.NewGuid() : x.ChatId,
                Username = x.Username,
                Fullname = x.Fullname,
                UserId = x.UserId,
                ProfileImg = x.ProfileImg
            }).ToList();
        }

        public async Task<List<OnlineUserViewMOdel>> GetUserRelatedFollowers(Guid CurrId)
        {
            var Query = chatRepository.GetAll();
            var Chats = Query.Where(x => x.UserId == CurrId).Select(x => x.ChatId);

            var data = await (from follow in context.ApplicationUserFollowlist
                              where follow.UserId == CurrId && follow.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active
                              select new OnlineUserViewMOdel
                              {
                                  ChatId = context.ChatUsers.Where(x => Chats.Contains(x.ChatId) && x.UserId == follow.UserId).Select(x => x.ChatId).FirstOrDefault(),

                                  Username = context.Users.Where(x => x.Id == follow.FollowedBy).Select(x => x.UserName).FirstOrDefault(),
                                  Fullname = context.Users.Where(x => x.Id == follow.FollowedBy).Select(x => x.Fullname).FirstOrDefault(),
                                  UserId = follow.FollowedBy,
                                  ProfileImg = string.IsNullOrEmpty(context.Users.Where(x => x.Id == follow.FollowedBy).Select(x => x.ProfileImgLocation).FirstOrDefault()) ? "/Content/gallery/userimg.png" : context.Users.Where(x => x.Id == follow.FollowedBy).Select(x => x.ProfileImgLocation).FirstOrDefault(),
                              }).ToListAsync();
            return data.Select(x => new OnlineUserViewMOdel
            {

                ChatId = x.ChatId == Guid.Empty ? Guid.NewGuid() : x.ChatId,
                Username = x.Username,
                Fullname = x.Fullname,
                UserId = x.UserId,
                ProfileImg = x.ProfileImg,

            }).ToList();
        }
        public async Task<List<OnlineUserViewMOdel>> GetUserRelatedSubscribers(Guid CurrId)
        {
            var Query = chatRepository.GetAll();
            var Chats = Query.Where(x => x.UserId == CurrId).Select(x => x.ChatId);

            var data = await (from follow in context.SubscriptionByUser
                              where follow.PaidForUserId == CurrId
                               && follow.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active
                              select new OnlineUserViewMOdel
                              {
                                  ChatId = context.ChatUsers.Where(x => Chats.Contains(x.ChatId) && x.UserId == follow.PaidByUserId).Select(x => x.ChatId).FirstOrDefault(),


                                  Username = context.Users.Where(x => x.Id == follow.PaidByUserId).Select(x => x.UserName).FirstOrDefault(),
                                  Fullname = context.Users.Where(x => x.Id == follow.PaidByUserId).Select(x => x.Fullname).FirstOrDefault(),
                                  UserId = follow.PaidByUserId,
                                  ProfileImg = string.IsNullOrEmpty(context.Users.Where(x => x.Id == follow.PaidByUserId).Select(x => x.ProfileImgLocation).FirstOrDefault()) ? "/Content/gallery/userimg.png" : context.Users.Where(x => x.Id == follow.PaidByUserId).Select(x => x.ProfileImgLocation).FirstOrDefault(),
                              }).ToListAsync();
            return data.Select(x => new OnlineUserViewMOdel
            {

                ChatId = x.ChatId == Guid.Empty ? Guid.NewGuid() : x.ChatId,
                Username = x.Username,
                Fullname = x.Fullname,
                UserId = x.UserId,
                ProfileImg = x.ProfileImg
            }).ToList();
        }
        public async Task<UserDetailsViewModel> GetUserDetails(Guid CurrId)
        {
            var data = await (from usr in context.Users
                              join usrdtls in context.ApplicationUserDetails on usr.Id equals usrdtls.UserId into yG
                              from y1 in yG.DefaultIfEmpty()

                              where usr.Id == CurrId && usr.AccountType != FanBucketEntity.Model.ENUM.AccountType.Superadmin
                              select new UserDetailsViewModel
                              {
                                  Id = usr.Id,
                                  Fullname = usr.Fullname,
                                  Username = usr.UserName,
                                  LatitudeCoordinate = y1.LatitudeCoordinate,
                                  LongitudeCoordinate = y1.LongitudeCoordinate,
                                  Location = y1.Location,
                                  Profession = y1.Profession,
                                  ShortBio = y1.ShortBio,
                                  ProfileImgLocation = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation,
                                  Badges = context.UserBadges.Where(x => x.UserId == CurrId).Include(c => c.BadgeInfo)
                                  .Select(x => new UserBadgesViewModel()
                                  {
                                      Name = x.BadgeInfo.Name,
                                      ImageUrl = x.BadgeInfo.ImageUrl
                                  }).ToList(),//userBadgesRepository.GetBagesById(x.Id),
                                  UserInterestCount = context.UserInterests.Where(z => z.UserId == CurrId).Count(),
                                  HideNewUserBadge = usr.CreatedOn.AddDays(7) >= DateTime.Now,
                                  Email = usr.Email,
                                  NSFW = usr.NSFW,
                                  Interests = context.UserInterests.Where(z => z.UserId == CurrId).Select(z => new SelectListItem
                                  {
                                      Text = context.Interest.Where(y => y.Id == z.InterestId).Select(y => y.Name).FirstOrDefault(),
                                      Value = z.InterestId.ToString()
                                  }).ToList(),
                                  AccountType = usr.AccountType,
                                  CoverImgLocation = usr.CoverImgLocation,
                                  KYC_Details = context.ApplicationUserKYC.Where(z => z.UserId == usr.Id).Select(z => new EditKycViewModel
                                  {
                                      IdNO = z.IdNO,
                                      ContactNumber = z.ContactNumber,
                                      Country = z.Country,
                                      DOB = z.DOB.ToShortDateString(),
                                      Gender = z.Gender,
                                      Identification = z.Identification,
                                      IdIssuedFrom = z.IdIssuedFrom
                                  }).FirstOrDefault(),
                                  SubsInfo = context.ApplicationUserSubscriptionInfo
                                  .Where(z => z.UserId == usr.Id && z.Record_Status != FanBucketEntity.Model.ENUM.Record_Status.Inactive)
                                  .Select(z => new ApplicationUserSubscriptionINfo
                                  {
                                      Id = z.Id,
                                      UserId = z.UserId,
                                      CreatedOn = z.CreatedOn,
                                      Record_Status = z.Record_Status,
                                      SubscriptionAmount = z.SubscriptionAmount,
                                      Tenure = z.Tenure
                                  }).ToList(),

                                  UserNameModifiedTimes = usr.UserNameModifiedTimes,
                                  LastUserNameModifiedDate = usr.LastUserNameModifiedDate,


                              }).FirstOrDefaultAsync();


            var SubIds = data.SubsInfo.Select(x => x.Id);

            data.HasSubscribed = context.SubscriptionByUser.Any(x => SubIds.Contains(x.SubscriptionId) && x.PaidByUserId == CurrId);
            if (data.HasSubscribed)
            {
                data.SubscriptionId = context.SubscriptionByUser.Where(x => x.PaidByUserId == CurrId && SubIds.Contains(x.SubscriptionId)).Select(x => x.SubscriptionId).FirstOrDefault();
            }
            return data;
        }
        public bool UserAccessAllowedF(Guid UserId, Guid CurrId)//FollowerCheck
        {
            if (context.Users.Where(x => x.Id == UserId && x.AccountType == FanBucketEntity.Model.ENUM.AccountType.Private).Count() > 0)
            {
                return context.ApplicationUserFollowlist.Any(x => x.FollowedBy == CurrId && x.UserId == UserId);
            }
            return true;
        }
        public bool UserAccessAllowedS(Guid UserId, Guid CurrId)//SubscriptionCheck
        {
            if (context.Users.Where(x => x.Id == UserId && x.AccountType == FanBucketEntity.Model.ENUM.AccountType.Private).Count() > 0)
            {
                var Subcriptions = context.ApplicationUserSubscriptionInfo.Where(x => x.UserId == UserId).Select(x => x.Id).ToList();
                return context.SubscriptionByUser.Any(x => x.SubscribedOn >= DateTime.Now && x.SubscribedTill >= DateTime.Now
                && Subcriptions.Contains(x.SubscriptionId) && x.PaidByUserId == CurrId
                );
            }
            return true;
        }

        public async Task<List<BadgeViewModel>> GetUserBadgesById(Guid id)
        {
            var UserBadgeList = await context.UserBadges.Include(x => x.BadgeInfo).Where(x => x.UserId == id).Select(x => new BadgeViewModel()
            {
                Code = x.BadgeInfo.Code,
                Id = x.BadgeInfo.Id,
                Name = x.BadgeInfo.Name,
                ImageUrl = x.BadgeInfo.ImageUrl
            }).ToListAsync();
            //var UserBadgesDto=_
            return UserBadgeList;

        }

        public async Task<bool> UpdateUserBadge(Guid id, string NewFeaturedBadgeCode)
        {
            var user = await GetById(id);
            if (user == null)
            {
                return false;
            }
            user.FeaturedBadgeId = Guid.Empty;

            if (!string.IsNullOrEmpty(NewFeaturedBadgeCode))
            {
                var badge = context.BadgeInfo.FirstOrDefault(x => x.Code == NewFeaturedBadgeCode);
                if (badge == null)
                {
                    return false;
                }
                var UserBadgeList = await context.UserBadges.FirstOrDefaultAsync(x => x.BadgeId == badge.Id && x.UserId == id);
                if (badge == null)
                {
                    return false;
                }
                user.FeaturedBadgeId = badge.Id;

            }
            context.Entry(user).State = EntityState.Modified;
            await context.CommitAsync();
            //var UserBadgesDto=_
            return true;

        }
    }
}
