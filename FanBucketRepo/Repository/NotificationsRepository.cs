﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Extension;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Repository
{
    public class NotificationsRepository : Repository<AppNotifications>, INotificationsRepository
    {
        public NotificationsRepository(FbDbContext context) : base(context)
        {
        }

        public async Task<int> GetNotifCount(Guid CurrId)
        {
            return await context.AppNotifications.Where(x => x.PostCreatorId == CurrId && x.ActivityUserId != CurrId && x.Viewed == false).CountAsync();
        }

        public async Task<List<NotificationViewModel>> GetUserNotification(Guid CurrId)
        {
            var notif = await (from notifs in context.AppNotifications
                               join usr in context.Users on notifs.ActivityUserId equals usr.Id
                               where notifs.PostCreatorId == CurrId && notifs.ActivityUserId != CurrId
                               select new NotificationViewModel()
                               {
                                   Id = notifs.Id,
                                   ActivityId = notifs.ActivityId,
                                   CreatedOn = notifs.CreatedDate,
                                   CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation,
                                   Fullname = usr.Fullname,
                                   Message = "",
                                   PostId = notifs.PKId,
                                   UserId = notifs.ActivityUserId,
                                   Viewed = notifs.Viewed,
                                   nType = notifs.nType,
                                   Username = usr.UserName,
                                   DateString = notifs.CreatedDate.Stringify()

                               }).ToListAsync();

            return notif;


        }
        public async Task<List<NotificationViewModel>> GetUserNotificationByType(Guid CurrId, NotificationType type)
        {
            var notif = await (from notifs in context.AppNotifications
                               join usr in context.Users on notifs.PostCreatorId equals usr.Id
                               where notifs.PostCreatorId == CurrId && notifs.nType == type && notifs.ActivityUserId != CurrId
                               select new NotificationViewModel()
                               {
                                   ActivityId = notifs.ActivityId,
                                   CreatedOn = notifs.CreatedDate,
                                   CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation,
                                   Fullname = usr.Fullname,
                                   Message = "",
                                   PostId = notifs.PKId,
                                   UserId = notifs.ActivityUserId,
                                   Viewed = notifs.Viewed

                               }).ToListAsync();

            return notif;


        }
        public async Task<bool> MarkAllAsRead(Guid UserId)
        {
            var notifications = await (from notifs in context.AppNotifications
                                       join usr in context.Users on notifs.ActivityUserId equals usr.Id
                                       where notifs.PostCreatorId == UserId && notifs.ActivityUserId != UserId
                                       select notifs
                                      ).ToListAsync();
            Parallel.ForEach(notifications, notification =>
            {
                notification.Viewed = true;
            });
            context.UpdateRange(notifications);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
