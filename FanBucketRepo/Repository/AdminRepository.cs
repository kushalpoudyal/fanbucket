﻿using FanBucketAPI.ViewModel;
using FanBucketEntity.DBContext;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.Model.Rendering;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using FanBucketRepo.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketRepo.Repository
{
    public class AdminRepository : Repository<ApplicationUser>, IAdminRepository
    {
        private readonly IUserBadgesRepository userBadgesRepository;
        private readonly IUserRepository _userRepository;

        public AdminRepository(FbDbContext context,

            IUserBadgesRepository userBadgesRepository,
            IUserRepository userRepository) : base(context)
        {
            this.userBadgesRepository = userBadgesRepository;
            this._userRepository = userRepository;
        }

        public async Task<DashboardViewModel> GetDashBoardValues(DateTime FromDate, DateTime ToDate)
        {
            var model = new DashboardViewModel();
            var newusr = await context.Users.Where(x => x.CreatedOn >= FromDate && x.CreatedOn <= ToDate).ToListAsync();
            var userreportslist = await context.UserReportedByUser.Where(x => x.Record_Status == Record_Status.Active).ToListAsync();
            var reportedPosts =  context.PostReportingByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).ToList();
            model.NewUsers = new List<SelectListVal>(newusr.GroupBy(x => x.CreatedOn.ToString("MMM", CultureInfo.InvariantCulture)).Select(x => new SelectListVal() { title = x.Key, value = x.Count().ToString() }));
            model.TotalUsers = await context.Users.CountAsync();
            model.ReportedUsers = new List<SelectListVal>(reportedPosts.GroupBy(x => x.CreatedOn.ToString("MMM", CultureInfo.InvariantCulture)).Select(x => new SelectListVal() { title = x.Key, value = x.Count().ToString() }));
            //model.TotalUsers = await context.Users.CountAsync();
            model.TotalEarnings = await context.PaymentCreditByUser.Where(x => x.UserId == Guid.Parse("4fd83189-3f7a-4508-8bdb-e68b40d41c64")).Select(x => x.CurrentBalance).FirstOrDefaultAsync();
            model.NewUsersTotal = newusr.Count();
            model.ReportedPostsTotal = reportedPosts.GroupBy(x=>x.PostId).DefaultIfEmpty().Count();
            model.ReportedUsersCount =userreportslist.GroupBy(x=>x.UserId).DefaultIfEmpty().Count();

            return model;
        }

        public async Task<UserInteractionViewModel> GetUserMediaData(Guid CurrId)
        {
            var model = new UserInteractionViewModel();
            var reporteduser = await context.PostReportingByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).ToListAsync();

            var mysubs = await context.ApplicationUserSubscriptionInfo.Where(x => x.UserId == CurrId).Select(x => x.Id).ToListAsync();
            var myPosts = await context.PostMaster.Where(x => x.CreatedById == CurrId).Select(x => x.Id).ToListAsync();
            var mySubTranId = await context.SubscriptionByUser.Where(x => mysubs.Contains(x.SubscriptionId)).Select(x => x.TranId).ToListAsync();

            model.TotalSubscribers = await context.SubscriptionByUser.Where(x => mysubs.Contains(x.SubscriptionId)).CountAsync();
            model.TotalLikes = await context.PostReacts.Where(x => myPosts.Contains(x.PostId) && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).CountAsync();
            model.ProfileViews = 0;
            model.TotalEarnings = await context.PaymentTransactionByUser.Where(x => x.UserId == CurrId && x.DebitCredit == "C" && x.ResponseCode == FanBucketEntity.Model.ENUM.TransactionResponseCode.OK).SumAsync(x => x.Amount);
            model.EarningByPost = await context.PaymentPostByUser.Where(x => myPosts.Contains(x.Id)).SumAsync(x => x.PaidAmount);
            model.EarningBySubscription = await context.PaymentTransactionByUser.Where(x => mySubTranId.Contains(x.TranId) && x.UserId == CurrId).SumAsync(x => x.Amount);
            model.NewUser = await context.SubscriptionByUser.Where(x => mysubs.Contains(x.SubscriptionId) && x.SubscribedOn >= DateTime.Now.AddDays(-30)).CountAsync();
            model.TotalReported = await context.PostReportingByUser.Where(x => myPosts.Contains(x.PostId)).CountAsync();

            return model;
        }

        public async Task<List<PostReportedViewModel>> GetReportedPosts()
        {
            var reportedPostList = await context.PostReportingByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Select(x =>
            new PostReportedViewModel()
            {
                Id = x.Id,
                PostId = x.PostId,
                CreatedOn = x.CreatedOn,
                Record_Status = x.Record_Status,
                Remarks = x.Remarks,
                ReportedById = x.ReportedById,
                ReportingReason = x.ReportingReason,
                //UserDetailsViewModel=await _userRepository.GetUserById(x.ReportedById, x.ReportedById)
            }).ToListAsync();
            return reportedPostList;
        }

        public async Task<List<PostReportSummaryViewModel>> ReportedPosts()
        {
            var activePostReports = context.PostReportingByUser.Where(x => x.Record_Status == Record_Status.Active);
            var activePostReportsIds = activePostReports.Select(x => x.PostId).ToArray();
            var post = context.PostMaster.Where(x => activePostReportsIds.Contains(x.Id) && x.Record_Status == Record_Status.Active && !x.Deleted_Status);

            var reportedPostList = await post.Select(x => new PostReportSummaryViewModel()
            {
                PostId = x.Id,
                PostTitle = x.PostTitle,
                NoOfReports = activePostReports.Where(y => y.PostId == x.Id).Count(),
                LastReportedDate = activePostReports.Where(y => y.PostId == x.Id).Min(x => x.CreatedOn),
                PostCreatedBy = context.Users.FirstOrDefault(y => y.Id == x.CreatedById).Fullname,
                PostCreatedById = x.CreatedById,
            }).ToListAsync();

            return reportedPostList;
        }

        public async Task<PostReportSummaryViewModel> ReportedPostWithDetail(Guid Id)
        {
            var activePostReports = context.PostReportingByUser.Where(x => x.PostId == Id && x.Record_Status == Record_Status.Active);

            var post = context.PostMaster.Where(x => x.Id == Id && x.Record_Status == Record_Status.Active && !x.Deleted_Status);
            var reportedPostDetail = await post.Select(x => new PostReportSummaryViewModel()
            {
                PostId = x.Id,
                PostTitle = x.PostTitle,
                Status=x.Record_Status,
                NoOfReports = activePostReports.Count(),
                LastReportedDate = activePostReports.Min(x => x.CreatedOn),
                PostCreatedBy = context.Users.FirstOrDefault(y => y.Id == x.CreatedById).Fullname,
                PostCreatedById = x.CreatedById,
                Reports = activePostReports.Select(y => new ProductReportDetailViewModel()
                {
                    Remarks = y.Remarks,
                    ReportingReason = y.ReportingReason,
                    CreatedOn = y.CreatedOn,
                    ReportedById = y.ReportedById,
                    ReportedByFullname=context.Users.FirstOrDefault(z=>z.Id==y.ReportedById).Fullname
                })
            }).FirstOrDefaultAsync();
            return reportedPostDetail;
        }


        public async Task<List<UserReportSummaryViewModel>> ReportedUsers()
        {
            var activeUserReports = context.UserReportedByUser.Where(x => x.Record_Status == Record_Status.Active);
            var activeReportsUserIds = activeUserReports.Select(x => x.UserId).ToArray();
            var users = context.Users.Where(x => activeReportsUserIds.Contains(x.Id) && x.Status == Record_Status.Active);

            var reportedUserSummaryList = await users.Select(x => new UserReportSummaryViewModel()
            {
                UserId = x.Id,
                UserFullname = x.Fullname,
                NoOfReports = activeUserReports.Where(y => y.UserId == x.Id).Count(),
                LastReportedDate = activeUserReports.Where(y => y.UserId == x.Id).Min(x => x.CreatedOn),
            }).ToListAsync();

            return reportedUserSummaryList;
        }

        public async Task<UserReportSummaryViewModel> ReportedUserWithDetail(Guid Id)
        {
            var activeUserReports = context.UserReportedByUser.Where(x => x.Record_Status == Record_Status.Active);
            var activeReportsUserIds = activeUserReports.Select(x => x.UserId).ToArray();
            var users = context.Users.Where(x => activeReportsUserIds.Contains(x.Id) && x.Id==Id && x.Status == Record_Status.Active);

            var reportedUserSummaryList = await users.Select(x => new UserReportSummaryViewModel()
            {
                UserId = x.Id,
                UserFullname = x.Fullname,
                NoOfReports = activeUserReports.Where(y => y.UserId == x.Id).Count(),
                LastReportedDate = activeUserReports.Where(y => y.UserId == x.Id).Min(x => x.CreatedOn),
                Reports = activeUserReports.Select(y => new UserReportDetailViewModel()
                {
                    Remarks = y.Remarks,
                    ReportingReason = y.ReportingReason,
                    CreatedOn = y.CreatedOn,
                    ReportedById = y.ReportedById,
                    ReportedByUsername = context.Users.FirstOrDefault(x => x.Id == y.ReportedById).UserName,
                    ReportedbyFullname = context.Users.FirstOrDefault(x => x.Id == y.ReportedById).Fullname
                }).ToList()
            }).FirstOrDefaultAsync();
            return reportedUserSummaryList;
        }

        public async Task<List<UserDetailsViewModel>> GetNewUsers(int Days)
        {
            var userList = await context.Users.Where(x => x.CreatedOn >= DateTime.Now.AddDays(-Days) && x.CreatedOn <= DateTime.Now)
                .Take(1000).Select(x => new UserDetailsViewModel()
                {
                    Id = x.Id,
                    Fullname = x.Fullname,
                    Email = x.Email,
                    ProfileImgLocation = string.IsNullOrEmpty(x.ProfileImgLocation) ? "/Content/gallery/userimg.png" : x.ProfileImgLocation,
                    Username = x.UserName,
                    UserFollowed = context.ApplicationUserFollowlist.Any(z => z.UserId == x.Id && z.Record_Status == Record_Status.Active),
                    TotalFollowers = context.ApplicationUserFollowlist.Where(z => z.UserId == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                    TotalFollowing = context.ApplicationUserFollowlist.Where(z => z.FollowedBy == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                    TotalSubscribers = context.ApplicationUserFollowlist.Where(z => z.UserId == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                    Badges = userBadgesRepository.GetBagesById(x.Id),
                }).ToListAsync();

            return userList;
        }

        public async Task<List<UserDetailsViewModel>> GetUsers()
        {
            var userList = await context.Users.Take(1000).Select(x => new UserDetailsViewModel()
            {
                Id = x.Id,
                Fullname = x.Fullname,
                Email = x.Email,
                ProfileImgLocation = string.IsNullOrEmpty(x.ProfileImgLocation) ? "/Content/gallery/userimg.png" : x.ProfileImgLocation,
                Username = x.UserName,
                UserFollowed = context.ApplicationUserFollowlist.Any(z => z.UserId == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                TotalFollowers = context.ApplicationUserFollowlist.Where(z => z.UserId == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                TotalFollowing = context.ApplicationUserFollowlist.Where(z => z.FollowedBy == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                TotalSubscribers = context.ApplicationUserFollowlist.Where(z => z.UserId == x.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                Badges = userBadgesRepository.GetBagesById(x.Id),
            }).ToListAsync();

            return userList;
        }

        public async Task<bool> UserCommissionReview(CommissionReviewViewModel commissionReviewViewModel, Guid LoggedInUserId)
        {
            var usr = await context.PaymentCreditByUser.FirstOrDefaultAsync(x => x.UserId == commissionReviewViewModel.UserId);
            if (usr == null)
            {
                throw new Exception("User with given Id not found");
            }

            var UserCommissionReviewLogs = new UserCommissionReviewLog()
            {
                Id = Guid.NewGuid(),
                Remarks = commissionReviewViewModel.Remarks,
                CreatedOn = DateTime.UtcNow,
                OldCommissionPer = usr.EarningPercentage,
                NewCommissionPer = commissionReviewViewModel.NewCommissionPer,
                UserId = commissionReviewViewModel.UserId,
                ReviewedBy = LoggedInUserId
            };
            usr.EarningPercentage = commissionReviewViewModel.NewCommissionPer;

            context.PaymentCreditByUser.Update(usr);
            await context.UserCommissionReviewLogs.AddAsync(UserCommissionReviewLogs);
            await context.CommitAsync();
            return true;
        }

        public async Task<bool> UserReportAction(ReportActionViewModel reportActionViewModel, Guid LoggedInUserId)
        {
            var usr = await context.Users.FirstOrDefaultAsync(x => x.Id == reportActionViewModel.UserId);
            if (usr == null)
            {
                throw new Exception($"User with given Id:{reportActionViewModel.UserId} not found");
            }

            var userModerationLog = new UserModerationLog()
            {
                Id = Guid.NewGuid(),
                Remarks = reportActionViewModel.Remarks,
                CreatedOn = DateTime.UtcNow,
                UserId = reportActionViewModel.UserId,
                CreatedById = LoggedInUserId,
                Action = reportActionViewModel.Action,
                BlockDays = reportActionViewModel.BlockDays,
            };
            var todaysUTCDate = DateTime.UtcNow;

            if (reportActionViewModel.Action == EnumReportAction.PermanentBlock)
            {
                usr.Status = Record_Status.Reported;
            }
            else if (reportActionViewModel.Action == EnumReportAction.TemporaryBlock)
            {
                //all reported is set to reported in case of temporary block block till date is updated to field
                usr.Status = Record_Status.Reported;

                switch (reportActionViewModel.BlockDays)
                {
                    case EnumDays.OneDay:
                        usr.BlockTill = todaysUTCDate.AddDays(1);
                        break;

                    case EnumDays.SevenDays:
                        usr.BlockTill = todaysUTCDate.AddDays(7);

                        break;

                    case EnumDays.FifthteenDays:
                        usr.BlockTill = todaysUTCDate.AddDays(15);
                        break;

                    case EnumDays.OneMonth:
                        usr.BlockTill = todaysUTCDate.AddMonths(1);
                        break;

                    case EnumDays.ThreeMonths:
                        usr.BlockTill = todaysUTCDate.AddMonths(3);
                        break;

                    case EnumDays.SixMonths:
                        usr.BlockTill = todaysUTCDate.AddMonths(6);
                        break;

                    case EnumDays.Permanent:
                        usr.Status = Record_Status.Reported;
                        break;
                }
            }
            else
            {
                //do nothing
            }

            context.Users.Update(usr);
            await context.UserModerationLogs.AddAsync(userModerationLog);
            await context.CommitAsync();
            return true;
        }

        public async Task<bool> PostReview(PostReviewViewModel postReviewViewModel, Guid LoggedInUserId)
        {
            var post = await context.PostMaster.FirstOrDefaultAsync(x => !x.Deleted_Status & x.Id == postReviewViewModel.PostId);
            if (post == null)
            {
                throw new Exception($"Post with given Id:{postReviewViewModel.PostId} not found");
            }
            var postRelatedReports = await context.PostReportingByUser.Where(x => x.Record_Status == Record_Status.Active && x.PostId == postReviewViewModel.PostId).ToListAsync();
            foreach (var item in postRelatedReports)
            {
                item.Record_Status = Record_Status.Inactive;//set active post reports to inactive
            }

            var userModerationLog = new PostModerationLog()
            {
                Id = Guid.NewGuid(),
                Remarks = postReviewViewModel.Remarks,
                CreatedOn = DateTime.UtcNow,
                CreatedById = LoggedInUserId,
                NewStatus = postReviewViewModel.Status,
                OldStatus = post.Record_Status,
                PostId = post.Id,
            };
            post.Record_Status = userModerationLog.NewStatus;// change the status of post according to review
            context.PostReportingByUser.UpdateRange(postRelatedReports);//set active reports to inactive

            context.PostMaster.Update(post);
            await context.PostModerationLogs.AddAsync(userModerationLog);
            await context.CommitAsync();
            return true;
        }
    }


}