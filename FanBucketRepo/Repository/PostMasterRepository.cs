﻿using FanBucketAPI.Models;
using FanBucketEntity.DBContext;
using FanBucketEntity.Extension;
using FanBucketEntity.Model.PostDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
namespace FanBucketRepo.Repository
{
    public class PostMasterRepository : Repository<PostMaster>, PostMasterIRepository
    {

        public PostMasterRepository(FbDbContext context) : base(context)
        {

        }

        public async Task<bool> AddPostCollection(List<PostCollection> entity)
        {
            try
            {
                context.PostCollection.AddRange(entity);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }
        public async Task<bool> AddPostToMoments(PostMoments entity)
        {
            try
            {
                context.PostMoments.Add(entity);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }
        public async Task<List<GetUserMediaViewModel>> GetMediaByUser(Guid CurrId)
        {
            try
            {
                var data = await (from post in context.PostMaster
                                  join col in context.PostCollection on post.Id equals col.PostId
                                  where post.CreatedById == CurrId && col.PostType == FanBucketEntity.Model.ENUM.ContentTypes.Image
                                  select new GetUserMediaViewModel()
                                  {
                                      PostId = post.Id,
                                      CollectionId = col.ContentId,
                                      FileLocation = col.FileLocation
                                  }).ToListAsync();

                return data;
            }
            catch (Exception ex)
            {

                return null;
            }

        }
        public async Task<List<GetUserMediaViewModel>> GetPublicPhotos(Guid UserId)
        {
            try
            {
                var data = await (from post in context.PostMaster
                                  join col in context.PostCollection on post.Id equals col.PostId
                                  where col.PaidContent == false && post.CreatedById == UserId && col.PostType == FanBucketEntity.Model.ENUM.ContentTypes.Image
                                  select new GetUserMediaViewModel()
                                  {
                                      PostId = post.Id,
                                      CollectionId = col.ContentId,
                                      FileLocation = col.FileLocation,
                                      //Description=post.PostTitle,

                                  }).ToListAsync();

                return data;
            }
            catch (Exception ex)
            {

                return null;
            }

        }
        public async Task<List<GetUserMediaViewModel>> GetUserImageContents(Guid UserId, Guid CurrId)
        {
            try
            {

                var dataEntity = await (from master in context.PostMaster
                                            //join usr in context.Users on master.CreatedById equals usr.Id
                                        join colection in context.PostCollection on master.Id equals colection.PostId
                                        where master.CreatedById == UserId && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        && colection.PostType == FanBucketEntity.Model.ENUM.ContentTypes.Image
                                        let IsCreator = UserId == CurrId 
                                        let HasPaid =  master.PaidPostType == FanBucketEntity.Model.ENUM.PaidPostType.Free || context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id)
                                        select new GetUserMediaViewModel()
                                        {
                                            PostId = master.Id,
                                            CollectionId = colection.ContentId,
                                            HasPaid = colection.PaidContent ? HasPaid : true,
                                            PaidContent = colection.PaidContent,
                                            FileLocation = (!colection.PaidContent || HasPaid || IsCreator) ? colection.FileLocation : "",
                                            PostTitle = master.PostTitle,
                                        }).ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// User Profile
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="CurrId"></param>
        /// <param name="PageNumber"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public async Task<List<PostMasterCollectionViewModel>> GetByUser(Guid Id, Guid CurrId, int PageNumber, int PageSize)
        {
            try
            {
                //var HasPaid = await context.PaymentPostByUser.AnyAsync(x => x.PaidById == CurrId && x.PostId == OPostId);
                var UserSubscribedIds = await GetUserSubscribedIds(CurrId);

                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        where master.CreatedById == Id && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        let HasSubscribed = UserSubscribedIds.Contains(master.CreatedById)
                                        let HasPaid = context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.PostId == master.Id)
                                        select new PostMasterCollectionViewModel()
                                        {
                                            ParentPostMasterCollectionViewModel = master.ParentPostId.HasValue == false ? null : GetSinglePostByIdSync(master.ParentPostId.Value, CurrId, context),
                                            ParentPostId = master.ParentPostId,
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },

                                            //HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                            HasPaid = HasPaid,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            HasSubscribed = HasSubscribed,

                                            collection = context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                            {
                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = ((z.PaidContent && HasPaid) || (z.PaidContent && HasSubscribed) || !z.PaidContent || master.CreatedById == CurrId) ? z.FileLocation : null,
                                                HasPaid = (z.PaidContent && HasPaid) || (z.PaidContent && HasSubscribed),
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                                TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                                TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()
                                            }).ToList()
                                        })
                                        .OrderByDescending(x => x.CreatedOn)
                                        .Skip((PageNumber - 1) * PageSize)
                                        .Take(PageSize)
                                        .ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<List<PostMasterCollectionViewModel>> GetByUserSubscribed(Guid Id, Guid CurrId, int PageNumber, int PageSize)
        {
            try
            {

                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        where master.CreatedById == Id && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        select new PostMasterCollectionViewModel()
                                        {
                                            ParentPostMasterCollectionViewModel = master.ParentPostId.HasValue == false ? null : GetSinglePostByIdSync(master.ParentPostId.Value, CurrId, context),
                                            ParentPostId = master.ParentPostId,
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            collection = context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                            {

                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = z.FileLocation,
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                                TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                                TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                            }).ToList()
                                        }).OrderByDescending(x => x.CreatedOn)
                                        .Skip((PageNumber - 1) * PageSize)
                                        .Take(PageSize)
                                        .ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<List<PostMasterCollectionViewModel>> GetPostByUsersVideo(Guid Id, Guid CurrId)
        {
            try
            {

                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        join colection in context.PostCollection on master.Id equals colection.PostId
                                        where master.CreatedById == Id && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        && colection.PostType == FanBucketEntity.Model.ENUM.ContentTypes.Video
                                        select new PostMasterCollectionViewModel()
                                        {
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            collection = context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                            {

                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = z.FileLocation,
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                                TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                                TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                            }).ToList()

                                        }).ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<List<PostMasterCollectionViewModel>> GetPostByUsersImage(Guid Id, Guid CurrId)
        {
            try
            {

                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        join colection in context.PostCollection on master.Id equals colection.PostId
                                        where master.CreatedById == Id && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        && colection.PostType == FanBucketEntity.Model.ENUM.ContentTypes.Image
                                        select new PostMasterCollectionViewModel()
                                        {
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            collection = context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                            {

                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = z.FileLocation,
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                                TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                                TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                            }).ToList()

                                        }).Distinct().ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<PostMasterCollectionViewModel> GetSingleCollectionById(Guid PostId, Guid CurrId)
        {
            try
            {
                var OPostId = context.PostCollection.Where(x => x.ContentId == PostId).Select(x => x.PostId).FirstOrDefault();
                var HasPaid = await context.PaymentPostByUser.AnyAsync(x => x.PaidById == CurrId && x.PostId == OPostId);
                var UserSubscribedIds = await GetUserSubscribedIds(CurrId);
                var dataEntity = await (from master in context.PostMaster
                                        join col in context.PostCollection on master.Id equals col.PostId
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        where col.ContentId == PostId && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        select new PostMasterCollectionViewModel()
                                        {
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = master.CreatedById == CurrId || (master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true),
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            HasSubscribed = UserSubscribedIds.Contains(master.CreatedById),
                                            collection = context.PostCollection.Where(z => z.PostId == master.Id)
                                            .Select(z => new PostCollectionViewModel
                                            {

                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = ((z.PaidContent && HasPaid) || (z.PaidContent && UserSubscribedIds.Contains(master.CreatedById)) || !z.PaidContent || master.CreatedById == CurrId) ? z.FileLocation : null,
                                                HasPaid = (z.PaidContent && HasPaid) || (z.PaidContent && UserSubscribedIds.Contains(master.CreatedById)),
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                //HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                                HasReacted = context.PostReacts.Any(x => x.PostId == z.ContentId && x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && !x.Deleted_Status),
                                                TotalComments = context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && !x.Deleted_Status).Count(),
                                                //TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                TotalReacts = context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                            }).ToList()
                                        }).FirstOrDefaultAsync();

                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<bool> UpdateCollectionAsync(PostCollection entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(entity)} entity must not be null");
            }

            try
            {
                context.Update(entity);
                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }

        public async Task<PostMasterCollectionViewModel> GetSinglePostById(Guid PostId, Guid NotifyId, Guid CurrId)
        {
            try
            {
                var OPostId = PostId;
                var HasPaid = await context.PaymentPostByUser.AnyAsync(x => x.PaidById == CurrId && x.PostId == OPostId);
                var UserSubscribedIds = await GetUserSubscribedIds(CurrId);
                var dataEntity = await (from master in context.PostMaster
                                            //join col in context.PostCollection on master.Id equals col.PostId
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        where master.Id == PostId && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        let HasSubscribed= UserSubscribedIds.Contains(master.CreatedById)
                                        select new PostMasterCollectionViewModel()
                                        {
                                            ParentPostMasterCollectionViewModel = master.ParentPostId.HasValue == false ? null : GetSinglePostByIdSync(master.ParentPostId.Value, CurrId, context),
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = HasPaid,
                                            HasSubscribed= HasSubscribed,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            //collection = context.PostCollection.Where(z => z.PostId == master.Id && ((z.PaidContent && HasPaid) || (z.PaidContent && UserSubscribedIds.Contains(master.CreatedById)) || !z.PaidContent || master.CreatedById == CurrId))
                                            collection = GetCollectionOfPost(context,CurrId,master,HasSubscribed,HasPaid)
                                        }).FirstOrDefaultAsync();
                if (NotifyId != default)
                {
                    var activenotification = await context.AppNotifications.FirstOrDefaultAsync(x => x.PostCreatorId == CurrId && !x.Viewed && x.Id == NotifyId);
                    if (activenotification != null)
                    {
                        activenotification.Viewed = true;//chaged the postnotifcation to viewed
                        await context.SaveChangesAsync();


                    }
                }

                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static PostMasterCollectionViewModel GetSinglePostByIdSync(Guid PostId, Guid CurrId, FbDbContext _context)
        {
            try
            {

                var dataEntity = (from master in _context.PostMaster
                                      //join col in _context.PostCollection on master.Id equals col.PostId
                                  join usr in _context.Users on master.CreatedById equals usr.Id
                                  where master.Id == PostId && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                  select new PostMasterCollectionViewModel()
                                  {
                                      PostId = master.Id,
                                      PostTitle = master.PostTitle,
                                      PaidPostType = master.PaidPostType,
                                      PostPrivacy = master.PostPrivacy,
                                      PostPrice = master.PostPrice,
                                      CreatedOn = master.CreatedOn,
                                      NSFW = master.NSFW,
                                      Scheduled = master.Scheduled,
                                      ScheduledFor = master.ScheduledTime,
                                      TotalComments = _context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                      DateString = master.CreatedOn.Stringify(),
                                      Creator = new CreatorViewModel
                                      {
                                          Creator = usr.Fullname,
                                          CreatorUsrName = usr.UserName,
                                          CreatorId = usr.Id,
                                          CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                      },
                                      HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? _context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                      HasReacted = _context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                      HasBookMarked = _context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                      TotalReacts = _context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                      collection = _context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                      {

                                          ContentId = z.ContentId,
                                          ContentTitle = z.ContentTitle,
                                          FileLocation = z.FileLocation,
                                          PaidPost = z.PaidContent,
                                          PostType = z.PostType,
                                          HasReacted = _context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? _context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : _context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                          TotalComments = _context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? _context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : _context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                          TotalReacts = _context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? _context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : _context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                          HasBookMarked = _context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                          HasVoted = _context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                          TotalVote = _context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                      }).ToList()
                                  }).FirstOrDefault();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //main feeed
        public async Task<List<PostMasterCollectionViewModel>> GetFeedByUser(Guid CurrId, int PageNumber = 1, int PageSize = 10)
        {
            try
            {
                var UserSubscribedIds = await GetUserSubscribedIds(CurrId);
                var FollowingId = context.ApplicationUserFollowlist.Where(x => x.FollowedBy == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Select(x => x.UserId).ToList();
                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        where master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public && master.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active
                                        && (FollowingId.Contains(master.CreatedById) || master.CreatedById == CurrId)
                                        let HasSubscribed = UserSubscribedIds.Contains(master.CreatedById)
                                        let HasPaid = context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.PostId == master.Id)
                                        select new PostMasterCollectionViewModel()
                                        {
                                            ParentPostMasterCollectionViewModel = master.ParentPostId.HasValue == false ? null : GetSinglePostByIdSync(master.ParentPostId.Value, CurrId, context),
                                            ParentPostId = master.ParentPostId,
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            HasSubscribed = HasSubscribed,
                                            collection = GetCollectionOfPost(context, CurrId, master, HasSubscribed, HasPaid)
                                        })
                                        .OrderByDescending(x => x.CreatedOn)

                                        .Skip((PageNumber - 1) * PageSize)
                                        .Take(PageSize)
                                        .ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static List<PostCollectionViewModel> GetCollectionOfPost(FbDbContext _context, Guid CurrId, PostMaster master, bool HasSubscribed, bool HasPaid)
        {
            return _context.PostCollection.Where(z => z.PostId == master.Id)
                .Select(z => new PostCollectionViewModel
                {
                    ContentId = z.ContentId,
                    ContentTitle = z.ContentTitle,
                    FileLocation = ((z.PaidContent && HasPaid) || (z.PaidContent && HasSubscribed) || !z.PaidContent || master.CreatedById == CurrId) ? z.FileLocation : null,
                    HasPaid = (z.PaidContent && HasPaid) || (z.PaidContent && HasSubscribed),
                    PaidPost = z.PaidContent,
                    PostType = z.PostType,
                    HasReacted = _context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? _context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : _context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                    TotalComments = _context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? _context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : _context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                    TotalReacts = _context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? _context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : _context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                    HasBookMarked = _context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                    HasVoted = _context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                    TotalVote = _context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()
                }).ToList();
        }

        public async Task<List<PostMasterCollectionViewModel>> GetFeedByUserNSFW(Guid CurrId, int PageNumber = 1, int PageSize = 10)
        {
            try
            {
                //var ActiveUserSubscribedIds = await context.UserSubscribedStatus.Where(x => x.CreatedById == CurrId && x.SubscribedTill >= DateTime.Now).Select(x => x.UserId).ToListAsync();
                var UserSubscribedIds = await GetUserSubscribedIds(CurrId);
                var FollowingId = context.ApplicationUserFollowlist.Where(x => x.FollowedBy == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Select(x => x.UserId).ToList();
                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        where master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public && master.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active
                                        && (FollowingId.Contains(master.CreatedById) || master.CreatedById == CurrId)
                                        let HasSubscribed = UserSubscribedIds.Contains(master.CreatedById)
                                        let HasPaid = context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.PostId == master.Id)
                                        select new PostMasterCollectionViewModel()
                                        {
                                            ParentPostMasterCollectionViewModel = master.ParentPostId.HasValue == false ? null : GetSinglePostByIdSync(master.ParentPostId.Value, CurrId, context),
                                            ParentPostId = master.ParentPostId,
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = false,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = HasPaid,
                                            HasSubscribed = HasSubscribed,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            collection = GetCollectionOfPost(context, CurrId, master, HasSubscribed, HasPaid)

                                        })
                                        .OrderByDescending(x => x.CreatedOn)
                                        .Skip((PageNumber - 1) * PageSize)
                                        .Take(PageSize)
                                        .ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private async Task<List<Guid>> GetUserSubscribedIds(Guid CurrId)
        {
            //return await context.SubscriptionByUser
            //                 .Where(x => x.PaidByUserId == CurrId && x.SubscribedTill >= DateTime.Now && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active)
            //                 .Select(x => x.PaidForUserId).ToListAsync();
            return await context.UserSubscribedStatus.Where(x => x.CreatedById == CurrId && x.SubscribedTill >= DateTime.Now).Select(x => x.UserId).ToListAsync();
        }

        private static List<CreatorViewModel> OptionVotedByUser(Guid OptionId, FbDbContext fbDbContext)
        {
            var UserIDs = fbDbContext.PostOptionByUser
            .Where(op => op.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && op.OptionId == OptionId)
            .Select(voterId => voterId.UserId).ToList();
            return fbDbContext.Users.Where(usr => UserIDs
            .Contains(usr.Id))
            .Select(usr => new CreatorViewModel
            {
                Creator = usr.Fullname,
                CreatorUsrName = usr.UserName,
                CreatorId = usr.Id,
                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
            }).ToList();
        }

        public async Task<List<PostMasterCollectionViewModel>> GetHiddenPost(Guid CurrId)
        {
            try
            {
                var DataList = await (from master in context.PostMaster
                                      join usr in context.Users on master.CreatedById equals usr.Id
                                      where master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                      && master.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Inactive
                                      && master.CreatedById == CurrId
                                      && !master.Deleted_Status
                                      select new PostMasterCollectionViewModel()
                                      {
                                          PostId = master.Id,
                                          PostTitle = master.PostTitle,
                                          PaidPostType = master.PaidPostType,
                                          PostPrivacy = master.PostPrivacy,
                                          PostPrice = master.PostPrice,
                                          CreatedOn = master.CreatedOn,
                                          NSFW = master.NSFW,
                                          Scheduled = master.Scheduled,
                                          ScheduledFor = master.ScheduledTime,
                                          TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                          DateString = master.CreatedOn.Stringify(),
                                          Creator = new CreatorViewModel
                                          {
                                              Creator = usr.Fullname,
                                              CreatorUsrName = usr.UserName,
                                              CreatorId = usr.Id,
                                              CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                          },
                                          HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                          HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                          HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                          TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                          TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                          collection = context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                          {

                                              ContentId = z.ContentId,
                                              ContentTitle = z.ContentTitle,
                                              FileLocation = z.FileLocation,
                                              PaidPost = z.PaidContent,
                                              PostType = z.PostType,
                                              HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                              TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                              TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                              HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                              HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                              TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                          }).ToList()
                                      }).ToListAsync();

                return DataList;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<List<PostMasterCollectionViewModel>> GetBookmarkByUser(Guid CurrId)
        {
            try
            {
                var DataList = new List<PostMasterCollectionViewModel>();
                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        join booked in context.PostBookmark on master.Id equals booked.PostId
                                        where master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        && booked.UserId == CurrId
                                        select new PostMasterCollectionViewModel()
                                        {
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            collection = context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                            {

                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = z.FileLocation,
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                                TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                                TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                            }).ToList()
                                        }).ToListAsync();


                var data2 = await (from master in context.PostMaster
                                   join col in context.PostCollection on master.Id equals col.PostId
                                   join usr in context.Users on master.CreatedById equals usr.Id
                                   join booked in context.PostBookmark on col.ContentId equals booked.PostId
                                   where master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                   && booked.UserId == CurrId
                                   select new PostMasterCollectionViewModel()
                                   {
                                       PostId = master.Id,
                                       PostTitle = master.PostTitle,
                                       PaidPostType = master.PaidPostType,
                                       PostPrivacy = master.PostPrivacy,
                                       PostPrice = master.PostPrice,
                                       CreatedOn = master.CreatedOn,
                                       NSFW = master.NSFW,
                                       Scheduled = master.Scheduled,
                                       ScheduledFor = master.ScheduledTime,
                                       TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                       DateString = master.CreatedOn.Stringify(),
                                       Creator = new CreatorViewModel
                                       {
                                           Creator = usr.Fullname,
                                           CreatorUsrName = usr.UserName,
                                           CreatorId = usr.Id,
                                           CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                       },
                                       HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                       HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                       HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                       TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                       TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                       collection = context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                       {

                                           ContentId = z.ContentId,
                                           ContentTitle = z.ContentTitle,
                                           FileLocation = z.FileLocation,
                                           PaidPost = z.PaidContent,
                                           PostType = z.PostType,
                                           HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                           TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                           TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                           HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                           HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                           TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                       }).ToList()
                                   }).ToListAsync();

                DataList.AddRange(dataEntity);
                DataList.AddRange(data2);
                return DataList;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<List<PostMasterCollectionViewModel>> GetScheduledPostUser(Guid CurrId)
        {
            try
            {

                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        where master.CreatedById == CurrId && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                       && master.Scheduled == true
                                        select new PostMasterCollectionViewModel()
                                        {
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            collection = context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                            {

                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = z.FileLocation,
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                                TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                                TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                            }).ToList()

                                        }).ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<List<TopAccountsViewModel>> GetTopAccounts(Guid UserId)
        {
            return await context.Users.Where(x => x.Id != UserId && x.AccountType != FanBucketEntity.Model.ENUM.AccountType.Superadmin).Select(x => new TopAccountsViewModel()
            {
                UserId = x.Id,
                FullName = x.Fullname,
                Username = x.UserName,
                ProfileImgLocation = string.IsNullOrEmpty(x.ProfileImgLocation) ? "/Content/gallery/userimg.png" : x.ProfileImgLocation,
                FeaturedImageLeft = x.FeaturedImageLeft,
                FeaturedImageRight = x.FeaturedImageRight
            }).Take(10).ToListAsync();
        }

        public async Task<List<PostCommentViewModel>> GetCommentByPost(Guid PostId)
        {

            var dataEntity = await (from comments in context.PostComments
                                    join usr in context.Users on comments.CreatedById equals usr.Id
                                    where comments.PostId == PostId
                                    select new PostCommentViewModel()
                                    {
                                        Comment = comments.Comment,
                                        CreatedById = usr.Id,
                                        CreatedBy = usr.Fullname,
                                        CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                        ,
                                        CreatorUsrName = usr.UserName,
                                        CreatedDate = comments.CreatedOn,
                                        PostId = comments.PostId,
                                        Id = comments.Id,
                                        DateString = comments.CreatedOn.Stringify()
                                    }).ToListAsync();
            return dataEntity;
        }

        public async Task<List<PostMomentsViewModel>> GetMoments(Guid CurrId)
        {
            try
            {
                var FollowingId = context.ApplicationUserFollowlist.Where(x => x.FollowedBy == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Select(x => x.UserId).ToList();
                var MomentUsrId = context.PostMoments.Where(x => (FollowingId.Contains(x.CreatedById) || x.CreatedById == CurrId) && DateTime.Now >= x.StartDate && DateTime.Now <= x.StartDate.AddDays(1)).Select(x => x.CreatedById).Distinct().ToList();
                var dataEntity = await context.Users.Where(x => MomentUsrId.Contains(x.Id)).Select(y => new PostMomentsViewModel()
                {

                    Creator = new CreatorViewModel
                    {
                        Creator = y.Fullname,
                        CreatorUsrName = y.UserName,
                        CreatorId = y.Id,
                        CreatorImg = string.IsNullOrEmpty(y.ProfileImgLocation) ? "/Content/gallery/userimg.png" : y.ProfileImgLocation
                    },
                    MomentDetail = context.PostMoments.Where(z => z.CreatedById == y.Id &&
                    (DateTime.Now >= z.StartDate && DateTime.Now <= z.StartDate.AddDays(1)))
                                               .Select(x => new MomentsViewModel
                                               {
                                                   DateString = y.CreatedOn.Stringify(),
                                                   CreatedDate = x.StartDate,
                                                   PostId = x.PostId.Value
                                               }).ToList(),
                    MaxDate = context.PostMoments.Where(x => x.CreatedById == y.Id).Max(x => x.StartDate)


                }).ToListAsync();


                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<List<PollVoteViewModel>> RegisterPoll(Guid PostId, Guid ContentId, Guid UserId)
        {
            var PollByUser = await context.PostOptionByUser.Where(x => x.PostId == PostId && x.UserId == UserId).ToListAsync();
            if (PollByUser != null)
            {
                PollByUser.Select(x => { x.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Inactive; return x; }).ToList();
                context.PostOptionByUser.UpdateRange(PollByUser);
                //await context.SaveChangesAsync();
                if (PollByUser.Any(x => x.OptionId == ContentId))
                {
                    PollByUser.Where(x => x.OptionId == ContentId).Select(x => { x.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active; return x; }).FirstOrDefault();
                    context.PostOptionByUser.UpdateRange(PollByUser);
                    //await context.SaveChangesAsync();
                }
                else
                {
                    var postOption = new PostOptionByUser();
                    postOption.UserId = UserId;
                    postOption.OptionId = ContentId;
                    postOption.PostId = PostId;
                    postOption.CreatedOn = DateTime.Now;
                    postOption.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active;

                    context.PostOptionByUser.Add(postOption);


                }

            }
            else
            {
                var postOption = new PostOptionByUser();
                postOption.UserId = UserId;
                postOption.OptionId = ContentId;
                postOption.PostId = PostId;
                postOption.CreatedOn = DateTime.Now;
                postOption.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active;

                context.PostOptionByUser.Add(postOption);

            }
            await context.SaveChangesAsync();

            var PollOptions = await context.PostCollection.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostType == FanBucketEntity.Model.ENUM.ContentTypes.Polls && x.PostId == PostId).Select(x => new PollVoteViewModel()
            {
                ContentId = x.ContentId,
                Voters = OptionVotedByUser(x.ContentId, context),
                TotalVote = context.PostOptionByUser.Where(z => z.OptionId == x.ContentId && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
            }).ToListAsync();
            return PollOptions;
            //return await (context.PostOptionByUser.Where(x => x.PostId == PostId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).CountAsync());

        }
        public async Task<PostCollection> RemoveCollectionAsync(PostCollection entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                context.Remove(entity);
                await context.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }

        public async Task<PostMoments> RemoveMomentsAsync(PostMoments entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                context.Remove(entity);
                await context.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }
        public ValueTask<PostMoments> MomentsGetById(Guid Id)
        {
            try
            {
                return context.FindAsync<PostMoments>(Id);
            }
            catch (Exception ex)
            {
                throw new Exception($"Couldn't retrieve entities: {ex.Message}");
            }
        }
        public bool PostExists(Guid id)
        {
            return context.PostMaster.Any(e => e.Id == id);
        }
        public bool MomentsExists(Guid id)
        {
            return context.PostMoments.Any(e => e.Id == id);
        }
        public bool PostExistsByUser(Guid id, Guid CurrId)
        {
            return context.PostMaster.Any(e => e.Id == id && e.CreatedById == CurrId);
        }
        public async Task<PostCollection> GetCollectionById(Guid Id)
        {
            return await context.PostCollection.Where(x => x.ContentId == Id).FirstOrDefaultAsync();
        }
        public async Task<List<PostCollection>> GetPostCollectionsById(Guid PostId)
        {
            return await context.PostCollection.Where(x => x.PostId == PostId).ToListAsync();
        }

        public int GetUserPaidPostCount(Guid CurrId)
        {
            return context.PostMaster.Where(x => x.CreatedById == CurrId && x.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free).Count();
        }

        public bool UserIsContentCreator(Guid CurrId)
        {
            return context.PostMaster.Where(x => x.CreatedById == CurrId && x.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free).Any();
        }

        public async Task<PostCommentViewModel> GetCommentById(Guid Id)
        {
            var dataEntity = await (from comments in context.PostComments
                                    join usr in context.Users on comments.CreatedById equals usr.Id
                                    where comments.Id == Id
                                    select new PostCommentViewModel()
                                    {
                                        Comment = comments.Comment,
                                        CreatedById = usr.Id,
                                        CreatedBy = usr.Fullname,
                                        CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                        ,
                                        CreatorUsrName = usr.UserName,
                                        CreatedDate = comments.CreatedOn,
                                        PostId = comments.PostId,
                                        Id = comments.Id,
                                        DateString = comments.CreatedOn.Stringify()
                                    }).FirstOrDefaultAsync();

            return dataEntity;
        }

        public async Task<List<PostMasterCollectionViewModel>> Search(Guid CurrId, string hashtag)
        {
            try
            {
                //var FollowingId = context.ApplicationUserFollowlist.Where(x => x.FollowedBy == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Select(x => x.UserId).ToList();

                var postMaster = context.PostMaster.AsQueryable();
                var users = context.Users.AsQueryable();
                if (!string.IsNullOrEmpty(hashtag))
                {
                    postMaster = postMaster.Where(x => x.PostTitle.Contains("#" + hashtag));

                }
                var dataEntity = await (from master in postMaster
                                        join usr in users on master.CreatedById equals usr.Id
                                        where master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        //&& (FollowingId.Contains(master.CreatedById) || master.CreatedById == CurrId)
                                        select new PostMasterCollectionViewModel()
                                        {
                                            ParentPostMasterCollectionViewModel = master.ParentPostId.HasValue == false ? null : GetSinglePostByIdSync(master.ParentPostId.Value, CurrId, context),
                                            ParentPostId = master.ParentPostId,
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = master.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free ? context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id) : true,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            collection = context.PostCollection.Where(z => z.PostId == master.Id).Select(z => new PostCollectionViewModel
                                            {

                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = z.FileLocation,
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId),
                                                TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                            }).ToList()
                                        }).ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public async Task<bool> RemovePostTagsAsync(Guid PostId)
        {
            var postTags = await context.PostTags.Where(x => x.PostId == PostId).ToListAsync();
            if (postTags.Count != 0)
            {
                context.RemoveRange(postTags);
                await context.SaveChangesAsync();
            }
            return true;
        }
        public async Task<bool> AddPostTagsAsync(Guid PostId, List<string> tagsList)
        {
            foreach (var tag in tagsList)
            {
                var oldTag = await context.Tag.FirstOrDefaultAsync(x => x.Title == tag);
                if (oldTag == null)
                {
                    //add tag to db
                    Tag entity = new Tag()
                    {
                        Id = Guid.NewGuid(),
                        CreatedOn = DateTime.UtcNow,
                        Title = tag,
                    };
                    entity.PostTags.Add(new PostTags()
                    {
                        Id = Guid.NewGuid(),
                        PostId = PostId,
                        CreatedOn = DateTime.UtcNow
                    });
                    context.Tag.Add(entity);
                }
                else
                {
                    var postTag = new PostTags()
                    {

                        Id = Guid.NewGuid(),
                        PostId = PostId,
                        CreatedOn = DateTime.UtcNow,
                        TagId = oldTag.Id
                    };
                    context.PostTags.Add(postTag);
                }

            }
            await context.SaveChangesAsync();
            return true;
        }

        //public int GetUserPostCount(Guid CurrId)
        //{
        //    return context.PostMaster.Where(x => x.CreatedById == CurrId && x.PaidPostType != FanBucketEntity.Model.ENUM.PaidPostType.Free).Count();
        //}
        #region ViewAsGuestUser
        public async Task<List<PostMasterCollectionViewModel>> ViewAsGuestUser(Guid CurrId, int PageNumber, int PageSize)
        {
            try
            {

                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        where master.CreatedById == CurrId && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        select new PostMasterCollectionViewModel()
                                        {
                                            ParentPostMasterCollectionViewModel = master.ParentPostId.HasValue == false ? null : GetSinglePostByIdSync(master.ParentPostId.Value, CurrId, context),
                                            ParentPostId = master.ParentPostId,
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },

                                            HasPaid = false,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId && x.Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            collection = context.PostCollection.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Select(z => new PostCollectionViewModel
                                            {
                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = z.PaidContent ? "" : z.FileLocation,
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                                TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                                TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()


                                            }).ToList()
                                        })
                                        .OrderByDescending(x => x.CreatedOn)
                                        .Skip((PageNumber - 1) * PageSize)
                                        .Take(PageSize)
                                        .ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<List<PostMasterCollectionViewModel>> GuestUserGetPostByVideo(Guid CurrId)
        {
            try
            {

                var dataEntity = await (from master in context.PostMaster
                                        join usr in context.Users on master.CreatedById equals usr.Id
                                        join colection in context.PostCollection on master.Id equals colection.PostId
                                        where master.CreatedById == CurrId && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        && colection.PostType == FanBucketEntity.Model.ENUM.ContentTypes.Video
                                        select new PostMasterCollectionViewModel()
                                        {
                                            PostId = master.Id,
                                            PostTitle = master.PostTitle,
                                            PaidPostType = master.PaidPostType,
                                            PostPrivacy = master.PostPrivacy,
                                            PostPrice = master.PostPrice,
                                            CreatedOn = master.CreatedOn,
                                            NSFW = master.NSFW,
                                            Scheduled = master.Scheduled,
                                            ScheduledFor = master.ScheduledTime,
                                            TotalComments = context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                            DateString = master.CreatedOn.Stringify(),
                                            Creator = new CreatorViewModel
                                            {
                                                Creator = usr.Fullname,
                                                CreatorUsrName = usr.UserName,
                                                CreatorId = usr.Id,
                                                CreatorImg = string.IsNullOrEmpty(usr.ProfileImgLocation) ? "/Content/gallery/userimg.png" : usr.ProfileImgLocation
                                            },
                                            HasPaid = false,
                                            HasReacted = context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                            HasBookMarked = context.PostBookmark.Any(x => x.PostId == master.Id && x.UserId == CurrId && x.Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                            TotalReacts = context.PostReacts.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                            TotalVotes = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id).Count(),
                                            collection = context.PostCollection.Where(z => z.PostId == master.Id && z.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active)
                                            .Select(z => new PostCollectionViewModel
                                            {

                                                ContentId = z.ContentId,
                                                ContentTitle = z.ContentTitle,
                                                FileLocation = z.FileLocation,
                                                PaidPost = z.PaidContent,
                                                PostType = z.PostType,
                                                HasReacted = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == z.ContentId) : context.PostReacts.Any(x => x.CreatedById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id),
                                                TotalComments = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostComments.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count() : context.PostComments.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.Deleted_Status == false).Count(),
                                                TotalReacts = context.PostCollection.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.DeletedStatus == false).Count() > 1 ? context.PostReacts.Where(x => x.PostId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count() : context.PostReacts.Where(x => x.PostId == master.Id && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active).Count(),
                                                HasBookMarked = context.PostBookmark.Any(x => x.PostId == z.ContentId && x.UserId == CurrId),
                                                HasVoted = context.PostOptionByUser.Any(x => x.UserId == CurrId && x.OptionId == z.ContentId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active),
                                                TotalVote = context.PostOptionByUser.Where(x => x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.OptionId == z.ContentId).Count()

                                            }).ToList()

                                        }).ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public async Task<List<GetUserMediaViewModel>> GuestUserGetPostByImage(Guid CurrId)
        {
            try
            {
                var dataEntity = await (from master in context.PostMaster
                                            //join usr in context.Users on master.CreatedById equals usr.Id
                                        join colection in context.PostCollection on master.Id equals colection.PostId
                                        where master.CreatedById == CurrId && master.PostPrivacy == FanBucketEntity.Model.ENUM.Privacy.Public
                                        && colection.PostType == FanBucketEntity.Model.ENUM.ContentTypes.Image
                                        //let HasPaid = UserId == CurrId || master.PaidPostType == FanBucketEntity.Model.ENUM.PaidPostType.Free || context.PaymentPostByUser.Any(x => x.PaidById == CurrId && x.Record_Status == FanBucketEntity.Model.ENUM.Record_Status.Active && x.PostId == master.Id)
                                        select new GetUserMediaViewModel()
                                        {
                                            PostId = master.Id,
                                            CollectionId = colection.ContentId,
                                            HasPaid = false,
                                            PaidContent = colection.PaidContent,
                                            FileLocation = !colection.PaidContent ? colection.FileLocation : "",
                                            PostTitle = master.PostTitle,
                                        }).ToListAsync();
                return dataEntity;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }
}
