﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Model;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.Payments;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Utility;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Interface;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace FanBucketRepo.Repository
{
    public class PaymentRepository : Repository<PaymentCreditByUser>, IPaymentRepository
    {
        private const string AdminUserId = "4fd83189-3f7a-4508-8bdb-e68b40d41c64";
        //private const string AdminUserId = "59b6a46b-1341-4206-b8dc-9af2a7b78403";//local
        public PaymentRepository(FbDbContext context) : base(context)
        {
        }
        public async Task<TransactionResponseCode> LoadFund(PaymentValidationModel model, bool first)
        {
            //Transaction Initiate
            var transactionLedger = new PaymentTransactionByUser();
            transactionLedger.Id = Guid.NewGuid();
            transactionLedger.TranId = Guid.NewGuid();
            transactionLedger.UserId = model.CurrId;
            transactionLedger.Amount = model.PayingAmount;
            transactionLedger.DebitCredit = "C";
            transactionLedger.ResponseCode = TransactionResponseCode.OK;
            transactionLedger.TranRemarks = "Fund Loaded";
            transactionLedger.TranDate = DateTime.Now;
            context.PaymentTransactionByUser.Add(transactionLedger);

            if (!first)
            {
                //Stripe Initiate
                var StripeLedger = new StripePaymentByUser();
                StripeLedger.Id = Guid.NewGuid();
                StripeLedger.TranId = Guid.NewGuid();
                StripeLedger.UserId = model.CurrId;
                StripeLedger.Amount = model.PayingAmount;
                StripeLedger.DebitCredit = "C";
                StripeLedger.ResponseCode = TransactionResponseCode.OK;
                StripeLedger.TranRemarks = "Fund Loaded";
                StripeLedger.TranDate = DateTime.Now;
                context.StripePaymentByUser.Add(StripeLedger);
            }

            var updateLedger = await context.PaymentCreditByUser.Where(x => x.UserId == model.CurrId).FirstOrDefaultAsync();

            if (updateLedger == null)
            {
                var ledger = new PaymentCreditByUser();
                ledger.CreditSum = model.PayingAmount;
                ledger.Id = Guid.NewGuid();
                ledger.UserId = model.CurrId;
                ledger.DebitSum = 0;
                ledger.Currency = "USD";
                ledger.CurrentBalance = model.PayingAmount;
                ledger.EarningPercentage = first ? 80 : ledger.EarningPercentage;
                context.PaymentCreditByUser.Add(ledger);

                transactionLedger.WalletAmount = ledger.CurrentBalance;// set the updated wallet balance

            }
            else
            {
                //Update balance and other details
                updateLedger.CurrentBalance = updateLedger.CurrentBalance + model.PayingAmount;

                updateLedger.CreditSum = updateLedger.CreditSum + model.PayingAmount;

                transactionLedger.WalletAmount = updateLedger.CurrentBalance;

            }

            await context.CommitAsync();

            return TransactionResponseCode.OK;
        }
        public async Task<List<TEntity>> ExecuteProcedure<TEntity>(string Procedure, object[] param) where TEntity : class
        {
            var data = await context.Set<TEntity>().FromSqlRaw(Procedure, param).ToListAsync();
            return data;
        }
        public string TestPaymentSplit(decimal Percentage, decimal Amount)
        {
            StringBuilder sb = new StringBuilder();
            var data = CalculateEarnings(Percentage, Amount);
            sb.Append("User Received Amount: " + data.UserAmount);
            sb.Append("Admin Received Amount: " + data.AdminAmount);
            return sb.ToString();


        }
        public static EarningSplit CalculateEarnings(decimal Percentage, decimal Amount)
        {
            var Split = new EarningSplit();
            Split.UserAmount = (Amount * Percentage) / 100;
            Split.AdminAmount = Amount - Split.UserAmount;

            return Split;
        }
        public async Task PaymentForViaStripe(PaymentValidationModel model, Guid TranId)
        {
            try
            {
                var PostDetails = await context.PostMaster.Where(x => x.Id == model.PostId).Select(x => new
                {
                    x.PostPrice,
                    x.CreatedById
                }).FirstOrDefaultAsync();

                model.PayingAmount = PostDetails.PostPrice;

                var commisionDetail = context.PaymentCreditByUser.Where(x => x.UserId == PostDetails.CreatedById).Select(x => x.EarningPercentage).FirstOrDefault();
                //Transaction Initiate
                var transactionLedgerDebit = new StripePaymentByUser();
                transactionLedgerDebit.Id = Guid.NewGuid();
                transactionLedgerDebit.TranId = TranId;
                transactionLedgerDebit.UserId = model.CurrId;
                transactionLedgerDebit.Amount = model.PayingAmount;
                transactionLedgerDebit.DebitCredit = "D";
                transactionLedgerDebit.ResponseCode = TransactionResponseCode.OK;
                transactionLedgerDebit.TranDate = DateTime.Now;
                transactionLedgerDebit.TranRemarks = "Payment for post:" + model.PostId.ToString();
                context.StripePaymentByUser.Add(transactionLedgerDebit);

                //Transaction Deposit to Content Creator
                var transactionLedgerCredit = new PaymentEarningsByUser();
                transactionLedgerCredit.Id = Guid.NewGuid();
                transactionLedgerCredit.TranId = transactionLedgerDebit.TranId;
                transactionLedgerCredit.UserId = PostDetails.CreatedById;
                transactionLedgerCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).UserAmount;
                transactionLedgerCredit.TranDate = DateTime.Now;
                transactionLedgerCredit.TranRemarks = "Payment of post:" + model.PostId.ToString();
                transactionLedgerCredit.Stripe = true;
                context.PaymentEarningsByUser.Add(transactionLedgerCredit);

                //Transaction Deposit to Admin
                var adminCredit = new PaymentEarningsByUser();
                adminCredit.Id = Guid.NewGuid();
                adminCredit.TranId = transactionLedgerDebit.TranId;
                adminCredit.UserId = Guid.Parse(AdminUserId);
                adminCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).AdminAmount;
                adminCredit.TranDate = DateTime.Now;
                adminCredit.TranRemarks = commisionDetail + "% Commission of post:" + model.PostId.ToString();
                transactionLedgerCredit.Stripe = true;
                context.PaymentEarningsByUser.Add(adminCredit);

                //Update Payment for Post
                var entity = new PaymentPostByUser();
                entity.TranId = transactionLedgerDebit.TranId;
                entity.PostId = model.PostId;
                entity.Id = Guid.NewGuid();
                entity.PaidById = model.CurrId;
                entity.PaidAmount = model.PayingAmount;
                entity.CreatedById = model.CurrId;
                entity.CreatedOn = DateTime.Now;
                entity.Record_Status = Record_Status.Active;
                entity.Deleted_Status = false;
                context.PaymentPostByUser.Add(entity);

                var creditLedger = await context.PaymentCreditByUser.Where(x => x.UserId == PostDetails.CreatedById).FirstOrDefaultAsync();
                creditLedger.TotalEarnings = creditLedger.TotalEarnings + transactionLedgerCredit.Amount;
                creditLedger.DuesRemaining = creditLedger.DuesRemaining + transactionLedgerCredit.Amount;
                creditLedger.CreditSum = creditLedger.CreditSum + transactionLedgerCredit.Amount;

                var adminCrLedger = await context.PaymentCreditByUser.Where(x => x.UserId == adminCredit.UserId).FirstOrDefaultAsync();
                adminCrLedger.TotalEarnings = adminCrLedger.TotalEarnings + transactionLedgerCredit.Amount;
                adminCrLedger.DuesRemaining = adminCrLedger.DuesRemaining + creditLedger.DuesRemaining;
                adminCrLedger.CreditSum = adminCrLedger.CreditSum + transactionLedgerCredit.Amount;

                var notifications = new AppNotifications();
                notifications.Id = Guid.NewGuid();
                notifications.ActivityId = entity.Id;
                notifications.PKId = entity.PostId;
                notifications.nType = FanBucketEntity.Model.ENUM.NotificationType.Paid;
                notifications.ActivityUserId = model.CurrId;
                notifications.PostCreatorId = context.PostMaster
                    .Where(x => x.Id == entity.PostId)
                    .Select(x => x.CreatedById)
                    .FirstOrDefault();
                notifications.CreatedDate = DateTime.Now;
                notifications.Viewed = false;

                context.AppNotifications.Add(notifications);

                await context.CommitAsync();
            }
            catch
            {
                return;
            }

        }
        public async Task<TransactionResponseCode> PaymentForPost(PaymentValidationModel model)
        {
            TransactionResponseCode response;
            try
            {
                var PostDetails = context.PostMaster.Where(x => x.Id == model.PostId).Select(x => new
                {
                    x.PostPrice,
                    x.CreatedById
                }).FirstOrDefault();
                model.PayingAmount = PostDetails.PostPrice;




                response = await ValidateCustomerForPayment(model);
                var commisionDetail = context.PaymentCreditByUser.Where(x => x.UserId == PostDetails.CreatedById).Select(x => x.EarningPercentage).FirstOrDefault();

                if (response == TransactionResponseCode.OK)
                {
                    //Transaction Initiate
                    var transactionLedgerDebit = new PaymentTransactionByUser();
                    transactionLedgerDebit.Id = Guid.NewGuid();
                    transactionLedgerDebit.TranId = Guid.NewGuid();
                    transactionLedgerDebit.UserId = model.CurrId;
                    transactionLedgerDebit.Amount = model.PayingAmount;
                    transactionLedgerDebit.DebitCredit = "D";
                    transactionLedgerDebit.ResponseCode = response;
                    transactionLedgerDebit.TranDate = DateTime.Now;
                    transactionLedgerDebit.TranRemarks = "Payment for post:" + model.PostId.ToString();
                    context.PaymentTransactionByUser.Add(transactionLedgerDebit);


                    //Transaction Deposit to Content Creator
                    var transactionLedgerCredit = new PaymentTransactionByUser();
                    transactionLedgerCredit.Id = Guid.NewGuid();
                    transactionLedgerCredit.TranId = transactionLedgerDebit.TranId;
                    transactionLedgerCredit.UserId = PostDetails.CreatedById;
                    transactionLedgerCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).UserAmount;
                    transactionLedgerCredit.DebitCredit = "C";
                    transactionLedgerCredit.ResponseCode = response;
                    transactionLedgerCredit.TranDate = DateTime.Now;
                    transactionLedgerCredit.TranRemarks = "Payment of post:" + model.PostId.ToString();
                    context.PaymentTransactionByUser.Add(transactionLedgerCredit);

                    //Transaction Deposit to Admin
                    var adminLedgerCredit = new PaymentTransactionByUser();
                    adminLedgerCredit.Id = Guid.NewGuid();
                    adminLedgerCredit.TranId = transactionLedgerDebit.TranId;
                    adminLedgerCredit.UserId = Guid.Parse(AdminUserId);
                    adminLedgerCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).AdminAmount;
                    adminLedgerCredit.DebitCredit = "C";
                    adminLedgerCredit.ResponseCode = response;
                    adminLedgerCredit.TranDate = DateTime.Now;
                    adminLedgerCredit.TranRemarks = "Commision for Payment of post :" + model.PostId.ToString();
                    context.PaymentTransactionByUser.Add(adminLedgerCredit);

                    //Transaction Deposit to Content Creator
                    var EarningLedgerCredit = new PaymentEarningsByUser();
                    EarningLedgerCredit.Id = Guid.NewGuid();
                    EarningLedgerCredit.TranId = transactionLedgerDebit.TranId;
                    EarningLedgerCredit.UserId = PostDetails.CreatedById;
                    EarningLedgerCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).UserAmount;
                    EarningLedgerCredit.TranDate = DateTime.Now;
                    EarningLedgerCredit.TranRemarks = "Payment of post:" + model.PostId.ToString();
                    //EarningLedgerCredit.Stripe = true;
                    context.PaymentEarningsByUser.Add(EarningLedgerCredit);

                    //Transaction Deposit to Admin
                    var adminCredit = new PaymentEarningsByUser();
                    adminCredit.Id = Guid.NewGuid();
                    adminCredit.TranId = transactionLedgerDebit.TranId;
                    adminCredit.UserId = Guid.Parse(AdminUserId);
                    adminCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).AdminAmount;
                    adminCredit.TranDate = DateTime.Now;
                    adminCredit.TranRemarks = commisionDetail + "% Commission of post:" + model.PostId.ToString();
                    //transactionLedgerCredit.Stripe = true;
                    context.PaymentEarningsByUser.Add(adminCredit);

                    //Update Payment for Post
                    var entity = new PaymentPostByUser();
                    entity.TranId = transactionLedgerDebit.TranId;
                    entity.PostId = model.PostId;
                    entity.Id = Guid.NewGuid();
                    entity.PaidById = model.CurrId;
                    entity.PaidAmount = model.PayingAmount;
                    entity.CreatedById = model.CurrId;
                    entity.CreatedOn = DateTime.Now;
                    entity.Record_Status = Record_Status.Active;
                    entity.Deleted_Status = false;
                    context.PaymentPostByUser.Add(entity);

                    //Update balance and other details
                    var debitLedger = await context.PaymentCreditByUser.Where(x => x.UserId == model.CurrId).FirstOrDefaultAsync();
                    debitLedger.CurrentBalance = debitLedger.CurrentBalance - model.PayingAmount;
                    debitLedger.DebitSum = debitLedger.DebitSum + model.PayingAmount;



                    var creditLedger = await context.PaymentCreditByUser.Where(x => x.UserId == PostDetails.CreatedById).FirstOrDefaultAsync();
                    creditLedger.TotalEarnings = creditLedger.TotalEarnings + transactionLedgerCredit.Amount;
                    creditLedger.DuesRemaining = creditLedger.DuesRemaining + transactionLedgerCredit.Amount;
                    creditLedger.CreditSum = creditLedger.CreditSum + transactionLedgerCredit.Amount;

                    var adminCrLedger = await context.PaymentCreditByUser.Where(x => x.UserId == adminCredit.UserId).FirstOrDefaultAsync();
                    adminCrLedger.TotalEarnings = adminCrLedger.TotalEarnings + transactionLedgerCredit.Amount;
                    adminCrLedger.DuesRemaining = adminCrLedger.DuesRemaining + creditLedger.DuesRemaining;
                    adminCrLedger.CreditSum = adminCrLedger.CreditSum + transactionLedgerCredit.Amount;

                    transactionLedgerDebit.WalletAmount = debitLedger.CurrentBalance;// set the updated wallet balance
                    transactionLedgerCredit.WalletAmount = creditLedger.CurrentBalance;// set the updated wallet balance
                    adminLedgerCredit.WalletAmount = debitLedger.TotalEarnings;// set the updated wallet balance


                    var notifications = new AppNotifications();
                    notifications.Id = Guid.NewGuid();
                    notifications.ActivityId = entity.Id;
                    notifications.PKId = entity.PostId;
                    notifications.nType = FanBucketEntity.Model.ENUM.NotificationType.Paid;
                    notifications.ActivityUserId = model.CurrId;
                    notifications.PostCreatorId = context.PostMaster.Where(x => x.Id == entity.PostId).Select(x => x.CreatedById).FirstOrDefault();
                    notifications.CreatedDate = DateTime.Now;
                    notifications.Viewed = false;

                    context.AppNotifications.Add(notifications);

                    await context.CommitAsync();


                }
            }
            catch (Exception ex)
            {

                //response = TransactionResponseCode.ServerError;
                response = TransactionResponseCode.ServerError;
            }

            return response;
        }
        public async Task<TransactionResponseCode> TipUser(PaymentValidationModel model, Guid MessageId)
        {
            TransactionResponseCode response;
            try
            {
                var commisionDetail = context.PaymentCreditByUser.Where(x => x.UserId == model.PostId).Select(x => x.EarningPercentage).FirstOrDefault();



                response = await ValidateCustomerForPayment(model);
                if (response == TransactionResponseCode.OK)
                {
                    //Transaction Initiate
                    var transactionLedgerDebit = new PaymentTransactionByUser();
                    transactionLedgerDebit.Id = Guid.NewGuid();
                    transactionLedgerDebit.TranId = Guid.NewGuid();
                    transactionLedgerDebit.UserId = model.CurrId;
                    transactionLedgerDebit.Amount = model.PayingAmount;
                    transactionLedgerDebit.DebitCredit = "D";
                    transactionLedgerDebit.ResponseCode = response;
                    transactionLedgerDebit.TranDate = DateTime.Now;
                    transactionLedgerDebit.TranRemarks = "Sent Tips";
                    context.PaymentTransactionByUser.Add(transactionLedgerDebit);


                    //Transaction Deposit to Content Creator
                    var transactionLedgerCredit = new PaymentTransactionByUser();
                    transactionLedgerCredit.Id = Guid.NewGuid();
                    transactionLedgerCredit.TranId = transactionLedgerDebit.TranId;
                    transactionLedgerCredit.UserId = model.PostId;
                    transactionLedgerCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).UserAmount;
                    transactionLedgerCredit.DebitCredit = "C";
                    transactionLedgerCredit.ResponseCode = response;
                    transactionLedgerCredit.TranDate = DateTime.Now;
                    transactionLedgerCredit.TranRemarks = "Received Tips";
                    context.PaymentTransactionByUser.Add(transactionLedgerCredit);

                    //Transaction Deposit to Content Creator
                    var EarningLedgerCredit = new PaymentEarningsByUser();
                    EarningLedgerCredit.Id = Guid.NewGuid();
                    EarningLedgerCredit.TranId = transactionLedgerDebit.TranId;
                    EarningLedgerCredit.UserId = model.PostId;
                    EarningLedgerCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).UserAmount;
                    EarningLedgerCredit.TranDate = DateTime.Now;
                    EarningLedgerCredit.TranRemarks = "Received Tips";
                    context.PaymentEarningsByUser.Add(EarningLedgerCredit);

                    // Transaction Deposit to Admin
                    var adminCredit = new PaymentTransactionByUser();
                    adminCredit.Id = Guid.NewGuid();
                    adminCredit.TranId = transactionLedgerDebit.TranId;
                    adminCredit.UserId = Guid.Parse(AdminUserId);
                    adminCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).AdminAmount;
                    adminCredit.DebitCredit = "C";
                    adminCredit.ResponseCode = response;
                    adminCredit.TranDate = DateTime.Now;
                    adminCredit.TranRemarks = commisionDetail + "% Commision of Received Tips:" + model.PostId.ToString();
                    context.PaymentTransactionByUser.Add(adminCredit);

                    // Transaction Deposit to Admin
                    var EarningLedgerAdCredit = new PaymentEarningsByUser();
                    EarningLedgerAdCredit.Id = Guid.NewGuid();
                    EarningLedgerAdCredit.TranId = transactionLedgerDebit.TranId;
                    EarningLedgerAdCredit.UserId = Guid.Parse(AdminUserId);
                    EarningLedgerAdCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).AdminAmount;
                    EarningLedgerAdCredit.TranDate = DateTime.Now;
                    EarningLedgerAdCredit.TranRemarks = "Received Tips";
                    context.PaymentEarningsByUser.Add(EarningLedgerAdCredit);

                    //Update balance and other details
                    var debitLedger = await context.PaymentCreditByUser.Where(x => x.UserId == model.CurrId).FirstOrDefaultAsync();
                    debitLedger.CurrentBalance = debitLedger.CurrentBalance - model.PayingAmount;
                    debitLedger.DebitSum = debitLedger.DebitSum + model.PayingAmount;

                    var creditLedger = await context.PaymentCreditByUser.Where(x => x.UserId == model.PostId).FirstOrDefaultAsync();
                    creditLedger.TotalEarnings = creditLedger.TotalEarnings + model.PayingAmount;
                    creditLedger.CreditSum = creditLedger.CreditSum + model.PayingAmount;

                    var adminCrLedger = await context.PaymentCreditByUser.Where(x => x.UserId == adminCredit.UserId).FirstOrDefaultAsync();
                    adminCrLedger.TotalEarnings = adminCrLedger.TotalEarnings + adminCredit.Amount;
                    adminCrLedger.DuesRemaining = adminCrLedger.DuesRemaining + transactionLedgerCredit.Amount;
                    adminCrLedger.CreditSum = adminCrLedger.CreditSum + adminCredit.Amount;

                    var notifications = new AppNotifications();
                    notifications.Id = Guid.NewGuid();
                    notifications.ActivityId = MessageId;
                    notifications.PKId = model.CurrId;
                    notifications.nType = FanBucketEntity.Model.ENUM.NotificationType.Tips;
                    notifications.ActivityUserId = model.CurrId;
                    notifications.PostCreatorId = model.PostId;
                    notifications.CreatedDate = DateTime.Now;
                    notifications.Viewed = false;

                    context.AppNotifications.Add(notifications);

                    await context.CommitAsync();


                }
            }
            catch (Exception ex)
            {
                response = TransactionResponseCode.ServerError;
            }

            return response;
        }
        public async Task<SubscriptionByUser> CancelSubscription(Guid SubscriptionId, Guid CurrId)
        {
            try
            {
                var data = context.SubscriptionByUser
                    .Where(x => x.SubscriptionId == SubscriptionId && x.PaidByUserId == CurrId && x.Record_Status == Record_Status.Active)
                    .FirstOrDefault();
                data.Record_Status = Record_Status.Inactive;
                data.DeactivatedOn = DateTime.UtcNow;

                context.SubscriptionByUser.Update(data);
                await context.SaveChangesAsync();
                return data;
            }
            catch
            {
                return null;
            }

        }
        public async Task<TransactionResponseCode> PaymentForSubscription(Guid SubscriptionId, Guid CurrId, int PIN, ApplicationUserSubscriptionINfo data)
        {
            TransactionResponseCode response;
            try
            {
                var subDetails = data;

                var commisionDetail = context.PaymentCreditByUser
                    .Where(x => x.UserId == subDetails.UserId)
                    .Select(x => x.EarningPercentage)
                    .FirstOrDefault();
                var model = new PaymentValidationModel();
                model.CurrId = CurrId;
                model.PayingAmount = subDetails.SubscriptionAmount;
                model.PIN = PIN;
                model.PostId = SubscriptionId;

                response = await ValidateCustomerForPayment(model);
                if (response == TransactionResponseCode.OK)
                {
                    //Transaction Initiate
                    var transactionLedgerDebit = new PaymentTransactionByUser();
                    transactionLedgerDebit.Id = Guid.NewGuid();
                    transactionLedgerDebit.TranId = Guid.NewGuid();
                    transactionLedgerDebit.UserId = model.CurrId;
                    transactionLedgerDebit.Amount = model.PayingAmount;
                    transactionLedgerDebit.DebitCredit = "D";
                    transactionLedgerDebit.ResponseCode = response;
                    transactionLedgerDebit.TranDate = DateTime.Now;
                    transactionLedgerDebit.TranRemarks = "Payment for Subscription:" + model.PostId.ToString();
                    context.PaymentTransactionByUser.Add(transactionLedgerDebit);


                    //Transaction Deposit to Content Creator
                    var transactionLedgerCredit = new PaymentTransactionByUser();
                    transactionLedgerCredit.Id = Guid.NewGuid();
                    transactionLedgerCredit.TranId = transactionLedgerDebit.TranId;
                    transactionLedgerCredit.UserId = subDetails.UserId;
                    transactionLedgerCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).UserAmount;
                    transactionLedgerCredit.DebitCredit = "C";
                    transactionLedgerCredit.ResponseCode = response;
                    transactionLedgerCredit.TranDate = DateTime.Now;
                    transactionLedgerCredit.TranRemarks = "Payment of Subscription:" + model.PostId.ToString();
                    context.PaymentTransactionByUser.Add(transactionLedgerCredit);

                    // Transaction Deposit to Content Creator
                    var EarningLedgerCredit = new PaymentEarningsByUser();
                    EarningLedgerCredit.Id = Guid.NewGuid();
                    EarningLedgerCredit.TranId = transactionLedgerDebit.TranId;
                    EarningLedgerCredit.UserId = model.PostId;
                    EarningLedgerCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).UserAmount;
                    EarningLedgerCredit.TranDate = DateTime.Now;
                    EarningLedgerCredit.TranRemarks = "Payment for Subscription";
                    context.PaymentEarningsByUser.Add(EarningLedgerCredit);


                    //Transaction Deposit to Admin
                    var adminCredit = new PaymentTransactionByUser();
                    adminCredit.Id = Guid.NewGuid();
                    adminCredit.TranId = transactionLedgerDebit.TranId;
                    adminCredit.UserId = Guid.Parse(AdminUserId);
                    adminCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).AdminAmount;
                    adminCredit.DebitCredit = "C";
                    adminCredit.ResponseCode = response;
                    adminCredit.TranDate = DateTime.Now;
                    adminCredit.TranRemarks = commisionDetail + "% Commision of Subscription:" + model.PostId.ToString();
                    context.PaymentTransactionByUser.Add(adminCredit);

                    // Transaction Deposit to Admin
                    var EarningLedgerAdCredit = new PaymentEarningsByUser();
                    EarningLedgerAdCredit.Id = Guid.NewGuid();
                    EarningLedgerAdCredit.TranId = transactionLedgerDebit.TranId;
                    //EarningLedgerAdCredit.UserId = Guid.Parse(AdminUserId);
                    EarningLedgerAdCredit.UserId =Guid.Parse(AdminUserId);//local
                    EarningLedgerAdCredit.Amount = CalculateEarnings(commisionDetail, model.PayingAmount).AdminAmount;
                    EarningLedgerAdCredit.TranDate = DateTime.Now;
                    EarningLedgerAdCredit.TranRemarks = "Payment for Subscription";
                    context.PaymentEarningsByUser.Add(EarningLedgerAdCredit);

                    //Update Payment for Post
                    var subscribe = new SubscriptionByUser();
                    subscribe.SubscriptionId = data.Id;
                    subscribe.PaidByUserId = CurrId;
                    subscribe.TranId = transactionLedgerCredit.TranId;
                    subscribe.SubscribedOn = DateTime.Now;
                    int noOfDaysSubscribed = subDetails.Tenure * 30;

                    subscribe.SubscribedNoOfDays = noOfDaysSubscribed;
                    subscribe.SubscribedTill = DateTime.Now.AddDays(noOfDaysSubscribed);//1month =30 days
                    //subscribe.SubscribedTill = DateTime.Now.AddMonths(subDetails.Tenure);
                    subscribe.Record_Status = Record_Status.Active;
                    subscribe.PaidForUserId = data.UserId;
                    context.SubscriptionByUser.Add(subscribe);

                    // update master user subscription status
                    await UpdateUserSubscribtionDays(CurrId, data, noOfDaysSubscribed);

                    if (!UserFollowExist(data.UserId, CurrId))
                    {
                        var FollowReq = new ApplicationUserFollowlist();
                        FollowReq.Id = Guid.NewGuid();
                        FollowReq.SubId = Guid.Parse("05018EF5-72B9-4549-BC25-B68F6D36F7F6");
                        FollowReq.FollowedBy = CurrId;
                        FollowReq.UserId = data.UserId;
                        FollowReq.FollowedFrom = DateTime.Now;
                        FollowReq.Record_Status = FanBucketEntity.Model.ENUM.Record_Status.Active;
                        FollowReq.ModifiedOn = DateTime.Now;

                        context.ApplicationUserFollowlist.Add(FollowReq);
                    }

                    //Update balance and other details
                    var debitLedger = await context.PaymentCreditByUser.Where(x => x.UserId == model.CurrId).FirstOrDefaultAsync();
                    debitLedger.CurrentBalance = debitLedger.CurrentBalance - model.PayingAmount;
                    debitLedger.DebitSum = debitLedger.DebitSum + model.PayingAmount;

                    var creditLedger = await context.PaymentCreditByUser.Where(x => x.UserId == subDetails.UserId).FirstOrDefaultAsync();
                    creditLedger.TotalEarnings = creditLedger.TotalEarnings + transactionLedgerCredit.Amount;
                    creditLedger.DuesRemaining = creditLedger.DuesRemaining + transactionLedgerCredit.Amount;
                    creditLedger.CreditSum = creditLedger.CreditSum + transactionLedgerCredit.Amount;

                    var adminCrLedger = await context.PaymentCreditByUser.Where(x => x.UserId == adminCredit.UserId).FirstOrDefaultAsync();
                    adminCrLedger.TotalEarnings = adminCrLedger.TotalEarnings + adminCredit.Amount;
                    adminCrLedger.DuesRemaining = adminCrLedger.DuesRemaining + transactionLedgerCredit.Amount;
                    adminCrLedger.CreditSum = adminCrLedger.CreditSum + adminCredit.Amount;

                    var notifications = new AppNotifications();
                    notifications.Id = Guid.NewGuid();
                    notifications.ActivityId = subscribe.SubscriptionId;
                    notifications.PKId = subscribe.SubscriptionId;
                    notifications.nType = FanBucketEntity.Model.ENUM.NotificationType.Subscribed;
                    notifications.ActivityUserId = model.CurrId;
                    notifications.PostCreatorId = data.UserId;
                    notifications.CreatedDate = DateTime.Now;
                    notifications.Viewed = false;

                    context.AppNotifications.Add(notifications);

                    await context.SaveChangesAsync();


                }
            }
            catch (Exception ex)
            {
                response = TransactionResponseCode.ServerError;
            }

            return response;
        }

        private async Task<bool> UpdateUserSubscribtionDays(Guid CurrId, ApplicationUserSubscriptionINfo data, int noOfDaysSubscribed)
        {
            var oldData = await context.UserSubscribedStatus.FirstOrDefaultAsync(x => x.UserId == data.UserId && x.CreatedById == CurrId);

            if (oldData == null)
            {
                var UserSubscribed = new UserSubscribedStatus()
                {
                    Id = Guid.NewGuid(),
                    UserId = data.UserId,
                    CreatedById = CurrId,
                    SubscribedFrom = DateTime.Now,
                    Status = Record_Status.Active,
                    SubscribedTill = DateTime.Now.AddDays(noOfDaysSubscribed)
                };
                await context.UserSubscribedStatus.AddAsync(UserSubscribed);
            }
            else
            {
                //old subscribed is more than current or is existing subscriber
                if (oldData.SubscribedTill.Date > DateTime.Now.Date)
                {
                     oldData.SubscribedTill = oldData.SubscribedTill.Date.AddDays(noOfDaysSubscribed);
                }
                else
                {
                    oldData.SubscribedTill = DateTime.Now.AddDays(noOfDaysSubscribed);
                }
                context.Update(oldData);
            }
            return true;
        }

        private bool UserFollowExist(Guid UserId, Guid CurrId)
        {
            return context.ApplicationUserFollowlist.Any(x => x.FollowedBy == CurrId && x.UserId == UserId);
        }
        public async Task<TransactionResponseCode> ValidateCustomerForPayment(PaymentValidationModel model)
        {
            TransactionResponseCode response = TransactionResponseCode.OK;
            if (model.PayingAmount <= 0)
            {
                return TransactionResponseCode.ServerError;
            }

            var balRresponse = await CheckBalance(model.CurrId, model.PayingAmount);
            if (balRresponse != TransactionResponseCode.OK)
                response = balRresponse;

            var pinResponse = await VerifyPin(model.PIN, model.CurrId);
            if (pinResponse != TransactionResponseCode.OK)
                response = pinResponse;




            return response;
        }
        public async Task<TransactionResponseCode> ValidateBeforePayment(PaymentValidationModel model)
        {
            TransactionResponseCode response;
            var HasPin = context.PaymentUPIN.Any(x => x.UserId == model.CurrId);

            if (HasPin)
            {
                response = await CheckBalance(model.CurrId, model.PayingAmount);
            }
            else
            {
                response = TransactionResponseCode.PinRequired;
            }


            return response;
        }
        public async Task<decimal> GetBalance(Guid CurrId)
        {

            try
            {
                var availBalance = await context.PaymentCreditByUser.Where
                    (x => x.UserId == CurrId).Select(x => x.CurrentBalance).FirstOrDefaultAsync();
                return availBalance;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }
        public async Task<TransactionResponseCode> CheckBalance(Guid CurrId, decimal PayingAmount)
        {
            TransactionResponseCode response;
            try
            {
                var availBalance = await context.PaymentCreditByUser.AnyAsync(x => x.UserId == CurrId && x.CurrentBalance >= PayingAmount);
                if (availBalance)
                {
                    response = TransactionResponseCode.OK;
                }
                else
                {
                    response = TransactionResponseCode.InsufficientBalance;
                }

            }
            catch
            {
                response = TransactionResponseCode.ServerError;
            }
            return response;
        }
        public async Task<bool> HasSubscribed(Guid UserId, Guid CurrId)
        {
            return await context.SubscriptionByUser.AnyAsync(x => x.PaidByUserId == CurrId && x.PaidForUserId == UserId && x.Record_Status == Record_Status.Active);
        }
        public async Task<TransactionResponseCode> VerifyPin(int PIN, Guid CurrId)
        {
            TransactionResponseCode response;
            try
            {
                if (PIN.ToString().Length == 6)
                {
                    var model = context.PaymentUPIN.Where(x => x.UserId == CurrId).FirstOrDefault();
                    if (model == null)
                    {
                        return TransactionResponseCode.PinNotSet;

                    }
                    var match = PasswordHasher.VerifyHashedPassword(model.PINHash, PIN.ToString());

                    if (match)
                    {
                        return TransactionResponseCode.OK;

                    }
                    else
                    {

                        if (model.ErrorCount >= 3)
                        {
                            model.LockTime = DateTime.Now;
                            model.Locked = true;
                            response = TransactionResponseCode.PinLocked;
                        }
                        else
                        {
                            model.ErrorCount = model.ErrorCount + 1;
                            response = TransactionResponseCode.IncorrectPIN;

                        }
                        context.Update(model);
                        await context.SaveChangesAsync();
                        return response;


                    }


                }

                else
                {
                    return TransactionResponseCode.IncorrectPIN;
                }

            }
            catch (Exception ex)
            {
                return TransactionResponseCode.ServerError;
            }

        }

    }

}
