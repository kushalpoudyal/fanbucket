﻿using FanBucketEntity.Model;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface IUserBadgesRepository : IRepository<UserBadges>
    {
        List<UserBadgesViewModel> GetBagesById(Guid userId);

    }
}
