﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface IRepository<TEntity> where TEntity : class
    {
        public IQueryable<TEntity> GetAll();
       
        public ValueTask<TEntity> GetById(Guid Id);
        public Task<TEntity> AddAsync(TEntity entity);
        public Task<TEntity> UpdateAsync(TEntity entity);

        public Task<TEntity> RemoveAsync(TEntity entity);
        Task<int> SaveAsync();
        int Save();

    }
}
