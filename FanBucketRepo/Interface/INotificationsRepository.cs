﻿using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.PostActivity;
using FanBucketEntity.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface INotificationsRepository : IRepository<AppNotifications>
    {
        public Task<List<NotificationViewModel>> GetUserNotification(Guid CurrId);
        public Task<List<NotificationViewModel>> GetUserNotificationByType(Guid CurrId, NotificationType type);
        public Task<int> GetNotifCount(Guid CurrId);
        Task<bool> MarkAllAsRead(Guid UserId);
    }
}
