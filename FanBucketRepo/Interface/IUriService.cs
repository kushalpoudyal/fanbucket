﻿using FanBucketEntity.Wrappers;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketRepo.Interface
{
    public interface IUriService
    {
        public Uri GetPageUri(PaginationFilter filter, string route);
    }
}
