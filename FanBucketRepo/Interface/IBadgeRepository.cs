﻿using FanBucketEntity.Model;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface IBadgeRepository : IRepository<BadgeInfo>
    {
        Task<BadgeInfo> GetByCode(string BadgeCode);
        public Task<UserBadges> AddUserBadgeAsync(UserBadges entity);

    }
}
