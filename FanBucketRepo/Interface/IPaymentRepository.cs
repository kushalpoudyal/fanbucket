﻿using FanBucketEntity.Model;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Model.Payments;
using FanBucketEntity.Model.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Repository;
using Microsoft.AspNetCore.DataProtection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface IPaymentRepository : IRepository<PaymentCreditByUser>
    {
        Task PaymentForViaStripe(PaymentValidationModel model,Guid TranId);
        public Task<TransactionResponseCode> VerifyPin(int PIN, Guid CurrId);
        public Task<TransactionResponseCode> CheckBalance(Guid UserId, decimal PayingAmount);
        public string TestPaymentSplit(decimal Percentage, decimal Amount);

        Task<List<TEntity>> ExecuteProcedure<TEntity>(string Procedure, object[] param) where TEntity : class;

        public Task<TransactionResponseCode> ValidateCustomerForPayment(PaymentValidationModel model);
        public Task<TransactionResponseCode> ValidateBeforePayment(PaymentValidationModel model);
        public Task<TransactionResponseCode> PaymentForPost(PaymentValidationModel model);
        Task<TransactionResponseCode> TipUser(PaymentValidationModel model, Guid MessageId);
        public Task<decimal> GetBalance(Guid CurrId);

        public Task<TransactionResponseCode> LoadFund(PaymentValidationModel model, bool first);
        public Task<TransactionResponseCode> PaymentForSubscription(Guid SubscriptionId, Guid CurrId, int PIN, ApplicationUserSubscriptionINfo data);
        Task<bool> HasSubscribed(Guid UserId, Guid CurrId);

        Task<SubscriptionByUser> CancelSubscription(Guid SubscriptionId, Guid CurrId);


    }
}
