﻿using FanBucketAPI.ViewModel;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.ViewModel;
using System;
using System.Collections.Generic;

using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface IAdminRepository : IRepository<ApplicationUser>
    {
        public Task<DashboardViewModel> GetDashBoardValues(DateTime FromDate, DateTime ToDate);
        public Task<List<UserDetailsViewModel>> GetUsers();
        public Task<List<UserDetailsViewModel>> GetNewUsers(int Days);
        public Task<List<PostReportedViewModel>> GetReportedPosts();
        public Task<UserInteractionViewModel> GetUserMediaData(Guid CurrId);
        Task<bool> UserCommissionReview(CommissionReviewViewModel commissionReviewViewModel, Guid LoggedInUserId);
        Task<bool> UserReportAction(ReportActionViewModel reportActionViewModel, Guid LoggedInUserId);
        Task<List<PostReportSummaryViewModel>> ReportedPosts();
        Task<PostReportSummaryViewModel> ReportedPostWithDetail(Guid Id);
        Task<bool> PostReview(PostReviewViewModel postReviewViewModel, Guid LoggedInUserId);
        Task<List<UserReportSummaryViewModel>> ReportedUsers();
        Task<UserReportSummaryViewModel> ReportedUserWithDetail(Guid Id);
    }
}
