﻿using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        public Task<UserDetailsViewModel> GetUserById(Guid Id, Guid CurrId);
        public Task<List<OnlineUserViewMOdel>> GetUserRelatedFollowers(Guid CurrId);
        public Task<List<OnlineUserViewMOdel>> GetUserRelatedFollowing(Guid CurrId);
        public Task<UserDetailsViewModel> GetUserDetails(Guid CurrId);

        public bool UserAccessAllowedF(Guid UserId, Guid CurrId);
        public bool UserAccessAllowedS(Guid UserId, Guid CurrId);
        Task<List<BadgeViewModel>> GetUserBadgesById(Guid id);
        Task<bool> UpdateUserBadge(Guid id, string NewFeaturedBadgeCode);
        Task<UserDetailsViewModel> GetUserById(Guid Id);

        Task<List<OnlineUserViewMOdel>> GetUserRelatedSubscribers(Guid CurrId);

    }
}
