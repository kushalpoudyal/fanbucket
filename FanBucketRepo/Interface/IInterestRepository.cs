﻿using FanBucketEntity.Model;
using FanBucketEntity.Models.UserDetails;
using FanBucketEntity.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface IInterestRepository : IRepository<Interest>
    {

    }
}
