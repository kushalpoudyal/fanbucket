﻿using FanBucketEntity.Model.Chat;
using FanBucketEntity.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface IChatRepository : IRepository<ChatUser>
    {
        public Task<List<ChatUsersViewModel>> GetUserChats(Guid UserId);
        public Task<ChatViewModel> GetMessagesOfChat(Guid ChatId, Guid CurrId, Guid UserId); 
        public Task<Guid> CreatePrivateRoom(Guid rootId, Guid targetId, Guid chatId);
        public Task CreateRoom(string name, Guid userId);
        public Chat GetChat(Guid id);
        public IEnumerable<Chat> GetPrivateChats(Guid userId);
        public Task<List<ChatUsersViewModel>> GetChats(Guid userId);
        public Task JoinRoom(Guid chatId, Guid userId);

        public Task<List<OnlineUserViewMOdel>> GetOnlineUserById(Guid UserId);

        public List<ChatUser> GetChatUsers(Guid ChatId);

        Task<bool> UpdateChatStatus(Guid UserId, Guid ChatId);

        Task<int> GetUnreadMessagesCount(Guid CurrId);

        Task<Messages> CreateMessage(Guid chatId, string message, string FileLocation, decimal? TipAmount, Guid userId, Guid targetId, Guid MessageId, List<PostMessagesAttachmentViewModel> messagesAttachments);
        Task<bool> MarkAsUnread(Guid UserId, Guid ChatId);
        Task<bool> Delete(Guid CurrentId, Guid MessageId);
        Task<bool> DeleteConversation(Guid CurrentId, Guid ChatId);
        Task<bool> HideConversation(Guid UserId, Guid ChatId);
        Task<List<MessageReportTypeViewModel>> GetReportTypesAsync();
        Task<bool> ReportConversationAsync(Guid chatId, Guid userId, string reportType);
        Task<bool> BlockMessage(Guid ChatId, Guid CurrentId, Guid UserId);
        Task<bool> UnblockMessage(Guid ChatId, Guid CurrentId, Guid UserId);
    }
}
