﻿using FanBucketAPI.Models;
using FanBucketEntity.Model.PostDetails;
using FanBucketEntity.ViewModel;
using FanBucketRepo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketRepo.Interface
{
    public interface PostMasterIRepository : IRepository<PostMaster>
    {
        Task<PostMoments> RemoveMomentsAsync(PostMoments entity);
        ValueTask<PostMoments> MomentsGetById(Guid Id);
        bool MomentsExists(Guid id);
        bool PostExists(Guid id);
        bool PostExistsByUser(Guid id, Guid CurrId);

        Task<List<GetUserMediaViewModel>> GetMediaByUser(Guid CurrId);

        Task<PostCollection> GetCollectionById(Guid Id);
        Task<List<PostMasterCollectionViewModel>> GetByUser(Guid Id, Guid CurrId, int pageNumber, int pageSize);

        Task<List<PostCommentViewModel>> GetCommentByPost(Guid PostId);

        Task<PostCommentViewModel> GetCommentById(Guid Id);

        public Task<bool> AddPostCollection(List<PostCollection> entity);

        public Task<bool> AddPostToMoments(PostMoments entity);

        public Task<List<PostMasterCollectionViewModel>> GetFeedByUser(Guid CurrId,int PageNumber, int PageSize=10);
        public Task<List<TopAccountsViewModel>> GetTopAccounts(Guid UserId);
        public Task<PostMasterCollectionViewModel> GetSinglePostById(Guid PostId, Guid NotifyId, Guid CurrId);

        public Task<List<PostMasterCollectionViewModel>> GetBookmarkByUser(Guid CurrId);

        public Task<List<PostMasterCollectionViewModel>> GetPostByUsersVideo(Guid Id, Guid CurrId);
        public Task<List<PostMasterCollectionViewModel>> GetPostByUsersImage(Guid Id, Guid CurrId);
        public Task<PostMasterCollectionViewModel> GetSingleCollectionById(Guid PostId, Guid CurrId);
        public int GetUserPaidPostCount(Guid CurrId);
        public bool UserIsContentCreator(Guid CurrId);

        public Task<List<PostMasterCollectionViewModel>> GetByUserSubscribed(Guid Id, Guid CurrId, int pageNumber, int pageSize);
        public Task<List<PostMomentsViewModel>> GetMoments(Guid CurrId);

        public Task<List<PostMasterCollectionViewModel>> GetScheduledPostUser(Guid CurrId);

        public Task<List<PollVoteViewModel>> RegisterPoll(Guid PostId, Guid ContentId, Guid UserId);

        public Task<List<PostMasterCollectionViewModel>> GetFeedByUserNSFW(Guid CurrId,int PageNumber, int PageSize=10);

        Task<PostCollection> RemoveCollectionAsync(PostCollection entity);
        Task<List<PostCollection>> GetPostCollectionsById(Guid PostId);
        Task<List<PostMasterCollectionViewModel>> Search(Guid CurrId, string hashtag);
        Task<bool> AddPostTagsAsync(Guid id, List<string> tagsList);
        Task<bool> UpdateCollectionAsync(PostCollection entity);
        Task<List<PostMasterCollectionViewModel>> GetHiddenPost(Guid CurrId);
        Task<bool> RemovePostTagsAsync(Guid PostId);
        Task<List<PostMasterCollectionViewModel>> ViewAsGuestUser(Guid CurrId, int PageNumber, int PageSize);
        Task<List<PostMasterCollectionViewModel>> GuestUserGetPostByVideo(Guid CurrId);
        Task<List<GetUserMediaViewModel>> GetUserImageContents(Guid UserId, Guid CurrId);
        Task<List<GetUserMediaViewModel>> GuestUserGetPostByImage(Guid CurrId);
    }
}
