﻿using FanBucketEntity.Model.Chat;
using System;
using System.Collections.Generic;
using System.Text;

namespace FanBucketRepo.Interface
{
    public interface IMessageRepository : IRepository<Messages>
    {
    }
}
