﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace FanBucketSignalR.API_Context
{
    public static class IdentityExtension
    {
        //
        // Summary:
        //     Gets the Bearer Token Used For Authentication.
        //
        // Returns:
        //     The primary claims identity associated with this claims principal.
        public static string GetJWT(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;

            Claim claim = claimsIdentity.FindFirst(ClaimTypes.Hash);

            return claim.Value;
        }
        public static string GetUsername(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;

            Claim claim = claimsIdentity.FindFirst(ClaimTypes.Name);

            return claim.Value;
        }
        public static string GetName(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;

            Claim claim = claimsIdentity.FindFirst(ClaimTypes.Actor);

            return claim.Value;
        }
        public static string GetProfileImg(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;

            Claim claim = claimsIdentity.FindFirst("ProfileImage");

            return claim.Value;
        }
        public static string GetCoverImg(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;

            Claim claim = claimsIdentity.FindFirst(ClaimTypes.GivenName);

            return claim.Value;
        }
        public static void UpdateProPic(this IIdentity identity, string value)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;

            Claim claim = claimsIdentity.FindFirst(ClaimTypes.PostalCode);

            claimsIdentity.RemoveClaim(claim);

            claimsIdentity.AddClaim(new Claim(ClaimTypes.PostalCode, value));

            return;
        }

        public static void UpdateCoverPic(this IIdentity identity, string value)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;

            Claim claim = claimsIdentity.FindFirst(ClaimTypes.GivenName);

            claimsIdentity.RemoveClaim(claim);

            claimsIdentity.AddClaim(new Claim(ClaimTypes.GivenName, value));

            return;
        }
        public static string GetUserId(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;

            Claim claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            return claim.Value;
        }
        public static string GetNameIdentifier(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;

            Claim claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            return claim.Value;
        }
    }
}
