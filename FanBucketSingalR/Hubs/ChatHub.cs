﻿using FanBucketEntity.DBContext;
using FanBucketEntity.Model.Chat;
using FanBucketSignalR.API_Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanBucketSignalR.Hubs
{
    public class UserList
    {
        public string ConnectionID { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
    [Authorize]
    public class ChatHub : Hub
    {

        public static List<UserList> CurrentConnections = new List<UserList>();

        //public async Task SendMessage(Messages message)
        //{
        //    await Clients.All.SendAsync("ReceiveMessage", message);
        //}
        public Task JoinRoom(string roomId)
        {

            return Groups.AddToGroupAsync(Context.ConnectionId, roomId);
        }

        public Task LeaveRoom(string roomId)
        {
            return Groups.RemoveFromGroupAsync(Context.ConnectionId, roomId);
        }
        public override async Task OnConnectedAsync()
        {
            var id = Context.ConnectionId;
            CurrentConnections.Add(new UserList()
            {
                ConnectionID = id,
                UserId = Context.User.Identity.GetUserId(),
                UserName = Context.User.Identity.GetUsername()
            });

            await Clients.All.SendAsync("SendAction", Context.User.Identity.GetNameIdentifier(), "joined");
            await base.OnConnectedAsync();
        }

       
        public override async Task OnDisconnectedAsync(Exception ex)
        {
            var connection = CurrentConnections.FirstOrDefault(x => x.ConnectionID == Context.ConnectionId); ;
            if (ex!=null)
            {

            }
            if (connection != null)
            {
                CurrentConnections.Remove(connection);
            }
            await Clients.All.SendAsync("SendAction", Context.User.Identity.GetNameIdentifier(), "left");
            await base.OnDisconnectedAsync(ex);


        }
        //public override async Task OnDisconnectedAsync(Exception exception)
        //{
        //    await Groups.RemoveFromGroupAsync(Context.ConnectionId, "SignalR Users");
        //    await base.OnDisconnectedAsync(exception);
        //}
    }
}

