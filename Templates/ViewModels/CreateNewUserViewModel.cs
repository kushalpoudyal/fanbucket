﻿namespace Templates.ViewModels
{
    public class CreateNewUserViewModel
    {
        public string ToEmail { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public string CallbackUrl { get; set; }
        public string Message { get; set; }

        public CreateNewUserViewModel()
        {

        }
    }
}
public class SendEmailNewUserModel
{
    public SendEmailNewUserModel()
    {
    }

    public string ToEmail { get; set; }
    public string FullName { get; set; }
    public string Username { get; set; }
    public string CallbackUrl { get; set; }
}