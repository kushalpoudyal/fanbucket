﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketContentAPI.Models
{
    public class FileUploadModel
    {
        public string FilePath { get; set; }
        public string bucketName { get; set; }
        public string filename { get; set; }

        public string KeyName { get; set; }
        public long fileSize { get; set; }
        public string fileExt { get; set; }

        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

    }
}
