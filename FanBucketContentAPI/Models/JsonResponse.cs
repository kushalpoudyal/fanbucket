﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketContentAPI.Models
{

    public class JsonResponse
    {
        public bool success { get; set; }
        public string ReturnMsg { get; set; }
    }

}
