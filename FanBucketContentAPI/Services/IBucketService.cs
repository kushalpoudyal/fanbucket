﻿using FanBucketContentAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanBucketContentAPI.Services
{
    public interface IBucketService
    {
        Task<JsonResponse> UploadFileAsync(FileUploadModel model);
    }
}
