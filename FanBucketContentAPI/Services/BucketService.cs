﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using FanBucketContentAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FanBucketContentAPI.Services
{
    public class BucketService : IBucketService
    {

        private static IAmazonS3 s3Client;
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast2;

        //public BucketService(IAmazonS3 _s3Client)
        //{
        //    s3Client = _s3Client;
        //}

        public async Task<JsonResponse> UploadFileAsync(FileUploadModel model)
        {
            var response = new JsonResponse();
            try
            {
                s3Client = new AmazonS3Client(model.AccessKey, model.SecretKey, bucketRegion);

                var returnFile = string.Empty;


                var fileTransferUtility =
                    new TransferUtility(s3Client);

                var bucketExists = await s3Client.DoesS3BucketExistAsync(model.bucketName);
                var keyName = model.KeyName + model.filename;
                if (bucketExists)
                {
                    var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = model.bucketName,
                        FilePath = model.FilePath,
                        StorageClass = S3StorageClass.Standard,
                        PartSize = model.fileSize,
                        Key = keyName,
                        CannedACL = S3CannedACL.PublicRead,
                        ContentType = model.fileExt,

                    };
                    await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);
                    response.success = true;
                    response.ReturnMsg = "https://" + model.bucketName + ".s3." + bucketRegion.SystemName + ".amazonaws.com/" + keyName;

                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Bucket does not exists";
                }


            }
            catch (AmazonS3Exception e)
            {
                response.success = false;
                response.ReturnMsg = "Error encountered on server. " + e.Message;
            }
            catch (Exception e)
            {
                response.success = false;
                response.ReturnMsg = "Error encountered on server. " + e.Message;
            }
            return response;

        }
    }
}
