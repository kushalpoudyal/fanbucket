﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Amazon.S3.Model;
using FanBucketAPI.Helper;
using FanBucketContentAPI.Helper;
using FanBucketContentAPI.Models;
using FanBucketContentAPI.Services;
using FanBucketEntity.Model.ENUM;
using FanBucketEntity.Models.UserDetails;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FanBucketContentAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BucketController : ControllerBase
    {
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IBucketService bucketService;
        private readonly IConfiguration _configuration;
        private readonly UserManager<ApplicationUser> userManager;
        public BucketController(IWebHostEnvironment hostingEnvironment, IBucketService bucketService,
            IConfiguration configuration,
            UserManager<ApplicationUser> userManager)
        {
            this.hostingEnvironment = hostingEnvironment;
            this.bucketService = bucketService;
            _configuration = configuration;
        }
        [HttpGet]
        public void Get()
        {

        }
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<JsonResponse> Post(List<IFormFile> files, string contentType)
        {
            var ContenType = (ContentTypes)Enum.Parse(typeof(ContentTypes), contentType);
            var response = new JsonResponse();
            var Keyname = GetKeyName(ContenType);
            try
            {
                if (!string.IsNullOrEmpty(Keyname))
                {
                    var Id = Guid.NewGuid().ToString();

                    foreach (IFormFile source in files)
                    {
                        var fileName = Id + Path.GetExtension(source.FileName);
                        long FileLength = source.Length;

                        if (!Directory.Exists(Path.Combine(hostingEnvironment.ContentRootPath, "Main")))
                        {
                            Directory.CreateDirectory(Path.Combine(hostingEnvironment.ContentRootPath, "Main"));
                        }
                        string VideoFolder = Path.Combine(hostingEnvironment.ContentRootPath, "Main", fileName);
                        await FanBucketAPI.Helper.CreateFile.UploadToServer(source, VideoFolder);
                        var requestModel = new FileUploadModel()
                        {
                            fileSize = FileLength,
                            bucketName = _configuration.GetSection("AWS")["bucketName"],
                            fileExt = source.ContentType,
                            filename = fileName,
                            FilePath = VideoFolder,
                            AccessKey = _configuration.GetSection("AWS")["AccessKey"],
                            SecretKey = _configuration.GetSection("AWS")["SecretKey"],
                            KeyName = Keyname
                        };
                        if (ContenType == ContentTypes.Image)
                        {

                            using (Image image = Image.FromFile(VideoFolder))
                            {
                                using (MemoryStream m = new MemoryStream())
                                {
                                    image.Save(m, image.RawFormat);
                                    image.Dispose();
                                    ImageCompressor.CompressImage(Image.FromStream(m), 20, VideoFolder);
                                }
                            }
                        }
                        response = await this.bucketService.UploadFileAsync(requestModel);


                        System.IO.File.Delete(VideoFolder);

                    }

                }
                else
                {
                    response.success = false;
                    response.ReturnMsg = "Invalid Content Type";
                }



            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = ex.Message.ToString();
            }

            return response;

        }
        [Route("UploadUserPic")]
        [HttpPost]
        public async Task<JsonResponse> UploadUserPic(List<IFormFile> files, string Id, string Type)
        {

            var response = new JsonResponse();
            var Keyname = Type == "CoverPicture" ? "CoverPicture/" : "ProfilePicture/";
            try
            {
                var UserId = Guid.Parse(Id);
                var FileId = UserId.ToString();

                foreach (IFormFile source in files)
                {
                    var fileName = FileId + Path.GetExtension(source.FileName);
                    long FileLength = source.Length;

                    if (!Directory.Exists(Path.Combine(hostingEnvironment.ContentRootPath, "Main")))
                    {
                        Directory.CreateDirectory(Path.Combine(hostingEnvironment.ContentRootPath, "Main"));
                    }
                    string VideoFolder = Path.Combine(hostingEnvironment.ContentRootPath, "Main", fileName);
                    await FanBucketAPI.Helper.CreateFile.UploadToServer(source, VideoFolder);
                    var requestModel = new FileUploadModel()
                    {
                        fileSize = FileLength,
                        bucketName = _configuration.GetSection("AWS")["bucketName"],
                        fileExt = source.ContentType,
                        filename = fileName,
                        FilePath = VideoFolder,
                        AccessKey = _configuration.GetSection("AWS")["AccessKey"],
                        SecretKey = _configuration.GetSection("AWS")["SecretKey"],
                        KeyName = Keyname
                    };
                    using (Image image = Image.FromFile(VideoFolder))
                    {
                        using (MemoryStream m = new MemoryStream())
                        {
                            image.Save(m, image.RawFormat);
                            image.Dispose();
                            ImageCompressor.CompressImage(Image.FromStream(m), 30, VideoFolder);
                        }
                    }
                    response = await this.bucketService.UploadFileAsync(requestModel);
                    System.IO.File.Delete(VideoFolder);
                    var user = await userManager.FindByIdAsync(UserId.ToString());
                    if (Type == "CoverPicture")
                        user.CoverImgLocation = response.ReturnMsg;
                    else
                        user.ProfileImgLocation = response.ReturnMsg;



                }
            }
            catch (Exception ex)
            {

                response.success = false;
                response.ReturnMsg = ex.Message.ToString();
            }

            return response;

        }
        private string GetKeyName(ContentTypes contentType)
        {
            var ContentType = string.Empty;
            switch (contentType)
            {

                case ContentTypes.File:
                    ContentType = "Content/Document/";
                    break;
                case ContentTypes.Image:
                    ContentType = "Contents/Image/";
                    break;
                case ContentTypes.Audio:
                    ContentType = "Contents/Audio/";
                    break;
                case ContentTypes.Video:
                    ContentType = "Contents/Video/";
                    break;

                default:
                    ContentType = "";
                    break;
            }
            return ContentType;

        }
    }
}